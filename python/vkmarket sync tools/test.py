#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import json

from PIL import Image

from vk_api import VKMarketUtils, VKMarketPhotos, VKMarket
from vk_upsert_app_config import AppSettings
from models.SyncedCategory import SyncedCategory

from save_product_pics import *
from resize_product_pics import *

DEFAULT_CATALOG_DB_PATH = u'catalog.db'
CATALOG_DB_PATH = u''

# CATALOG_SYNC_DB_SCHEME_QUERY = '''
#     CREATE TABLE "categories_sync" (
#     `id`    INTEGER,
# 	`name`	TEXT,
# 	`picture_path`	TEXT,
# 	`vk_album_id`	INTEGER,
# 	`vk_response`	TEXT
# )
# '''


def create_categories_sync_db_scheme(catalogPath):
    try:
        connection = sqlite3.connect(catalogPath)
        with connection:
            cursor = connection.cursor()
            cursor.execute(
                "SELECT * FROM sqlite_master WHERE name ='categories_sync' and type='table';")
            if (cursor.fetchone() == None):
                logging.log(
                    logging.WARNING,
                    "categories_sync table not found in catalog database. Will create it with default values...")
                cursor.execute(CATALOG_SYNC_DB_SCHEME_QUERY)
    except sqlite3.Error, e:
        logging.log(logging.ERROR, "Database error occurred : '" + str(e))
    finally:
        if connection:
            connection.commit()
            connection.close()


def clean_categories_sync_table(catalogPath):
    try:
        connection = sqlite3.connect(catalogPath)
        with connection:
            cursor = connection.cursor()
            cursor.execute("DELETE FROM categories_sync;")
    except sqlite3.Error, e:
        logging.log(logging.ERROR, "Database error occurred : '" + str(e) + "'")
    finally:
        if connection:
            connection.commit()
            connection.close()


# def save_synced_category(syncedCategory, catalogPath):
#     try:
#         connection = sqlite3.connect(catalogPath)
#         with connection:
#             cursor = connection.cursor()
#             cursor.execute('''INSERT INTO categories_sync(id, name, picture_path, vk_album_id, vk_response)
#                 VALUES(:id, :name, :picture_path, :vk_album_id, :vk_response);''',
#                            {'id': syncedCategory.id,
#                             'name': syncedCategory.name,
#                             'picture_path': syncedCategory.picture_path,
#                             'vk_album_id': syncedCategory.vk_album_id,
#                             'vk_response': syncedCategory.vk_response
#                             })
#     except sqlite3.Error, e:
#         logging.log(logging.ERROR, "Database error occurred : '" + str(e) + "'")
#     finally:
#         if connection:
#             connection.commit()
#             connection.close()


def setAndParseArguments():
    parser = argparse.ArgumentParser(description='VK sync utility arguments description')
    parser.add_argument('-d', metavar='D', type=str, help='Path to catalog database file')
    # parser.add_argument('-c', '--clear', action="store_true",
    #                     help="Clear vk app credentials")
    args = parser.parse_args()
    return args


def main():
    global CATALOG_DB_PATH
    logging.basicConfig(stream=sys.stdout, level=logging.INFO,
                        format="[%(filename)s:%(funcName)s:%(lineno)s] %(message)s")
    args = setAndParseArguments()
    if args.d:
        CATALOG_DB_PATH = args.d
    else:
        CATALOG_DB_PATH = DEFAULT_CATALOG_DB_PATH

    logging.log(logging.INFO, CATALOG_DB_PATH)

    appConfig = AppSettings()
    logging.log(logging.INFO, appConfig.readFomDB(CATALOG_DB_PATH).toJSON())
    logging.log(logging.INFO, appConfig.app_token)

    vkMarket = VKMarket(
        access_token=appConfig.app_token,
        owner_id=appConfig.vk_group_id,
        version=appConfig.app_version)

    albums = vkMarket.getAlbums(0, 100)

    albumsSubset = vkMarket.getAlbumById([78,79])


if __name__ == '__main__':
    main()

    # appConfig = AppSettings()
    # appConfig.init_from_db()
    #
    # VKMarketPhotos = VKMarketPhotos(
    #     access_token=appConfig.app_token,
    #     owner_id=appConfig.vk_group_id,
    #     version=appConfig.app_version)
    #
    # VKMarket = VKMarket(
    #     access_token=appConfig.app_token,
    #     owner_id=appConfig.vk_group_id,
    #     version=appConfig.app_version)

    # update_short_products_name(get_all_products())

    # VK_PRODUCT_CATEGORY_ID = 702
    #
    # CATEGORY = 2319405
    #
    # products = get_not_synced_products()
    #
    # for product in products:
    #     logging.log(logging.INFO, product)
    #     if os.path.isfile(product.resized_picture_path):
    #
    #         if product.vk_product_id:
    #             pass
    #         else:
    #
    #             upload_result = VKMarketPhotos.getMarketUploadServer(main_photo='1')
    #             time.sleep(2)
    #             upload_url = VKMarketUtils.extractImageUploadServer(upload_result)
    #
    #             image_data = open(unicode(product.resized_picture_path), 'rb')
    #
    #             upload_response = VKMarketPhotos.uploadMarketPhotoBytes(upload_url, image_data)
    #             logging.log(logging.INFO, upload_response)
    #             time.sleep(2)
    #
    #             upload_result = VKMarketUtils.buildImageUploadResult(upload_response)
    #             time.sleep(2)
    #
    #             save_result = VKMarketPhotos.saveMarketPhoto(
    #                 photo=upload_result.photo,
    #                 server=upload_result.server,
    #                 hash=upload_result.hash,
    #                 crop_data=upload_result.crop_data,
    #                 crop_hash=upload_result.crop_hash)
    #             time.sleep(2)
    #
    #             photo_id = VKMarketUtils.extractImageSaveResult(save_result)
    #             upload_photo_id = VKMarketUtils.extractPhotoId(photo_id)
    #
    #             if not product.description:
    #                 desc = product.product_url + "\n\n"
    #             else:
    #                 desc = product.product_url + "\n\n" + product.description
    #
    #             time.sleep(2)
    #             response = VKMarket.add(
    #                 name=product.short_name,
    #                 description=desc,
    #                 category_id=VK_PRODUCT_CATEGORY_ID,
    #                 price=product.price,
    #                 deleted=0,
    #                 main_photo_id=upload_photo_id,
    #                 photo_ids=[])
    #
    #             logging.log(logging.INFO, response)
    #             vk_product_id = VKMarketUtils.extractProductAddResult(response)
    #
    #             if vk_product_id:
    #                 update_vk_id_for_product(product_name=product.name, vk_product_id=vk_product_id)
    #                 vk_album_id = get_synced_vk_album_id(CATEGORY)
    #                 if vk_album_id:
    #                     vk_album_ids = []
    #                     vk_album_ids.append(vk_album_id)
    #                     time.sleep(1)
    #                     response = VKMarket.addToAlbum(item_id=vk_product_id, album_ids=vk_album_ids)
    #                     logging.log(logging.INFO, str(response))
