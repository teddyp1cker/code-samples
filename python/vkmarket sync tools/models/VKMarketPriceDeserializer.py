# -*- coding: utf-8 -*-


from models.VKMarketCurrencyDeserializer import VKMarketCurrencyDeserializer
from models.VKMarketObjectDeserializer import VKMarketObjectDeserializer
from models.VKMarketPrice import VKMarketPrice


class VKMarketPriceDeserializer(VKMarketObjectDeserializer):
    @staticmethod
    def deserialize(serializedprice):
        currency = VKMarketCurrencyDeserializer.deserialize(serializedprice['currency'])

        price = VKMarketPrice()
        price.amount = serializedprice['amount']
        price.text = serializedprice['text']
        price.currency = currency

        return price
