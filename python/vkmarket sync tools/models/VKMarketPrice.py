# -*- coding: utf-8 -*-

import json


class VKMarketPrice(object):
    def __init__(self,
                 amount,
                 text,
                 currency):
        self.amount = amount
        self.text = text
        self.currency = currency

    def __init__(self):
        pass

    def __str__(self):
        return "VKMarketPrice: amount = " + self.amount + \
               ", currency = " + str(self.currency) + \
               ", text = " + self.text

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=False, indent=4)
