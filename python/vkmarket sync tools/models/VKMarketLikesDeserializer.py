# -*- coding: utf-8 -*-

from models.VKMarketCurrency import VKMarketCurrency
from models.VKMarketLikes import VKMarketLikes
from models.VKMarketObjectDeserializer import VKMarketObjectDeserializer


class VKMarketLikesDeserializer(VKMarketObjectDeserializer):
    @staticmethod
    def deserialize(serializedcurrency):
        likes = VKMarketLikes()

        likes.user_likes = serializedcurrency['user_likes']
        likes.count = serializedcurrency['count']

        return likes
