# -*- coding: utf-8 -*-


from models.VKMarketCategoryDeserializer import VKMarketCategoryDeserializer
from models.VKMarketLikesDeserializer import VKMarketLikesDeserializer
from models.VKMarketObjectDeserializer import VKMarketObjectDeserializer
from models.VKMarketPhotoDeserializer import VKMarketPhotoDeserializer
from models.VKMarketPriceDeserializer import VKMarketPriceDeserializer
from models.VKMarketProduct import VKMarketProduct


class VKMarketProductDeserializer(VKMarketObjectDeserializer):
    @staticmethod
    def deserialize(serializedproduct):
        product = VKMarketProduct()

        price = VKMarketPriceDeserializer.deserialize(serializedproduct['price'])
        category = VKMarketCategoryDeserializer.deserialize(serializedproduct['category'])

        product.id = serializedproduct['id']
        product.description = serializedproduct['description']
        product.owner_id = serializedproduct['owner_id']
        product.text = serializedproduct['text']
        product.date = serializedproduct['date']
        product.thumb_photo = serializedproduct['thumb_photo']
        product.availability = serializedproduct['availability']
        product.price = price
        product.category = category

        return product

    def deserializeExtended(serializedproductextended):
        product = VKMarketProduct()

        price = VKMarketPriceDeserializer.deserialize(serializedproductextended['price'])
        category = VKMarketCategoryDeserializer.deserialize(serializedproductextended['category'])

        product.id = serializedproductextended['id']
        product.description = serializedproductextended['description']
        product.owner_id = serializedproductextended['owner_id']
        product.text = serializedproductextended['text']
        product.date = serializedproductextended['date']
        product.thumb_photo = serializedproductextended['thumb_photo']
        product.availability = serializedproductextended['availability']
        product.price = price
        product.category = category

        likes = VKMarketLikesDeserializer.deserialize(serializedproductextended['likes'])

        product.likes = likes

        product.albums_ids = []
        for album_id in serializedproductextended['albums_ids']:
            product.albums_ids.append(album_id)

        product.photos = []
        for photo in serializedproductextended['photos']:
            product.photos.append(VKMarketPhotoDeserializer.deserialize(photo))

        product.views_count = serializedproductextended['views_count']
        product.can_repost = serializedproductextended['can_repost']
        product.can_comment = serializedproductextended['can_comment']

        return product
