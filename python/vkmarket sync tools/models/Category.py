#!/usr/bin/env python
# -*- coding: utf-8 -*-

class Category(object):
    def __init__(self, id, name, parentId, picture_path):
        self.id = id
        self.name = name
        self.parentId = parentId
        self.picture_path = picture_path

    def __str__(self):
        if self.parentId:
            return "Category: name = " + self.name + ", id = " + str(
                self.id) + ", parentId = " + self.parentId + ", picture_path = " + self.picture_path
        else:
            return "Category: name = " + self.name + ", id = " + str(self.id) + ", picture_path = " + self.picture_path
