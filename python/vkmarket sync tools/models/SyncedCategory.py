#!/usr/bin/env python
# -*- coding: utf-8 -*-

class SyncedCategory(object):
    def __init__(self, id, name, picture_path, vk_album_id, vk_response):
        self.id = id
        self.name = name
        self.picture_path = picture_path
        self.vk_album_id = vk_album_id
        self.vk_response = vk_response

    def __str__(self):
        return "SyncedCategory: + id = " + str(
            self.id) + " name = " + self.name + ", picture_path = " + self.picture_path + ", vk_album_id = " + str(
            self.vk_album_id) + ", vk_response = " + self.vk_response
