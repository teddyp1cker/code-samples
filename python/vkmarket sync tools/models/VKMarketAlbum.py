# -*- coding: utf-8 -*-

import json
import logging as logger
import sys


class VKMarketAlbum(object):
    def __init__(self, id, owner_id, title, count, updated_time, photo):
        logger.basicConfig(stream=sys.stdout, level=logger.INFO,
                           format="[%(filename)s:%(funcName)s:%(lineno)s] %(message)s")
        self.id = id
        self.owner_id = owner_id
        self.title = title
        self.count = count
        self.updated_time = updated_time
        self.photo = photo

    def __init__(self):
        logger.basicConfig(stream=sys.stdout, level=logger.INFO,
                           format="[%(filename)s:%(funcName)s:%(lineno)s] %(message)s")

    def __str__(self):
        return "VKMarketAlbum: id = " + self.id + \
               ", owner_id = " + self.owner_id + \
               ", title = " + self.title + \
               ", count = " + self.count + \
               ", updated_time = " + self.updated_time + \
               ", photo : " + str(self.photo)

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=False, indent=4)
