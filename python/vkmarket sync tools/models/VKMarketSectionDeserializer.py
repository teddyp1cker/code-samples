# -*- coding: utf-8 -*-

from models.VKMarketObjectDeserializer import VKMarketObjectDeserializer
from models.VKMarketSection import VKMarketSection


class VKMarketSectionDeserializer(VKMarketObjectDeserializer):
    @staticmethod
    def deserialize(serializedsection):
        section = VKMarketSection()

        section.id = serializedsection['id']
        section.name = serializedsection['name']

        return section
