# -*- coding: utf-8 -*-

import json


class SiteCategory(object):
    def __init__(self,
                 id,
                 parent_id,
                 vk_album_id,
                 name,
                 picture):
        self.id = id
        self.parent_id = parent_id
        self.vk_album_id = vk_album_id
        self.name = name
        self.picture = picture

    def __init__(self):
        pass

    def __str__(self):
        return "SiteCategory: id = " + self.id + \
               ", parent_id = " + self.parent_id + \
               ", vk_album_id = " + self.vk_album_id + \
               ", name = " + self.name

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=False, indent=4)
