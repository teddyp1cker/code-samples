#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json


class SiteProduct(object):
    def __init__(self, name, url, picture_url, escaped_name, resized_picture_path, price, currency, description,
                 category_id, root_category_id, short_name, vk_product_id):
        self.name = name
        self.product_url = url
        self.picture_url = picture_url
        self.escaped_name = escaped_name
        self.resized_picture_path = resized_picture_path
        self.price = price
        self.currency = currency
        self.description = description
        self.category_id = category_id
        self.root_category_id = root_category_id
        self.short_name = short_name
        self.vk_product_id = vk_product_id

    def __init__(self):
        pass

    def __str__(self):
        return "Product: short_name = " + self.short_name + ", name = " + self.name + ", url = " + self.product_url + ", picture_url = " + self.picture_url + ", resized_picture_path = " \
               + self.resized_picture_path + ", price = " + str(self.price) + ", currency = " + self.currency \
               + ", description = " + self.description + ", category_id = " + str(
            self.category_id) + ", escaped_name = " + self.escaped_name + ", root_category_id = " + str(
            self.root_category_id) + ", vk_product_id = " + str(self.vk_product_id)

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=True, indent=4)
