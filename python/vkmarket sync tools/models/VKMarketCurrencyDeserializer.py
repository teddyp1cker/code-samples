# -*- coding: utf-8 -*-

from models.VKMarketCurrency import VKMarketCurrency
from models.VKMarketObjectDeserializer import VKMarketObjectDeserializer


class VKMarketCurrencyDeserializer(VKMarketObjectDeserializer):
    @staticmethod
    def deserialize(serializedcurrency):
        currency = VKMarketCurrency()

        currency.id = serializedcurrency['id']
        currency.name = serializedcurrency['name']

        return currency