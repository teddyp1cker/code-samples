# -*- coding: utf-8 -*-

import json


class VKMarketProduct(object):
    def __init__(self,
                 id,
                 title,
                 owner_id,
                 description,
                 date,
                 thumb_photo,
                 availability,
                 category,
                 price,
                 albums_ids,
                 photos,
                 can_comment,
                 can_repost,
                 views_count,
                 likes
                 ):
        self.id = id
        self.title = title
        self.owner_id = owner_id
        self.description = description
        self.date = date
        self.thumb_photo = thumb_photo
        self.availability = availability
        self.category = category
        self.price = price
        self.albums_ids = albums_ids
        self.photos = photos
        self.can_comment = can_comment
        self.can_repost = can_repost
        self.views_count = views_count
        self.likes = likes

    def __init__(self):
        pass

    def __str__(self):
        return "VKMarketProduct: id = " + self.id + \
               ", title = " + self.title + \
               ", owner_id = " + self.owner_id + \
               ", description = " + self.description + \
               ", date = " + self.date + \
               ", thumb_photo = " + self.thumb_photo + \
               ", availability = " + self.availability + \
               ", category = " + str(self.category) + \
               ", price = " + str(self.price) + \
               ", albums_ids = " + self.albums_ids + \
               ", photos = " + self.photos + \
               ", can_comment = " + self.can_comment + \
               ", can_repost = " + self.can_repost + \
               ", views_count = " + self.views_count + \
               ", likes = " + self.likes

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=False, indent=4)
