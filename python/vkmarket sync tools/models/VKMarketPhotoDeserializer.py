# -*- coding: utf-8 -*-

from models.VKMarketObjectDeserializer import VKMarketObjectDeserializer
from models.VKMarketPhoto import VKMarketPhoto


class VKMarketPhotoDeserializer(VKMarketObjectDeserializer):
    @staticmethod
    def deserialize(serializedphoto):
        vkphoto = VKMarketPhoto()

        vkphoto.aid = serializedphoto['aid']
        vkphoto.pid = serializedphoto['pid']
        vkphoto.owner_id = serializedphoto['owner_id']
        vkphoto.user_id = serializedphoto['user_id']
        vkphoto.src = serializedphoto['src']
        vkphoto.src_big = serializedphoto['src_big']
        vkphoto.src_small = serializedphoto['src_small']
        vkphoto.width = serializedphoto['width']
        vkphoto.height = serializedphoto['height']
        vkphoto.text = serializedphoto['text']
        vkphoto.created = serializedphoto['created']

        return vkphoto
