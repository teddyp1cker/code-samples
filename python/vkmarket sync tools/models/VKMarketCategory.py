# -*- coding: utf-8 -*-

import json


class VKMarketCategory(object):
    def __init__(self,
                 id,
                 name,
                 section):
        self.id = id
        self.name = name
        self.section = section

    def __init__(self):
        pass

    def __str__(self):
        return "VKMarketCategory: id = " + self.id + \
               ", name = " + self.name + \
               ", section = " + self.section

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=False, indent=4)
