# -*- coding: utf-8 -*-

from models.VKMarketAlbum import VKMarketAlbum
from models.VKMarketObjectDeserializer import VKMarketObjectDeserializer
from models.VKMarketPhotoDeserializer import VKMarketPhotoDeserializer


class VKMarketAlbumDeserializer(VKMarketObjectDeserializer):
    @staticmethod
    def deserialize(serializedalbum):
        photo = VKMarketPhotoDeserializer.deserialize(serializedalbum['photo'])
        album = VKMarketAlbum()

        album.id = serializedalbum['id']
        album.owner_id = serializedalbum['owner_id']
        album.title = serializedalbum['title']
        album.count = serializedalbum['count']
        album.updated_time = serializedalbum['updated_time']
        album.photo = photo

        return album
