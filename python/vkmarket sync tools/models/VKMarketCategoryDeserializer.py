# -*- coding: utf-8 -*-

from models.VKMarketCategory import VKMarketCategory
from models.VKMarketObjectDeserializer import VKMarketObjectDeserializer
from models.VKMarketSectionDeserializer import VKMarketSectionDeserializer


class VKMarketCategoryDeserializer(VKMarketObjectDeserializer):
    @staticmethod
    def deserialize(serializedcategory):
        section = VKMarketSectionDeserializer.deserialize(serializedcategory['section'])
        category = VKMarketCategory()

        category.id = serializedcategory['id']
        category.name = serializedcategory['name']
        category.section = section

        return category
