# -*- coding: utf-8 -*-

import json


class VKMarketLikes(object):
    def __init__(self,
                 user_likes,
                 count):
        self.user_likes = user_likes
        self.count = count

    def __init__(self):
        pass

    def __str__(self):
        return "VKMarketLikes: user_likes = " + self.user_likes + \
               ", count = " + self.count

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=False, indent=4)
