# -*- coding: utf-8 -*-

import json


class VKMarketPhoto(object):
    def __init__(self,
                 pid,
                 aid,
                 owner_id,
                 user_id,
                 src,
                 src_big,
                 src_small,
                 width,
                 height,
                 text,
                 created
                 ):
        self.pid = pid
        self.aid = aid
        self.owner_id = owner_id
        self.user_id = user_id
        self.src = src
        self.src_big = src_big
        self.src_small = src_small
        self.width = width
        self.height = height
        self.text = text
        self.created = created

    def __init__(self):
        pass

    def __str__(self):
        return "VKMarketPhoto: pid = " + self.pid + \
               ", aid = " + self.aid + \
               ", owner_id = " + self.owner_id + \
               ", user_id = " + self.user_id + \
               ", src = " + self.src + \
               ", src_big = " + self.src_big + \
               ", src_small = " + self.src_small + \
               ", width = " + self.width + \
               ", height = " + self.height + \
               ", text = " + self.text + \
               ", created = " + str(self.created)

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=False, indent=4)
