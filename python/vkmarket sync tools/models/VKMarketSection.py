# -*- coding: utf-8 -*-

import json


class VKMarketSection(object):
    def __init__(self,
                 id,
                 name):
        self.id = id
        self.name = name

    def __init__(self):
        pass

    def __str__(self):
        return "VKMarketSection: id = " + self.id + \
               ", name = " + self.name

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=False, indent=4)
