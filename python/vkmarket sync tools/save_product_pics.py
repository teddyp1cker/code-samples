#!/usr/bin/env python
# -*- coding: utf-8 -*-

import time
import urllib2
import logging
import sys
import os
import sqlite3

from lxml import etree
from models.SiteProduct import SiteProduct
from models.Category import Category

MAX_NAME_LENGTH = 95

SLASH_ESCAPE_STRING = u'{{slash}}'

CATALOG_URL = u'http://korealove.ru/pricelist.xml'
CATALOG_XML_FILENAME = u'catalog.xml'
PRODUCT_PICTURES_DEFAULT_DIR_NAME = u'originals'
RESIZED_PRODUCT_PICTURES_DEFAULT_DIR_PREFIX = u'resized'

DEFAULT_USER_AGENT = [('User-Agent',
                       'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.1847.137 Safari/537.36')]

DEFAULT_CATALOG_DB_PATH = u'catalog.db'

CATEGORY_DB_SCHEME_QUERY = '''CREATE TABLE `category` (
	`name`	TEXT,
	`id`	INTEGER NOT NULL,
	`parent`	INTEGER,
	`picture_path`	TEXT,
	`image`	BLOB,
	PRIMARY KEY(id)
)
'''

PRODUCT_DB_SCHEME_QUERY = '''CREATE TABLE `product` (
	`name`	TEXT NOT NULL,
	`url`	TEXT NOT NULL,
	`picture_url`	TEXT,
	`escaped_name`	TEXT NOT NULL,
	`resized_picture_path`	TEXT,
	`price`	NUMERIC NOT NULL,
	`currency`	TEXT,
	`description`	TEXT,
	`category_id`	INTEGER,
	`root_category_id`	INTEGER,
	`vk_product_id`	INTEGER,
	`original_image`	BLOB,
	`resized_image`	BLOB,
	`full_description`	TEXT,
	`short_name`	TEXT
)
'''


def create_catalog_db_scheme():
    try:
        connection = sqlite3.connect(DEFAULT_CATALOG_DB_PATH)
        with connection:
            cursor = connection.cursor()
            cursor.execute(
                "SELECT * FROM sqlite_master WHERE name ='category' and type='table';")
            if (cursor.fetchone() == None):
                logging.log(
                    logging.WARNING, "Categories table not found in catalog database. Will create it...")
                cursor.execute(CATEGORY_DB_SCHEME_QUERY)
            cursor.execute("SELECT * FROM sqlite_master WHERE name ='product' and type='table';")
            if (cursor.fetchone() == None):
                logging.log(logging.WARNING, "Products table not found in catalog database. Will create it...")
                cursor.execute(PRODUCT_DB_SCHEME_QUERY)
    except sqlite3.Error, e:
        logging.log(logging.ERROR, "Database error occurred : '" + str(e))
    finally:
        if connection:
            connection.commit()
            connection.close()


def clean_categories_table():
    try:
        connection = sqlite3.connect(DEFAULT_CATALOG_DB_PATH)
        with connection:
            cursor = connection.cursor()
            cursor.execute("DELETE FROM category;")
    except sqlite3.Error, e:
        logging.log(logging.ERROR, "Database error occurred : '" + str(e) + "'")
    finally:
        if connection:
            connection.commit()
            connection.close()


def clean_products_table():
    try:
        connection = sqlite3.connect(DEFAULT_CATALOG_DB_PATH)
        with connection:
            cursor = connection.cursor()
            cursor.execute("DELETE FROM product;")
    except sqlite3.Error, e:
        logging.log(logging.ERROR, "Database error occurred : '" + str(e) + "'")
    finally:
        if connection:
            connection.commit()
            connection.close()


def fill_categories_table(categories_list):
    try:
        connection = sqlite3.connect(DEFAULT_CATALOG_DB_PATH)
        with connection:
            cursor = connection.cursor()
            for category in categories_list:
                cursor.execute('''INSERT INTO category(id, name, parent)
                    VALUES(:id, :name, :parent);''',
                               {'id': category.id, 'name': category.name, 'parent': category.parentId})
                connection.commit()
    except sqlite3.Error, e:
        logging.log(logging.ERROR, "Database error occurred : '" + str(e) + "'")
    finally:
        if connection:
            connection.close()


def get_all_products():
    all_products_list = []
    try:
        connection = sqlite3.connect(DEFAULT_CATALOG_DB_PATH)
        connection.row_factory = sqlite3.Row
        with connection:
            cursor = connection.cursor()
            for product_row in cursor.execute(
                    "SELECT `name`,`url`,`picture_url`,`escaped_name`,`resized_picture_path`,`price`,`currency`,"
                    "`description`,`category_id`,`root_category_id`, `short_name`, `vk_product_id` FROM product;"):
                product = SiteProduct()
                product.name = product_row[0]
                product.product_url = product_row[1]
                product.picture_url = product_row[2]
                product.escaped_name = product_row[3]
                product.resized_picture_path = product_row[4]
                product.price = product_row[5]
                product.currency = product_row[6]
                product.description = product_row[7]
                product.category_id = product_row[8]
                product.root_category_id = product_row[9]
                product.short_name = product_row[10]
                product.vk_product_id = product_row[11]
                all_products_list.append(product)
    except sqlite3.Error, e:
        logging.log(logging.ERROR, "Database error occurred : '" + str(e) + "'")
    finally:
        if connection:
            connection.close()
    return all_products_list


def update_short_products_name(products_list):
    try:
        connection = sqlite3.connect(DEFAULT_CATALOG_DB_PATH)
        with connection:
            cursor = connection.cursor()
            for product in products_list:
                short_name = product.name[0:MAX_NAME_LENGTH]
                cursor.execute(
                    '''UPDATE product SET short_name = :short_name WHERE name = :product_name;''',
                    {'short_name': short_name,
                     'product_name': product.name})
            connection.commit()
    except sqlite3.Error, e:
        logging.log(logging.ERROR, "Database error occurred : '" + str(e) + "'")
    finally:
        if connection:
            connection.close()


def update_vk_id_for_product(product_name, vk_product_id):
    try:
        connection = sqlite3.connect(DEFAULT_CATALOG_DB_PATH)
        with connection:
            cursor = connection.cursor()
            cursor.execute(
                '''UPDATE product SET vk_product_id = :vk_product_id WHERE name = :product_name;''',
                {'vk_product_id': vk_product_id,
                 'product_name': product_name})
            connection.commit()
    except sqlite3.Error, e:
        logging.log(logging.ERROR, "Database error occurred : '" + str(e) + "'")
    finally:
        if connection:
            connection.close()


def fill_products_table(products_list):
    try:
        connection = sqlite3.connect(DEFAULT_CATALOG_DB_PATH)
        with connection:
            cursor = connection.cursor()
            for product in products_list:
                cursor.execute('''INSERT INTO product(name, url, picture_url, escaped_name, resized_picture_path, price, currency, description, category_id)
                    VALUES(:name, :url, :picture_url, :escaped_name, :resized_picture_path, :price, :currency, :description, :category_id);''',
                               {'name': product.name,
                                'resized_picture_path': product.resized_picture_path,
                                'url': product.product_url,
                                'picture_url': product.picture_url,
                                'escaped_name': product.escaped_name,
                                'price': product.price,
                                'currency': product.currency,
                                'description': product.description,
                                'category_id': product.category_id,
                                })
                connection.commit()
    except sqlite3.Error, e:
        logging.log(logging.ERROR, "Database error occurred : '" + str(e) + "'")
    finally:
        if connection:
            connection.close()


def get_root_categories_list():
    root_categories_list = []
    try:
        connection = sqlite3.connect(DEFAULT_CATALOG_DB_PATH)
        connection.row_factory = sqlite3.Row
        with connection:
            cursor = connection.cursor()
            for root_category_row in cursor.execute(
                    'SELECT name, id, parent, picture_path FROM category WHERE parent IS NULL;'):
                root_categories_list.append(Category(
                    root_category_row[1],
                    root_category_row[0],
                    root_category_row[2],
                    root_category_row[3]))
    except sqlite3.Error, e:
        logging.log(logging.ERROR, "Database error occurred : '" + str(e) + "'")
    finally:
        if connection:
            connection.close()
    return root_categories_list


def get_not_synced_products():
    not_synced_products = []
    try:
        connection = sqlite3.connect(DEFAULT_CATALOG_DB_PATH)
        connection.row_factory = sqlite3.Row
        with connection:
            cursor = connection.cursor()
            for product_row in cursor.execute(
                    '''SELECT `name`,`url`,`picture_url`,`escaped_name`,`resized_picture_path`,`price`,`currency`,"
                    "`description`,`category_id`,`root_category_id`, `short_name`, `vk_product_id` FROM product "
                    "where vk_product_id is null or vk_product_id = "";'''):
                product = SiteProduct()
                product.name = product_row[0]
                product.product_url = product_row[1]
                product.picture_url = product_row[2]
                product.escaped_name = product_row[3]
                product.resized_picture_path = product_row[4]
                product.price = product_row[5]
                product.currency = product_row[6]
                product.description = product_row[7]
                product.category_id = product_row[8]
                product.root_category_id = product_row[9]
                product.short_name = product_row[10]
                product.vk_product_id = product_row[11]
                not_synced_products.append(product)
    except sqlite3.Error, e:
        logging.log(logging.ERROR, "Database error occurred : '" + str(e) + "'")
    finally:
        if connection:
            connection.close()
    return not_synced_products


def get_product_by_name(name):
    product = None
    try:
        connection = sqlite3.connect(DEFAULT_CATALOG_DB_PATH)
        connection.row_factory = sqlite3.Row
        with connection:
            cursor = connection.cursor()
            product_row = cursor.execute(
                'SELECT `name`,`url`,`picture_url`,`escaped_name`,`resized_picture_path`,`price`,`currency`,`description`,`category_id`,`root_category_id`, `short_name`, `vk_product_id` FROM product WHERE `name` = :name;',
                {'name': name}).fetchone()
            if product_row:
                product = SiteProduct()
                product.name = product_row[0]
                product.product_url = product_row[1]
                product.picture_url = product_row[2]
                product.escaped_name = product_row[3]
                product.resized_picture_path = product_row[4]
                product.price = product_row[5]
                product.currency = product_row[6]
                product.description = product_row[7]
                product.category_id = product_row[8]
                product.root_category_id = product_row[9]
                product.short_name = product_row[10]
                product.vk_product_id = product_row[11]
    except sqlite3.Error, e:
        logging.log(logging.ERROR, "Database error occurred : '" + str(e) + "'")
    finally:
        if connection:
            connection.close()
    return product


def get_synced_vk_album_id(root_category_id):
    vk_album_id = None
    try:
        connection = sqlite3.connect(DEFAULT_CATALOG_DB_PATH)
        connection.row_factory = sqlite3.Row
        with connection:
            cursor = connection.cursor()
            vk_album_id = cursor.execute('SELECT vk_album_id FROM categories_sync WHERE id = :root_category_id;',
                                         {'root_category_id': root_category_id}).fetchone()[0]
    except sqlite3.Error, e:
        logging.log(logging.ERROR, "Database error occurred : '" + str(e) + "'")
    finally:
        if connection:
            connection.close()
    return vk_album_id


def get_products_by_root_category(root_category_id):
    products_list = []
    try:
        connection = sqlite3.connect(DEFAULT_CATALOG_DB_PATH)
        connection.row_factory = sqlite3.Row
        with connection:
            cursor = connection.cursor()
            for product_row in cursor.execute(
                    "SELECT `name`,`url`,`picture_url`,`escaped_name`,`resized_picture_path`,`price`,`currency`,"
                    "`description`,`category_id`,`root_category_id`, `short_name`, `vk_product_id` FROM product WHERE `root_category_id` = :root_category_id",
                    {
                        'root_category_id': root_category_id
                    }):
                product = SiteProduct()
                product.name = product_row[0]
                product.product_url = product_row[1]
                product.picture_url = product_row[2]
                product.escaped_name = product_row[3]
                product.resized_picture_path = product_row[4]
                product.price = product_row[5]
                product.currency = product_row[6]
                product.description = product_row[7]
                product.category_id = product_row[8]
                product.root_category_id = product_row[9]
                product.short_name = product_row[10]
                product.vk_product_id = product_row[11]
                products_list.append(product)
    except sqlite3.Error, e:
        logging.log(logging.ERROR, "Database error occurred : '" + str(e) + "'")
    finally:
        if connection:
            connection.close()
    return products_list


def update_products_root_category_id():
    try:
        connection = sqlite3.connect(DEFAULT_CATALOG_DB_PATH)
        connection.row_factory = sqlite3.Row
        with connection:
            cursor = connection.cursor()
            for product_row in cursor.execute('SELECT name, category_id FROM product;').fetchall():
                product_name = product_row[0]
                product_category_id = product_row[1]
                if cursor.execute(
                        '''SELECT id from category WHERE id IN (SELECT parent FROM category WHERE id = :category_id);''',
                        {'category_id': product_category_id}).fetchone() == None:
                    product_root_category_id = product_category_id
                else:
                    product_root_category_id = cursor.execute(
                        '''SELECT id from category WHERE id IN (SELECT parent FROM category WHERE id = :category_id);''',
                        {'category_id': product_category_id}).fetchone()[0]
                cursor.execute(
                    '''UPDATE product SET root_category_id = :root_category_id WHERE name = :product_name;''',
                    {'root_category_id': product_root_category_id,
                     'product_name': product_name})
                connection.commit()
    except sqlite3.Error, e:
        logging.log(logging.ERROR, "Database error ocurred : '" + str(e) + "'")
    finally:
        if connection:
            connection.close()


def main():
    if not os.path.exists(PRODUCT_PICTURES_DEFAULT_DIR_NAME):
        os.makedirs(PRODUCT_PICTURES_DEFAULT_DIR_NAME)

    download_catalog(CATALOG_XML_FILENAME)
    create_catalog_db_scheme()
    catalog = get_catalog_object(CATALOG_XML_FILENAME)
    categories_list = get_categories_list(catalog)
    logging.log(logging.INFO, "Found " + str(len(categories_list)) + " categories")
    clean_categories_table()
    fill_categories_table(categories_list)
    clean_products_table()
    products_list = get_products_list(catalog)
    fill_products_table(products_list)
    logging.log(logging.INFO, "Found " + str(len(products_list)) + " products")
    get_root_categories_list()
    update_products_root_category_id()
    for picture_url, picture_name in get_pictures_list(catalog).iteritems():
        logging.log(logging.INFO, "Processing item " +
                    picture_url + " : " + picture_name)
        picture_file_format_extension = picture_url.strip().split(".")[-1]
        filename = unicode(PRODUCT_PICTURES_DEFAULT_DIR_NAME + "/" + escape_product_picture_filename(
            picture_name) + "." + picture_file_format_extension)
        if not os.path.exists(filename):
            download_product_picture(picture_url, picture_name, filename)
            logging.log(logging.INFO, "Time to sleep for 1 sec")
            time.sleep(1)
        else:
            logging.log(logging.INFO, picture_name + " done. Skipping...")


def download_product_picture(picture_url, picture_name, picture_filename):
    opener = urllib2.build_opener()
    opener.addheaders = DEFAULT_USER_AGENT
    try:
        response = opener.open(picture_url)
    except urllib2.HTTPError as e:
        if e.code == 404:
            logging.log(logging.WARNING,
                        "Houston, we have a 404 here. Trying to download " + picture_url + " one more time in 1 sec")
            time.sleep(1)
            opener.close()
            response = opener.open(picture_url)
            response_data = response.read()
            f = open(picture_filename, 'wb')
            f.write(response_data)
            f.close()
            logging.log(logging.INFO, picture_name +
                        " : " + picture_url + " done.")
            opener.close()
    except urllib2.URLError as e:
        logging.log(logging.ERROR, "Exception : " + e +
                    " I'm not going to try download " + picture_url + " anymore")
    else:
        response_data = response.read()
        f = open(picture_filename, 'wb')
        f.write(response_data)
        f.close()
        logging.log(logging.INFO, picture_name +
                    " : " + picture_url + " done.")
        opener.close()


def get_pictures_list(catalog):
    logging.log(logging.INFO, "Extracting pictures list...")
    pictures_list = {}
    offers = catalog.xpath('//offer')
    for offer in offers:
        picture_url = offer.findall('./picture')[0].text
        picture_name = offer.findall('./name')[0].text
        pictures_list[picture_url] = picture_name
    logging.log(logging.INFO, "Found " +
                str(len(pictures_list)) + " pictures.")
    return pictures_list


def get_categories_list(catalog):
    categories_list = []
    root_categories = catalog.xpath('//categories')
    for root_category in root_categories:
        categories = root_category.findall('./category')
        for category in categories:
            category_name = category.text
            category_id = category.attrib['id']
            category_parent_id = None
            if 'parentId' in category.attrib:
                category_parent_id = category.attrib['parentId']
            categories_list.append(
                Category(category_id, category_name, category_parent_id, None))
    return categories_list


def get_products_list(catalog):
    products_list = []
    products = catalog.xpath('//offer')
    for product in products:
        product_name = product.find('./name').text
        product_url = product.find('./url').text
        product_picture_url = product.find('./picture').text
        product_price = product.find('./price').text
        product_currency = product.find('./currencyId').text
        if product.find('./description').text == None:
            product_description = ""
        else:
            product_description = product.find('./description').text
        product_category = product.find('./categoryId').text
        products_list.append(SiteProduct(name=product_name,
                                         url=product_url,
                                         picture_url=product_picture_url,
                                         escaped_name=escape_product_picture_filename(product_name),
                                         resized_picture_path=build_resized_picture_filename(escape_product_picture_filename(product_name) + ".jpg"),
                                         price=product_price,
                                         currency=product_currency,
                                         description=product_description,
                                         category_id=product_category))
    return products_list


def get_catalog_object(CATALOG_XML_FILENAME):
    return etree.parse(CATALOG_XML_FILENAME)


def download_catalog(name):
    logging.log(logging.INFO, "Downloading catalog xml from " +
                CATALOG_URL + " to " + name + "...")
    opener = urllib2.build_opener()
    opener.addheaders = DEFAULT_USER_AGENT
    response = opener.open(CATALOG_URL)
    response_data = response.read()
    f = open(unicode(name), 'w')
    f.write(response_data)
    f.close()
    logging.log(logging.INFO, "Downloading catalog xml complete.")


def escape_product_picture_filename(filename):
    return filename.replace("/", SLASH_ESCAPE_STRING)


def build_resized_picture_filename(filename):
    return RESIZED_PRODUCT_PICTURES_DEFAULT_DIR_PREFIX + "/" + filename


if __name__ == '__main__':
    logging.basicConfig(stream=sys.stdout, level=logging.INFO)
    main()
