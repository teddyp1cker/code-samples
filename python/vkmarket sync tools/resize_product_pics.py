#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os
import logging
import imghdr
import shutil

from PIL import Image

PRODUCT_PICTURES_DEFAULT_DIR_NAME = u'originals'
RESIZED_PRODUCT_PICTURES_DEFAULT_DIR_PREFIX = u'resized'

RESIZE_WIDTH_THRESHOLD = 400
RESIZE_HEIGHT_THRESHOLD = 400


def main():
    if not os.path.exists(RESIZED_PRODUCT_PICTURES_DEFAULT_DIR_PREFIX):
        os.makedirs(RESIZED_PRODUCT_PICTURES_DEFAULT_DIR_PREFIX)

    processed_pictires_count = 0
    skipped_pictires_count = 0
    logging.log(logging.INFO, "Found " + str(len(os.listdir(PRODUCT_PICTURES_DEFAULT_DIR_NAME))) + " pictures")
    for picture_file in os.listdir(PRODUCT_PICTURES_DEFAULT_DIR_NAME):
        logging.log(logging.INFO, "Processing file " + picture_file)
        picture_filename = PRODUCT_PICTURES_DEFAULT_DIR_NAME + "/" + picture_file
        if (not os.path.isdir(picture_filename)) and is_image_file(picture_filename):
            picture = Image.open(picture_filename)
            width, height = get_image_size(picture)
            logging.log(logging.INFO, "Original size: " + str(width) + "x" + str(height))
            if need_to_upscale_image({'width': width, 'height': height}):
                resized_picture = resize_image(picture, RESIZE_HEIGHT_THRESHOLD, RESIZE_WIDTH_THRESHOLD)
                resized_picture.save(build_resized_picture_filename(picture_file))
                resized_picture.close()
                logging.log(logging.INFO, "Resize of " +
                            RESIZED_PRODUCT_PICTURES_DEFAULT_DIR_PREFIX + "/" + picture_file + " to " + str(
                    RESIZE_WIDTH_THRESHOLD) + "x" + str(RESIZE_HEIGHT_THRESHOLD) + " complete.")
                processed_pictires_count += 1
            else:
                logging.log(logging.INFO, picture_filename + " is big enough. Skip resize, just copy")
                shutil.copy(picture_filename, RESIZED_PRODUCT_PICTURES_DEFAULT_DIR_PREFIX + "/" + picture_file)
                logging.log(logging.INFO,
                            picture_filename + " copied to " + RESIZED_PRODUCT_PICTURES_DEFAULT_DIR_PREFIX + "/" + picture_file)
                skipped_pictires_count += 1
        else:
            logging.log(logging.INFO, picture_filename + " is not image file")
    logging.log(logging.INFO, "Resized count : " + str(processed_pictires_count))
    logging.log(logging.INFO, "Skipped count : " + str(skipped_pictires_count))


def build_resized_picture_filename(filename):
    return RESIZED_PRODUCT_PICTURES_DEFAULT_DIR_PREFIX + "/" + filename


def resize_image(image, height, width):
    return image.resize((width, height), Image.ANTIALIAS)


def get_image_size(image):
    return image.size


def need_to_upscale_image(image_size):
    width = image_size['width']
    height = image_size['height']
    return width < RESIZE_WIDTH_THRESHOLD or height < RESIZE_HEIGHT_THRESHOLD


def is_image_file(file):
    return imghdr.what(file)


if __name__ == '__main__':
    logging.basicConfig(stream=sys.stdout, level=logging.INFO)
    main()
