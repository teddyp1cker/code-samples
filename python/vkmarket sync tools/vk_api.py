# -*- coding: utf-8 -*-

import requests
import logging
import sys
import re
import json

from Queue import Queue
from collections import namedtuple

from models.VKMarketAlbumDeserializer import VKMarketAlbumDeserializer
from models.VKMarketProductDeserializer import VKMarketProductDeserializer
from requests_throttler import BaseThrottler


class VKMarket:
    API_URL_ENDPOINT = u'https://api.vk.com'
    API_VERSION = u'5.62'
    MAXIMUM_PRODUCT_NAME_LENGTH = 100

    logging.basicConfig(stream=sys.stdout, level=logging.INFO,
                        format="[%(filename)s:%(funcName)s:%(lineno)s] %(message)s")

    baseThrottler = BaseThrottler(name='base-throttler', delay=3)

    access_token = u''
    owner_id = u''
    version = u''

    requests_queue = None

    def __init__(self, access_token, owner_id, version):
        self.access_token = access_token
        self.owner_id = abs(int(owner_id)).__str__()
        self.version = version
        # self.requests_queue = Queue(1)
        logging.basicConfig(stream=sys.stdout, level=logging.INFO)

    def setAccessToken(self, access_token):
        self.access_token = access_token

    def setOwnerId(self, owner_id):
        self.owner_id = abs(int(owner_id)).__str__()

    def setVersion(self, version):
        self.version = version

    def getAccessToken(self):
        return self.access_token

    def getVersion(self):
        return self.version

    def getOwnerId(self):
        return self.owner_id

    def schedule_get_request(self, method_params, method_uri):
        response = None
        try:
            # response = requests.get(method_uri, params=method_params) .json()
            response = requests.get(method_uri, params=method_params)
            # baseThrottler.submit(requests.Request(method='GET', url=method_uri, params=method_params))
        except requests.HTTPError as e:
            logging.log(logging.ERROR, 'Could not execute ' + method_uri + ' method due to: ' + e)
        finally:
            return response.content

    def get(self, album_id, count, offset, extended):
        method_uri = self.API_URL_ENDPOINT + "/method/" + "market.get"
        method_params = {'version': self.version,
                         'access_token': self.access_token,
                         'owner_id': '-' + self.owner_id,
                         'album_id': album_id,
                         'count': count,
                         'offset': offset,
                         'extended': extended}
        response = self.schedule_get_request(method_params, method_uri)

        return response

    def getById(self, item_ids, extended):
        if len(item_ids) > 0 and len(item_ids) == 1:
            item_ids_params = item_ids[0]
        else:
            item_ids_params = ','.join(item_ids)
        method_uri = self.API_URL_ENDPOINT + "/method/" + "market.getById"
        method_params = {'version': self.version,
                         'access_token': self.access_token,
                         'item_ids': item_ids_params,
                         'extended': extended}
        response = self.schedule_get_request(method_params, method_uri)
        return response

    def getAlbums(self, offset, count):
        method_uri = self.API_URL_ENDPOINT + "/method/" + "market.getAlbums"
        method_params = {'version': self.version,
                         'access_token': self.access_token,
                         'owner_id': '-' + self.owner_id,
                         'offset': offset,
                         'count': count}
        response = self.schedule_get_request(method_params, method_uri)
        deserializedAlbums = self.createDeserializedAlbumsList(response)
        return deserializedAlbums

    def getAlbumById(self, album_ids):
        if len(album_ids) == 1:
            album_ids_param_string = album_ids[0]
        else:
            album_ids_param_string = ','.join(str(id) for id in album_ids)
        method_uri = self.API_URL_ENDPOINT + "/method/" + "market.getAlbumById"
        method_params = {'version': self.version,
                         'access_token': self.access_token,
                         'owner_id': '-' + self.owner_id,
                         'album_ids': album_ids_param_string}
        response = self.schedule_get_request(method_params, method_uri)

        deserializedAlbums = self.createDeserializedAlbumsList(response)
        return deserializedAlbums

    def createDeserializedAlbumsList(self, response):
        serializedAlbums = json.loads(response)
        deserializedAlbums = []
        if (len(serializedAlbums['response']) - 1) > 1:
            for i in range(1, len(serializedAlbums['response'])):
                deserializedAlbums.append(VKMarketAlbumDeserializer.deserialize(serializedAlbums['response'][i]))
        else:
            deserializedAlbums.append(VKMarketAlbumDeserializer.deserialize(serializedAlbums['response'][1]))

        return deserializedAlbums

    def createDeserializedProductsList(self, response):
        serializedProducts = json.loads(response)
        deserializedProducts = []
        if (len(serializedProducts['response']) - 1) > 1:
            for i in range(1, len(serializedProducts['response'])):
                deserializedProducts.append(VKMarketProductDeserializer.deserialize(serializedProducts['response'][i]))
        else:
            deserializedProducts.append(VKMarketProductDeserializer.deserialize(serializedProducts['response'][1]))

        return deserializedProducts

    def search(self, album_id, q, price_from, price_to, tags, sort, rev, offset, count, extended):
        if len(tags) > 0 and len(tags) == 1:
            tags_params = tags[0]
        else:
            tags_params = ','.join(tags)
        method_uri = self.API_URL_ENDPOINT + "/method/" + "market.search"
        method_params = {'version': self.version,
                         'access_token': self.access_token,
                         'owner_id': self.owner_id,
                         'album_id': album_id,
                         'q': q,
                         'price_from': price_from,
                         'price_to': price_to,
                         'tags': tags_params,
                         'sort': sort,
                         'rev': rev,
                         'offset': offset,
                         'count': count,
                         'extended': extended}
        response = self.schedule_get_request(method_params, method_uri)
        return response

    def getCategories(self, count, offset):
        method_uri = self.API_URL_ENDPOINT + "/method/" + "market.getCategories"
        method_params = {'version': self.version,
                         'access_token': self.access_token,
                         'count': count,
                         'offset': offset}
        response = self.schedule_get_request(method_params, method_uri)
        return response

    def add(self, name, description, category_id, price, deleted, main_photo_id, photo_ids):
        if len(photo_ids) > 0 and len(photo_ids) == 1:
            photo_ids_params = photo_ids[0]
        elif len(photo_ids) > 0:
            photo_ids_params = None
        else:
            photo_ids_params = ','.join(photo_ids)
        method_uri = self.API_URL_ENDPOINT + "/method/" + "market.add"
        name = self.cutProductName(name)
        method_params = {'version': self.version,
                         'access_token': self.access_token,
                         'owner_id': '-' + self.owner_id,
                         'name': name,
                         'description': description,
                         'category_id': category_id,
                         'price': price,
                         'deleted': deleted,
                         'main_photo_id': main_photo_id
                         # 'photo_ids': photo_ids_params
                         }
        response = self.schedule_get_request(method_params, method_uri)
        return response

    def cutProductName(self, name):
        if len(name.decode('cp1251')) > self.MAXIMUM_PRODUCT_NAME_LENGTH:
            name = name[0:87] + "..."
        return name

    def edit(self, item_id, name, description, category_id, price, deleted, main_photo_id, photo_ids):
        if len(photo_ids) > 0 and len(photo_ids) == 1:
            photo_ids_params = photo_ids[0]
        else:
            photo_ids_params = ','.join(photo_ids)
        name = self.cutProductName(name)
        method_uri = self.API_URL_ENDPOINT + "/method/" + "market.edit"
        method_params = {'version': self.version,
                         'access_token': self.access_token,
                         'owner_id': '-' + self.owner_id,
                         'name': name,
                         'description': description,
                         'category_id': category_id,
                         'price': price,
                         'deleted': deleted,
                         'main_photo_id': main_photo_id,
                         'photo_ids': photo_ids_params,
                         'item_id': item_id}
        response = self.schedule_get_request(method_params, method_uri)
        return response

    def delete(self, item_id):
        method_uri = self.API_URL_ENDPOINT + "/method/" + "market.delete"
        method_params = {'version': self.version,
                         'access_token': self.access_token,
                         'owner_id': '-' + self.owner_id,
                         'item_id': item_id}
        response = self.schedule_get_request(method_params, method_uri)
        return response

    def restore(self, item_id):
        method_uri = self.API_URL_ENDPOINT + "/method/" + "market.restore"
        method_params = {'version': self.version,
                         'access_token': self.access_token,
                         'owner_id': '-' + self.owner_id,
                         'item_id': item_id}
        response = self.schedule_get_request(method_params, method_uri)
        return response

    def reorderItems(self, album_id, item_id, before, after):
        method_uri = self.API_URL_ENDPOINT + "/method/" + "market.reorderItems"
        method_params = {'version': self.version,
                         'access_token': self.access_token,
                         'owner_id': '-' + self.owner_id,
                         'album_id': album_id,
                         'item_id': item_id,
                         'before': before,
                         'after': after}
        response = self.schedule_get_request(method_params, method_uri)
        return response

    def reorderAlbums(self, album_id, before, after):
        method_uri = self.API_URL_ENDPOINT + "/method/" + "market.reorderAlbums"
        method_params = {'version': self.version,
                         'access_token': self.access_token,
                         'owner_id': '-' + self.owner_id,
                         'album_id': album_id,
                         'before': before,
                         'after': after}
        response = self.schedule_get_request(method_params, method_uri)
        return response

    def addAlbum(self, title, photo_id, main_album):
        method_uri = self.API_URL_ENDPOINT + "/method/" + "market.addAlbum"
        method_params = {'version': self.version,
                         'access_token': self.access_token,
                         'owner_id': '-' + self.owner_id,
                         'title': title,
                         'photo_id': photo_id,
                         'main_album': main_album}
        response = self.schedule_get_request(method_params, method_uri)
        return response

    def editAlbum(self, album_id, title, photo_id, main_album):
        method_uri = self.API_URL_ENDPOINT + "/method/" + "market.editAlbum"
        method_params = {'version': self.version,
                         'access_token': self.access_token,
                         'owner_id': '-' + self.owner_id,
                         'title': title,
                         'photo_id': photo_id,
                         'main_album': main_album,
                         'album_id': album_id}
        response = self.schedule_get_request(method_params, method_uri)
        return response

    def deleteAlbum(self, album_id):
        method_uri = self.API_URL_ENDPOINT + "/method/" + "market.deleteAlbum"
        method_params = {'version': self.version,
                         'access_token': self.access_token,
                         'owner_id': '-' + self.owner_id,
                         'album_id': album_id}
        response = self.schedule_get_request(method_params, method_uri)
        return response

    def removeFromAlbum(self, item_id, album_ids):
        if len(album_ids) > 0 and len(album_ids) == 1:
            album_ids_params = album_ids[0]
        else:
            album_ids_params = ','.join(album_ids)
        method_uri = self.API_URL_ENDPOINT + "/method/" + "market.removeFromAlbum"
        method_params = {'version': self.version,
                         'access_token': self.access_token,
                         'owner_id': '-' + self.owner_id,
                         'item_id': item_id,
                         'album_ids': album_ids_params}
        response = self.schedule_get_request(method_params, method_uri)
        return response

    def addToAlbum(self, item_id, album_ids):
        if len(album_ids) > 0 and len(album_ids) == 1:
            album_ids_params = album_ids[0]
        else:
            album_ids_params = ','.join(album_ids)
        method_uri = self.API_URL_ENDPOINT + "/method/" + "market.addToAlbum"
        method_params = {'version': self.version,
                         'access_token': self.access_token,
                         'owner_id': '-' + self.owner_id,
                         'item_id': item_id,
                         'album_ids': album_ids_params}
        response = self.schedule_get_request(method_params, method_uri)
        return response


class VKMarketPhotos:
    access_token = u''
    owner_id = u''
    version = u''
    api_uri_endpoint = u'https://api.vk.com'
    api_version = u'5.62'

    logging.basicConfig(stream=sys.stdout, level=logging.INFO,
                        format="[%(filename)s:%(funcName)s:%(lineno)s] %(message)s")

    def __init__(self, access_token, owner_id, version):
        self.access_token = access_token
        self.owner_id = abs(int(owner_id)).__str__()
        self.version = version
        logging.basicConfig(stream=sys.stdout, level=logging.INFO)

    def setAccessToken(self, access_token):
        self.access_token = access_token

    def setOwnerId(self, owner_id):
        self.owner_id = abs(int(owner_id)).__str__()

    def setVersion(self, version):
        self.version = version

    def getAccessToken(self):
        return self.access_token

    def getVersion(self):
        return self.version

    def getOwnerId(self):
        return self.owner_id

    def get_request(self, method_params, method_uri):
        response = None
        try:
            response = requests.get(method_uri, params=method_params).json()
        except requests.HTTPError as e:
            logging.log(logging.ERROR, "Can't  execute ' + method_uri + ' method due to: " + e)
        finally:
            return response

    def getMarketUploadServer(self, main_photo):
        method_uri = self.api_uri_endpoint + "/method/" + "photos.getMarketUploadServer"
        method_params = {
            'version': self.version,
            'access_token': self.access_token,
            'group_id': self.owner_id,
            'main_photo': main_photo}
        response = self.get_request(method_params, method_uri)
        logging.log(logging.INFO, "getMarketUploadServer result: " + str(response))
        return response

    def saveMarketPhoto(self, photo, server, hash, crop_data, crop_hash):
        method_uri = self.api_uri_endpoint + "/method/" + "photos.saveMarketPhoto"
        method_params = {'version': self.version,
                         'access_token': self.access_token,
                         'group_id': self.owner_id,
                         'photo': photo,
                         'server': server,
                         'hash': hash}
        if crop_data and crop_hash:
            method_params['crop_data'] = crop_data
            method_params['crop_hash'] = crop_hash
        response = self.get_request(method_params, method_uri)
        return response

    def uploadMarketPhotoBytes(self, upload_url, image_data):
        response = None
        try:
            method_uri = upload_url
            image_payload = {'photo': ('product_pic.jpg', image_data)}
            response = requests.post(method_uri, files=image_payload).json()
        except requests.HTTPError as e:
            logging.log(logging.ERROR, "Could not execute uploadMarketPhotoBytes due to: " + str(e))
        finally:
            return response


class VKMarketUtils:
    logging.basicConfig(stream=sys.stdout, level=logging.INFO,
                        format="[%(filename)s:%(funcName)s:%(lineno)s] %(message)s")

    @staticmethod
    def extractPhotoId(id):
        combined_picture_id = re.search(re.compile(ur'\d+_\d+'), id)
        if combined_picture_id:
            return re.search(re.compile(ur'_\d+'), combined_picture_id.group(0)).group(0)[1:]
        else:
            return None

    @staticmethod
    def buildImageUploadResult(upload_response):
        VKImageUploadResult = namedtuple('VKImageUploadResult', ['server', 'hash', 'photo',
                                                                 'crop_data', 'crop_hash'], verbose=False)
        given_server = upload_response['server']
        given_hash = upload_response['hash']
        given_photo = upload_response['photo']
        given_crop_data = upload_response['crop_data']
        given_crop_hash = upload_response['crop_hash']
        return VKImageUploadResult(server=given_server, hash=given_hash,
                                   photo=given_photo, crop_data=given_crop_data,
                                   crop_hash=given_crop_hash)

    @staticmethod
    def extractImageUploadServer(response):
        return response['response']['upload_url']

    @staticmethod
    def extractImageSaveResult(response):
        return response['response'][0]['id']

    @staticmethod
    def extractProductAddResult(response):
        res = None
        try:
            res = response['response']['market_item_id']
        except Exception:
            logging.log(logging.INFO, Exception)
        finally:
            return res
