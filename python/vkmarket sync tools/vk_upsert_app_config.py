#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import json

import sys
import logging
import sqlite3
import urlparse

import functools
from PySide.QtCore import *
from PySide.QtGui import *
from PySide.QtWebKit import *


class AppSettings(object):
    def __init__(self, app_id, app_version, app_secret, app_token, app_scope, vk_group_id, app_token_expires,
                 app_desired_response_type, app_desktop_redirect_uri):
        self.app_id = app_id
        self.app_version = app_version
        self.app_secret = app_secret
        self.app_scope = app_scope
        self.app_token = app_token
        self.vk_group_id = vk_group_id
        self.app_token_expires = app_token_expires
        self.app_desired_response_type = app_desired_response_type
        self.app_desktop_redirect_uri = app_desktop_redirect_uri

    def __init__(self):
        pass

    def readFomDB(self, CATALOG_DB_PATH):
        try:
            connection = sqlite3.connect(CATALOG_DB_PATH)
            connection.row_factory = sqlite3.Row
            with connection:
                cursor = connection.cursor()
                cursor.execute(
                    "SELECT app_id AS app_id, "
                    "app_version AS app_version, "
                    "app_secret AS app_secret, "
                    "app_desktop_redirect_uri AS app_desktop_redirect_uri, "
                    "app_scope AS app_scope, "
                    "app_desired_response_type AS app_desired_response_type, "
                    "app_token AS app_token, "
                    "app_token_expires AS app_token_expires, "
                    "vk_group_id AS vk_group_id "
                    "FROM 'vk_app_settings';")
                result = cursor.fetchone()
                if (result != None):
                    self.app_id = result['app_id']
                    self.app_version = result['app_version']
                    self.app_secret = result['app_secret']
                    self.app_desktop_redirect_uri = result['app_desktop_redirect_uri']
                    self.app_scope = result['app_scope']
                    self.app_desired_response_type = result['app_desired_response_type']
                    self.app_token = result['app_token']
                    self.app_token_expires = result['app_token_expires']
                    self.vk_group_id = result['vk_group_id']
                else:
                    logging.log(logging.WARNING, "App config table is empty. Nothing to initialize")
        except sqlite3.Error, e:
            logging.log(logging.ERROR, "Database error occurred : '" + str(e))
        finally:
            if connection:
                connection.commit()
                connection.close()
            return self

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=False, indent=4)


DEFAULT_CATALOG_DB_PATH = u'catalog.db'
CATALOG_DB_PATH = u''

VK_GROUP_ID = u'VK_GROUP_ID'
VK_API_VERSION = u'VK_API_VERSION'
VK_APP_ID = None
VK_APP_SECRET = u'VK_APP_SECRET'
VK_APP_REDIRECT_URI = u'https://oauth.vk.com/blank.html'
VK_APP_RESPONSE_TYPE = u'token'
VK_APP_SCOPE = u'offline,market,photos'

VK_APP_OAUTH_TOKEN = None

APP_SETTINGS_DB_SCHEME_QUERY = '''CREATE TABLE `vk_app_settings` (
	`app_id`	INTEGER NOT NULL DEFAULT ''' + str(VK_APP_ID) + ''' ,
	`app_version`	TEXT NOT NULL DEFAULT ''' + "'" + VK_API_VERSION + "'" + ''',
	`app_secret`	TEXT NOT NULL DEFAULT ''' + "'" + VK_APP_SECRET + "'" + ''',
	`app_desktop_redirect_uri`	TEXT NOT NULL DEFAULT ''' + "'" + VK_APP_REDIRECT_URI + "'" + ''',
	`app_scope`	TEXT NOT NULL DEFAULT ''' + "'" + VK_APP_SCOPE + "'" + ''',
	`app_desired_response_type`	TEXT NOT NULL DEFAULT ''' + VK_APP_RESPONSE_TYPE + ''',
	`app_token`	TEXT NOT NULL,
	`app_token_expires`	INTEGER NOT NULL DEFAULT 0,
	`vk_group_id`	TEXT NOT NULL DEFAULT ''' + VK_GROUP_ID + ''');'''


def createSettingsDBScheme(catalogPath):
    try:
        connection = sqlite3.connect(catalogPath)
        with connection:
            cursor = connection.cursor()
            cursor.execute(
                "SELECT * FROM sqlite_master WHERE name ='vk_app_settings' and type='table';")
            if (cursor.fetchone() == None):
                logging.log(
                    logging.WARNING,
                    "Settings table not found in catalog database. Will create it with default values...")
                cursor.execute(APP_SETTINGS_DB_SCHEME_QUERY)
    except sqlite3.Error, e:
        logging.log(logging.ERROR, "Database error occurred : '" + str(e))
    finally:
        if connection:
            connection.commit()
            connection.close()


def resetAuthToken(catalogPath):
    try:
        connection = sqlite3.connect(catalogPath)
        with connection:
            cursor = connection.cursor()
            cursor.execute(
                "UPDATE vk_app_settings SET app_token = '', app_token_expires = 0;")
    except sqlite3.Error, e:
        logging.log(logging.ERROR, "Database error occurred : '" + str(e))
    finally:
        if connection:
            connection.commit()
            connection.close()


def clearAppSettings(catalogPath):
    try:
        connection = sqlite3.connect(catalogPath)
        with connection:
            cursor = connection.cursor()
            cursor.execute(
                "SELECT * FROM sqlite_master WHERE name ='vk_app_settings' and type='table';")
            if (cursor.fetchone() != None):
                cursor.execute("DROP TABLE vk_app_settings;")
    except sqlite3.Error, e:
        logging.log(logging.ERROR, "Database error occurred : '" + str(e) + "'")
    finally:
        if connection:
            connection.commit()
            connection.close()


def getAppToken(catalogPath):
    token = None
    connection = sqlite3.connect(catalogPath)
    try:
        with connection:
            cursor = connection.cursor()
            cursor.execute("SELECT app_token FROM 'vk_app_settings';")
            token = cursor.fetchone()
            if (token == None):
                logging.log(logging.WARNING, "App token is not found")
    except sqlite3.Error, e:
        logging.log(logging.ERROR, "Database error occurred : '" + str(e))
    finally:
        if connection:
            connection.commit()
            connection.close()
    return token


def saveAppToken(catalogPath, token):
    try:
        connection = sqlite3.connect(catalogPath)
        with connection:
            cursor = connection.cursor()
            cursor.execute(
                "INSERT INTO vk_app_settings(app_token) VALUES (:token);", {'token': token})
            logging.log(logging.INFO, "App token is '" + token + "' saved in app settings table")
    except sqlite3.Error, e:
        logging.log(logging.ERROR, "Database error occurred : '" + str(e))
    finally:
        if connection:
            connection.commit()
            connection.close()


def buildOauthUrl():
    uri = u'https://oauth.vk.com/authorize' \
          u'?client_id=' + str(VK_APP_ID) + \
          u'&display=page' \
          u'&redirect_uri=' + VK_APP_REDIRECT_URI + \
          u'&scope=' + VK_APP_SCOPE + \
          u'&response_type=' + VK_APP_RESPONSE_TYPE + \
          u'&v=5.52'
    return uri


def main():
    args = setAndParseArguments()

    if args.d:
        CATALOG_DB_PATH = args.d
    else:
        CATALOG_DB_PATH = DEFAULT_CATALOG_DB_PATH

    logging.log(logging.INFO, CATALOG_DB_PATH)

    if args.clear:
        logging.log(logging.INFO, "App token is going to reset")
        clearAppSettings(CATALOG_DB_PATH)
        createSettingsDBScheme(CATALOG_DB_PATH)
        logging.log(logging.INFO, "App token does'nt exists in app config table")
        app = QApplication(sys.argv)
        oauthDialogWebView = QWebView()
        oauthDialogWebView.load(QUrl(buildOauthUrl()))
        oauthDialogWebView.urlChanged.connect(functools.partial(uri_changed_handler, CATALOG_DB_PATH))
        oauthDialogWebView.show()
        sys.exit(app.exec_())
    else:
        token = getAppToken(CATALOG_DB_PATH)
        if token == None:
            logging.log(logging.INFO, "App token does'nt exists in app config table")
            app = QApplication(sys.argv)
            oauthDialogWebView = QWebView()
            oauthDialogWebView.load(QUrl(buildOauthUrl()))
            oauthDialogWebView.urlChanged.connect(functools.partial(uri_changed_handler, CATALOG_DB_PATH))
            oauthDialogWebView.show()
            sys.exit(app.exec_())
        else:
            logging.log(logging.INFO, "App token already exists in app config table")

        appConfig = AppSettings()
        logging.log(logging.INFO, appConfig.readFomDB(CATALOG_DB_PATH).toJSON())


def setAndParseArguments():
    parser = argparse.ArgumentParser(description='VK sync utility arguments description')
    parser.add_argument('-d', metavar='D', type=str, help='Path to catalog database file')
    parser.add_argument('-c', '--clear', action="store_true",
                        help="Clear vk app credentials")
    args = parser.parse_args()
    return args


def uri_changed_handler(catalogPath, url):
    token = extractApiToken(str(url))
    if token != None:
        logging.log(logging.INFO, "Token: " + str(token))
        saveAppToken(catalogPath, str(token))
        sys.exit()


def extractApiToken(redirect_uri):
    params = urlparse.parse_qs(urlparse.urlparse(redirect_uri).fragment)
    token = params.get('access_token', None)
    if token != None:
        token = str(token[0])
    return token


if __name__ == '__main__':
    logging.basicConfig(stream=sys.stdout, level=logging.INFO,
                        format="[%(filename)s:%(funcName)s:%(lineno)s] %(message)s")
    main()
