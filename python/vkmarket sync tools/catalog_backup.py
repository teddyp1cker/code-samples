#!/usr/bin/env python
# -*- coding: utf-8 -*-

import codecs
import logging
import os
import pprint
import sys
from itertools import chain
from logging import handlers
from time import sleep

import requests
from lxml import etree
from lxml import html
from lxml.etree import tostring
from peewee import *
from playhouse.sqlite_ext import SqliteExtDatabase
from retrying import retry

CATALOG_URL = u'https://korealove.ru/pricelist.xml'
CATALOG_XML_FILENAME = u'catalog.xml'

PRODUCT_DESC_SHORT_XPATH_QUERY = u'//*[@id={{itemId}}]/div[1]/div/div[2]/div[1]/h1'
PRODUCT_DESC_FULL_XPATH_QUERY = u'//*[@id="product-full-desc"]'

DEFAULT_USER_AGENT = [('User-Agent',
                       'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.1847.137 Safari/537.36')]

DEFAULT_CATALOG_DB_PATH = u'full-catalog-backup.db'

db = SqliteExtDatabase(DEFAULT_CATALOG_DB_PATH)

RESERVED_TABLES_NAMES = (u'catalog', u'product')

LOGFILE = u'full-catalog-backup.log'


class BaseModel(Model):
    class Meta:
        database = db


class Currency(BaseModel):
    iso_code = CharField()
    native_symbol = CharField(null=True)


class Category(BaseModel):
    category_id = BigIntegerField()
    name = CharField()
    parentId = BigIntegerField(null=True)


class Product(BaseModel):
    product_id = BigIntegerField(null=True)
    code = CharField(null=True)
    category = ForeignKeyField(Category, null=True)
    name = CharField()
    manufacturer = CharField(null=True)
    short_description = TextField(null=True)
    full_description = TextField(null=True)
    currency = ForeignKeyField(Currency, null=True)
    price = DecimalField(null=True)
    old_price = DecimalField(null=True)
    url = CharField(null=True)
    comment = TextField(null=True)
    image_url = CharField(null=True)
    is_available = BooleanField(default=False)
    is_in_stock = BooleanField(default=False)
    qty_in_stock = DecimalField(null=True)
    image_blob = BlobField(null=True)


class Application:
    __args = None
    __printer = pprint.PrettyPrinter(indent=4)

    def __init__(self):
        log = logging.getLogger('')
        log.setLevel(logging.INFO)
        format = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")

        ch = logging.StreamHandler(sys.stdout)
        ch.setFormatter(format)
        log.addHandler(ch)

        fh = handlers.RotatingFileHandler(LOGFILE, maxBytes=(1048576 * 5), backupCount=7)
        fh.setFormatter(format)
        log.addHandler(fh)

    def generate_catalog_filename(self, datetime):
        return CATALOG_XML_FILENAME + datetime + '.db'

    def get_catalog_object(self, CATALOG_XML_FILENAME):
        with codecs.open(CATALOG_XML_FILENAME, 'r', 'utf-8') as catalog_file:
            return etree.parse(catalog_file)

    def download_catalog(self, name):
        logging.log(logging.INFO, "Downloading catalog xml from " +
                    CATALOG_URL + " to " + name + "...")
        response_data = requests.get(CATALOG_URL, timeout=10).content or None
        f = open(unicode(name), 'w')
        f.write(response_data)
        f.close()
        logging.log(logging.INFO, "Downloading catalog xml complete.")

    def get_categories_list(self, catalog):
        categories_list = []
        root_categories = catalog.xpath('//categories')
        for root_category in root_categories:
            categories = root_category.findall('./category')
            for category in categories:
                category_name = category.text
                category_id = category.attrib['id']
                category_parent_id = None
                if 'parentId' in category.attrib:
                    category_parent_id = category.attrib['parentId']
                categories_list.append(
                    {'id': category_id,
                     'name': category_name,
                     'parentId': category_parent_id}
                )
        self.__printer.pprint(categories_list)
        return categories_list

    def get_products_list(self, catalog):
        products_list = []
        products = catalog.xpath('//offer')
        for product in products:
            product_name = product.find('./name').text
            product_url = product.find('./url').text
            product_picture_url = product.find('./picture').text
            product_price = product.find('./price').text
            product_currency = product.find('./currencyId').text
            if product.find('./description').text == None:
                product_description = ""
            else:
                product_description = product.find('./description').text
            product_category = product.find('./categoryId').text
            if product.find('./oldprice') == None:
                product_old_price = None
            else:
                product_old_price = product.find('./oldprice').text
            product_id = product.get('id')
            product_available = product.get('available')
            products_list.append(
                {
                    'name': product_name,
                    'picture_url': product_picture_url,
                    'price': product_price,
                    'old_price': product_old_price,
                    'currency': product_currency,
                    'category_id': product_category,
                    'comment': product_description,
                    'url': product_url,
                    'product_id': product_id,
                    'available': product_available
                }
            )
        self.__printer.pprint(products_list)
        return products_list

    def fill_categories(self, categories):
        for category_item in categories:
            Category.create(
                category_id=category_item['id'],
                name=category_item['name'] or "",
                parentId=category_item['parentId']
            )

    def fill_products(self, products):
        for product_item in products:
            sleep(0.5)
            category = Category.get(Category.category_id == product_item['category_id']) or None
            Product.create(
                category=category,
                name=product_item['name'],
                price=product_item['price'],
                old_price=product_item['old_price'],
                image_url=product_item['picture_url'],
                comment=product_item['comment'],
                url=product_item['url'],
                product_id=product_item['product_id'],
                is_available=product_item['available'],
                full_description=self.extract_full_description(url=product_item['url']),
                image_blob=self.get_product_image_bytes(image_url=product_item['picture_url'])
            )

    @retry(wait_fixed=1000, stop_max_attempt_number=3)
    def extract_full_description(self, url):
        logging.log(logging.INFO, "Starting parsing full product description for url " + str(url))
        product_full_text_desc = None
        headers = {
            'User-Agent': DEFAULT_USER_AGENT
        }
        try:
            product_page = requests.get(url, headers=headers, timeout=3) or None
            if product_page:
                product_page_tree = html.fromstring(product_page.content)
                full_desc_container = product_page_tree.xpath(PRODUCT_DESC_FULL_XPATH_QUERY)
                if len(full_desc_container) > 0:
                    full_desc = full_desc_container[0] or None
                    if full_desc:
                        product_full_text_desc = self.stringify_children(full_desc)
        except requests.Timeout:
            logging.log(logging.WARNING, "Request for url " + url + " timeout")

        return product_full_text_desc

    @retry(wait_fixed=1000, stop_max_attempt_number=3)
    def get_product_image_bytes(self, image_url):
        logging.log(logging.INFO, "Download product image for " + str(image_url))
        headers = {
            'User-Agent': DEFAULT_USER_AGENT
        }
        image_blob = None
        try:
            image_blob = requests.get(image_url, headers=headers, stream=True, timeout=3).content
        except requests.Timeout:
            logging.log(logging.WARNING, "Request for url " + image_url + " timeout")
        return image_blob

    def stringify_children(self, node):
        parts = ([node.text] +
                 list(chain(*([node_child.text, tostring(node_child), node_child.tail] for node_child in
                              node.getchildren()))) +
                 [node.tail])
        return ''.join(filter(None, parts))

    def run(self):
        logging.log(logging.INFO, "Backup started...")
        if not os.path.isfile(DEFAULT_CATALOG_DB_PATH):
            open(DEFAULT_CATALOG_DB_PATH, "w")
        db.connect()
        if any(table in RESERVED_TABLES_NAMES for table in db.get_tables()):
            logging.log(logging.INFO, "Clear tables invoke...")
            db.drop_tables([Product, Category])
            db.create_tables([Category, Product])
        else:
            db.create_tables([Category, Product])
        self.download_catalog(CATALOG_XML_FILENAME)
        catalog = self.get_catalog_object(CATALOG_XML_FILENAME)
        categories = self.get_categories_list(catalog)
        products = self.get_products_list(catalog)
        self.fill_categories(categories=categories)
        self.fill_products(products=products)
        logging.log(logging.INFO, "Backup completed")


if __name__ == '__main__':
    app = Application()
    app.run()
