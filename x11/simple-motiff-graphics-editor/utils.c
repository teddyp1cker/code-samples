#include <Xm/Xm.h>

#include "utils.h"

Pixel convert_color_name_to_pixel(Widget widget, char *color_name) {

    XrmValue from_value;
    XrmValue to_value;

    if (widget != NULL) {

        from_value.addr = color_name;
        from_value.size = strlen(color_name) + 1;
        to_value.addr = (XtPointer) 0;

        XtConvertAndStore(widget, XmRString, &from_value, XmRPixel, &to_value);

        if (to_value.addr != (XtPointer) 0) {
            return (*(Pixel *) to_value.addr);
        }
    }
    return XmUNSPECIFIED_PIXEL;
}
