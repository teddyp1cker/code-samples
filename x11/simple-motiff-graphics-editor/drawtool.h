#ifndef DRAWTOOL_H
#define DRAWTOOL_H

#define TRUE 1
#define FALSE 0

#include "shared_globals.h"

void set_color_defaults(Widget widget);

void set_shape_types_defaults();

void set_line_type_defaults();

void set_line_width_defaults();

void create_draw_widget(Widget parent, void (*draw_callback)(Widget, XtPointer, XtPointer),
                        void (*expose_callback)(Widget, XtPointer, XtPointer),
                        void (*motion_callback)(Widget, XtPointer, XEvent *, Boolean *));

void create_clear_draw_button_widget(Widget widget, void (*callback_func)(Widget, XtPointer, XtPointer));

void create_main_window_widget(Widget parent);

void create_frame_tools_container_widget(Widget parent);

void create_tools_rowcolumn_create_widget(Widget parent);

void create_scrolled_window_container(Widget parent);

void create_top_level_shell_widget(int *argc, char **argv);

void draw_area_widget_draw_callback(Widget widget, XtPointer client_data, XtPointer call_data);

void draw_area_widget_expose_callback(Widget widget, XtPointer client_data, XtPointer call_data);

void draw_area_widget_motion_callback(Widget widget, XtPointer client_data, XEvent *event, Boolean *cont);

void clear_draw_callback(Widget widget, XtPointer client_data, XtPointer call_data);

void top_level_shell_close_callback(Widget widget, XtPointer client_data, XtPointer call_data);

void draw_shape(struct _WidgetRec *widget, GC gc, Shape shape);

GC reinitialize_gc(Widget *widget, Pixel *draw_foreground, Pixel *draw_background);

void line_width_change_callback(Widget widget, XtPointer client_data, XtPointer call_data);

void color_selected_callback(Widget widget, XtPointer client_data, XtPointer call_data);

void create_shape_selector(Widget parent, void (*callback_func)(Widget, XtPointer, XtPointer));

void create_line_foreground_color_selector(Widget parent, void (*callback_func)(Widget, XtPointer, XtPointer));

void create_line_background_color_selector(Widget parent, void (*callback_func)(Widget, XtPointer, XtPointer));

void create_fill_background_color_selector(Widget parent, void (*callback_func)(Widget, XtPointer, XtPointer));

void create_fill_foreground_color_selector(Widget parent, void (*callback_func)(Widget, XtPointer, XtPointer));

void create_line_width_selector(Widget parent, void (*callback_func)(Widget, XtPointer, XtPointer));

void create_line_type_selector(Widget parent, void (*callback_func)(Widget, XtPointer, XtPointer));

void xt_manage_all_child_widgets();

void clear_shapes_list();

int main(int argc, char **argv);

#endif
