#ifndef SHARED_GLOBALS_H
#define SHARED_GLOBALS_H

#define TRUE 1
#define FALSE 0

#define POINT 0
#define LINE 1
#define RECTANGLE 2
#define FILLED_RECTANGLE 3
#define ELLIPSE 4
#define FILLED_ELLIPSE 5

#define SOLID_LINE 0
#define DOUBLE_DASHED_LINE 1

#define DEFAULT_LINE_WIDTH 0

#define MOUSE_BUTTON_RELEASED 0
#define MOUSE_BUTTON_PRESSED 1
#define MOUSE_BUTTON_PRESSED_AND_MOVE 2

#define LINE_FOREGROUND_COLOR_TYPE 0
#define LINE_BACKROUND_COLOR_TYPE 1
#define FILL_FOREGROUND_COLOR_TYPE 2
#define FILL_BACKGROUND_COLOR_TYPE 3

#define DEFAULT_LINE_FG_COLOR 0
#define DEFAULT_LINE_BG_COLOR 1
#define DEFAULT_FILL_FG_COLOR 0
#define DEFAULT_FILL_BG_COLOR 1

#include <stdio.h>
#include <stdlib.h>
#include <Xm/Xm.h>
#include <Xm/MainW.h>

#include <X11/Intrinsic.h>
#include <X11/StringDefs.h>
#include <Xm/Xm.h>
#include <Xm/Protocols.h>
#include <Xm/MainW.h>
#include <Xm/RowColumn.h>
#include <Xm/PushB.h>
#include <Xm/Frame.h>
#include <Xm/DrawingA.h>

extern XtAppContext app_context;

typedef struct shape {
    Pixel line_foreground_color;
    Pixel line_background_color;
    Pixel fill_foreground_color;
    Pixel fill_background_color;
    int x1, y1;
    int x2, y2;
    int shape_type;
    int line_width;
    int line_type;
} Shape;

extern Shape *shapes_list;
extern Shape current_shape;

typedef struct drawToolsMenu {
    Widget shape_label;
    Widget shape_radio_box;
    Widget line_width_select_scale_widget;
    Widget line_width_label;

    Widget Line_foreground_color_label;
    Widget line_foreground_color_combo_box;

    Widget line_background_color_label;
    Widget line_background_color_combo_box;

    Widget fill_foreground_color_label;
    Widget fill_foreground_color_combo_box;

    Widget fill_background_color_label;
    Widget fill_background_color_combo_box;

    Widget line_type_label;
    Widget line_type_combo_box;
} DrawToolsMenu;

DrawToolsMenu draw_tools_menu;

extern size_t shapes_allocated;
extern size_t shapes_count;

extern Display *display;
extern Screen *screen;

extern Pixel current_line_foreground_color;
extern Pixel current_line_background_color;
extern Pixel current_fill_foreground_color;
extern Pixel current_fill_background_color;

extern Widget top_level_shell;
extern Widget main_window_widget;
extern Widget frame_container;
extern Widget tools_row_column_container;
extern Widget scrolled_window;
extern Widget draw_area;
extern Widget clear_button;

extern Widget close_dialog;

extern int close_dialog_visibility_state;

extern int input_button_state;

extern int current_shape_type;
extern int default_shape_type;

extern int current_line_type;
extern int default_line_type;

extern int current_line_width;
extern int default_line_width;

char *supported_color_names[];

static const int INITIAL_SCROLL_VIEW_WIDTH = 800;
static const int INITIAL_SCROLL_VIEW_HEIGHT = 600;

static const int SUPPORTED_COLORS_NUMBER = 5;

#endif
