#include "shared_globals.h"

XtAppContext app_context;

Display *display;
Screen *screen;

Pixel current_line_foreground_color = NULL;
Pixel current_line_background_color = NULL;
Pixel current_fill_foreground_color = NULL;
Pixel current_fill_background_color = NULL;

Widget top_level_shell = NULL;
Widget scrolled_window = NULL;
Widget draw_area = NULL;
Widget clear_button = NULL;
Widget main_window_widget = NULL;
Widget frame_container = NULL;
Widget tools_row_column_container = NULL;
Widget close_dialog = NULL;

Shape *shapes_list = NULL;
Shape current_shape;

size_t shapes_allocated = 0;
size_t shapes_count = 0;

extern DrawToolsMenu draw_tools_menu;

int input_button_state = MOUSE_BUTTON_RELEASED;

int current_shape_type;
int default_shape_type = LINE;

int current_line_type;
int default_line_type = SOLID_LINE;

int current_line_width;
int default_line_width = DEFAULT_LINE_WIDTH;

int close_dialog_visibility_state = FALSE;

char *supported_color_names[] = {"Black", "White", "Red", "Green", "Blue"};
