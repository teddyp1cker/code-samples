#include <stdint.h>

#include <Xm/MessageB.h>

#include "shared_globals.h"

#include "close_dialog.h"


void switch_close_dialog_visibility_state(int state) { close_dialog_visibility_state = state; }

void close_dialog_buttons_callback(Widget widget,
                                   XtPointer client_data,
                                   XtPointer call_data) {

	intptr_t must_close_app_selected = (intptr_t) client_data;

	if (must_close_app_selected == TRUE) {

		XClearWindow(XtDisplay(draw_area), XtWindow(draw_area));

		XtDestroyWidget(top_level_shell);
		XtDestroyWidget(draw_area);
		XtDestroyWidget(scrolled_window);
		XtDestroyWidget(main_window_widget);
		XtDestroyWidget(frame_container);
		XtDestroyWidget(tools_row_column_container);
		XtDestroyWidget(clear_button);

		XtUnrealizeWidget(top_level_shell);
		XtUnrealizeWidget(draw_area);
		XtUnrealizeWidget(scrolled_window);
		XtUnrealizeWidget(main_window_widget);
		XtUnrealizeWidget(frame_container);
		XtUnrealizeWidget(tools_row_column_container);
		XtUnrealizeWidget(clear_button);

		XCloseDisplay(display);

		exit(0);
	}

	switch_close_dialog_visibility_state(FALSE);
}

void create_close_dialog(Widget parent) {

	XtSetLanguageProc(NULL, NULL, NULL);

	if (close_dialog_visibility_state == FALSE) {

		close_dialog = XmCreateQuestionDialog(parent, "Close drawing dialog", NULL, 0);

		XmString question_dialog_message = XmStringCreateSimple("Are you really going to close this awesome app?!");
		XmString question_dialog_yes_button_title = XmStringCreateSimple("Yes");
		XmString question_dialog_no_button_title = XmStringCreateSimple("No");
		XmString question_dialog_title = XmStringCreateSimple("Close dialog");

		XtVaSetValues(close_dialog,
		              XmNmessageString, question_dialog_message,
		              XmNokLabelString, question_dialog_yes_button_title,
		              XmNdialogTitle, question_dialog_title,
		              XmNcancelLabelString, question_dialog_no_button_title,
		              XmNdialogStyle, XmDIALOG_FULL_APPLICATION_MODAL,
		              XmNnoResize, True,
		              NULL);

		// TRUE if we are going to destroy all widgets and close the app

		XtAddCallback(close_dialog, XmNokCallback, close_dialog_buttons_callback, (XtPointer) TRUE);
		XtAddCallback(close_dialog, XmNcancelCallback, close_dialog_buttons_callback, (XtPointer) FALSE);

		XmStringFree(question_dialog_yes_button_title);
		XmStringFree(question_dialog_no_button_title);
		XmStringFree(question_dialog_message);
		XmStringFree(question_dialog_title);

		switch_close_dialog_visibility_state(TRUE);
	}

	XtManageChild(close_dialog);
	return;

}
