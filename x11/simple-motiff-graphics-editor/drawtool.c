#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include <X11/Intrinsic.h>

#include <Xm/Xm.h>
#include <Xm/DrawingA.h>
#include <Xm/PushB.h>
#include <Xm/ScrolledW.h>
#include <Xm/Protocols.h>
#include <Xm/Label.h>
#include <Xm/Scale.h>
#include <Xm/ComboBox.h>

#include "shared_globals.h"
#include "utils.h"

#include "close_dialog.h"
#include "drawtool.h"


void top_level_shell_close_callback(Widget widget, XtPointer client_data, XtPointer call_data) {

    if (top_level_shell != NULL) {
        create_close_dialog(top_level_shell);
    } else {
        fprintf(stderr, "Error while creating close dialog, top shell widget isn't initialized");
    }
}

void create_tools_rowcolumn_create_widget(Widget parent) {

    if (parent != NULL) {
        tools_row_column_container = XtVaCreateManagedWidget(
                "Raw Columns Tools Container",
                xmRowColumnWidgetClass,
                parent,
                XmNentryAlignment, XmALIGNMENT_CENTER,
                XmNorientation, XmVERTICAL,
                XmNpacking, XmPACK_TIGHT,
                NULL);
    }
}

void create_frame_tools_container_widget(Widget parent) {

    if (parent != NULL) {
        frame_container = XtVaCreateManagedWidget(
                "Frame Widget Container",
                xmFrameWidgetClass,
                parent,
                NULL);
    }
}

void create_main_window_widget(Widget parent) {

    if (parent != NULL) {
        main_window_widget = XtVaCreateManagedWidget(
                "Main Window Container",
                xmMainWindowWidgetClass,
                top_level_shell,
                XmNcommandWindowLocation, XmCOMMAND_BELOW_WORKSPACE,
                XmNwidth, INITIAL_SCROLL_VIEW_WIDTH,
                XmNheight, INITIAL_SCROLL_VIEW_HEIGHT,
                NULL);
    }
}

void create_scrolled_window_container(Widget parent) {

    if (parent != NULL) {
        scrolled_window = XtVaCreateManagedWidget("Scrolled Window Container",
                                                  xmScrolledWindowWidgetClass,
                                                  parent,
                                                  XmNwidth, INITIAL_SCROLL_VIEW_WIDTH,
                                                  XmNheight, INITIAL_SCROLL_VIEW_HEIGHT,
                                                  XmNscrollingPolicy, XmAUTOMATIC,
                                                  XmNscrollBarDisplayPolicy, XmAS_NEEDED,
                                                  XmNtopAttachment, XmATTACH_FORM,
                                                  XmNbottomAttachment, XmATTACH_FORM,
                                                  XmNleftAttachment, XmATTACH_WIDGET,
                                                  XmNleftWidget, NULL,
                                                  XmNrightAttachment, XmATTACH_FORM,
                                                  XmNvisualPolicy, XmVARIABLE,
                                                  NULL);

    }
}

void create_top_level_shell_widget(int *argc, char **argv) {

    top_level_shell = XtVaOpenApplication(&app_context, "DrawTool Application", NULL, 0,
                                          argc, argv, NULL, sessionShellWidgetClass, NULL);
    display = XtDisplay(top_level_shell);
    screen = XtScreen(top_level_shell);

    XtVaSetValues(top_level_shell, XmNdeleteResponse, XmDO_NOTHING, NULL);
    XtVaSetValues(top_level_shell, XmNtitle, "GUX Semester Project #1 by xkuzne00", NULL);

    Atom wm_delete = XInternAtom(XtDisplay(top_level_shell), "WM_DELETE_WINDOW", False);
    XmAddWMProtocolCallback(top_level_shell, wm_delete, top_level_shell_close_callback, NULL);
    XmActivateWMProtocol(top_level_shell, wm_delete);
}

void create_clear_draw_button_widget(Widget parent, void (*callback_func)(Widget, XtPointer, XtPointer)) {

    if (parent != NULL) {
        clear_button = XtVaCreateManagedWidget("Clear",
                                               xmPushButtonWidgetClass, parent,
                                               NULL);

        XtAddCallback(clear_button, XmNactivateCallback,
                      callback_func,
                      draw_area);
    }
}

void create_draw_widget(Widget parent, void (*draw_callback)(Widget, XtPointer, XtPointer),
                        void (*expose_callback)(Widget, XtPointer, XtPointer),
                        void (*motion_callback)(Widget, XtPointer, XEvent *, Boolean *)) {

    if (parent != NULL) {
        draw_area = XtVaCreateManagedWidget("Draw Area",
                                            xmDrawingAreaWidgetClass,
                                            parent,
                                            XmNwidth, XWidthOfScreen(screen),
                                            XmNheight, XHeightOfScreen(screen),
                                            XmNbackground, convert_color_name_to_pixel(top_level_shell, "White"),
                                            NULL);

        if (draw_area != NULL) {

            set_color_defaults(draw_area);
            set_shape_types_defaults();
            set_line_type_defaults();
            set_line_width_defaults();
            XtAddEventHandler(draw_area, ButtonMotionMask, False, motion_callback, NULL);
            XtAddCallback(draw_area, XmNinputCallback, draw_callback, NULL);
            XtAddCallback(draw_area, XmNexposeCallback, expose_callback, NULL);
        }
    }
}

void set_line_width_defaults() {
    current_line_width = default_line_width;
}

void set_line_type_defaults() {
    current_line_type = default_line_type;
}

void set_shape_types_defaults() {
    current_shape_type = default_shape_type;
}

void set_color_defaults(Widget widget) {

    //    current_line_foreground_color = convert_color_name_to_pixel(widget, "Black");
    //    current_line_background_color = convert_color_name_to_pixel(widget, "White");
    //    current_fill_foreground_color = convert_color_name_to_pixel(widget, "Black");
    //    current_fill_background_color = convert_color_name_to_pixel(widget, "White");

    current_line_foreground_color = convert_color_name_to_pixel(widget,
                                                                supported_color_names[DEFAULT_FILL_FG_COLOR]);
    current_line_background_color = convert_color_name_to_pixel(widget,
                                                                supported_color_names[DEFAULT_LINE_BG_COLOR]);
    current_fill_foreground_color = convert_color_name_to_pixel(widget,
                                                                supported_color_names[DEFAULT_FILL_FG_COLOR]);
    current_fill_background_color = convert_color_name_to_pixel(widget,
                                                                supported_color_names[DEFAULT_FILL_BG_COLOR]);
}

void draw_area_widget_motion_callback(Widget widget, XtPointer client_data, XEvent *event, Boolean *cont) {

    Pixel draw_foreground, draw_background;
    Pixel fg, bg;

    static GC gc = 0;

    if (input_button_state != MOUSE_BUTTON_RELEASED) {

        if (!gc) {
            //gc = reinitialize_gc(widget, &draw_foreground, &draw_background);

            gc = XCreateGC(XtDisplay(widget), XtWindow(widget), 0, NULL);

            XtVaGetValues(widget, XmNforeground, &fg, XmNbackground, &bg, NULL);
            XSetForeground(XtDisplay(widget), gc, fg ^ bg);
            XSetFunction(XtDisplay(widget), gc, GXxor);
            XSetPlaneMask(XtDisplay(widget), gc, ~0);
        }

        if (input_button_state == MOUSE_BUTTON_PRESSED_AND_MOVE) {
            draw_shape(widget, gc, current_shape);
        } else {
            input_button_state = MOUSE_BUTTON_PRESSED_AND_MOVE;
        }

        current_shape.x2 = event->xmotion.x;
        current_shape.y2 = event->xmotion.y;

        draw_shape(widget, gc, current_shape);
    }
}

//GC reinitialize_gc(Widget *widget, Pixel *draw_foreground, Pixel *draw_background) {
//
//    GC gc;
//
//    gc = XCreateGC(XtDisplay(widget), XtWindow(widget), 0, NULL);
//
//    XtVaGetValues(widget, XmNforeground, draw_foreground, XmNbackground, draw_background, NULL);
//    XSetForeground(XtDisplay(widget), gc, (*draw_foreground) ^ (*draw_background));
//    XSetFunction(XtDisplay(widget), gc, GXxor);
//    XSetPlaneMask(XtDisplay(widget), gc, -0);
//    return gc;
//}

void draw_area_widget_expose_callback(Widget widget, XtPointer client_data, XtPointer call_data) {

    static GC gc = 0;
    int i;

    XmDrawingAreaCallbackStruct *expose_callback =
            (XmDrawingAreaCallbackStruct *) call_data;

    if (shapes_count > 0) {

        if (!gc) {
            gc = XCreateGC(XtDisplay(widget), XtWindow(widget), 0, NULL);
        }

        for (i = 0; i < shapes_count; i++) {
            draw_shape(widget, gc, shapes_list[i]);
        }
    }
}

void draw_area_widget_draw_callback(Widget widget, XtPointer client_data, XtPointer call_data) {

    XmDrawingAreaCallbackStruct *draw_callback = (XmDrawingAreaCallbackStruct *) call_data;

    static Position x, y;

    if (draw_callback->reason == XmCR_INPUT) {
        switch (draw_callback->event->type) {
            case ButtonPress:

                x = draw_callback->event->xbutton.x;
                y = draw_callback->event->xbutton.y;

                current_shape.line_foreground_color =
                        current_line_foreground_color ^ convert_color_name_to_pixel(widget, "White");
                current_shape.line_background_color =
                        current_line_background_color ^ convert_color_name_to_pixel(widget, "White");
                current_shape.fill_foreground_color =
                        current_fill_foreground_color ^ convert_color_name_to_pixel(widget, "Black");
                current_shape.fill_background_color =
                        current_fill_background_color ^ convert_color_name_to_pixel(widget, "White");

                current_shape.x1 = x;
                current_shape.y1 = y;

                current_shape.shape_type = current_shape_type;

                current_shape.line_type = current_line_type;
                current_shape.line_width = current_line_width;

                input_button_state = MOUSE_BUTTON_PRESSED;

                break;
            case ButtonRelease:

                if (shapes_count >= shapes_allocated) {
                    shapes_allocated = shapes_allocated ? shapes_allocated << 1 : 1;
                    shapes_list = (Shape *) realloc(shapes_list, shapes_allocated * sizeof(Shape));
                }

                x = draw_callback->event->xbutton.x;
                y = draw_callback->event->xbutton.y;

                shapes_list[shapes_count].line_foreground_color =
                        current_shape.line_foreground_color ^ convert_color_name_to_pixel(widget, "White");
                shapes_list[shapes_count].line_background_color =
                        current_shape.line_background_color ^ convert_color_name_to_pixel(widget, "White");
                shapes_list[shapes_count].fill_foreground_color =
                        current_shape.fill_foreground_color ^ convert_color_name_to_pixel(widget, "Black");
                shapes_list[shapes_count].fill_background_color =
                        current_shape.fill_background_color ^ convert_color_name_to_pixel(widget, "White");

                shapes_list[shapes_count].x2 = x;
                shapes_list[shapes_count].y2 = y;
                shapes_list[shapes_count].x1 = current_shape.x1;
                shapes_list[shapes_count].y1 = current_shape.y1;

                shapes_list[shapes_count].line_type = current_shape.line_type;
                shapes_list[shapes_count].line_width = current_shape.line_width;
                shapes_list[shapes_count].shape_type = current_shape.shape_type;

                input_button_state = MOUSE_BUTTON_RELEASED;

                XClearArea(XtDisplay(widget), XtWindow(widget), 0, 0, 0, 0, True);

                shapes_count++;

                break;
            default:
                break;
        }
    }
}

void draw_shape(struct _WidgetRec *widget, GC gc, Shape shape) {

    XGCValues values;
    values.line_style = shape.line_type;
    values.line_width = shape.line_width;
    values.foreground = shape.line_foreground_color;
    values.background = shape.line_background_color;

    XChangeGC(XtDisplay(widget), gc, GCLineStyle | GCLineWidth | GCForeground | GCBackground, &values);

    switch (shape.shape_type) {

        case POINT:
            if (shape.line_width > 0)
                XFillArc(XtDisplay(widget), XtWindow(widget), gc, (int) (shape.x2 - shape.line_width / 2.0),
                         (int) (shape.y2 - shape.line_width / 2.0), shape.line_width, shape.line_width, 0, 360 * 64);
            else
                XDrawPoint(XtDisplay(widget), XtWindow(widget), gc, shape.x2, shape.y2);
            break;
        case LINE:
            XDrawLine(XtDisplay(widget), XtWindow(widget), gc, shape.x1, shape.y1, shape.x2, shape.y2);
            break;
        case RECTANGLE: {
            int left_x1 = shape.x1 < shape.x2 ? shape.x1 : shape.x2;
            int left_y1 = shape.y1 < shape.y2 ? shape.y1 : shape.y2;
            XDrawRectangle(XtDisplay(widget), XtWindow(widget), gc, left_x1,
                           left_y1, abs(shape.x2 - shape.x1), abs(shape.y2 - shape.y1));
        }
            break;
        case FILLED_RECTANGLE: {
            int left_x1 = shape.x1 < shape.x2 ? shape.x1 : shape.x2;
            int left_y1 = shape.y1 < shape.y2 ? shape.y1 : shape.y2;
            XSetForeground(XtDisplay(widget), gc, shape.fill_foreground_color);
            XDrawRectangle(XtDisplay(widget), XtWindow(widget), gc, left_x1,
                           left_y1, abs(shape.x2 - shape.x1), abs(shape.y2 - shape.y1));
            XSetForeground(XtDisplay(widget), gc, shape.line_foreground_color);
            XDrawRectangle(XtDisplay(widget), XtWindow(widget), gc, left_x1,
                           left_y1, abs(shape.x2 - shape.x1), abs(shape.y2 - shape.y1));
        }
            break;
        case ELLIPSE:
            XDrawArc(XtDisplay(widget), XtWindow(widget), gc,
                     shape.x1 - abs(shape.x2 - shape.x1),
                     shape.y1 - abs(shape.y2 - shape.y1),
                     abs(shape.x2 - shape.x1) * 2,
                     abs(shape.y2 - shape.y1) * 2, 0, 360 * 64);
            break;
        case FILLED_ELLIPSE:
            XSetForeground(XtDisplay(widget), gc, shape.fill_foreground_color);
            XDrawArc(XtDisplay(widget), XtWindow(widget), gc,
                     shape.x1 - abs(shape.x2 - shape.x1),
                     shape.y1 - abs(shape.y2 - shape.y1),
                     abs(shape.x2 - shape.x1) * 2,
                     abs(shape.y2 - shape.y1) * 2, 0, 360 * 64);
            XSetForeground(XtDisplay(widget), gc, shape.line_foreground_color);
            XDrawArc(XtDisplay(widget), XtWindow(widget), gc,
                     shape.x1 - abs(shape.x2 - shape.x1),
                     shape.y1 - abs(shape.y2 - shape.y1),
                     abs(shape.x2 - shape.x1) * 2,
                     abs(shape.y2 - shape.y1) * 2, 0, 360 * 64);
            break;
        default:
            break;
    }
}

void clear_draw_callback(Widget widget, XtPointer client_data, XtPointer call_data) {

    clear_shapes_list();
    Widget draw_area_widget = (Widget) client_data;
    XClearArea(XtDisplay(draw_area_widget), XtWindow(draw_area_widget), 0, 0, 0, 0, True);
}

void clear_shapes_list() {

    if (shapes_list != NULL) {
        free(shapes_list);
        shapes_allocated = 0;
        shapes_count = 0;
        shapes_list = NULL;
    }
}

void xt_manage_all_child_widgets() {

    if (draw_tools_menu.line_width_select_scale_widget != NULL)
        XtManageChild(draw_tools_menu.line_width_select_scale_widget);
    if (draw_tools_menu.line_width_label != NULL) XtManageChild(draw_tools_menu.line_width_label);

    if (draw_tools_menu.shape_radio_box != NULL) XtManageChild(draw_tools_menu.shape_radio_box);
    if (draw_tools_menu.shape_label != NULL) XtManageChild(draw_tools_menu.shape_label);

    if (draw_tools_menu.Line_foreground_color_label != NULL) XtManageChild(draw_tools_menu.Line_foreground_color_label);
    if (draw_tools_menu.line_foreground_color_combo_box != NULL)
        XtManageChild(draw_tools_menu.line_foreground_color_combo_box);

    if (draw_tools_menu.line_background_color_label != NULL) XtManageChild(draw_tools_menu.line_background_color_label);
    if (draw_tools_menu.line_background_color_combo_box != NULL)
        XtManageChild(draw_tools_menu.line_background_color_combo_box);

    if (draw_tools_menu.fill_foreground_color_label != NULL) XtManageChild(draw_tools_menu.Line_foreground_color_label);
    if (draw_tools_menu.fill_foreground_color_combo_box != NULL)
        XtManageChild(draw_tools_menu.line_foreground_color_combo_box);

    if (draw_tools_menu.fill_background_color_label != NULL) XtManageChild(draw_tools_menu.Line_foreground_color_label);
    if (draw_tools_menu.fill_background_color_combo_box != NULL)
        XtManageChild(draw_tools_menu.line_foreground_color_combo_box);

    if (draw_tools_menu.line_type_label != NULL) XtManageChild(draw_tools_menu.line_type_label);
    if (draw_tools_menu.line_type_combo_box != NULL) XtManageChild(draw_tools_menu.line_type_combo_box);
}

void create_line_width_selector(Widget parent, void (*callback_func)(Widget, XtPointer, XtPointer)) {

    if (parent != NULL) {

        XmString lineWidthLabelString = XmStringCreateSimple("Line width:");
        draw_tools_menu.line_width_label = XtVaCreateManagedWidget(
                "Line Width Label",
                xmLabelWidgetClass,
                parent,
                XmNlabelString, lineWidthLabelString,
                NULL);
        XmStringFree(lineWidthLabelString);

        draw_tools_menu.line_width_select_scale_widget = XtVaCreateManagedWidget("Line Width Selector Widget",
                                                                                 xmScaleWidgetClass,
                                                                                 parent,
                                                                                 XmNvalue, 1,
                                                                                 XmNhighlightOnEnter, True,
                                                                                 XmNminimum, 0,
                                                                                 XmNmaximum, 9,
                                                                                 XmNshowValue, True,
                                                                                 XmNorientation, XmHORIZONTAL,
                                                                                 NULL);

        XtAddCallback(draw_tools_menu.line_width_select_scale_widget, XmNdragCallback, callback_func, NULL);
        XtAddCallback(draw_tools_menu.line_width_select_scale_widget, XmNvalueChangedCallback, callback_func, NULL);
    }
}

void create_line_foreground_color_selector(Widget parent, void (*callback_func)(Widget, XtPointer, XtPointer)) {

    if (parent != NULL) {

        XmString foregroundLabelString = XmStringCreateSimple("Line foreground color:");
        draw_tools_menu.Line_foreground_color_label = XtVaCreateManagedWidget(
                "Line Foreground Color Label",
                xmLabelWidgetClass,
                parent,
                XmNlabelString, foregroundLabelString,
                NULL);
        XmStringFree(foregroundLabelString);

        int i;
        XmString colors_combobox_items_strings[SUPPORTED_COLORS_NUMBER];

        for (i = 0; i < SUPPORTED_COLORS_NUMBER; i++) {
            colors_combobox_items_strings[i] = XmStringCreateSimple(supported_color_names[i]);
        }

        draw_tools_menu.line_foreground_color_combo_box = XtVaCreateManagedWidget(
                "Line Foreground Color ComboBox",
                xmComboBoxWidgetClass,
                parent,
                XmNcomboBoxType, XmDROP_DOWN_LIST,
                XmNitemCount, SUPPORTED_COLORS_NUMBER,
                XmNitems, colors_combobox_items_strings,
                XmNselectedPosition, 0,
                NULL);
        XtAddCallback(draw_tools_menu.line_foreground_color_combo_box, XmNselectionCallback, callback_func,
                      (XtPointer) LINE_FOREGROUND_COLOR_TYPE);

        for (i = 0; i < SUPPORTED_COLORS_NUMBER; i++) {
            XmStringFree(colors_combobox_items_strings[i]);
        }
    }
}

void create_fill_foreground_color_selector(Widget parent, void (*callback_func)(Widget, XtPointer, XtPointer)) {

    if (parent != NULL) {

        XmString foregroundLabelString = XmStringCreateSimple("Fill Foreground color:");
        draw_tools_menu.fill_foreground_color_label = XtVaCreateManagedWidget(
                "Fill Foreground Color Label",
                xmLabelWidgetClass,
                parent,
                XmNlabelString, foregroundLabelString,
                NULL);
        XmStringFree(foregroundLabelString);

        int i;
        XmString colors_combobox_items_strings[SUPPORTED_COLORS_NUMBER];

        for (i = 0; i < SUPPORTED_COLORS_NUMBER; i++) {
            colors_combobox_items_strings[i] = XmStringCreateSimple(supported_color_names[i]);
        }

        draw_tools_menu.fill_foreground_color_combo_box = XtVaCreateManagedWidget(
                "Fill Foreground Color ComboBox",
                xmComboBoxWidgetClass,
                parent,
                XmNcomboBoxType, XmDROP_DOWN_LIST,
                XmNitemCount, SUPPORTED_COLORS_NUMBER,
                XmNitems, colors_combobox_items_strings,
                XmNselectedPosition, 0,
                NULL);
        XtAddCallback(draw_tools_menu.fill_foreground_color_combo_box, XmNselectionCallback, callback_func,
                      (XtPointer) FILL_FOREGROUND_COLOR_TYPE);

        for (i = 0; i < SUPPORTED_COLORS_NUMBER; i++) {
            XmStringFree(colors_combobox_items_strings[i]);
        }
    }
}

void create_line_background_color_selector(Widget parent, void (*callback_func)(Widget, XtPointer, XtPointer)) {

    if (parent != NULL) {

        XmString backgroundLabelString = XmStringCreateSimple("Line Background color:");
        draw_tools_menu.line_background_color_label = XtVaCreateManagedWidget(
                "Line Background Color Label",
                xmLabelWidgetClass,
                parent,
                XmNlabelString, backgroundLabelString,
                NULL);

        XmStringFree(backgroundLabelString);

        int i;
        XmString colors_combobox_items_strings[SUPPORTED_COLORS_NUMBER];

        for (i = 0; i < SUPPORTED_COLORS_NUMBER; i++) {
            colors_combobox_items_strings[i] = XmStringCreateSimple(supported_color_names[i]);
        }

        draw_tools_menu.line_background_color_combo_box = XtVaCreateManagedWidget(
                "Line Background Color ComboBox",
                xmComboBoxWidgetClass,
                parent,
                XmNcomboBoxType, XmDROP_DOWN_LIST,
                XmNitemCount, SUPPORTED_COLORS_NUMBER,
                XmNitems, colors_combobox_items_strings,
                XmNselectedPosition, 1,
                NULL);
        XtAddCallback(draw_tools_menu.line_background_color_combo_box, XmNselectionCallback, callback_func,
                      (XtPointer) LINE_BACKROUND_COLOR_TYPE);

        for (i = 0; i < SUPPORTED_COLORS_NUMBER; i++) {
            XmStringFree(colors_combobox_items_strings[i]);
        }
    }
}

void create_fill_background_color_selector(Widget parent, void (*callback_func)(Widget, XtPointer, XtPointer)) {

    if (parent != NULL) {

        XmString backgroundLabelString = XmStringCreateSimple("Fill Background color:");
        draw_tools_menu.fill_background_color_label = XtVaCreateManagedWidget(
                "Fill Background Color Label",
                xmLabelWidgetClass,
                parent,
                XmNlabelString, backgroundLabelString,
                NULL);

        XmStringFree(backgroundLabelString);

        int i;
        XmString colors_combobox_items_strings[SUPPORTED_COLORS_NUMBER];

        for (i = 0; i < SUPPORTED_COLORS_NUMBER; i++) {
            colors_combobox_items_strings[i] = XmStringCreateSimple(supported_color_names[i]);
        }

        draw_tools_menu.fill_background_color_combo_box = XtVaCreateManagedWidget(
                "Fill Background Color ComboBox",
                xmComboBoxWidgetClass,
                parent,
                XmNcomboBoxType, XmDROP_DOWN_LIST,
                XmNitemCount, SUPPORTED_COLORS_NUMBER,
                XmNselectedPosition, 1,
                XmNitems, colors_combobox_items_strings,
                NULL);
        XtAddCallback(draw_tools_menu.fill_background_color_combo_box, XmNselectionCallback, callback_func,
                      (XtPointer) FILL_BACKGROUND_COLOR_TYPE);

        for (i = 0; i < SUPPORTED_COLORS_NUMBER; i++) {
            XmStringFree(colors_combobox_items_strings[i]);
        }
    }
}

void create_shape_selector(Widget parent, void (*callback_func)(Widget, XtPointer, XtPointer)) {

    if (parent != NULL) {
        XmString point = XmStringCreateSimple("Point");
        XmString line = XmStringCreateSimple("Line");
        XmString rectangle = XmStringCreateSimple("Rectangle");
        XmString filledRectangle = XmStringCreateSimple("Filled rectangle");
        XmString ellipse = XmStringCreateSimple("Ellipse");
        XmString filledEllipse = XmStringCreateSimple("Filled ellipse");

        draw_tools_menu.shape_label = XtVaCreateManagedWidget("Select draw instrument:", xmLabelWidgetClass,
                                                              parent, NULL);

        draw_tools_menu.shape_radio_box = XmVaCreateSimpleRadioBox(
                tools_row_column_container,
                "Shape Select ComboBox",
                LINE,
                callback_func,
                XmVaRADIOBUTTON, point, NULL, NULL, NULL,
                XmVaRADIOBUTTON, line, NULL, NULL, NULL,
                XmVaRADIOBUTTON, rectangle, NULL, NULL, NULL,
                XmVaRADIOBUTTON, filledRectangle, NULL, NULL, NULL,
                XmVaRADIOBUTTON, ellipse, NULL, NULL, NULL,
                XmVaRADIOBUTTON, filledEllipse, NULL, NULL, NULL,
                NULL);

        XmStringFree(point);
        XmStringFree(line);
        XmStringFree(rectangle);
        XmStringFree(filledRectangle);
        XmStringFree(ellipse);
        XmStringFree(filledEllipse);
    }
}

void create_line_type_selector(Widget parent, void (*callback_func)(Widget, XtPointer, XtPointer)) {

    if (parent != NULL) {
        XmString solid_line = XmStringCreateSimple("Solid");
        XmString double_dashed_line = XmStringCreateSimple("Double dashed");


        draw_tools_menu.line_type_label = XtVaCreateManagedWidget("Select line type:", xmLabelWidgetClass,
                                                                  parent, NULL);

        draw_tools_menu.line_type_combo_box = XmVaCreateSimpleRadioBox(
                tools_row_column_container,
                "Shape Line Type ComboBox",
                LINE,
                callback_func,
                XmVaRADIOBUTTON, solid_line, NULL, NULL, NULL,
                XmVaRADIOBUTTON, double_dashed_line, NULL, NULL, NULL,
                NULL);

        XmStringFree(solid_line);
        XmStringFree(double_dashed_line);
    }
}


void line_width_change_callback(Widget widget, XtPointer client_data, XtPointer call_data) {

    XmScaleCallbackStruct *width_select_callback = (XmScaleCallbackStruct *) call_data;
    current_line_width = width_select_callback->value;
}


void color_selected_callback(Widget widget, XtPointer client_data, XtPointer call_data) {

//    int colorButton;
//    colorButton = (intptr_t) client_data;
//    XmComboBoxCallbackStruct *cbs = (XmComboBoxCallbackStruct *) call_data;
////    if (colorButton == FOREGROUND) {
////        foregroundColorSelected = cbs->item_position;
//////        current_fill_foreground_color = cbs->item_position;
//////        current_line_foreground_color = cbs->item_position;
////    } else {
//////        current_line_background_color = cbs->item_position;
//////        current_fill_background_color = cbs->item_position;
////        backgroundColorSelected = cbs->item_position;
//    }

    XmComboBoxCallbackStruct *data = (XmComboBoxCallbackStruct *) call_data;
    intptr_t color_type = (intptr_t) client_data;
    int selected_color = data->item_position;

    fprintf(stdout, "\nindex %d, color %s", selected_color,
            supported_color_names[selected_color]);

    switch (color_type) {
        case LINE_FOREGROUND_COLOR_TYPE:
            current_line_foreground_color = convert_color_name_to_pixel(
                    widget, supported_color_names[selected_color]);
            break;
        case LINE_BACKROUND_COLOR_TYPE:
            current_line_background_color = convert_color_name_to_pixel(
                    widget, supported_color_names[selected_color]);
            break;
        case FILL_FOREGROUND_COLOR_TYPE:
            current_fill_foreground_color = convert_color_name_to_pixel(
                    widget, supported_color_names[selected_color]);
            break;
        case FILL_BACKGROUND_COLOR_TYPE:
            current_fill_background_color = convert_color_name_to_pixel(
                    widget, supported_color_names[selected_color]);
            break;
        default:
            break;
    }
}

void line_type_change_callback(Widget widget, XtPointer client_data, XtPointer call_data) {

    XmToggleButtonCallbackStruct *type_select_callback = (XmToggleButtonCallbackStruct *) call_data;

    if (type_select_callback->set) {
        intptr_t selected_line_type = (intptr_t) client_data;

        switch (selected_line_type) {

            case DOUBLE_DASHED_LINE:
                current_line_type = DOUBLE_DASHED_LINE;
                break;
            case SOLID_LINE:
                current_line_type = SOLID_LINE;
                break;
            default:
                current_line_type = SOLID_LINE;
                break;
        }
    }
}

void shape_type_change_callback(Widget widget, XtPointer client_data, XtPointer call_data) {

    XmToggleButtonCallbackStruct *type_select_callback = (XmToggleButtonCallbackStruct *) call_data;

    if (type_select_callback->set) {
        intptr_t selected_shape_type = (intptr_t) client_data;

        switch (selected_shape_type) {
            case POINT:
                current_shape_type = POINT;
                break;
            case LINE:
                current_shape_type = LINE;
                break;
            case RECTANGLE:
                current_shape_type = RECTANGLE;
                break;
            case FILLED_RECTANGLE:
                current_shape_type = FILLED_RECTANGLE;
                break;
            case ELLIPSE:
                current_shape_type = ELLIPSE;
                break;
            case FILLED_ELLIPSE:
                current_shape_type = FILLED_ELLIPSE;
                break;
            default:
                current_shape_type = LINE;
                break;
        }
    }
}

int main(int argc, char **argv) {

    XtSetLanguageProc(NULL, NULL, NULL);

    create_top_level_shell_widget(&argc, argv);
    create_main_window_widget(top_level_shell);
    create_frame_tools_container_widget(main_window_widget);
    create_tools_rowcolumn_create_widget(frame_container);
    create_scrolled_window_container(frame_container);
    create_draw_widget(scrolled_window, draw_area_widget_draw_callback, draw_area_widget_expose_callback,
                       draw_area_widget_motion_callback);

    create_shape_selector(tools_row_column_container, shape_type_change_callback);
    create_line_type_selector(tools_row_column_container, line_type_change_callback);
    create_line_width_selector(tools_row_column_container, line_width_change_callback);

    create_line_foreground_color_selector(tools_row_column_container, color_selected_callback);
    create_line_background_color_selector(tools_row_column_container, color_selected_callback);
    create_fill_foreground_color_selector(tools_row_column_container, color_selected_callback);
    create_fill_background_color_selector(tools_row_column_container, color_selected_callback);

    create_clear_draw_button_widget(tools_row_column_container, clear_draw_callback);

    xt_manage_all_child_widgets();

    XtRealizeWidget(top_level_shell);

    XtAppMainLoop(app_context);

    return 0;
}
