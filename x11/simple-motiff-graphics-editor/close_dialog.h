#ifndef QUIT_DIALOG_H
#define QUIT_DIALOG_H

#define TRUE 1
#define FALSE 0

#include <stdio.h>
#include <stdlib.h>

#include "shared_globals.h"

#include <Xm/Xm.h>

void create_close_dialog(Widget parent);

void close_dialog_buttons_callback(Widget widget, XtPointer client_data, XtPointer call_data);

void switch_close_dialog_visibility_state(int state);

#endif