## Rozhraní pro slovník (GUX 2. semestr project 2016)


Simple dictionary GTK2 application. You can view multiple translations for given word in one text view. The "main"
translation will be marked by **bold** font.


Application uses DDF file - "Dumb Dictionary File" - format :). Actually it's just a well-known sqlite database with
scheme that you can check in `schemes/dictionary` folder.


A sample English - Czech (one way) dictionary file provided in this repo (`samples/sample_en-cz.ddf`).


App require **gtk2-dev**, **sqlite3-dev**, **pango-1.0** package installed. It was build and tested under Debian 8, Cent OS (7 ?) and FreeBSD 10,
Obviously you must have `gcc` and `make` for building an application. Just run `make` in root repository folder.
