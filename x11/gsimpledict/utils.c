#define _GNU_SOURCE

#include <stdio.h>
#include <ctype.h>

char *strconcat(char *str1, char *str2) {

    char *new_str;
    asprintf(&new_str, "%s%s", str1, str2);

    return new_str;
}
