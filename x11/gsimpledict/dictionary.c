
#include <stdio.h>
#include <sqlite3.h>
#include <string.h>
#include <glib.h>
#include <stdlib.h>

#include "dictionary.h"


/**
 * Helper function for conversion from enum type instance to string representation;
 *
 * @param table enum type
 * @return string representation - table name
 */
static const inline char *get_string_table_name(enum DICTIONARY_DB_TABLES table) {

    static const char *strings[] = {"dictionary", "dictionary_author", "dictionary_metadata"};
    return strings[table];
}


DictionaryOpenResult open_dictionary_file(char *dictFilePath) {

    DictionaryOpenResult dictionaryOpenResult;
    dictionaryOpenResult.succeeded = 1;
    dictionaryOpenResult.status = OK;

    if (dictFilePath != NULL && (dictFilePath[0] != '\0')) {
        int rc = sqlite3_open(dictFilePath, &currentDictionaryDB);
        if (rc) {
            fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(currentDictionaryDB));
            dictionaryOpenResult.succeeded = 0;
            dictionaryOpenResult.status = FILE_OPEN_ERROR;
        } else {
            currentDistionaryDBFilePath = dictFilePath;
            char *tableQuerySqlString = "SELECT count(*) FROM sqlite_master WHERE type='table' AND name = ?";
            sqlite3_stmt *stmt;
            const char *pzTail;

            rc = sqlite3_prepare(currentDictionaryDB, tableQuerySqlString, (int) strlen(tableQuerySqlString), &stmt,
                                 &pzTail);
            if (rc == SQLITE_OK) {
                sqlite3_bind_text(stmt, 1, get_string_table_name(DICTIONARY),
                                  (int) strlen(get_string_table_name(DICTIONARY)), 0);
                while ((sqlite3_step(stmt)) == SQLITE_ROW) {
                    if (sqlite3_column_int(stmt, 0) < 1) {
                        dictionaryOpenResult.succeeded = 0;
                        dictionaryOpenResult.status = FILE_FORMAT_ERROR;
                        break;
                    }
                }
            }

            sqlite3_finalize(stmt);
            rc = sqlite3_prepare(currentDictionaryDB, tableQuerySqlString, (int) strlen(tableQuerySqlString), &stmt,
                                 &pzTail);
            if (rc == SQLITE_OK) {
                sqlite3_bind_text(stmt, 1, get_string_table_name(DICTIONARY_METADATA),
                                  (int) strlen(get_string_table_name(DICTIONARY_METADATA)), 0);

                while ((sqlite3_step(stmt)) == SQLITE_ROW) {
                    if (sqlite3_column_int(stmt, 0) < 1) {
                        dictionaryOpenResult.succeeded = 0;
                        dictionaryOpenResult.status = FILE_FORMAT_ERROR;
                        break;
                    }
                }
            }

            sqlite3_finalize(stmt);
            rc = sqlite3_prepare(currentDictionaryDB, tableQuerySqlString, (int) strlen(tableQuerySqlString), &stmt,
                                 &pzTail);
            if (rc == SQLITE_OK) {
                sqlite3_bind_text(stmt, 1, get_string_table_name(DICTIONARY_AUTHOR),
                                  (int) strlen(get_string_table_name(DICTIONARY_AUTHOR)), 0);

                while ((sqlite3_step(stmt)) == SQLITE_ROW) {
                    if (sqlite3_column_int(stmt, 0) < 1) {
                        dictionaryOpenResult.succeeded = 0;
                        dictionaryOpenResult.status = FILE_FORMAT_ERROR;
                        break;
                    }
                }
            }

            sqlite3_finalize(stmt);
        }
    } else {
        dictionaryOpenResult.succeeded = 0;
        dictionaryOpenResult.status = FILE_OPEN_ERROR;
    }
    return dictionaryOpenResult;
}


int close_dictionary_file() {

    if (currentDictionaryDB != NULL) {
        sqlite3_close(currentDictionaryDB);
        return 1;
    } else {
        return 0;
    }
}

GSList *get_all_words_list() {

    GSList *words = NULL;
    int rc = sqlite3_open(currentDistionaryDBFilePath, &currentDictionaryDB);
    if (rc) {
        fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(currentDictionaryDB));
    } else {
        char *tableQuerySqlString = "SELECT word FROM dictionary ORDER BY word ASC";
        sqlite3_stmt *stmt;
        const char *pzTail;

        rc = sqlite3_prepare(currentDictionaryDB, tableQuerySqlString, (int) strlen(tableQuerySqlString), &stmt,
                             &pzTail);
        if (rc == SQLITE_OK) {
            while ((sqlite3_step(stmt)) == SQLITE_ROW) {
                const unsigned char *currentWord = sqlite3_column_text(stmt, 0);
                words = g_slist_append(words, g_strdup((const gchar *) currentWord));
            }
        }
    }
    return words;
}


GSList *get_all_translations_for_word(char *word) {

    GSList *translations = NULL;
    char *tableQuerySqlString = "SELECT main_translation, full_translation FROM dictionary WHERE word = ?";
    sqlite3_stmt *stmt;
    const char *pzTail;

    int rc = sqlite3_prepare(currentDictionaryDB, tableQuerySqlString, (int) strlen(tableQuerySqlString), &stmt,
                             &pzTail);
    if (rc == SQLITE_OK) {
        sqlite3_bind_text(stmt, 1, word, (int) strlen(word), 0);
        while ((sqlite3_step(stmt)) == SQLITE_ROW) {
            const unsigned char *mainTranslation = sqlite3_column_text(stmt, 0);
            const unsigned char *fullTranslation = sqlite3_column_text(stmt, 1);
            translations = g_slist_append(translations, g_strdup((const gchar *) mainTranslation));
            translations = g_slist_append(translations, g_strdup((const gchar *) fullTranslation));
            break;
        }
    }
    return translations;
}

GSList *get_all_words_starting_with(char *startStr) {

    GSList *words = NULL;
    char *tableQuerySqlString = "SELECT word FROM dictionary WHERE word like ? ORDER BY word ASC";
    sqlite3_stmt *stmt;
    const char *pzTail;

    int rc = sqlite3_prepare(currentDictionaryDB, tableQuerySqlString, (int) strlen(tableQuerySqlString), &stmt,
                             &pzTail);
    if (rc == SQLITE_OK) {
        sqlite3_bind_text(stmt, 1, startStr, (int) strlen(startStr), 0);
        while ((sqlite3_step(stmt)) == SQLITE_ROW) {
            const unsigned char *currentWord = sqlite3_column_text(stmt, 0);
            words = g_slist_append(words, g_strdup((const gchar *) currentWord));
        }
    }
    return words;
}

void get_source_lang_code(char **src_lang) {

    *src_lang = malloc(16);
    char *tableQuerySqlString = "SELECT source_lang_code FROM dictionary_metadata";
    sqlite3_stmt *stmt;
    const char *pzTail;
    int rc = sqlite3_prepare(currentDictionaryDB, tableQuerySqlString, (int) strlen(tableQuerySqlString), &stmt,
                             &pzTail);
    if (rc == SQLITE_OK) {
        while ((sqlite3_step(stmt)) == SQLITE_ROW) {
            strcpy(*src_lang, (const char *) sqlite3_column_text(stmt, 0));
        }
    }

}

void get_dest_lang_code(char **dest_lang) {

    *dest_lang = malloc(16);
    char *tableQuerySqlString = "SELECT dest_lang_code FROM dictionary_metadata";
    sqlite3_stmt *stmt;
    const char *pzTail;
    int rc = sqlite3_prepare(currentDictionaryDB, tableQuerySqlString, (int) strlen(tableQuerySqlString), &stmt,
                             &pzTail);
    if (rc == SQLITE_OK) {
        while ((sqlite3_step(stmt)) == SQLITE_ROW) {
            strcpy(*dest_lang, (const char *) sqlite3_column_text(stmt, 0));
        }
    }
}

void get_dictionary_description(char **desc) {

    *desc = malloc(2048);
    char *tableQuerySqlString = "SELECT description FROM dictionary_metadata";
    sqlite3_stmt *stmt;
    const char *pzTail;
    int rc = sqlite3_prepare(currentDictionaryDB, tableQuerySqlString, (int) strlen(tableQuerySqlString), &stmt,
                             &pzTail);
    if (rc == SQLITE_OK) {
        while ((sqlite3_step(stmt)) == SQLITE_ROW) {
            strcpy(*desc, (const char *) sqlite3_column_text(stmt, 0));
        }
    }
}

int get_words_count() {

    int count = 0;
    char *tableQuerySqlString = "SELECT count(*) FROM dictionary";
    sqlite3_stmt *stmt;
    const char *pzTail;

    int rc = sqlite3_prepare(currentDictionaryDB, tableQuerySqlString, (int) strlen(tableQuerySqlString), &stmt,
                             &pzTail);
    if (rc == SQLITE_OK) {
        while ((sqlite3_step(stmt)) == SQLITE_ROW) {
            count = sqlite3_column_int(stmt, 0);

        }
    }
    return count;
}


void get_formatted_author_info_string(char **authorInfo) {

    *authorInfo = malloc(2048);
    char *tableQuerySqlString = "SELECT name || ' ' || '' || lastname || ' ' || email || ' - ' || company FROM dictionary_author";
    sqlite3_stmt *stmt;
    const char *pzTail;
    int rc = sqlite3_prepare(currentDictionaryDB, tableQuerySqlString, (int) strlen(tableQuerySqlString), &stmt,
                             &pzTail);
    if (rc == SQLITE_OK) {
        while ((sqlite3_step(stmt)) == SQLITE_ROW) {
            strcpy(*authorInfo, (const char *) sqlite3_column_text(stmt, 0));
        }
    }
}