#include <gtk/gtk.h>
#include <string.h>
#include <printf.h>

#include "gsimpledict.h"
#include "dictionary.h"
#include "utils.h"


int main(int argc, char *argv[]) {

    gtk_init(&argc, &argv);

    intitialize_widgets();

    gtk_widget_show_all(topLevelWindow);
    gtk_main();

    return (0);
}


void top_level_window_close_callback(GtkWidget *widget, gpointer data) {

    close_dictionary_file();
    dictionaryFileIsOpened = FALSE;
    gtk_main_quit();
}

void open_about_dialog(GtkWidget *widget, gpointer data) {

    char *authorInfoString;
    char *dictInfoString;

    GtkWidget *dialog = gtk_about_dialog_new();
    gtk_about_dialog_set_name(GTK_ABOUT_DIALOG(dialog), "GSimpleDict");
    gtk_about_dialog_set_version(GTK_ABOUT_DIALOG(dialog), "0.1");
    gtk_about_dialog_set_copyright(GTK_ABOUT_DIALOG(dialog), "Iurii Kuznetcov");

    if (dictionaryFileIsOpened) {

        char *fullDictAndAuthorInfoString = "";

        get_formatted_author_info_string(&authorInfoString);
        get_dictionary_description(&dictInfoString);

        fullDictAndAuthorInfoString = strconcat(dictInfoString, "\n");
        fullDictAndAuthorInfoString = strconcat(fullDictAndAuthorInfoString, authorInfoString);

        gtk_about_dialog_set_comments(GTK_ABOUT_DIALOG(dialog),
                                      fullDictAndAuthorInfoString);
    }
    gtk_about_dialog_set_website(GTK_ABOUT_DIALOG(dialog),
                                 "mailto://xkuzne00@stud.fit.vutbr.cz");
    gtk_dialog_run(GTK_DIALOG (dialog));
    gtk_widget_destroy(dialog);
}

DictionaryOpenResult open_file_open_dialog(GtkWidget *widget, gpointer data) {

    GtkWidget *dictFileOpenDialog;
    DictionaryOpenResult openResult;
    dictFileOpenDialog = gtk_file_chooser_dialog_new("Open File",
                                                     data,
                                                     GTK_FILE_CHOOSER_ACTION_OPEN,
                                                     GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
                                                     GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT,
                                                     NULL);
    gint dialogResponse = gtk_dialog_run(GTK_DIALOG(dictFileOpenDialog));
    if (dialogResponse == GTK_RESPONSE_ACCEPT) {
        close_dictionary_file();
        dictionaryFileIsOpened = FALSE;
        gchar *filename;
        filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(dictFileOpenDialog));
        openResult = open_dictionary_file(filename);
        if (openResult.succeeded) {
            dictionaryFileIsOpened = TRUE;
            gtk_widget_destroy(dictFileOpenDialog);
            GSList *wordsList = get_all_words_list();
            update_words_list_widgets_store(wordsList);
            update_status_label_text();
            g_slist_free(wordsList);
        } else {
            dictionaryFileIsOpened = FALSE;
            gtk_widget_destroy(dictFileOpenDialog);
            GtkWidget *errorDialog = gtk_message_dialog_new(data,
                                                            GTK_DIALOG_DESTROY_WITH_PARENT,
                                                            GTK_MESSAGE_ERROR,
                                                            GTK_BUTTONS_CLOSE,
                                                            "Error loading file '%s': %s",
                                                            filename, "Unaccessable or invalid dictionary file");
            close_dictionary_file();
            gtk_dialog_run(GTK_DIALOG(errorDialog));
            gtk_widget_destroy(errorDialog);
        }
        g_free(filename);
    } else if (dialogResponse == GTK_RESPONSE_CANCEL) {
        gtk_widget_destroy(dictFileOpenDialog);
    }
    return openResult;
}

void intitialize_widgets() {

    create_top_level_window();
    create_top_level_container();
    create_menubar();
    create_translation_view();
    create_find_word_view();
    create_words_list_view();
}


void update_translation_view_text(GSList *translations) {

    if (translationView.translationTextView != NULL) {

        char *mainTranslationSection;
        char *fullTranslationSection;

        char *concatenatedTranslationString;

        GtkTextBuffer *translationTextViewBuffer;
        GtkTextIter start, end;

        PangoFontDescription *font_desc;

        translationTextViewBuffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(translationView.translationTextView));

        mainTranslationSection = g_slist_nth_data(translations, 0);
        fullTranslationSection = g_slist_nth_data(translations, 1);

        concatenatedTranslationString = strconcat("\n\n", mainTranslationSection);
        concatenatedTranslationString = strconcat(concatenatedTranslationString, " \n\n");
        concatenatedTranslationString = strconcat(concatenatedTranslationString, fullTranslationSection);

        int mainTranslationSectionStartIndex = 0;
        int mainTranslationSectionEndIndex = (int) (strlen(mainTranslationSection) + 3);
        int fullTranslationSectionStartIndex = mainTranslationSectionEndIndex + 2;
        int fullTranslationSectionEndIndex = (int) (strlen(concatenatedTranslationString) + 1);

        gtk_text_buffer_set_text(translationTextViewBuffer, concatenatedTranslationString, -1);
        gtk_text_view_set_left_margin(GTK_TEXT_VIEW(translationView.translationTextView), 20);


        if (mainTranslationTag == NULL && fullTranslationTag == NULL) {
            mainTranslationTag = gtk_text_buffer_create_tag(translationTextViewBuffer, "bold",
                                                            "weight", PANGO_WEIGHT_BOLD,
                                                            NULL);

            fullTranslationTag = gtk_text_buffer_create_tag(translationTextViewBuffer, "italic",
                                                            "style", PANGO_STYLE_ITALIC, NULL);
        }

        gtk_text_buffer_get_iter_at_offset(translationTextViewBuffer, &start, fullTranslationSectionStartIndex);
        gtk_text_buffer_get_iter_at_offset(translationTextViewBuffer, &end, fullTranslationSectionEndIndex);
        gtk_text_buffer_apply_tag(translationTextViewBuffer, fullTranslationTag, &start, &end);

        gtk_text_buffer_get_iter_at_offset(translationTextViewBuffer, &start, mainTranslationSectionStartIndex);
        gtk_text_buffer_get_iter_at_offset(translationTextViewBuffer, &end, mainTranslationSectionEndIndex);
        gtk_text_buffer_apply_tag(translationTextViewBuffer, mainTranslationTag, &start, &end);

        font_desc = pango_font_description_from_string("Serif 13");
        gtk_widget_modify_font(translationView.translationTextView, font_desc);
        pango_font_description_free(font_desc);

        g_free(concatenatedTranslationString);
        g_free(mainTranslationSection);
        g_free(fullTranslationSection);
    }
}

void create_top_level_container() {

    appWidgets.topLevelVBox = gtk_vbox_new(FALSE, 0);
    gtk_container_add(GTK_CONTAINER(topLevelWindow), appWidgets.topLevelVBox);
}

void create_translation_view() {

    appWidgets.contentLevelHBox = gtk_hbox_new(FALSE, 0);
    gtk_box_pack_start(GTK_BOX(appWidgets.topLevelVBox), appWidgets.menuBar, FALSE, FALSE, 1);
    gtk_box_pack_start(GTK_BOX(appWidgets.topLevelVBox), appWidgets.contentLevelHBox, TRUE, TRUE, 1);

    statusView.statusLabel = gtk_label_new("");
    statusView.statusLevelHBox = gtk_hbox_new(FALSE, 0);
    gtk_box_pack_start(GTK_BOX(statusView.statusLevelHBox), statusView.statusLabel, FALSE, FALSE, 1);
    gtk_box_pack_start(GTK_BOX(appWidgets.topLevelVBox), statusView.statusLevelHBox, FALSE, FALSE, 1);

    translationView.parent = appWidgets.contentLevelHBox;
    translationView.translationViewScrollWindow = gtk_scrolled_window_new(NULL, NULL);
    gtk_box_pack_end(GTK_BOX(appWidgets.contentLevelHBox), translationView.translationViewScrollWindow, TRUE, TRUE, 1);
    translationView.translationTextView = gtk_text_view_new();
    gtk_container_add(GTK_CONTAINER(translationView.translationViewScrollWindow), translationView.translationTextView);

    gtk_text_view_set_editable(GTK_TEXT_VIEW(translationView.translationTextView), FALSE);
}

void create_find_word_view() {

    findView.parent = appWidgets.contentLevelHBox;
    findView.wordFindLevelVBox = gtk_vbox_new(FALSE, 1);
    gtk_box_pack_end(GTK_BOX(appWidgets.contentLevelHBox), findView.wordFindLevelVBox, FALSE, TRUE, 1);

    findView.wordFindLevelHBox = gtk_hbox_new(FALSE, 1);
    gtk_box_pack_start(GTK_BOX(findView.wordFindLevelVBox), findView.wordFindLevelHBox, FALSE, FALSE, 1);

    findView.wordTextEntry = gtk_entry_new();
    gtk_box_pack_start(GTK_BOX(findView.wordFindLevelHBox), findView.wordTextEntry, TRUE, TRUE, 1);

    findView.wordFindButton = gtk_button_new_with_label("Find");
    gtk_box_pack_start(GTK_BOX(findView.wordFindLevelHBox), findView.wordFindButton, FALSE, FALSE, 1);

    g_signal_connect(GTK_BUTTON(findView.wordFindButton), "clicked",
                     G_CALLBACK(find_button_click_handler), NULL);

    findView.wordsListScrollWindow = gtk_scrolled_window_new(NULL, NULL);
    gtk_box_pack_start(GTK_BOX(findView.wordFindLevelVBox), findView.wordsListScrollWindow, TRUE, TRUE, 1);
}

void find_button_click_handler(GtkWidget *widget, gpointer data) {

    gchar *searchWord = (gchar *) gtk_entry_get_text(GTK_ENTRY(findView.wordTextEntry));
    g_strstrip(searchWord);

    searchWord = strconcat((char *) searchWord, "%");

    GSList *translationsForWord = get_all_words_starting_with(searchWord);
    update_words_list_widgets_store(translationsForWord);
    g_slist_free(translationsForWord);
}


void create_words_list_view() {

    GtkCellRenderer *cellRenderer;

    findView.wordsListView = gtk_tree_view_new();

    cellRenderer = gtk_cell_renderer_text_new();
    gtk_tree_view_insert_column_with_attributes(GTK_TREE_VIEW(findView.wordsListView),
                                                -1,
                                                "Word",
                                                cellRenderer,
                                                "text", COL_WORD_KEY,
                                                NULL);

    cellRenderer = gtk_cell_renderer_text_new();

    wordsListStore = gtk_list_store_new(COLS_COUNT, G_TYPE_STRING);

    gtk_tree_view_set_headers_visible(GTK_TREE_VIEW(findView.wordsListView), FALSE);
    gtk_tree_view_set_model(GTK_TREE_VIEW(findView.wordsListView), GTK_TREE_MODEL(wordsListStore));
    gtk_container_add(GTK_CONTAINER(findView.wordsListScrollWindow), findView.wordsListView);

    GtkTreeSelection *selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(findView.wordsListView));
    gtk_tree_selection_set_select_function(selection, words_list_view_selection_handler, NULL, NULL);
    gtk_tree_view_set_enable_search(GTK_TREE_VIEW(findView.wordsListView), FALSE);

    g_object_unref(wordsListStore);
}

gboolean words_list_view_selection_handler(GtkTreeSelection *selection,
                                           GtkTreeModel *model,
                                           GtkTreePath *path,
                                           gboolean path_currently_selected,
                                           gpointer userdata) {
    GtkTreeIter iter;

    if (gtk_tree_model_get_iter(model, &iter, path)) {
        gchar *selectedWord;
        gtk_tree_model_get(model, &iter, COL_WORD_KEY, &selectedWord, -1);
        if (!path_currently_selected) {
            GSList *selectedWordsTranslations = get_all_translations_for_word(selectedWord);
            update_translation_view_text(selectedWordsTranslations/*, TRUE*/);

        }
        g_free(selectedWord);
    }
    return TRUE;
}


void clear_words_list_widgets_store() {

    if (wordsListStore != NULL) {
        gtk_list_store_clear(wordsListStore);
    }
}

void update_words_list_widgets_store(GSList *newWordsList) {

    GtkTreeIter iter;

    if (wordsListStore != NULL) {
        clear_words_list_widgets_store();
    } else {
        wordsListStore = gtk_list_store_new(COLS_COUNT, G_TYPE_STRING);
    }

    GSList *iterator = NULL;
    for (iterator = newWordsList; iterator; iterator = iterator->next) {
        gtk_list_store_append(wordsListStore, &iter);
        gtk_list_store_set(wordsListStore, &iter,
                           COL_WORD_KEY, iterator->data,
                           -1);
    }
}

GtkWidget *create_top_level_window() {

    topLevelWindow = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    appWidgets.parent = topLevelWindow;

    gtk_window_set_title(GTK_WINDOW(topLevelWindow), "GUX Semester Project #2 by xkuzne00");
    gtk_window_set_default_size(GTK_WINDOW(topLevelWindow), 800, 600);
    gtk_window_set_position(GTK_WINDOW(topLevelWindow), GTK_WIN_POS_CENTER);
    gtk_container_set_border_width(GTK_CONTAINER(topLevelWindow), 0);

    g_signal_connect(G_OBJECT(topLevelWindow), "destroy",
                     GTK_SIGNAL_FUNC(top_level_window_close_callback), G_OBJECT(topLevelWindow));

    return topLevelWindow;
}

GtkWidget *create_menubar() {

    GtkWidget *separator;
    GtkAccelGroup *accelGroup = NULL;

    accelGroup = gtk_accel_group_new();
    gtk_window_add_accel_group(GTK_WINDOW(topLevelWindow), accelGroup);
    separator = gtk_separator_menu_item_new();

    appWidgets.menuBar = gtk_menu_bar_new();
    menuBar.parent = appWidgets.menuBar;
    menuBar.fileMenu = gtk_menu_new();
    menuBar.fileMenuItem = gtk_menu_item_new_with_label("File");
    menuBar.fileMenuSubItemQuit = gtk_image_menu_item_new_from_stock(
            GTK_STOCK_QUIT, accelGroup);
    menuBar.fileMenuSubItemOpen = gtk_image_menu_item_new_from_stock(
            GTK_STOCK_OPEN, NULL);

    menuBar.helpMenu = gtk_menu_new();
    menuBar.helpMenuItem = gtk_menu_item_new_with_label("Help");
    menuBar.helpMenuSubItemAbout = gtk_image_menu_item_new_from_stock(
            GTK_STOCK_ABOUT, NULL);

    gtk_menu_item_set_submenu(GTK_MENU_ITEM(menuBar.fileMenuItem), menuBar.fileMenu);
    gtk_menu_shell_append(GTK_MENU_SHELL(menuBar.fileMenu), menuBar.fileMenuSubItemOpen);
    gtk_menu_shell_append(GTK_MENU_SHELL(menuBar.fileMenu), separator);
    gtk_menu_shell_append(GTK_MENU_SHELL(menuBar.fileMenu), menuBar.fileMenuSubItemQuit);
    gtk_menu_shell_append(GTK_MENU_SHELL(appWidgets.menuBar), menuBar.fileMenuItem);

    gtk_menu_item_set_submenu(GTK_MENU_ITEM(menuBar.helpMenuItem), menuBar.helpMenu);
    gtk_menu_shell_append(GTK_MENU_SHELL(menuBar.helpMenu), menuBar.helpMenuSubItemAbout);
    gtk_menu_shell_append(GTK_MENU_SHELL(appWidgets.menuBar), menuBar.helpMenuItem);

    g_signal_connect(G_OBJECT(menuBar.fileMenuSubItemQuit), "activate",
                     GTK_SIGNAL_FUNC(top_level_window_close_callback), NULL);

    g_signal_connect(G_OBJECT(menuBar.fileMenuSubItemOpen), "activate",
                     GTK_SIGNAL_FUNC(open_file_open_dialog), topLevelWindow);


    g_signal_connect(G_OBJECT(menuBar.helpMenuSubItemAbout), "activate",
                     GTK_SIGNAL_FUNC(open_about_dialog), topLevelWindow);

    return appWidgets.menuBar;
}


char *build_status_label_string(char *srcLangCode, char *destLangCode, int wordsCount) {

    char *statusLabelText = "";

    char itoaBuffer[100];
    snprintf(itoaBuffer, 10, "%d", wordsCount);

    statusLabelText = strconcat(statusLabelText, srcLangCode);
    statusLabelText = strconcat(statusLabelText, "-");
    statusLabelText = strconcat(statusLabelText, destLangCode);
    statusLabelText = strconcat(statusLabelText, " ");
    statusLabelText = strconcat(statusLabelText, itoaBuffer);
    statusLabelText = strconcat(statusLabelText, " words");

    return statusLabelText;
}

void update_status_label_text() {

    if (statusView.statusLabel != NULL) {

        char *sourceLangCode;
        char *destLangCode;

        get_source_lang_code(&sourceLangCode);
        get_dest_lang_code(&destLangCode);
        int wordsCount = get_words_count();

        char *labelString = build_status_label_string(sourceLangCode, destLangCode, wordsCount);

        gtk_label_set_text(GTK_LABEL(statusView.statusLabel), labelString);

        g_free(sourceLangCode);
        g_free(destLangCode);
    }
}