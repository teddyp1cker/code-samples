#ifndef GUX_PROJECT_2_GSIMPLEDICT_H
#define GUX_PROJECT_2_GSIMPLEDICT_H


#include <gtk/gtk.h>


typedef struct menu_bar {
    GtkWidget *parent;
    GtkWidget *fileMenu;
    GtkWidget *fileMenuItem;
    GtkWidget *fileMenuSubItemQuit;
    GtkWidget *fileMenuSubItemOpen;
    GtkWidget *helpMenu;
    GtkWidget *helpMenuItem;
    GtkWidget *helpMenuSubItemAbout;
} MenuBar;

typedef struct app_widgets {
    GtkWidget *parent;
    GtkWidget *topLevelVBox;
    GtkWidget *menuBar;
    GtkWidget *contentLevelHBox;
} AppWidgets;

typedef struct translation_view {
    GtkWidget *parent;
    GtkWidget *translationViewScrollWindow;
    GtkWidget *translationTextView;
} TranslationView;


typedef struct word_find_view {
    GtkWidget *parent;
    GtkWidget *wordFindLevelVBox;

    GtkWidget *wordFindLevelHBox;
    GtkWidget *wordTextEntry;
    GtkWidget *wordFindButton;

    GtkWidget *wordsListScrollWindow;
    GtkWidget *wordsListView;

} WordFindView;

typedef struct status_view {
    GtkWidget *parent;
    GtkWidget *statusLevelHBox;
    GtkWidget *statusLabel;

} StatusView;


GtkWidget *topLevelWindow;

AppWidgets appWidgets;
MenuBar menuBar;
TranslationView translationView;
WordFindView findView;
StatusView statusView;

GtkListStore *wordsListStore;

gboolean dictionaryFileIsOpened = FALSE;

enum {
    COL_WORD_KEY = 0,
    COLS_COUNT
};

GtkTextTag *mainTranslationTag;
GtkTextTag *fullTranslationTag;


GtkWidget *create_menubar();

GtkWidget *create_top_level_window();

void create_words_list_view();

void create_find_word_view();

void create_translation_view();

void create_top_level_container();

void intitialize_widgets();

gboolean words_list_view_selection_handler(GtkTreeSelection *selection,
                                           GtkTreeModel *model,
                                           GtkTreePath *path,
                                           gboolean path_currently_selected,
                                           gpointer userdata);

void find_button_click_handler(GtkWidget *widget, gpointer data);

char *build_status_label_string(char *srcLangCode, char *destLangCode, int wordsCount);

void update_status_label_text();

void update_words_list_widgets_store(GSList *newWordsList);

#endif
