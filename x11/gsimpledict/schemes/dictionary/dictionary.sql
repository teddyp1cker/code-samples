CREATE TABLE `dictionary` (
	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	`word`	TEXT NOT NULL,
	`main_translation`	TEXT NOT NULL,
	`full_translation`	TEXT,
	`bookmarked`	INTEGER NOT NULL DEFAULT ''0''
);