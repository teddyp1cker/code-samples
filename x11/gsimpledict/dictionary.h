

#ifndef GUX_PROJECT_2_DICTIONARY_H
#define GUX_PROJECT_2_DICTIONARY_H

#include <sqlite3.h>


enum DICTIONARY_DB_TABLES {
    DICTIONARY,
    DICTIONARY_AUTHOR,
    DICTIONARY_METADATA
};


typedef enum {
    OK, FILE_OPEN_ERROR, FILE_FORMAT_ERROR
} FileOpenResult;

typedef struct dictionary_open_status {
    FileOpenResult status;
    int succeeded;
} DictionaryOpenResult;


sqlite3 *currentDictionaryDB;

char *currentDistionaryDBFilePath;

DictionaryOpenResult open_dictionary_file(char *dictFilePath);

int close_dictionary_file();

GSList *get_all_words_list();

GSList *get_all_translations_for_word(char *word);

GSList *get_all_words_starting_with(char *startStr);

void get_source_lang_code(char **src_lang);

void get_dest_lang_code(char **dest_lang);

int get_words_count();

void get_dictionary_description(char **desc);

void get_formatted_author_info_string(char **authorInfo);

#endif


