//
//  NSPredicateWrapper.swift
//  bank
//
//  Created by Iurii Kuznetcov on 25/05/2017.
//  Copyright © 2017 awesomefm. All rights reserved.
//

import Foundation


// Wrapper for NSCompoundPredicate due to dumb NSPredicate design (you can't extract key/vales from object)
class NSPredicateWrapper {

    // actual NSPredicate
    var predicate : NSPredicate?

    // predicate key : predicate value
    var predicateContent: [String: Any] = [String: Any]()

    init(predicate: NSPredicate?) {
        self.predicate = predicate
    }

    init(predicate: NSPredicate?, predicateContent: [String: Any] ) {
        self.predicate = predicate
        self.predicateContent = predicateContent
    }

}
