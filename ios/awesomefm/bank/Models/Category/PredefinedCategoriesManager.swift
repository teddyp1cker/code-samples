//
//  PredefinedCategoriesManager.swift
//  bank
//
//  Created by Iurii Kuznetcov on 28/04/17.
//  Copyright © 2017 awesomefm. All rights reserved.
//

import Foundation
import CoreData
import UIKit

import CocoaLumberjack

class PredefinedCategoriesManager {

    private lazy var context = CoreDataStack().persistentContainer.viewContext

    private var categoriesFoundationObject: Any?

    func parseBundledCategoriesJson(jsonFile: String) -> Bool {

        var parsed = false
        if let asset = NSDataAsset(name: jsonFile) {
            let categoriesJsonData = asset.data
            do {
                self.categoriesFoundationObject = try? JSONSerialization.jsonObject(with: categoriesJsonData, options: [])
                parsed = true
            } catch let error as NSError {
                DDLogError("Json categories asset \(categoriesJsonData) parse exception: \(error), \(error.localizedDescription)")
                Thread.callStackSymbols.forEach {
                    DDLogError($0)
                }
            }
        }
        return parsed
    }

    func checkStoredPredefinedCategories() -> Bool {

        var entitiesCount = NSNotFound
        let predicate = NSPredicate(format: "isPredefined == %@", NSNumber(booleanLiteral: true))
        let categoriesFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Category")
        var curCount = 0

        context.performAndWait {
            do {
                curCount = try self.context.count(for: categoriesFetchRequest)
                entitiesCount = curCount
                if curCount == NSNotFound {
                    DDLogError("Can't fetch any categories")
                }
            } catch let error as NSError {
                entitiesCount = NSNotFound
                DDLogError("Fetching predefined categories exception: \(error), \(error.localizedDescription)")
                Thread.callStackSymbols.forEach {
                    DDLogError($0)
                }
            }
        }
        return entitiesCount != 0
    }

    func fillPredefinedCategories(jsonFile: String) -> Bool {

        var filled = false
        if !self.checkStoredPredefinedCategories() {
            var title, color, isDefault, isPredefined: Any?
            if self.categoriesFoundationObject is Array<AnyObject> {
                for categoryJsonRec in self.categoriesFoundationObject as! Array<AnyObject> {
                    title = categoryJsonRec["title"] as AnyObject? as? String ?? ""
                    color = categoryJsonRec["color"] as AnyObject? as? String ?? ""

                    isDefault = false
                    isPredefined = true

                    let categoriesDescription = NSEntityDescription.entity(forEntityName: "Category", in: context)
                    let categoriesManagedObject = NSManagedObject(entity: categoriesDescription!, insertInto: context)

                    categoriesManagedObject.setValue(title, forKey: "title")
                    categoriesManagedObject.setValue(color, forKey: "color")
                    categoriesManagedObject.setValue(isDefault, forKey: "isDefault")
                    categoriesManagedObject.setValue(isPredefined, forKey: "isPredefined")

                    context.performAndWait {
                        do {
                            try self.context.save()
                        } catch let error as NSError {
                            DDLogError("Could not save predefined category \(categoriesDescription) due to \(error), \(error.userInfo), \(error.localizedDescription)")
                            Thread.callStackSymbols.forEach {
                                DDLogError($0)
                            }
                        }
                    }
                }
                filled = true
            } else {
                DDLogError("Can't cast predefined categories json object to list")
            }
        } else {
            DDLogWarn("Predefined categories were already found")
        }
        return filled
    }
}
