//
//  PersistanceManager.swift
//  bank
//
//  Created by Iurii Kuznetcov on 25/04/17.
//  Copyright © 2017 awesomefm. All rights reserved.
//

import Foundation
import CoreData

import CocoaLumberjack

// dumb save callback wrapper for nserrors
struct EntitySaveResult {

    var succeed : Bool

    var nserror : NSError?
}

class PersistanceManager {

    private lazy var context = CoreDataStack().persistentContainer.viewContext

    private var cachedAccounts: [Account] = []
    private var cachedCurrencies: [Currency] = []
    private var cachedTransactions: [Transaction] = []
    private var cachedCategories: [Category] = []

    static let sharedInstance = PersistanceManager()

    func fillCache() {

        do {
            reloadAccountsCache()
            reloadCurrenciesCache()
            reloadCustomCategoriesCache()
            reloadTransactionsCache()
        } catch {
            let nserror = error as NSError
            DDLogError("fillCache error \(nserror), \(nserror.userInfo), \(nserror.localizedDescription)")
        }
    }

    func reloadAccountsCache() {

        do {
            cachedAccounts.removeAll()
            cachedAccounts = try context.fetch(Account.fetchRequest())
        } catch {
            let nserror = error as NSError
            DDLogError("reloadAccountsCache error \(nserror), \(nserror.userInfo), \(nserror.localizedDescription)")
        }
    }

    func reloadCurrenciesCache() {

        do {
            cachedCurrencies.removeAll()
            cachedCurrencies = try context.fetch(Currency.fetchRequest())
        } catch {
            let nserror = error as NSError
            DDLogError("reloadCurrenciesCache error \(nserror), \(nserror.userInfo), \(nserror.localizedDescription)")
        }
    }

    func reloadAllCategoriesCache() {

        do {
            cachedCategories.removeAll()
            cachedCategories = try context.fetch(Category.fetchRequest())
        } catch {
            let nserror = error as NSError
            DDLogError("reloadAllCategoriesCache error \(nserror), \(nserror.userInfo), \(nserror.localizedDescription)")
        }
    }

    func reloadTransactionsCache() {

        do {
            cachedTransactions.removeAll()
            let sortDescriptor = NSSortDescriptor(key: "date", ascending: false)
            let sortDescriptors = [sortDescriptor]
            let fetchRequest : NSFetchRequest<Transaction> = Transaction.fetchRequest()
            fetchRequest.sortDescriptors = sortDescriptors
            cachedTransactions = try context.fetch(fetchRequest)
        } catch {
            let nserror = error as NSError
            DDLogError("reloadTransactionsCache error \(nserror), \(nserror.userInfo), \(nserror.localizedDescription)")
        }
    }

    func reloadCustomCategoriesCache() {

        do {
            cachedCategories.removeAll()
            let predicate = NSPredicate(format: "isPredefined == %@", NSNumber(booleanLiteral: false))
            let categoriesFetchRequest: NSFetchRequest<Category> = Category.fetchRequest()
            categoriesFetchRequest.predicate = predicate
            cachedCategories = try context.fetch(categoriesFetchRequest)
        } catch {
            let nserror = error as NSError
            DDLogError("reloadCustomCategoriesCache error \(nserror), \(nserror.userInfo), \(nserror.localizedDescription)")
        }
    }

    func getAllBundledCategories() -> [Category] {

        var bundledCategories : [Category] = []

        do {
            let predicate = NSPredicate(format: "isPredefined == %@", NSNumber(booleanLiteral: true))
            let categoriesFetchRequest: NSFetchRequest<Category> = Category.fetchRequest()
            categoriesFetchRequest.predicate = predicate
            bundledCategories = try context.fetch(categoriesFetchRequest)
        } catch {
            let nserror = error as NSError
            DDLogError("getAllBundledCategories error \(nserror), \(nserror.userInfo), \(nserror.localizedDescription)")
        }

        return bundledCategories
    }

    func getAllCategories() -> [Category] {

        var allCategories : [Category] = []

        do {
            allCategories = try context.fetch(Category.fetchRequest())
        } catch {
            let nserror = error as NSError
            DDLogError("getAllCategories error \(nserror), \(nserror.userInfo), \(nserror.localizedDescription)")
        }

        return allCategories
    }


    func getBundledCategoryByIndex(index: Int) -> Category? {

        var bundledCategories = getAllBundledCategories()
        return bundledCategories[index]
    }

    func getAllBundledCategoriesCount() -> Int {
        return getAllBundledCategories().count
    }

    func getAllCachedCurrencies() -> [Currency]? {
        return cachedCurrencies
    }

    func getAllCachedAccounts() -> [Account]? {
        return cachedAccounts
    }

    func getAllCachedCategories() -> [Category]? {
        return cachedCategories
    }

    func getAllCachedCustomCategories() -> [Category]? {
        return cachedCategories
    }

    func getAllCachedTransactions() -> [Transaction]? {
        return cachedTransactions
    }

    func getCachedAccountsCount() -> Int {
        return cachedAccounts.count
    }

    func getCachedCurrenciesCount() -> Int {
        return cachedCurrencies.count
    }

    func getCachedTransactionsCount() -> Int {
        return cachedTransactions.count
    }

    func getCachedCustomCategoriesCount() -> Int {
        return cachedCategories.count
    }

    func getCurrency(code: String) -> [Currency]? {

        var foundCurrencies: [Currency]?
        do {
            let fetchRequest: NSFetchRequest<Currency> = Currency.fetchRequest()
            fetchRequest.predicate = NSPredicate(format: "code == %@", code)
            foundCurrencies = try context.fetch(fetchRequest)
        } catch {
            let nserror = error as NSError
            DDLogError("getCurrency error \(nserror), \(nserror.userInfo), \(nserror.localizedDescription)")
        }
        return foundCurrencies
    }

    func getCategory(code: String) -> [Category]? {

        var foundCategories: [Category]?
        do {
            let fetchRequest: NSFetchRequest<Category> = Category.fetchRequest()
            fetchRequest.predicate = NSPredicate(format: "code == %@", code)
            foundCategories = try context.fetch(fetchRequest)
        } catch {
            let nserror = error as NSError
            DDLogError("getCurrency error \(nserror), \(nserror.userInfo), \(nserror.localizedDescription)")
        }
        return foundCategories
    }

    func getAccount(title: String) -> [Account]? {

        var foundAccounts: [Account]?
        do {
            let fetchRequest: NSFetchRequest<Account> = Account.fetchRequest()
            fetchRequest.predicate = NSPredicate(format: "title == %@", title)
            foundAccounts = try context.fetch(fetchRequest)
        } catch {
            let nserror = error as NSError
            DDLogError("getAccount error \(nserror), \(nserror.userInfo), \(nserror.localizedDescription)")
        }
        return foundAccounts
    }

    func getCachedAccount(index: Int?) -> Account? {
        var account: Account?
        if let index: Int = index {
            if (index > -1 && index < cachedAccounts.count) {
                account = cachedAccounts[index]
            }
        }
        return account
    }

    func getCachedCurrency(index: Int?) -> Currency? {
        var currency: Currency?
        if let index: Int = index {
            if (index > -1 && index < cachedCurrencies.count) {
                currency = cachedCurrencies[index]
            }
        }
        return currency
    }

    func getCachedTransaction(index: Int?) -> Transaction? {
        var transaction: Transaction?
        if let index: Int = index {
            if (index > -1 && index < cachedTransactions.count) {
                transaction = cachedTransactions[index]
            }
        }
        return transaction
    }

    func getCachedCustomCategory(index: Int?) -> Category? {
        var category: Category?
        if let index: Int = index {
            if (index > -1 && index < cachedCategories.count) {
                category = cachedCategories[index]
            }
        }
        return category
    }

    func getCachedCurrencyIndex(code: String?) -> Int? {
        var index: Int?
        if let code: String = code {
            index = cachedCurrencies.index(where: { $0.code == code })
        }
        return index
    }

    func getCachedAccountIndex(title: String?) -> Int? {
        var index: Int?
        if let title: String = title {
            index = cachedAccounts.index(where: { $0.title == title })
        }
        return index
    }

    func getCachedCategoryIndex(title: String?) -> Int? {
        var index: Int?
        if let title: String = title {
            index = cachedCategories.index(where: { $0.title == title })
        }
        return index
    }

    func getCachedCurrencies(code: String?) -> [Currency]? {
        var currencies: [Currency]?
        if let code: String = code {
            currencies = cachedCurrencies.filter {
                $0.code == code
            }
        }
        return currencies
    }

    func deleleAccount(account: Account?) {
        if let account = account {
            context.performAndWait {
                do {
                    self.context.delete(account)
                    try self.context.save()
                } catch {
                    let nserror = error as NSError
                    DDLogError("deleleAccount error \(nserror), \(nserror.userInfo), \(nserror.localizedDescription)")
                }
            }
        }
    }

    private func updateOtherAccountsDefaultMark(accountTitle: String?) {

        if let accountTitle = accountTitle {
            context.performAndWait {
                //TODO: fix it using unique object id
                let predicate = NSPredicate(format: "title != %@", "\(accountTitle)")
                let fetchRequest: NSFetchRequest<Account> = Account.fetchRequest()
                fetchRequest.predicate = predicate
                do {
                    let otherAccounts = try self.context.fetch(fetchRequest)
                    for otherAccount in otherAccounts {
                        otherAccount.is_default = false
                    }
                } catch {
                    let nserror = error as NSError
                    DDLogInfo("Other account select failed, given account = \(accountTitle), " +
                            "\(nserror), \(nserror.localizedDescription)")
                }

                do {
                    try self.context.save()
                    DDLogInfo("Other account default mark updated, given account = \(accountTitle)")
                } catch {
                    let nserror = error as NSError
                    DDLogError("Other account default mark update fail, given account = \(accountTitle), " +
                            "\(nserror), \(nserror.localizedDescription)")
                }
            }
        }
    }

    private func updateOtherCategoriesDefaultMark(categoryTitle: String?) {

        if let categoryTitle = categoryTitle {
            context.performAndWait {
                //TODO: fix it using unique object id
                let predicate = NSPredicate(format: "title != %@", "\(categoryTitle)")
                let fetchRequest: NSFetchRequest<Category> = Category.fetchRequest()
                fetchRequest.predicate = predicate
                do {
                    let otherCategories = try self.context.fetch(fetchRequest)
                    for otherCategory in otherCategories {
                        otherCategory.isDefault = false
                    }
                } catch {
                    let nserror = error as NSError
                    DDLogInfo("Other category select failed, given account = \(categoryTitle), " +
                        "\(nserror), \(nserror.localizedDescription)")
                }

                do {
                    try self.context.save()
                    DDLogInfo("Other category default mark updated, given account = \(categoryTitle)")
                } catch {
                    let nserror = error as NSError
                    DDLogError("Other category default mark update fail, given account = \(categoryTitle), " +
                        "\(nserror), \(nserror.localizedDescription)")
                }
            }
        }
    }

    func deleteTransaction(transaction: Transaction?) {

        var accountAmount = (transaction?.account?.amount)!

        if "expense" == transaction?.type?.lowercased() {
            accountAmount = accountAmount.adding((transaction?.value)!)
        } else if "income" == transaction?.type?.lowercased() {
            accountAmount = accountAmount.subtracting((transaction?.value)!)

        } else {
        }


        if let transaction = transaction {
            context.performAndWait {
                do {
                    transaction.account?.amount = accountAmount
                    self.context.delete(transaction)
                    try self.context.save()
                    self.reloadTransactionsCache()
                    self.reloadAccountsCache()
                } catch {
                    let nserror = error as NSError
                    DDLogError("Delete error \(nserror), \(nserror.userInfo), \(nserror.localizedDescription)")
                }
            }
        }
    }

    func deleleCustomCategory(category: Category?) {
        if let category = category {
            context.performAndWait {
                do {
                    self.context.delete(category)
                    try self.context.save()
                } catch {
                    let nserror = error as NSError
                    DDLogError("Delete error \(nserror), \(nserror.userInfo), \(nserror.localizedDescription)")
                }
            }
        }
    }

    func updateAccount(account: Account?)  -> EntitySaveResult {

        var result : EntitySaveResult = EntitySaveResult(succeed: false, nserror: nil)

        if let account = account {
            context.performAndWait {
                do {
                    try self.context.save()
                    if account.is_default {
                        self.updateOtherAccountsDefaultMark(accountTitle: account.title)
                    }

                    result.succeed = true
                } catch {
                    let nserror = error as NSError
                    DDLogError("Could not save account object \(account), due to \(nserror), \(nserror.localizedDescription)")
                    result.succeed = false
                    result.nserror = nserror
                    self.context.reset()
                }
            }
        }
        return result
    }

    func updateCustomCategory(category: Category?) -> EntitySaveResult {
        
        var result : EntitySaveResult = EntitySaveResult(succeed: false, nserror: nil)

        if let category = category {
            context.performAndWait {
                do {
                    category.isPredefined = false
                    try self.context.save()
                    if category.isDefault {
                        self.updateOtherCategoriesDefaultMark(categoryTitle: category.title)
                    }
                    
                    result.succeed = true
                } catch {
                    let nserror = error as NSError
                    DDLogError("Could not update category object \(category), due to \(nserror), \(nserror.localizedDescription)")
                    result.succeed = false
                    result.nserror = nserror
                    self.context.reset()
                }
            }
        }
        return result
    }

    func updateTransaction(previousTransactionAmount: NSDecimalNumber,
                           previousTransactionType: String,
                           previousTransactionAccount: Account,
                           updatedTransaction: Transaction?) {

        var updatedAccountAmount: NSDecimalNumber?

        updatedAccountAmount = updatedTransaction?.account?.amount

        if previousTransactionAccount.title?.lowercased() == updatedTransaction?.account?.title?.lowercased() {

            if previousTransactionType.lowercased() == "expense" {
                if let newTransactionType = updatedTransaction?.type {
                    if "expense" == newTransactionType.lowercased() {
                        updatedAccountAmount = updatedAccountAmount?.adding(previousTransactionAmount)
                        updatedAccountAmount = updatedAccountAmount?.adding((updatedTransaction?.value!.multiplying(by: NSDecimalNumber(value: -1)))!)
                    }
                    // !!
                    if "income" == newTransactionType.lowercased() {
                        updatedAccountAmount = updatedAccountAmount?.adding(previousTransactionAmount)
                        updatedAccountAmount = updatedAccountAmount?.adding((updatedTransaction?.value!)!)
                    }
                } else {
                    DDLogError("Unable to update account amount due to undefined transaction amount, transaction : \(updatedTransaction), account : \(updatedTransaction?.account)")
                }

            } else if previousTransactionType.lowercased() == "income" {
                if let newTransactionType = updatedTransaction?.type {
                    if "income" == newTransactionType.lowercased() {
                        updatedAccountAmount = updatedAccountAmount?.subtracting(previousTransactionAmount)
                        updatedAccountAmount = updatedAccountAmount?.adding((updatedTransaction?.value)!)
                    }
                    // !!
                    if "expense" == newTransactionType.lowercased() {
                        updatedAccountAmount = updatedAccountAmount?.subtracting(previousTransactionAmount)
                        updatedAccountAmount = updatedAccountAmount?.adding((updatedTransaction?.value!.multiplying(by: NSDecimalNumber(value: -1)))!)
                    }
                } else {
                    DDLogError("Unable to update account amount due to undefined transaction amount, transaction : \(updatedTransaction), account : \(updatedTransaction?.account)")
                }

            } else {
                DDLogError("Unable to update account amount due to undefined previous transaction amount, transaction : \(updatedTransaction), account : \(updatedTransaction?.account)")
            }


            if let transaction = updatedTransaction {
                context.performAndWait {
                    do {
                        try self.context.save()
                        DDLogInfo("transaction updated, transaction : \(transaction)")
                        let transactionCommited = self.commitAccountAmountUpdate(account: transaction.account, newAccountAmount: updatedAccountAmount)
                        if transactionCommited {
                            DDLogInfo("New transaction commited, transaction : \(updatedTransaction), account \(updatedTransaction?.account)")
                        } else {
                            DDLogError("New transaction commit failed, transaction : \(updatedTransaction), account \(updatedTransaction?.account)")
                        }
                    } catch {
                        let nserror = error as NSError
                        DDLogError("Could not update transaction object \(transaction), due to \(nserror), \(nserror.localizedDescription)")
                    }
                }
            }
        } else {
            // TODO: refactor
            if previousTransactionType.lowercased() == updatedTransaction?.type?.lowercased() {

                if "expense" == updatedTransaction?.type?.lowercased() {
                    let previousTransactionAccountAmountBeforeTransaction = previousTransactionAccount.amount?.adding((updatedTransaction?.value)!)
                    commitAccountAmountUpdate(account: previousTransactionAccount, newAccountAmount: previousTransactionAccountAmountBeforeTransaction)

                    let newTransactionAccountAmountAfterTransaction = updatedTransaction?.account?.amount?.subtracting((updatedTransaction?.value)!)
                    commitAccountAmountUpdate(account: updatedTransaction?.account, newAccountAmount: newTransactionAccountAmountAfterTransaction)

                } else if "income" == updatedTransaction?.type?.lowercased() {

                    let previousTransactionAccountAmountBeforeTransaction = previousTransactionAccount.amount?.subtracting((updatedTransaction?.value)!)
                    commitAccountAmountUpdate(account: previousTransactionAccount, newAccountAmount: previousTransactionAccountAmountBeforeTransaction)

                    let newTransactionAccountAmountAfterTransaction = updatedTransaction?.account?.amount?.adding((updatedTransaction?.value)!)
                    commitAccountAmountUpdate(account: updatedTransaction?.account, newAccountAmount: newTransactionAccountAmountAfterTransaction)

                } else {
                    DDLogError("Unable to update account amount due to undefined updated transaction type transaction : \(updatedTransaction), account : \(updatedTransaction?.account)")
                }

                context.performAndWait {
                    do {
                        try self.context.save()
                    } catch {
                        let nserror = error as NSError
                        DDLogError("Could not update transaction object \(updatedTransaction), due to \(nserror), \(nserror.localizedDescription)")
                    }
                }
            } else {
                if previousTransactionType.lowercased() == "income" && updatedTransaction?.type?.lowercased() == "expense" {
                    let previousTransactionAccountAmountBeforeTransaction = previousTransactionAccount.amount?.adding((updatedTransaction?.value)!)
                    let newTransactionAccountAmountAfterTransaction = updatedTransaction?.account?.amount?.subtracting((updatedTransaction?.value)!)
                    commitAccountAmountUpdate(account: updatedTransaction?.account, newAccountAmount: newTransactionAccountAmountAfterTransaction)
                } else if previousTransactionType.lowercased() == "expense" && updatedTransaction?.type?.lowercased() == "income" {
                    let previousTransactionAccountAmountBeforeTransaction = previousTransactionAccount.amount?.subtracting((updatedTransaction?.value)!)
                    commitAccountAmountUpdate(account: previousTransactionAccount, newAccountAmount: previousTransactionAccountAmountBeforeTransaction)

                    let newTransactionAccountAmountAfterTransaction = updatedTransaction?.account?.amount?.adding((updatedTransaction?.value)!)
                    commitAccountAmountUpdate(account: updatedTransaction?.account, newAccountAmount: newTransactionAccountAmountAfterTransaction)
                }

                context.performAndWait {
                    do {
                        try self.context.save()
                    } catch {
                        let nserror = error as NSError
                        DDLogError("Could not update transaction object \(updatedTransaction), due to \(nserror), \(nserror.localizedDescription)")
                    }
                }
            }
        }
    }

    func createAccount(title: String?, amount: NSDecimalNumber?, color_tag: String?, is_default: Bool, currency: Currency?) -> EntitySaveResult {

        var result : EntitySaveResult = EntitySaveResult(succeed: false, nserror: nil)

        let accountDescription = NSEntityDescription.entity(forEntityName: "Account", in: context);
        let accountManagedObject = NSManagedObject(entity: accountDescription!, insertInto: context)

        accountManagedObject.setValue(amount, forKey: "amount")
        accountManagedObject.setValue(title, forKey: "title")
        accountManagedObject.setValue(color_tag, forKey: "color_tag")
        accountManagedObject.setValue(is_default, forKey: "is_default")
        accountManagedObject.setValue(currency, forKey: "currency")

        let isDefault = is_default

        context.performAndWait {
            do {
                try self.context.save()
                if isDefault {
                    self.updateOtherAccountsDefaultMark(accountTitle: title)
                }
                result.succeed = true
            } catch {
                let nserror = error as NSError
                DDLogError("Could not save account object \(nserror), \(nserror.localizedDescription)")
                result.nserror = nserror
                self.context.rollback()
            }
        }
        return result
    }


    func createAccount(title: String?, amount: NSDecimalNumber?, color_tag: String?, is_default: Bool) -> EntitySaveResult {

        var result : EntitySaveResult = EntitySaveResult(succeed: false, nserror: nil)

        let accountDescription = NSEntityDescription.entity(forEntityName: "Account", in: context);
        let accountManagedObject = NSManagedObject(entity: accountDescription!, insertInto: context)

        accountManagedObject.setValue(amount, forKey: "amount")
        accountManagedObject.setValue(title, forKey: "title")
        accountManagedObject.setValue(color_tag, forKey: "color_tag")
        accountManagedObject.setValue(is_default, forKey: "is_default")
        accountManagedObject.setValue(getCachedCurrencies(code: "USD")?[0], forKey: "currency")

        context.performAndWait {
            do {
                try self.context.save()
                result.succeed = true
            } catch {
                let nserror = error as NSError
                DDLogError("Could not save account object \(nserror), \(nserror.localizedDescription)")
                result.nserror = nserror
            }
        }
        return result
    }

    func createCustomCategory(title: String?, color: String?, isDefault: Bool) -> EntitySaveResult {

        var result : EntitySaveResult = EntitySaveResult(succeed: false, nserror: nil)
        
        let categoryDescription = NSEntityDescription.entity(forEntityName: "Category", in: context);
        let categoryManagedObject = NSManagedObject(entity: categoryDescription!, insertInto: context)

        categoryManagedObject.setValue(title, forKey: "title")
        categoryManagedObject.setValue(color, forKey: "color")
        categoryManagedObject.setValue(isDefault, forKey: "isDefault")
        categoryManagedObject.setValue(false, forKey: "isPredefined")

        context.performAndWait {
            do {
                try self.context.save()
                result.succeed = true
                if isDefault {
                    self.updateOtherCategoriesDefaultMark(categoryTitle: title)
                }
            } catch {
                let nserror = error as NSError
                DDLogError("Could not save account object \(nserror), \(nserror.localizedDescription)")
                result.nserror = nserror
            }
        }
        return result
    }

    func createTransaction(title: String,
                           account: Account,
                           value: NSDecimalNumber,
                           type: String,
                           date: Date,
                           category: Category?) -> EntitySaveResult {

        var transactionProcessed = false
        var result : EntitySaveResult = EntitySaveResult(succeed: false, nserror: nil)

        let transactionDescription = NSEntityDescription.entity(forEntityName: "Transaction", in: context)
        let transactionManagedObject = NSManagedObject(entity: transactionDescription!, insertInto: context)

        transactionManagedObject.setValue(title, forKey: "title")
        transactionManagedObject.setValue(account, forKey: "account")
        transactionManagedObject.setValue(value, forKey: "value")
        transactionManagedObject.setValue(type, forKey: "type")
        transactionManagedObject.setValue(date, forKey: "date")
        transactionManagedObject.setValue(category, forKey: "category")

        context.performAndWait {
            do {
                try self.context.save()
                if type.lowercased() == "expense" {
                    transactionProcessed = self.commitTransaction(account: account, transactionAmount: value.multiplying(by: NSDecimalNumber(value: -1)))
                    DDLogInfo("New transaction commited, transaction : \(transactionManagedObject), account \(account)")
                    result.succeed = true
                } else if type.lowercased() == "income" {
                    transactionProcessed = self.commitTransaction(account: account, transactionAmount: value)
                    DDLogInfo("New transaction commited, transaction : \(transactionManagedObject), account \(account)")
                    result.succeed = true
                } else {
                    //let nserror = error as NSError
                    DDLogError("Could not commit new transaction for account \(account)) due to undefined transaction type")
                    //result.nserror = nserror
                }
            } catch {
                let nserror = error as NSError
                DDLogError("Could not save and commit transaction due to : \(nserror), \(nserror.localizedDescription)")
                result.nserror = nserror
            }
        }

        return result
    }

    func getDefaultAccount() -> [Account]? {

        var foundAccounts: [Account]?
        do {
            let fetchRequest: NSFetchRequest<Account> = Account.fetchRequest()
            fetchRequest.predicate = NSPredicate(format: "is_default == %@", NSNumber(booleanLiteral: true))
            foundAccounts = try context.fetch(fetchRequest)
        } catch {
            let nserror = error as NSError
            DDLogError("Unresolved error \(nserror), \(nserror.userInfo), \(nserror.localizedDescription)")
        }
        return foundAccounts
    }

    func createExpense(title: String,
                       account: Account,
                       value: NSDecimalNumber,
                       type: String,
                       date: Date,
                       category: Category?) {

        createTransaction(title: title, account: account, value: value, type: type, date: date, category: category)
    }

    func createIncome(title: String,
                      account: Account,
                      value: NSDecimalNumber,
                      type: String,
                      date: Date,
                      category: Category?) {
        createTransaction(title: title, account: account, value: value, type: type, date: date, category: nil)
    }

    //TODO: брать из настроек
    func getCachedDefaultAccount() -> [Account]? {

        var foundAccounts: [Account]?
        foundAccounts = cachedAccounts.filter {
            $0.is_default == true
        }
        return foundAccounts
    }

    func commitTransaction(account: Account?, transaction: Transaction?) -> Bool {

        var transactionCommited = false

        if let account = account {
            if let transaction = transaction {
                // TODO: tons of validations here - enterprisify your app!
                if transaction.type?.lowercased() == "expense".lowercased() {
                    if let transactionAmount = transaction.value {
                        account.amount = account.amount?.subtracting(transactionAmount)
                    } else {
                        DDLogError("Could not commit transaction \(transaction)) for account \(account): nil transaction amount value")
                    }
                    context.performAndWait {
                        do {
                            try self.context.save()
                            transactionCommited = true
                        } catch {
                            let nserror = error as NSError
                            DDLogError("Could not save transaction object \(nserror), \(nserror.localizedDescription)")
                        }
                    }
                } else if transaction.type?.lowercased() == "income".lowercased() {

                    if let transactionAmount = transaction.value {
                        account.amount = account.amount?.adding(transactionAmount)
                    } else {
                        DDLogError("Could not commit transaction \(transaction)) for account \(account): nil transaction amount value")
                    }
                    context.performAndWait {
                        do {
                            try self.context.save()
                            transactionCommited = true
                        } catch {
                            let nserror = error as NSError
                            DDLogError("Could not save transaction object \(nserror), \(nserror.localizedDescription)")
                        }
                    }
                } else {
                    DDLogError("Transaction commit failed due to undefined transaction type, account : \(account), transaction: \(transaction)")
                }
            } else {
                DDLogError("Transaction commit failed due to non-existing transaction, account : \(account)")
            }
        } else {
            DDLogError("Transaction commit failed due to non-existing account")
        }
        return transactionCommited
    }


    func commitTransaction(account: Account?, transactionAmount: NSDecimalNumber?) -> Bool {

        var transactionCommited = false

        if let account = account {
            if let transactionAmount = transactionAmount {
                account.amount = account.amount?.adding(transactionAmount)
                context.performAndWait {
                    do {
                        try self.context.save()
                        DDLogInfo("Account \(account) amount updated, new amount value : \(account.amount)")
                        transactionCommited = true
                    } catch {
                        let nserror = error as NSError
                        DDLogError("Could not save transaction object \(nserror), \(nserror.localizedDescription)")
                    }
                }
            } else {
                DDLogError("Transaction commit failed due to non-existing transaction amount, account : \(account)")
            }
        } else {
            DDLogError("Transaction commit failed due to non-existing account")
        }
        return transactionCommited
    }

    func commitAccountAmountUpdate(account: Account?, newAccountAmount: NSDecimalNumber?) -> Bool {

        var amountUpdated = false

        if let account = account {
            if let newAccountAmount = newAccountAmount {
                account.amount = newAccountAmount
                DDLogInfo("Account \(account) amount updated, new amount value : \(account.amount)")
                amountUpdated = true
            } else {
                DDLogError("Account amount update failed due to non-existing new account amount, account : \(account)")
            }
        } else {
            DDLogError("Transaction commit failed due to non-existing account")
        }
        return amountUpdated
    }

    //TODO: refactor
    func filterTransactions(predicate: NSPredicate?) -> [Transaction] {

        do {
            if let predicate = predicate {
                cachedTransactions.removeAll()
                let filteredTransactionsFetchRequest: NSFetchRequest<Transaction> = Transaction.fetchRequest()
                filteredTransactionsFetchRequest.predicate = predicate
                let sortDescriptor = NSSortDescriptor(key: "date", ascending: false)
                let sortDescriptors = [sortDescriptor]
                filteredTransactionsFetchRequest.sortDescriptors = sortDescriptors
                cachedTransactions = try context.fetch(filteredTransactionsFetchRequest)
            } else {
                reloadTransactionsCache()
            }
        } catch {
            let nserror = error as NSError
            DDLogError("filterTransactions error \(nserror), \(nserror.userInfo), \(nserror.localizedDescription)")
        }

        return cachedTransactions
    }
}
