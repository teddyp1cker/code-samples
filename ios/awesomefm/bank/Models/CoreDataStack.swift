//
//  CoreDataStack.swift
//  bank
//
//  Created by Iurii Kuznetcov on 16/04/17.
//  Copyright © 2017 awesomefm. All rights reserved.
//

import Foundation
import CoreData

import CocoaLumberjack

class CoreDataStack {
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "AwesomeFMStorage")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                DDLogError("Unresolved error \(error), \(error.userInfo), \(error.localizedDescription)")
            }
        })
        return container
    }()

    func saveContext() {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                DDLogError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}
