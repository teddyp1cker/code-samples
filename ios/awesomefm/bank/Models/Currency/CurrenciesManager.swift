//
//  CurrenciesManager.swift
//  bank
//
//  Created by Iurii Kuznetcov on 21/04/17.
//  Copyright © 2017 awesomefm. All rights reserved.
//

import Foundation
import CoreData
import UIKit

import CocoaLumberjack


class CurrenciesManager {

    private lazy var context = CoreDataStack().persistentContainer.viewContext

    private var currenciesFoundationObject: Any?

    func parseBundledCurrenciesJson(jsonFile: String) -> Bool {

        var parsed = false
        if let asset = NSDataAsset(name: jsonFile) {
            let currenciesJsonData = asset.data
            do {
                self.currenciesFoundationObject = try? JSONSerialization.jsonObject(with: currenciesJsonData, options: [])
                parsed = true
            } catch let error as NSError {
                DDLogError("Json currencies asset \(currenciesJsonData) parse exception: \(error), \(error.localizedDescription)")
                Thread.callStackSymbols.forEach {
                    DDLogError($0)
                }
            }
        }
        return parsed
    }

    func checkStoredCurrencies() -> Bool {

        var entitiesCount = NSNotFound
        let currenciesFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Currency")
        var curCount = 0

        context.performAndWait {
            do {
                curCount = try self.context.count(for: currenciesFetchRequest)
                entitiesCount = curCount
                if curCount == NSNotFound {
                    DDLogError("Can't fetch any currencies")
                }
            } catch let error as NSError {
                entitiesCount = NSNotFound
                DDLogError("Fetching currencies exception: \(error), \(error.localizedDescription)")
                Thread.callStackSymbols.forEach {
                    DDLogError($0)
                }
            }
        }
        return entitiesCount != 0
    }


    func fillCurrencies(jsonFile: String) -> Bool {

        var filled = false
        if !self.checkStoredCurrencies() {
            var symbol, name, symbolNative, decimalDigits, rounding, code, namePlural: Any?
            if self.currenciesFoundationObject is Array<AnyObject> {
                for currencyJsonRec in self.currenciesFoundationObject as! Array<AnyObject> {
                    symbol = currencyJsonRec["symbol"] as AnyObject? as? String ?? ""
                    name = currencyJsonRec["name"] as AnyObject? as? String ?? ""
                    symbolNative = currencyJsonRec["symbol_native"] as AnyObject? as? String ?? ""
                    decimalDigits = currencyJsonRec["decimal_digits"] as AnyObject? as? Int ?? 0
                    rounding = currencyJsonRec["rounding"] as AnyObject? as? Int ?? 0
                    code = currencyJsonRec["code"] as AnyObject? as? String ?? ""
                    namePlural = currencyJsonRec["name_plural"] as AnyObject? as? String ?? ""

                    let currencyDescription = NSEntityDescription.entity(forEntityName: "Currency", in: context)
                    let currencyManagedObject = NSManagedObject(entity: currencyDescription!, insertInto: context)

                    currencyManagedObject.setValue(symbol, forKey: "symbol")
                    currencyManagedObject.setValue(name, forKey: "name")
                    currencyManagedObject.setValue(symbolNative, forKey: "symbol_native")
                    currencyManagedObject.setValue(decimalDigits, forKey: "decimal_digits")
                    currencyManagedObject.setValue(rounding, forKey: "rounding")
                    currencyManagedObject.setValue(code, forKey: "code")
                    currencyManagedObject.setValue(namePlural, forKey: "name_plural")

                    context.performAndWait {
                        do {
                            try self.context.save()
                        } catch let error as NSError {
                            DDLogError("Could not save currency \(currencyDescription) due to \(error), \(error.userInfo), \(error.localizedDescription)")
                            Thread.callStackSymbols.forEach {
                                DDLogError($0)
                            }
                        }
                    }
                }
                filled = true
            } else {
                DDLogError("Can't cast currencies json object to list")
            }
        } else {
            DDLogWarn("Some currencies were already found")
        }
        return filled
    }

}
