//
//  ColorConstants.swift
//  bank
//
//  Created by Iurii Kuznetcov on 26/05/2017.
//  Copyright © 2017 awesomefm. All rights reserved.
//

import Foundation

struct ColorConstants {

    static let sharedInstance = ColorConstants()

    public private(set) var accountsTabHeaderColor = "#00BC9C"

    public private(set) var transactionsTabHeaderColor = "#0081B6"

    public private(set) var categoriesTabHeaderColor = "#ED4940"

    public private(set) var graphsTabHeaderColor = "#17AE64"

    public private(set) var aboutTabHeaderColor = "#FFAE31"
}
