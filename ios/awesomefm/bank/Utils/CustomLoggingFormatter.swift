//
//  CustomLoggingFormatter.swift
//  bank
//
//  Created by Iurii Kuznetcov on 25/05/2017.
//  Copyright © 2017 awesomefm. All rights reserved.
//

import Foundation

import CocoaLumberjack

class CustomLoggingFormatter: NSObject, DDLogFormatter {

    private let dateSelectorDateFormat = "yyyy-MM-dd HH:mm:ss:SSS"
    private let dateFormatter = DateFormatter()

    func format(message logMessage: DDLogMessage) -> String? {

        var logLevel = ""
        var logMessageString = ""

        dateFormatter.dateFormat = dateSelectorDateFormat

        switch logMessage.flag {

        case DDLogFlag.info: logLevel = "[INFO]:"; break;
        case DDLogFlag.debug: logLevel = "[DEBUG]:"; break;
        case DDLogFlag.warning: logLevel = "[WARNING]:"; break;
        case DDLogFlag.error: logLevel = "[ERROR]:"; break;
        default: logLevel = "[VERBOSE]:"; break;
        }

        if let function = logMessage.function {
            logMessageString = "bank: (\(dateFormatter.string(from: Date())) \(logLevel) \(logMessage.fileName).\(logMessage.function!):\(logMessage.line) - \(logMessage.message)"
        } else {
            logMessageString = "bank: (\(dateFormatter.string(from: Date())) \(logLevel) \(logMessage.fileName):\(logMessage.line) - \(logMessage.message)"
        }

        return logMessageString
    }
}
