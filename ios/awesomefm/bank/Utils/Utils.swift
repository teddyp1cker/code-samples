//
//  Utils.swift
//  bank
//
//  Created by Iurii Kuznetcov on 27/04/17.
//  Copyright © 2017 awesomefm. All rights reserved.
//

import Foundation
import UIKit

import CocoaLumberjack

import Hue

extension Date {

    var startOfWeek: Date {
        let date = Calendar.current.date(from: Calendar.current.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self))!
        let destTimeOffset = NSTimeZone.local.daylightSavingTimeOffset(for: date)
        return Calendar.current.date(byAdding: .day, value: 1, to: date)!
    }

    var endOfWeek: Date {
        return Calendar.current.date(byAdding: .day, value: 6, to: self.startOfWeek)!
    }

    var today: Date {
        return self
    }

    var startOfMonth: Date {
        return Calendar.current.date(from: Calendar.current.dateComponents([.year, .month], from: Calendar.current.startOfDay(for: self)))!
    }

    var endOfMonth: Date {
        return Calendar.current.date(byAdding: DateComponents(month: 1, day: -1), to: self.startOfMonth)!
    }
}


class Utils {

    static func convertStringToDecimal(stringNumber: String?) -> NSDecimalNumber? {

        var decimalValue: NSDecimalNumber?
        let formatter = NumberFormatter()
        formatter.generatesDecimalNumbers = true
        formatter.numberStyle = NumberFormatter.Style.decimal
        if let formattedNumber = formatter.number(from: stringNumber!) as? NSDecimalNumber {
            decimalValue = formattedNumber
        }
        return decimalValue
    }

    static func chooseForegroundByBackgroundColor(backgroundColor: UIColor?) -> UIColor {

        var foregroundColor = UIColor.darkGray
        if let backgroundColor = backgroundColor {
            if backgroundColor.isDark {
                foregroundColor = UIColor.white
            }
        }
        return foregroundColor
    }
}

extension UIView {
    
    func shake() {
        let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        animation.duration = 0.6
        animation.values = [-20.0, 20.0, -20.0, 20.0, -10.0, 10.0, -5.0, 5.0, 0.0 ]
        layer.add(animation, forKey: "shake")
    }

    func fadeIn(_ duration: TimeInterval = 0.5, delay: TimeInterval = 0.0, completion: @escaping ((Bool) -> Void) = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: UIViewAnimationOptions.curveEaseIn, animations: {
            self.alpha = 1.0
        }, completion: completion)  }

    func fadeOut(_ duration: TimeInterval = 0.5, delay: TimeInterval = 0.0, completion: @escaping (Bool) -> Void = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: UIViewAnimationOptions.curveEaseIn, animations: {
            self.alpha = 0.0
        }, completion: completion)
    }
}

func getDocumentsDirectoryURL() -> URL? {

    let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
    let documentsDirectory = paths[0]
    return documentsDirectory
}

func setAppIconNotificationBadge(shouldNotify: Bool) {

    if shouldNotify {
        UIApplication.shared.applicationIconBadgeNumber = 1
    } else {
        UIApplication.shared.applicationIconBadgeNumber = 0
    }
}
