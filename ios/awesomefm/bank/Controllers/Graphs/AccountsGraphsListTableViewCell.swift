//
//  AccountsGraphsListTableViewCell.swift
//  bank
//
//  Created by Martin Cmarko on 09/05/2017.
//  Copyright © 2017 awesomefm. All rights reserved.
//

import Foundation
import UIKit

class AccountsGraphsListTableViewCell: UITableViewCell {
    
    var account: Account?

    @IBOutlet weak var accountTitleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
