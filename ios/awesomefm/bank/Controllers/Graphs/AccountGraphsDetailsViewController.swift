import Foundation
import UIKit
import CoreData
import Charts
import CocoaLumberjack

class AccountGraphsDetailsViewController: UIViewController, NSFetchedResultsControllerDelegate, GraphsFilterPredicateSetDelegate {

    @IBOutlet weak var pieChartView: PieChartView!
    
    var graphsFilterPredicate: NSPredicateWrapper?

    @IBOutlet weak var openAccountFilterNavButton: UIBarButtonItem!


    @IBOutlet weak var headerLabel: UILabel!

    var account: Account?
    
    private let persistanceManager = PersistanceManager.sharedInstance

    private let userDefaults = UserDefaults.standard
    
    override open func viewDidLoad() {

        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector:"openPINLockViewIfNeeded", name:
            NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
    }

    func openPINLockViewIfNeeded(){

        let pinEnabled = userDefaults.bool(forKey: "security_pin_lock_enabled")
        if pinEnabled {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let vc = (
                storyBoard.instantiateViewController(
                    withIdentifier: "CSASLandingLoginView")
            )
            vc.modalTransitionStyle = .crossDissolve
            present(vc, animated: false, completion: nil)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {

        headerLabel.textColor = UIColor(hex: ColorConstants.sharedInstance.graphsTabHeaderColor)

        if persistanceManager.getCachedAccountsCount() != 0 {
            if let graphsFilterPredicate = graphsFilterPredicate {
                if graphsFilterPredicate.predicateContent["account"] != nil {
                    openAccountFilterNavButton.title = (graphsFilterPredicate.predicateContent["account"] as! Account).title
                } else {
                    openAccountFilterNavButton.title = persistanceManager.getDefaultAccount()![0].title
                }
                persistanceManager.filterTransactions(predicate: graphsFilterPredicate.predicate)
                persistanceManager.reloadAccountsCache()
                persistanceManager.reloadCurrenciesCache()
                persistanceManager.reloadCustomCategoriesCache()
                //persistanceManager.reloadAllCategoriesCache()
            } else {
                persistanceManager.fillCache()
                if persistanceManager.getDefaultAccount()!.count >= 1 {
                    account = persistanceManager.getDefaultAccount()![0]
                    DDLogWarn("Account title \(account?.title!) for account : \(account)")
                }
            }
            loadGraphsData()
            //switchAccountsListVisibility(listIsVisible: false)
            self.pieChartView.animate(xAxisDuration: 0.0, yAxisDuration: 1.0)
            self.pieChartView.drawHoleEnabled = false
        } else {
            //switchAccountsListVisibility(listIsVisible: true)
        }

        setupNavigationHeaderColorScheme(backgroundColor: UIColor(hex: ColorConstants.sharedInstance.graphsTabHeaderColor), textColor: UIColor.white)
    }


    private func setupNavigationHeaderColorScheme(backgroundColor: UIColor, textColor: UIColor) {

        self.navigationController?.navigationBar.barTintColor = backgroundColor
        self.navigationController?.navigationBar.tintColor = textColor
        self.navigationController!.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: textColor]
    }

    @IBAction func filterButtonTapped(_ sender: UIBarButtonItem) {

        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let transactionsFilterViewController = storyBoard.instantiateViewController(withIdentifier: "FilterTransactionsListView") as! TransactionsListFilterViewController
        transactionsFilterViewController.graphFilter = true
        transactionsFilterViewController.setSelectedAccount(account: account)
        transactionsFilterViewController.graphsDelegate = self
        let transactionsFilterNavController = UINavigationController(rootViewController: transactionsFilterViewController)
        self.navigationController?.present(transactionsFilterNavController, animated: true, completion: nil)
    }
    
    private func loadGraphsData() {

        var chartData: Dictionary<String,Double> = [:]
        var chartColors: Array<NSUIColor> = []
        
        if let allCategories = persistanceManager.getAllCachedCustomCategories() {
            for category in allCategories {
                chartData[category.title!] = 0
            }
            
            chartColors = allCategories.map {NSUIColor(hex: $0.color!)}
            
            if let allTransactions = persistanceManager.getAllCachedTransactions() {
                
                for transaction in allTransactions {

                    if transaction.category?.title != nil {
                    chartData[(transaction.category?.title)!] = chartData[(transaction.category?.title)!]?.adding((transaction.value?.doubleValue)!)
                    }
                }
            }
        }
        
        let yse1 = chartData.map { x, y in return PieChartDataEntry(value: y, label: x) }
        
        let data = PieChartData()
        let ds1 = PieChartDataSet(values: yse1, label: "")
        
        ds1.colors = chartColors
        
        let format = NumberFormatter()
        format.positivePrefix = "- "
        let formatter = DefaultValueFormatter(formatter: format)
        ds1.valueFormatter = formatter
        
        data.addDataSet(ds1)
        
        self.pieChartView.data = data
        
        self.pieChartView.data?.setValueTextColor(NSUIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 1.0))
        
        self.pieChartView.chartDescription?.text = "Expenses in Categories"
    }
    
    func graphsFilterPredicateWillSet(filterGraphsPredicate: NSPredicateWrapper?) {
        graphsFilterPredicate = filterGraphsPredicate
    }
    
}
