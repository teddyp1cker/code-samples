//
//  GraphsAccountsListViewController.swift
//  bank
//
//  Created by Martin Cmarko on 09/05/2017.
//  Copyright © 2017 awesomefm. All rights reserved.
//

import UIKit
import CoreData
import CocoaLumberjack

class GraphsAccountsListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var accountsListTableView: UITableView!
    
    private let persistanceManager = PersistanceManager.sharedInstance

    private let userDefaults = UserDefaults.standard

    override func viewDidLoad() {
        super.viewDidLoad()
        super.viewDidLoad()
        accountsListTableView.delegate = self
        accountsListTableView.dataSource = self

        NotificationCenter.default.addObserver(self, selector:"openPINLockViewIfNeeded", name:
            NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
    }

    override func viewWillAppear(_ animated: Bool) {
        
        if persistanceManager.getCachedAccountsCount() == 0 {
            //switchAccountsListVisibility(listIsVisible: false)
        } else {
            accountsListTableView.reloadData()
            //switchAccountsListVisibility(listIsVisible: true)
        }
        
    }

    func openPINLockViewIfNeeded(){

        let pinEnabled = userDefaults.bool(forKey: "security_pin_lock_enabled")
        if pinEnabled {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let vc = (
                storyBoard.instantiateViewController(
                    withIdentifier: "CSASLandingLoginView")
            )
            vc.modalTransitionStyle = .crossDissolve
            present(vc, animated: false, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return persistanceManager.getCachedAccountsCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "AccountsGraphsListTableViewCell", for: indexPath) as! AccountsGraphsListTableViewCell
        
        if let account = persistanceManager.getCachedAccount(index: indexPath.row) {
            cell.account = account
            
            if let accountTitle = account.title {
                cell.accountTitleLabel?.text = accountTitle
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let account = persistanceManager.getCachedAccount(index: indexPath.row)
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let accountGraphsDetailsViewController = storyBoard.instantiateViewController(withIdentifier: "AccountGraphsView") as! AccountGraphsDetailsViewController
        accountGraphsDetailsViewController.account = account
        let accountGraphsDetailsNavController = UINavigationController(rootViewController: accountGraphsDetailsViewController)
        self.navigationController?.present(accountGraphsDetailsNavController, animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
