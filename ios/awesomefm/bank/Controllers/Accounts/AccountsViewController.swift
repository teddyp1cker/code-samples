import Foundation
import UIKit
import CoreData

import CocoaLumberjack

import Hue

class AccountsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var accountsListTableView: UITableView!

    @IBOutlet weak var accountsListHeaderLabel: UILabel!

    @IBOutlet weak var emptyAccountsListPlaceholderImageView: UIImageView!

    @IBOutlet weak var emptyAccountsListPlaceholderLabel: UILabel!

    private let defaultCurrencyCode = "USD"

    private let persistanceManager = PersistanceManager.sharedInstance

    private let userDefaults = UserDefaults.standard

    override func viewDidLoad() {

        super.viewDidLoad()
        accountsListTableView.delegate = self
        accountsListTableView.dataSource = self

        NotificationCenter.default.addObserver(self, selector:"openPINLockViewIfNeeded", name:
            NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
    }

    func openPINLockViewIfNeeded(){

        let pinEnabled = userDefaults.bool(forKey: "security_pin_lock_enabled")
        if pinEnabled {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let vc = (
                storyBoard.instantiateViewController(
                    withIdentifier: "CSASLandingLoginView")
            )
            vc.modalTransitionStyle = .crossDissolve
            present(vc, animated: false, completion: nil)
        }
    }

    override func viewWillAppear(_ animated: Bool) {

        fillAccountsCache()
        if persistanceManager.getCachedAccountsCount() == 0 {
            DDLogInfo("Accounts list is empty")
            switchAccountsListVisibility(listIsVisible: false)
        } else {
            accountsListTableView.reloadData()
            switchAccountsListVisibility(listIsVisible: true)
        }

        setupNavigationHeaderColorScheme(backgroundColor: UIColor(hex: ColorConstants.sharedInstance.accountsTabHeaderColor), textColor: UIColor.white)
    }

    private func setupNavigationHeaderColorScheme(backgroundColor: UIColor, textColor: UIColor) {

        self.navigationController?.navigationBar.barTintColor = backgroundColor
        self.navigationController?.navigationBar.tintColor = textColor
        self.navigationController!.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: textColor]
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return persistanceManager.getCachedAccountsCount()
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "AccountsListTableViewCell", for: indexPath) as! AccountsListTableViewCell

        if let account = persistanceManager.getCachedAccount(index: indexPath.row) {
            cell.account = account

            if let accountTitle = account.title {
                cell.accountTitleLabel?.text = accountTitle
            }

            if let accountAmount = account.amount {
                if let currency = account.currency {
                    cell.accountAmountLabel?.text = "\(String(describing: accountAmount)) \(account.currency?.symbol_native)"
                    if let nativeSymbol = currency.symbol_native {
                        cell.accountAmountLabel?.text = "\(String(describing: accountAmount)) \(String(describing: nativeSymbol))"
                    } else {
                        cell.accountAmountLabel?.text = "\(String(describing: accountAmount))"
                    }
                } else {
                    let defaultCurrency = persistanceManager.getCachedCurrencies(code: defaultCurrencyCode)?[0]
                    if let defaultCurrency = defaultCurrency {
                        if let nativeSymbol = defaultCurrency.symbol_native {
                            cell.accountAmountLabel?.text = "\(String(describing: accountAmount)) \(String(describing: nativeSymbol))"
                        } else {
                            cell.accountAmountLabel?.text = "\(String(describing: accountAmount))"
                        }
                    }
                }
            }

            if account.is_default {
                cell.accountDefaultMarkButton?.setImage(UIImage(named: "star_mark_selected"), for: UIControlState.normal)
            } else {
                cell.accountDefaultMarkButton?.setImage(UIImage(named: "star_mark_unselected"), for: UIControlState.normal)
            }

            if let colorTagValue = account.color_tag {
                cell.accountColorTagLabel.layer.cornerRadius = cell.accountColorTagLabel.frame.height / 2
                cell.accountColorTagLabel.clipsToBounds = true
                cell.accountColorTagLabel.backgroundColor = UIColor(hex: colorTagValue)
                cell.accountColorTagLabel.isHidden = false
            } else {
                DDLogInfo("\(account) doesn't have any color")
                cell.accountColorTagLabel.isHidden = true
            }
        }
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let account = persistanceManager.getCachedAccount(index: indexPath.row)
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let accountDetailsViewController = storyBoard.instantiateViewController(withIdentifier: "AccountEditView") as! AccountDetailsViewController
        accountDetailsViewController.account = account
        let accountDetailsNavController = UINavigationController(rootViewController: accountDetailsViewController)
        self.navigationController?.present(accountDetailsNavController, animated: true, completion: nil)
    }

    func fillAccountsCache() {

        do {
            persistanceManager.fillCache()
            DDLogInfo("Accounts and currencies Cache was succefully updated")
        } catch {
            let nserror = error as NSError
            DDLogError("Unresolved error \(nserror), \(nserror.userInfo), \(nserror.localizedDescription)")
        }
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {

        if editingStyle == .delete {
            let account = persistanceManager.getCachedAccount(index: indexPath.row)
            persistanceManager.deleleAccount(account: account)
            persistanceManager.reloadAccountsCache()
            DDLogInfo("\(account) was succefully deleted")

            if persistanceManager.getCachedAccountsCount() == 0 {
                switchAccountsListVisibility(listIsVisible: false)
            } else {
                tableView.reloadData()
                switchAccountsListVisibility(listIsVisible: true)
            }
        }
        tableView.reloadData()
        DDLogInfo("accounts list was succefully reloaded")
    }

    private func switchAccountsListVisibility(listIsVisible: Bool) {

        accountsListTableView.isHidden = !listIsVisible
        accountsListHeaderLabel.isHidden = !listIsVisible
        emptyAccountsListPlaceholderImageView.isHidden = listIsVisible
        emptyAccountsListPlaceholderLabel.isHidden = listIsVisible
    }

}
