//
//  AccountsListTableViewCell.swift
//  bank
//
//  Created by Iurii Kuznetcov on 17/04/17.
//  Copyright © 2017 awesomefm. All rights reserved.
//

import Foundation
import UIKit

class AccountsListTableViewCell: UITableViewCell {

    var account: Account?

    @IBOutlet weak var accountTitleLabel: UILabel!
    @IBOutlet weak var accountAmountLabel: UILabel!
    @IBOutlet weak var accountColorTagLabel: UILabel!
    @IBOutlet weak var accountDefaultMarkButton: UIButton!

    @IBAction func defaultMarkButtonTapped(_ sender: UIButton) {

        /*if let accountIsMarkedAsDefault = account?.is_default {
            if accountIsMarkedAsDefault {
                accountDefaultMarkButton?.setImage(UIImage(named: "star_mark_unselected"), for: UIControlState.normal)
            } else {
                accountDefaultMarkButton?.setImage(UIImage(named: "star_mark_selected"), for: UIControlState.normal)
            }
        }*/
    }

}
