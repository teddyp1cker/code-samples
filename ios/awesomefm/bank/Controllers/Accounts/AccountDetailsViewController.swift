//
//  AccountDetailsViewController.swift
//  bank
//
//  Created by Iurii Kuznetcov on 17/04/17.
//  Copyright © 2017 awesomefm. All rights reserved.
//

import Foundation
import UIKit

import CocoaLumberjack

import RappleColorPicker

import AnimatedTextInput

import DropDown

enum AccountEditProgressState {
    case inProgress
    case error
    case completed
    case save
}

struct AccountDetailsTextInputStyle: AnimatedTextInputStyle {

    let activeColor = UIColor.gray
    let inactiveColor = UIColor.gray.withAlphaComponent(0.5)
    let lineInactiveColor = UIColor.gray.withAlphaComponent(0.5)
    let errorColor = UIColor.red
    let textInputFont = UIFont.systemFont(ofSize: 21)
    let textInputFontColor = UIColor.black
    let placeholderMinFontSize: CGFloat = 14
    let counterLabelFont: UIFont? = UIFont.systemFont(ofSize: 9)
    let leftMargin: CGFloat = 0
    let topMargin: CGFloat = 30
    let rightMargin: CGFloat = 0
    let bottomMargin: CGFloat = 10
    let yHintPositionOffset: CGFloat = 7
    let yPlaceholderPositionOffset: CGFloat = 0
    public let textAttributes: [String: Any]? = nil
}


class AccountDetailsViewController: UIViewController, RappleColorPickerDelegate {

    @IBOutlet weak var accountTitleAnimatedTextField: AnimatedTextInput!
    @IBOutlet weak var accountAmountAnimatedTextField: AnimatedTextInput!

    @IBOutlet weak var accountEditDefaultLabel: UILabel!
    @IBOutlet weak var accountEditDefaultSwitch: UISwitch!

    @IBOutlet weak var colorTagButton: UIButton!

    @IBOutlet weak var headerView: UIView!

    @IBOutlet weak var accountLogoImageView: UIImageView!

    @IBOutlet weak var newAccountHeaderIndicator: UILabel!

    @IBOutlet weak var chooseCurrencyButton: UIButton!

    private var selectedCurrencyIndex: Index?

    private let persistanceManager = PersistanceManager.sharedInstance

    private var accountEditMode: AccountEditMode = AccountEditMode.defaultMode

    private var accountEditProgress : AccountEditProgressState = AccountEditProgressState.inProgress

    // TODO: move to NSPreferences
    private let defaultCurrencyCode = "USD"

    private let userDefaults = UserDefaults.standard

    var account: Account?

    private var selectedColorString: String?

    private let chooseCurrencyDropDown = DropDown()

    override func viewWillAppear(_ animated: Bool) {

        setAppIconNotificationBadge(shouldNotify: true)

        setupNavigationHeaderColorScheme(backgroundColor: UIColor(hex: ColorConstants.sharedInstance.accountsTabHeaderColor), textColor: UIColor.white)

        fillHeaderViewWithGradient(fromColor: UIColor(hex: ColorConstants.sharedInstance.accountsTabHeaderColor), toColor: UIColor(hex: ColorConstants.sharedInstance.accountsTabHeaderColor))

        super.viewWillAppear(animated)
    }

    private func setupNavigationHeaderColorScheme(backgroundColor: UIColor, textColor: UIColor) {

        self.navigationController?.navigationBar.barTintColor = backgroundColor
        self.navigationController?.navigationBar.tintColor = textColor
        self.navigationController!.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: textColor]
    }

    private func fillHeaderViewWithGradient(fromColor: UIColor, toColor: UIColor) {

        let layer = CAGradientLayer()
        layer.frame = CGRect(x: 0, y: 0, width: headerView.frame.width, height: 80)
        layer.colors = [fromColor.cgColor, toColor.cgColor]

        headerView.layer.insertSublayer(layer, at: 0)
    }

    func rotateHandler() {
        if UIDevice.current.orientation.isLandscape {
        } else {
        }

        fillHeaderViewWithGradient(fromColor: UIColor(hex: ColorConstants.sharedInstance.accountsTabHeaderColor), toColor: UIColor(hex: ColorConstants.sharedInstance.accountsTabHeaderColor))
    }

    override func viewDidLoad() {

        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: #selector(AccountDetailsViewController.rotateHandler), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)

        accountEditMode = getAccountEditMode(account: self.account)

        switchDefaultSwitcherState(account: self.account)
        switchAccountTitleTextFieldState(account: self.account)
        switchAccountAmountTextFieldState(account: self.account)
        switchColorPickerState(account: self.account)

        setupCurrencyDropDown(anchor: chooseCurrencyButton, items: persistanceManager.getAllCachedCurrencies()!.map {
            $0.symbol_native! + " - " + $0.name_plural!
        }, account: self.account)

        NotificationCenter.default.addObserver(self, selector:"openPINLockViewIfNeeded", name:
            NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
    }

    func openPINLockViewIfNeeded(){

        let pinEnabled = userDefaults.bool(forKey: "security_pin_lock_enabled")
        if pinEnabled {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let vc = (
                storyBoard.instantiateViewController(
                    withIdentifier: "CSASLandingLoginView")
            )
            vc.modalTransitionStyle = .crossDissolve
            present(vc, animated: false, completion: nil)
        }
    }

    private func getAccountEditMode(account: Account?) -> AccountEditMode {

        var mode = AccountEditMode.defaultMode
        if let account = account {
            mode = AccountEditMode.existingAccount
        } else {
            mode = AccountEditMode.newAccount
        }
        return mode
    }

    private func switchDefaultSwitcherState(account: Account?) {

        if let account = account {
            accountEditDefaultSwitch.isOn = account.is_default
        }
    }

    private func switchAccountTitleTextFieldState(account: Account?) {

        accountTitleAnimatedTextField.style = AccountDetailsTextInputStyle()
        accountTitleAnimatedTextField.placeHolderText = "Account title"

        if let account = account {
            accountTitleAnimatedTextField.text = account.title
        }
    }

    private func switchAccountAmountTextFieldState(account: Account?) {

        accountAmountAnimatedTextField.style = AccountDetailsTextInputStyle()
        accountAmountAnimatedTextField.placeHolderText = "Account amount"

        // disabled due to set text bug (?)
        // accountAmountAnimatedTextField.type = .numeric

        if let account = account {
            if let amountDecimal = account.amount {
                accountAmountAnimatedTextField.text = (String(describing: amountDecimal))
            } else {
                accountAmountAnimatedTextField.text = "Account amount is not set"
            }
        }

    }

    private func switchColorPickerState(account: Account?) {

        if let account = account {
            if let accountColor = account.color_tag {
                if !accountColor.isEmpty {
                    switchColorTagButtonAppearance(cornerRadius: 18,
                                                   clipToBounds: true,
                                                   borderWidth: 1,
                                                   borderColor: UIColor(hex: account.color_tag!),
                                                   backgroundColor: UIColor(hex: account.color_tag!),
                                                   title: "")
                } else {
                    switchColorTagButtonAppearance(cornerRadius: 18,
                                                   clipToBounds: true,
                                                   borderWidth: 1,
                                                   borderColor: UIColor.darkGray,
                                                   backgroundColor: UIColor.white,
                                                   title: "?")

                }
            } else {
                switchColorTagButtonAppearance(cornerRadius: 18,
                                               clipToBounds: true,
                                               borderWidth: 1,
                                               borderColor: UIColor.darkGray,
                                               backgroundColor: UIColor.white,
                                               title: "?")
            }
            newAccountHeaderIndicator.text = ""

        } else {
            newAccountHeaderIndicator.text = "+"
            switchColorTagButtonAppearance(cornerRadius: 18,
                                           clipToBounds: true,
                                           borderWidth: 1,
                                           borderColor: UIColor.darkGray,
                                           backgroundColor: UIColor.white,
                                           title: "?")
        }
    }


    private func switchColorTagButtonAppearance(cornerRadius: CGFloat,
                                                clipToBounds: Bool,
                                                borderWidth: CGFloat,
                                                borderColor: UIColor,
                                                backgroundColor: UIColor,
                                                title: String) {

        colorTagButton.layer.cornerRadius = cornerRadius
        colorTagButton.clipsToBounds = clipToBounds
        colorTagButton.layer.borderWidth = borderWidth
        colorTagButton.layer.borderColor = borderColor.cgColor
        colorTagButton.backgroundColor = backgroundColor
        colorTagButton.setTitle(title, for: .normal)

    }

    func colorSelected(_ color: UIColor) {

        selectedColorString = color.hex()
        switchColorTagButtonAppearance(cornerRadius: 18,
                                       clipToBounds: true,
                                       borderWidth: 1,
                                       borderColor: UIColor(hex: selectedColorString!),
                                       backgroundColor: UIColor(hex: selectedColorString!),
                                       title: "")

        RappleColorPicker.close()
    }

    private func setupCurrencyDropDown(anchor: UIView, items: [String], account: Account?) {

        chooseCurrencyDropDown.anchorView = chooseCurrencyButton
        chooseCurrencyDropDown.bottomOffset = CGPoint(x: 0, y: anchor.bounds.height + 10)
        chooseCurrencyDropDown.direction = .bottom
        chooseCurrencyDropDown.dismissMode = .automatic

        chooseCurrencyButton.setTitleColor(UIColor(hex: ColorConstants.sharedInstance.accountsTabHeaderColor), for: .normal)

        chooseCurrencyDropDown.dataSource = items

        let appearance = DropDown.appearance()
        appearance.cellHeight = 40
        appearance.backgroundColor = UIColor(white: 1, alpha: 1)
        appearance.selectionBackgroundColor = UIColor(hex: ColorConstants.sharedInstance.accountsTabHeaderColor).alpha(0.25)
        appearance.cornerRadius = 10
        appearance.shadowColor = UIColor(white: 0.6, alpha: 1)
        appearance.shadowOpacity = 0.9
        appearance.shadowRadius = 20
        appearance.animationduration = 0.25
        appearance.textColor = .darkGray

        if let account = account {
            if let currency = account.currency {
                if let cachedCurrencyIndex = persistanceManager.getCachedCurrencyIndex(code: currency.code) {
                    chooseCurrencyDropDown.selectRow(at: cachedCurrencyIndex)
                    chooseCurrencyButton.setTitle(currency.symbol_native! + " - " + currency.name_plural!, for: .normal)
                }

            }
        } else {
            chooseCurrencyDropDown.selectRow(at: 0)
            chooseCurrencyButton.setTitle(items[0], for: .normal)
        }

        chooseCurrencyDropDown.selectionAction = { [unowned self] (index, item) in
            self.chooseCurrencyButton.setTitle(item, for: .normal)
            self.selectedCurrencyIndex = self.chooseCurrencyDropDown.indexForSelectedRow
        }


        chooseCurrencyDropDown.cancelAction = { [unowned self] in
            if self.selectedCurrencyIndex == nil && self.accountEditMode == AccountEditMode.newAccount {
                self.chooseCurrencyDropDown.selectRow(at: 0)
                self.chooseCurrencyButton.setTitle(items[0], for: .normal)
                self.selectedCurrencyIndex = 0
            }
        }
    }

    @IBAction func chooseCurrencyButtonTapped(_ sender: UIButton) {

        accountEditProgress = AccountEditProgressState.inProgress

        chooseCurrencyDropDown.show()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func cancelButtonTapped(_ sender: UIBarButtonItem) {

        self.dismiss(animated: true, completion: {
            setAppIconNotificationBadge(shouldNotify: false)
            DDLogInfo("Account details view closed")
        })
    }


    private func closeView(saveResult: EntitySaveResult?) {

        if let saveResult = saveResult {
            if saveResult.succeed {
                accountEditProgress = AccountEditProgressState.save

                setAppIconNotificationBadge(shouldNotify: false)
                self.dismiss(animated: true)
            } else {
                accountEditProgress = AccountEditProgressState.inProgress
                showErrorPopup(title: "Save account error", description: (saveResult.nserror?.localizedDescription)!)
            }
        }

    }

    private func showErrorPopup(title: String, description: String) {

        let alert = UIAlertController(title: title, message: description, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

    private func validateAccountAmountField() {

        let updatedAccountAmount = stringToDecimal(stringNumber: accountAmountAnimatedTextField.text)

        if let updatedAccountAmount = updatedAccountAmount {
            if (updatedAccountAmount.compare(NSDecimalNumber.zero) == ComparisonResult.orderedAscending
                || (updatedAccountAmount.compare(NSDecimalNumber.zero) == ComparisonResult.orderedSame)) {
                switchEnvelope(textField: accountAmountAnimatedTextField,
                               state: TextFieldValidityState.error,
                               errorText: "Amount can't be less or equal to zero",
                               placeholderText: "Please type valid value")

                accountEditProgress = AccountEditProgressState.error
            }
        } else {
            switchEnvelope(textField: accountAmountAnimatedTextField,
                           state: TextFieldValidityState.error,
                           errorText: "Amount can't be less or equal to zero",
                           placeholderText: "Please type amount value")

            accountEditProgress = AccountEditProgressState.error
        }
    }

    private func validateAccountTitieField() {

        if accountTitleAnimatedTextField.text!.isEmpty {
            switchEnvelope(textField: accountTitleAnimatedTextField,
                           state: TextFieldValidityState.error,
                           errorText: "Title can't be empty",
                           placeholderText: "Please type valid title value")

            accountEditProgress = AccountEditProgressState.error
        }
    }

    @IBAction func saveButtonTapped(_ sender: UIBarButtonItem) {

        accountEditProgress = AccountEditProgressState.inProgress

        switch accountEditMode {
        case .existingAccount:

            let saveResult: EntitySaveResult?
            let updatedAccountAmount = stringToDecimal(stringNumber: accountAmountAnimatedTextField.text)

            validateAccountAmountField()
            validateAccountTitieField()

            if accountEditProgress != AccountEditProgressState.error {

                accountEditProgress = AccountEditProgressState.completed

                account?.amount = updatedAccountAmount

                account?.title = accountTitleAnimatedTextField.text
                account?.is_default = accountEditDefaultSwitch.isOn

                if let selectedColorString = selectedColorString {
                    account?.color_tag = selectedColorString
                }

                if let selectedCurrency = persistanceManager.getCachedCurrency(index: chooseCurrencyDropDown.indexForSelectedRow) {
                    account?.currency = selectedCurrency
                } else {
                    account?.currency = persistanceManager.getCachedCurrencies(code: defaultCurrencyCode)?[0]
                }

                saveResult = persistanceManager.updateAccount(account: account)
                closeView(saveResult: saveResult)
            }

        case .newAccount:

            let saveResult: EntitySaveResult?
            let newAccountAmount = stringToDecimal(stringNumber: accountAmountAnimatedTextField.text)

            validateAccountAmountField()
            validateAccountTitieField()

            if accountEditProgress != AccountEditProgressState.error {

                accountEditProgress = AccountEditProgressState.completed

                if let selectedCurrency = persistanceManager.getCachedCurrency(index: chooseCurrencyDropDown.indexForSelectedRow) {
                    saveResult = persistanceManager.createAccount(
                            title: accountTitleAnimatedTextField.text,
                            amount: newAccountAmount,
                            color_tag: selectedColorString,
                            is_default: accountEditDefaultSwitch.isOn,
                            currency: selectedCurrency)

                } else {
                    saveResult = persistanceManager.createAccount(
                            title: accountTitleAnimatedTextField.text,
                            amount: newAccountAmount,
                            color_tag: selectedColorString,
                            is_default: accountEditDefaultSwitch.isOn)
                }

                closeView(saveResult: saveResult)
            }
        default:
            DDLogInfo("Undefined behavior for account details saving")
        }
    }

    private func stringToDecimal(stringNumber: String?) -> NSDecimalNumber? {

        var decimalValue: NSDecimalNumber?
        let formatter = NumberFormatter()
        formatter.generatesDecimalNumbers = true
        formatter.numberStyle = NumberFormatter.Style.decimal
        if let formattedNumber = formatter.number(from: stringNumber!) as? NSDecimalNumber {
            decimalValue = formattedNumber
        }
        return decimalValue
    }

    private func switchEnvelope(textField: AnimatedTextInput?, state: TextFieldValidityState, errorText: String, placeholderText: String) {

        if let textField = textField {
            switch state {
            case .valid:
                break
            case .error:
                textField.show(error: errorText, placeholderText: placeholderText)
                textField.shake()
                break
            case .warning:
                textField.shake()
                break
            }
        }
    }

    @IBAction func colorTagButtonTapped(_ sender: UIButton) {

        accountEditProgress = AccountEditProgressState.inProgress

        let attributes: [RappleCPAttributeKey: AnyObject] = [
                .BGColor: UIColor.white.withAlphaComponent(1.0),
                .TintColor: UIColor.white.withAlphaComponent(1.0),
                .Style: RappleCPStyleCircle as AnyObject,
                .BorderColor: UIColor.white.withAlphaComponent(1.0)
        ]

        RappleColorPicker.openColorPallet(onViewController: self, origin: CGPoint(x: sender.frame.midX - 115, y: sender.frame.minY - 50), delegate: self, attributes: attributes)
    }

}
