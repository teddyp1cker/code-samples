//
//  AccountEditMode.swift
//  bank
//
//  Created by Iurii Kuznetcov on 21/04/17.
//  Copyright © 2017 awesomefm. All rights reserved.
//

import Foundation


enum AccountEditMode {
    case newAccount
    case existingAccount
    case defaultMode
}
