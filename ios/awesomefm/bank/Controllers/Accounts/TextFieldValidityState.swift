//
// Created by Iurii Kuznetcov on 24/04/17.
// Copyright (c) 2017 awesomefm. All rights reserved.
//

import Foundation

enum TextFieldValidityState {
    case valid
    case warning
    case error
}
