import Foundation
import UIKit
import CoreData

import CocoaLumberjack

import Hue

class TransactionsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate, TransactionsFilterPredicateSetDelegate {

    @IBOutlet weak var transactionsListTableView: UITableView!

    @IBOutlet weak var transactionsEmptyStateView: UIView!

    @IBOutlet weak var filterHeaderLabel: UILabel!

    @IBOutlet weak var resetFilterAndReloadButton: UIBarButtonItem!

    @IBOutlet weak var emptyTransactionsListPlaceholderImageView: UIImageView!

    @IBOutlet weak var emptyAccountsListPlaceholderLabel: UILabel!

    @IBOutlet weak var transactionsListHeaderLabel: UILabel!

    @IBOutlet weak var openTransactionsFilterViewButton: UIBarButtonItem!

    private let persistanceManager = PersistanceManager.sharedInstance

    private let dateFormatter = DateFormatter()

    private let userDefaults = UserDefaults.standard

    // TODO: move to NSPreferencies
    private let dateDisplayFormat = "d MMM"
    private let yearDisplayFormat = "yyyy"


    var transactionsFilterPredicate: NSPredicateWrapper? {
        didSet {
            if let transactionsFilterPredicate = transactionsFilterPredicate {
                filterHeaderLabel.text = buildFilterTransactionsIndicatorString(predicate: transactionsFilterPredicate)
            } else {
                filterHeaderLabel.text = "All transactions"
            }
        }
    }

    override func viewDidLoad() {

        super.viewDidLoad()
        transactionsListTableView.delegate = self
        transactionsListTableView.dataSource = self
        dateFormatter.dateFormat = dateDisplayFormat

        NotificationCenter.default.addObserver(self, selector:"openPINLockViewIfNeeded", name:
            NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
        
        openPINLockViewIfNeeded()
    }

    func openPINLockViewIfNeeded(){

        let pinEnabled = userDefaults.bool(forKey: "security_pin_lock_enabled")
        if pinEnabled {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let vc = (
            storyBoard.instantiateViewController(
                withIdentifier: "CSASLandingLoginView")
            )
            vc.modalTransitionStyle = .crossDissolve
            present(vc, animated: false, completion: nil)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func resetFilterAndReloadButtonTapped(_ sender: UIBarButtonItem) {

         resetTransactionsFilterPredicate()
         filterHeaderLabel.text = "All transactions"
         persistanceManager.fillCache()
         transactionsListTableView.reloadData()
         checkListTableState()

    }

    override func viewWillAppear(_ animated: Bool) {

        super.viewWillAppear(animated)

        if let transactionsFilterPredicate = transactionsFilterPredicate {
            persistanceManager.filterTransactions(predicate: transactionsFilterPredicate.predicate)
            persistanceManager.reloadAccountsCache()
            persistanceManager.reloadCurrenciesCache()
            persistanceManager.reloadCustomCategoriesCache()
        } else {
            filterHeaderLabel.text = "All transactions"
            persistanceManager.fillCache()
        }

        transactionsListTableView.reloadData()
        checkListTableState()

        setupNavigationHeaderColorScheme(backgroundColor: UIColor(hex: ColorConstants.sharedInstance.transactionsTabHeaderColor), textColor: UIColor.white)
    }

    private func setupNavigationHeaderColorScheme(backgroundColor: UIColor, textColor: UIColor) {

        self.navigationController?.navigationBar.barTintColor = backgroundColor
        self.navigationController?.navigationBar.tintColor = textColor
        self.navigationController!.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: textColor]
    }

    private func resetTransactionsFilterPredicate() {
        self.transactionsFilterPredicate = nil
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        if segue.identifier == "showFilterTransactionsView" {
            let navController = segue.destination as! UINavigationController
            let transactionsListFilterViewController = navController.topViewController as! TransactionsListFilterViewController
            transactionsListFilterViewController.delegate = self
        }
    }


    func transactionFilterPredicateWillSet(filterTransactionsPredicate: NSPredicateWrapper?) {
        transactionsFilterPredicate = filterTransactionsPredicate
    }

    private func transactionFilterIsPresent() -> Bool {
        return transactionsFilterPredicate != nil
    }

    private func buildFilterTransactionsIndicatorString(predicate: NSPredicateWrapper?) -> String {

        var filterIndicatorLabelText = "All transactions"
        if let predicate = predicate {
            filterIndicatorLabelText = "Filtered"
            if predicate.predicateContent.count != 0 {
                var startDateLabel = "∞"
                var endDateLabel = "∞"

                if (predicate.predicateContent["startDate"] != nil) {
                    startDateLabel = "\(dateFormatter.string(from: predicate.predicateContent["startDate"] as! Date))"
                }

                if (predicate.predicateContent["endDate"] != nil) {
                    endDateLabel = "\(dateFormatter.string(from: predicate.predicateContent["endDate"] as! Date))"
                }

                filterIndicatorLabelText = "\(startDateLabel) - \(endDateLabel)"
            }
        }

        return filterIndicatorLabelText
    }

    private func checkListTableState() {

        //TODO: improve missing cache handling

        if transactionFilterIsPresent() {}

        else {
            if persistanceManager.getCachedTransactionsCount() == 0 {
                transactionsListTableView.isHidden = true
                filterHeaderLabel.isHidden = true
                transactionsListHeaderLabel.isHidden = true
                emptyAccountsListPlaceholderLabel.isHidden = false
                emptyTransactionsListPlaceholderImageView.isHidden = false
            } else {
                transactionsListTableView.isHidden = false
                filterHeaderLabel.isHidden = false
                transactionsListHeaderLabel.isHidden = false
                emptyAccountsListPlaceholderLabel.isHidden = true
                emptyTransactionsListPlaceholderImageView.isHidden = true
            }
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return persistanceManager.getCachedTransactionsCount()
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        if let transaction = persistanceManager.getCachedTransaction(index: indexPath.row) {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let transactionDetilsViewController = storyBoard.instantiateViewController(withIdentifier: "EditTransactionView") as! TransactionDetailsViewController

            transactionDetilsViewController.transaction = transaction

            let transactionDetailsNavController = UINavigationController(rootViewController: transactionDetilsViewController)

            self.navigationController?.present(transactionDetailsNavController, animated: true, completion: nil)
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "TransactionTableViewCell", for: indexPath) as! TransactionTableViewCell

        if let transaction = persistanceManager.getCachedTransaction(index: indexPath.row) {

            cell.transaction = transaction
            cell.nameLabel.text = transaction.title
            cell.valueLabel.text = getFormattedTransactionValue(transaction: transaction)

            if let date = transaction.date {
                cell.dateLabel.text = "\(dateFormatter.string(from: date as Date))"
                // cell.yearLabel.text = "\(dateFormatter.string(from: date as Date))"
            }

            let transactionTransaction = getTransactionType(transaction: transaction)

            switch transactionTransaction {
            case .expense:
                cell.transactionIconView.image = UIImage(named: "expense_icon")
            case .income:
               cell.transactionIconView.image = UIImage(named: "income_icon")
            case .undefined:
                break
            }

            if indexPath.row % 2 == 0 {
                cell.backgroundColor = UIColor(hex: "#f2f1f1")
            } else {
                cell.backgroundColor = UIColor.white
            }

            switchTransactionFormattedValueTextForegroundColor(valueLabel: cell.valueLabel, transaction: transaction)

            cell.categoryButton.titleLabel?.text = transaction.category?.title

            switchAccountLabelButtonStyle(accountLabelButton: cell.accountButton, transaction: transaction)
            switchCategoryLabelButtonStyle(categoryLabelButton: cell.categoryButton, transaction: transaction)
        }

        return cell
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {

        persistanceManager.deleteTransaction(transaction: persistanceManager.getCachedTransaction(index: indexPath.row))
        transactionsListTableView.deleteRows(at: [indexPath], with: .automatic)
        checkListTableState()
    }

    private func switchAccountLabelButtonStyle(accountLabelButton: UIButton, transaction: Transaction?) {

        if let currentTransaction = transaction {
            if let currentTransactionAccount = currentTransaction.account {
                if let accountTitle = currentTransactionAccount.title {
                    accountLabelButton.setTitle(accountTitle, for: .normal)
                    if let accountColor = currentTransactionAccount.color_tag {
                        if accountColor.isEmpty {
                            accountLabelButton.backgroundColor = UIColor.white
                            accountLabelButton.layer.borderWidth = 1
                            accountLabelButton.layer.borderColor = UIColor.darkGray.cgColor
                        } else {
                            accountLabelButton.backgroundColor = UIColor(hex: accountColor)
                            accountLabelButton.layer.borderWidth = 1
                            accountLabelButton.layer.borderColor = UIColor(hex: accountColor).cgColor
                            accountLabelButton.setTitleColor(Utils.chooseForegroundByBackgroundColor(backgroundColor: UIColor(hex: accountColor)), for: .normal)
                        }
                    } else {
                        accountLabelButton.backgroundColor = UIColor.white
                        accountLabelButton.layer.borderWidth = 1
                        accountLabelButton.layer.borderColor = UIColor.darkGray.cgColor
                    }
                } else {
                    accountLabelButton.setTitle("No account", for: .normal)
                    accountLabelButton.backgroundColor = UIColor.white
                    accountLabelButton.layer.borderWidth = 1
                    accountLabelButton.layer.borderColor = UIColor.darkGray.cgColor
                }
            }
        }
    }

    private func switchCategoryLabelButtonStyle(categoryLabelButton: UIButton, transaction: Transaction?) {

        if let currentTransaction = transaction {
            if let transactionCategory = currentTransaction.category {
                if let transactionTitle = transactionCategory.title {
                    categoryLabelButton.setTitle(transactionTitle, for: .normal)
                    if let categoryColor = transactionCategory.color {
                        if categoryColor.isEmpty {
                            categoryLabelButton.backgroundColor = UIColor.white
                            categoryLabelButton.layer.borderWidth = 1
                            categoryLabelButton.layer.borderColor = UIColor.darkGray.cgColor
                        } else {
                            categoryLabelButton.backgroundColor = UIColor(hex: categoryColor)
                            categoryLabelButton.layer.borderWidth = 1
                            categoryLabelButton.layer.borderColor = UIColor(hex: categoryColor).cgColor
                            categoryLabelButton.setTitleColor(Utils.chooseForegroundByBackgroundColor(backgroundColor: UIColor(hex: categoryColor)), for: .normal)
                        }
                    } else {
                        categoryLabelButton.setTitleColor(UIColor.darkGray, for: .normal)
                        categoryLabelButton.backgroundColor = UIColor.white
                        categoryLabelButton.layer.borderWidth = 1
                        categoryLabelButton.layer.borderColor = UIColor.darkGray.cgColor
                    }
                } else {
                    categoryLabelButton.setTitleColor(UIColor.darkGray, for: .normal)
                    categoryLabelButton.setTitle("No category title", for: .normal)
                    categoryLabelButton.backgroundColor = UIColor.white
                    categoryLabelButton.layer.borderWidth = 1
                    categoryLabelButton.layer.borderColor = UIColor.darkGray.cgColor
                }
            } else {
                categoryLabelButton.setTitle("Uncategorizedt", for: .normal)
                categoryLabelButton.setTitleColor(UIColor.darkGray, for: .normal)
                categoryLabelButton.backgroundColor = UIColor.white
                categoryLabelButton.layer.borderWidth = 1
                categoryLabelButton.layer.borderColor = UIColor.darkGray.cgColor
            }
        }

        if getTransactionType(transaction: transaction) == TransactionType.income {
            categoryLabelButton.isHidden = true
        } else {
            categoryLabelButton.isHidden = false
        }
    }

    private func switchTransactionFormattedValueTextForegroundColor(valueLabel: UILabel, transaction: Transaction?) {

        if let currentTransaction = transaction {
            let type = getTransactionType(transaction: currentTransaction)
            switch type {
            case .expense:
                valueLabel.textColor = UIColor(hex: "#F1473A")
            case .income:
                valueLabel.textColor = UIColor(hex: "#00B26C")
            default:
                DDLogWarn("Undefined transaction type \(type) for transaction : \(currentTransaction)")
            }
        }
    }

    private func getTransactionType(transaction: Transaction?) -> TransactionType {

        if let transaction = transaction {
            if let type = transaction.type {
                if type.lowercased() == String(describing: TransactionType.expense).lowercased() {
                    return TransactionType.expense
                } else if type.lowercased() == String(describing: TransactionType.income).lowercased() {
                    return TransactionType.income
                } else {
                    return TransactionType.undefined
                }
            } else {
                return TransactionType.undefined
            }
        } else {
            return TransactionType.undefined
        }
    }

    func getFormattedTransactionValue(transaction: Transaction) -> String {

        var amountPrefix: String?
        var formattedTransactionString = ""

        let type = getTransactionType(transaction: transaction)
        switch type {
        case .expense:
            amountPrefix = "-"
        case .income:
            amountPrefix = "+"
        case .undefined:
            amountPrefix = "?"
        }

        if let formattedDecimalValue = formatDecimalTransactionValue(decimalValue: transaction.value) {
            if let currency_symbol = transaction.account?.currency?.symbol_native {
                if let amountPrefix = amountPrefix {
                    formattedTransactionString = "\(amountPrefix) \(formattedDecimalValue) \(currency_symbol)"
                }
            }
        }

        return formattedTransactionString
    }

    private func formatDecimalTransactionValue(decimalValue: NSDecimalNumber?) -> String? {

        var formattedDecimalValue: String?

        if let decimalValue = decimalValue {
            formattedDecimalValue = String(describing: decimalValue)
        }

        return formattedDecimalValue
    }
}

/*private extension TransactionsViewController {
    func validation(_ input: String) -> Bool {
        return input == "123456"
    }

    func validationSuccess() {
        print("*️⃣ success!")
        dismiss(animated: true, completion: nil)
    }

    func validationFail() {
        print("*️⃣ failure!")
        passwordContainerView.wrongPassword()
    }
}*/

/*extension TransactionsViewController: PasswordInputCompleteProtocol {
    func passwordInputComplete(_ passwordContainerView: PasswordContainerView, input: String) {
        if validation(input) {
            validationSuccess()
        } else {
            validationFail()
        }
    }

    func touchAuthenticationComplete(_ passwordContainerView: PasswordContainerView, success: Bool, error: Error?) {
        if success {
            self.validationSuccess()
        } else {
            passwordContainerView.clearInput()
        }
    }
}*/

@IBDesignable class MyButton: UIButton {

    override func layoutSubviews() {

        super.layoutSubviews()
        updateCornerRadius()
    }

    @IBInspectable var rounded: Bool = false {
        didSet {
            updateCornerRadius()
        }
    }

    func updateCornerRadius() {
        layer.cornerRadius = rounded ? frame.size.height / 2 : 0
    }

    override var intrinsicContentSize: CGSize {

        get {
            let labelSize = titleLabel?.sizeThatFits(CGSize(width: self.frame.size.width, height: CGFloat.greatestFiniteMagnitude)) ?? CGSize.zero
            let desiredButtonSize = CGSize(width: labelSize.width + titleEdgeInsets.left + titleEdgeInsets.right + 10, height: labelSize.height + titleEdgeInsets.top + titleEdgeInsets.bottom + 5)

            return desiredButtonSize
        }
    }
}
