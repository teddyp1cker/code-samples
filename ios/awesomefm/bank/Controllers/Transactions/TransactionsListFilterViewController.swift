import Foundation
import UIKit

import CocoaLumberjack

import DatePickerDialog

protocol TransactionsFilterPredicateSetDelegate: class {
    func transactionFilterPredicateWillSet(filterTransactionsPredicate: NSPredicateWrapper?)
}

protocol GraphsFilterPredicateSetDelegate: class {
    func graphsFilterPredicateWillSet(filterGraphsPredicate: NSPredicateWrapper?)
}

class TransactionsListFilterViewController: UIViewController {

    private let persistanceManager = PersistanceManager.sharedInstance

    // TODO: move to NSPreferencies
    private let dateSelectorDateFormat = "MMM d, yyyy"

    weak var delegate: TransactionsFilterPredicateSetDelegate? = nil

    weak var graphsDelegate: GraphsFilterPredicateSetDelegate? = nil

    @IBOutlet weak var filterTransactionsButton: UIBarButtonItem!

    @IBOutlet weak var cancelFilterTransactionsButton: UIBarButtonItem!

    @IBOutlet weak var startDatePicker: UIDatePicker!

    @IBOutlet weak var endDatePicker: UIDatePicker!

    @IBOutlet weak var accountPicker: TransactionDetailsAccountPicker!

    @IBOutlet weak var categoryPicker: TransactionDetailsCategoryPicker!

    @IBOutlet weak var todayFilterButton: UIButton!

    @IBOutlet weak var weekFilterButton: UIButton!

    @IBOutlet weak var monthFilterButton: UIButton!

    @IBOutlet weak var startDatePickerOpenButton: UIButton!

    @IBOutlet weak var endDatePickerOpenButton: UIButton!

    @IBOutlet weak var accountPickerOpenButton: UIButton!

    @IBOutlet weak var categoryPickerOpenButton: UIButton!

    @IBOutlet weak var clearSelectedAccountButton: UIButton!

    @IBOutlet weak var clearSelectedCategoryButton: UIButton!

    @IBOutlet weak var transactionTypeSegmentButton: UISegmentedControl!

    private var selectedTransactionType = TransactionType.expense

    private var selectedAccount: Account?

    private var selectedCategory: Category?

    private var selectedStartDate: Date?

    private var selectedEndDate: Date?

    private var accountPickerView: CustomPickerDialog<Account>?

    private var categoryPickerView: CustomPickerDialog<Category>?

    private let dateFormatter = DateFormatter()

    var graphFilter = false

    private let userDefaults = UserDefaults.standard

    override func viewDidLoad() {

        super.viewDidLoad()

        transactionTypeSegmentButton.tintColor = UIColor(hex: "#63819D")

        NotificationCenter.default.addObserver(self, selector:"openPINLockViewIfNeeded", name:
            NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
    }

    func openPINLockViewIfNeeded(){

        let pinEnabled = userDefaults.bool(forKey: "security_pin_lock_enabled")
        if pinEnabled {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let vc = (
                storyBoard.instantiateViewController(
                    withIdentifier: "CSASLandingLoginView")
            )
            vc.modalTransitionStyle = .crossDissolve
            present(vc, animated: false, completion: nil)
        }
    }

    override func viewWillAppear(_ animated: Bool) {

        if let acoounts = persistanceManager.getAllCachedAccounts() {
            accountPickerView = CustomPickerDialog<Account>(dataSource: acoounts) { (Account) -> String in
                return Account.title!
            }
        }

        if let account = selectedAccount {
            self.accountPicker.selectAccountByTitle(title: account.title!)
            self.accountPickerOpenButton.setTitle(account.title!, for: .normal)
        }

        if graphFilter {
            clearSelectedAccountButton.isEnabled = false
        }

        categoryPickerView = CustomPickerDialog<Category>(dataSource: persistanceManager.getAllCategories()) { (Category) -> String in
            return Category.title!
        }

        setupNavigationHeaderColorScheme(backgroundColor: UIColor(hex: "#63819D"), textColor: UIColor.white)

    }

    private func setupNavigationHeaderColorScheme(backgroundColor: UIColor, textColor: UIColor) {

        self.navigationController?.navigationBar.barTintColor = backgroundColor
        self.navigationController?.navigationBar.tintColor = textColor
        self.navigationController!.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: textColor]
    }

    @IBAction func todayButtonTapped(_ sender: UIButton) {

        dateFormatter.dateFormat = dateSelectorDateFormat
        selectedStartDate = Date().today
        selectedEndDate = Date().today

        startDatePickerOpenButton.setTitle("\(dateFormatter.string(from: selectedStartDate!))", for: .normal)
        endDatePickerOpenButton.setTitle("\(dateFormatter.string(from: selectedEndDate!))", for: .normal)
    }


    @IBAction func weekButtonTapped(_ sender: UIButton) {

        dateFormatter.dateFormat = dateSelectorDateFormat
        selectedStartDate = Date().startOfWeek
        selectedEndDate = Date().endOfWeek

        startDatePickerOpenButton.setTitle("\(dateFormatter.string(from: selectedStartDate!))", for: .normal)
        endDatePickerOpenButton.setTitle("\(dateFormatter.string(from: selectedEndDate!))", for: .normal)
    }


    @IBAction func monthButtonTapped(_ sender: UIButton) {

        dateFormatter.dateFormat = dateSelectorDateFormat
        selectedStartDate = Date().startOfMonth
        selectedEndDate = Date().endOfMonth

        startDatePickerOpenButton.setTitle("\(dateFormatter.string(from: selectedStartDate!))", for: .normal)
        endDatePickerOpenButton.setTitle("\(dateFormatter.string(from: selectedEndDate!))", for: .normal)
    }

    @IBAction func startDateButtonTapped(_ sender: UIButton) {

        DatePickerDialog().show(title: "Select start date", doneButtonTitle: "Select", cancelButtonTitle: "Cancel", datePickerMode: .date) {
            (date) -> Void in

            self.dateFormatter.dateFormat = self.dateSelectorDateFormat
            self.selectedStartDate = date
            if let date = date {
                self.startDatePickerOpenButton.setTitle("\(self.dateFormatter.string(from: date))", for: .normal)
            } else {
                self.startDatePickerOpenButton.setTitle("Any", for: .normal)
            }
        }
    }

    @IBAction func endDateButtonTapped(_ sender: UIButton) {

        DatePickerDialog().show(title: "Select end date", doneButtonTitle: "Select", cancelButtonTitle: "Cancel", datePickerMode: .date) {
            (date) -> Void in

            self.dateFormatter.dateFormat = self.dateSelectorDateFormat
            self.selectedEndDate = date
            if let date = date {
                self.endDatePickerOpenButton.setTitle("\(String(describing: self.dateFormatter.string(from: date)))", for: .normal)
            } else {
                self.endDatePickerOpenButton.setTitle("Any", for: .normal)
            }
        }
    }

    @IBAction func clearSelectedStartDateButtonTapped(_ sender: UIButton) {

        selectedStartDate = nil
        startDatePickerOpenButton.setTitle("Any", for: .normal)
    }

    @IBAction func clearSelectedEndDateButton(_ sender: UIButton) {

        selectedEndDate = nil
        endDatePickerOpenButton.setTitle("Any", for: .normal)
    }

    @IBAction func clearSelectedAccountButtonTapped(_ sender: UIButton) {

        selectedAccount = nil
        accountPickerOpenButton.setTitle("Any", for: .normal)
    }

    @IBAction func clearSelectedCategoryButtonTapped(_ sender: UIButton) {

        selectedAccount = nil
        categoryPickerOpenButton.setTitle("Any", for: .normal)
    }

    @IBAction func accountSelectOpenPickerButtonTapped(_ sender: UIButton) {

        accountPickerView?.showDialog("Select account", doneButtonTitle: "Select", cancelButtonTitle: "Cancel") { (result) -> Void in

            self.selectedAccount = result
            self.accountPickerOpenButton.setTitle(result.title!, for: .normal)
        }
    }

    @IBAction func categorySelectOpenPickerButtonTapped(_ sender: UIButton) {

        categoryPickerView?.showDialog("Select category", doneButtonTitle: "Select", cancelButtonTitle: "Cancel") { (result) -> Void in

            self.selectedCategory = result
            self.categoryPickerOpenButton.setTitle(result.title!, for: .normal)
        }
    }

    private func buildFilterPredicate() -> NSPredicateWrapper? {

        var finalFilterPredicate: NSPredicate?

        var predicates = [NSPredicate]()

        var predicateFiltersList: [String: NSPredicate] = [String: NSPredicate]()

        var predicateContent: [String: Any] = [String: Any]()

        // TODO: refactor
        if let selectedStartDate = selectedStartDate {
            predicateFiltersList["startDate"] = NSPredicate(format: "date >= %@", selectedStartDate as NSDate)
            predicateContent["startDate"] = selectedStartDate as NSDate
        }

        if let selectedEndDate = selectedEndDate {
            predicateFiltersList["endDate"] = NSPredicate(format: "date <= %@", selectedEndDate as NSDate)
            predicateContent["endDate"] = selectedEndDate as NSDate
        }

        if let selectedAccount = selectedAccount {
            predicateFiltersList["account"] = NSPredicate(format: "account = %@", selectedAccount)
            predicateContent["account"] = selectedAccount
        }

        if let selectedCategory = selectedCategory {
            predicateFiltersList["category"] = NSPredicate(format: "category = %@", selectedCategory)
            predicateContent["category"] = selectedCategory
        }


        if selectedTransactionType != TransactionType.undefined {
            predicateFiltersList["type"] = NSPredicate(format: "type = %@", String(describing: selectedTransactionType))
        }

        for (_, value) in predicateFiltersList {
            predicates.append(value)
        }


        let compoundPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: predicates)
        finalFilterPredicate = compoundPredicate


        let filterPredicateWrapper = NSPredicateWrapper(predicate: compoundPredicate)
        filterPredicateWrapper.predicateContent = predicateContent

        return filterPredicateWrapper
    }

    @IBAction func filterTransactionsButtonTapped(_ sender: UIBarButtonItem) {

        if let filterPredicate = buildFilterPredicate() {
            if !graphFilter {
                openTransactionsListView(predicate: filterPredicate)
            } else {
                openAccountGraphsView(predicate: filterPredicate)
            }
        } else {
            DDLogWarn("Empty filter predicate created.")
        }
    }

    private func openTransactionsListView(predicate: NSPredicateWrapper?) {

        if let predicate = predicate {
            delegate?.transactionFilterPredicateWillSet(filterTransactionsPredicate: predicate)

        } else {
            delegate?.transactionFilterPredicateWillSet(filterTransactionsPredicate: nil)
        }

        self.dismiss(animated: true)
    }

    private func openAccountGraphsView(predicate: NSPredicateWrapper?) {

        if let predicate = predicate {
            graphsDelegate?.graphsFilterPredicateWillSet(filterGraphsPredicate: predicate)

        } else {
            graphsDelegate?.graphsFilterPredicateWillSet(filterGraphsPredicate: nil)
        }
        self.dismiss(animated: true)
    }

    @IBAction func cancelFilterTransactionsButtonTapped(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true)
    }

    func setSelectedAccount(account: Account?) {

        if let select = account {
            DDLogWarn("Inside Account title \(String(describing: account?.title!)) for account : \(account)")
            selectedAccount = select
        }
    }

    @IBAction func transactionTypeSegmentButtonTapped(_ sender: UISegmentedControl) {

        switch (sender.selectedSegmentIndex) {
        case 0:
            selectedTransactionType = TransactionType.expense // "Expense"
            break
        case 1:
            selectedTransactionType = TransactionType.income // "Income"
            break
        case 2:
            selectedTransactionType = TransactionType.undefined // Both types
            break
        default:
            selectedTransactionType = TransactionType.undefined
            break
        }

    }

    private func switchTransactionTypeSwitchButton(transactionType: TransactionType) {

        switch transactionType {
        case .expense:
            transactionTypeSegmentButton.selectedSegmentIndex = 0
        case .income:
            transactionTypeSegmentButton.selectedSegmentIndex = 1
        case .undefined:
            transactionTypeSegmentButton.selectedSegmentIndex = 2
        default:
            transactionTypeSegmentButton.selectedSegmentIndex = 0
        }
    }

}
