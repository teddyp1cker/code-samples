//
//  TransactionDetailsAccountPicker.swift
//  bank
//
//  Created by Iurii Kuznetcov on 03/05/17.
//  Copyright © 2017 awesomefm. All rights reserved.
//

import Foundation
import UIKit

class TransactionDetailsAccountPicker : UIPickerView, UIPickerViewDataSource, UIPickerViewDelegate {

    var accounts: [Account] = [] {
        didSet {
            if accounts.count > 0 {
                selectedAccount = accounts[0]
            }
        }
    }

    var selectedAccount: Account?

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {

        return accounts.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {

        var accountTitle: String?
        accountTitle = accounts[row].title
        return accountTitle
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {

        selectedAccount = accounts[row]
    }

    func selectAccountByIndex(index: Int) {

        if index > -1 && index < accounts.count {
            self.selectRow(index, inComponent: 0, animated: false)
        }
    }

    func selectAccountByTitle(title: String) -> Bool {

        var selected = false

        if let foundIndex = accounts.index(where: { $0.title == title }) {
            selectAccountByIndex(index: foundIndex)
            selected = true
        }
        return selected
    }
}
