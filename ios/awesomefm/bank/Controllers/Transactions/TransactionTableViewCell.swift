//
//  TransactionTableViewCell.swift
//  bank
//
//  Created by Martin Cmarko on 21/04/2017.
//  Copyright © 2017 awesomefm. All rights reserved.
//

import UIKit

class TransactionTableViewCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!

    @IBOutlet weak var valueLabel: UILabel!

    @IBOutlet weak var categoryButton: MyButton!

    @IBOutlet weak var accountButton: MyButton!

    @IBOutlet weak var dateLabel: UILabel!

    @IBOutlet weak var yearLabel: UILabel!

    @IBOutlet weak var transactionIconView: UIImageView!

    var transaction: Transaction?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
