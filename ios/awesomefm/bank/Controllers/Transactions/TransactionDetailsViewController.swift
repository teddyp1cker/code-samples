import Foundation
import UIKit
import CoreData

import CocoaLumberjack

import AnimatedTextInput

import DropDown

enum TransactionEditProgressState {
    case inProgress
    case error
    case completed
    case save
}

struct TransactionDetailsTextInputStyle: AnimatedTextInputStyle {
    
    let activeColor = UIColor.gray
    let inactiveColor = UIColor.gray.withAlphaComponent(0.5)
    let lineInactiveColor = UIColor.gray.withAlphaComponent(0.5)
    let errorColor = UIColor.red
    let textInputFont = UIFont.systemFont(ofSize: 21)
    let textInputFontColor = UIColor.black
    let placeholderMinFontSize: CGFloat = 14
    let counterLabelFont: UIFont? = UIFont.systemFont(ofSize: 9)
    let leftMargin: CGFloat = 0
    let topMargin: CGFloat = 30
    let rightMargin: CGFloat = 0
    let bottomMargin: CGFloat = 10
    let yHintPositionOffset: CGFloat = 7
    let yPlaceholderPositionOffset: CGFloat = 0
    public let textAttributes: [String: Any]? = nil
}

class TransactionDetailsViewController: UIViewController /*, UIPickerViewDelegate, UIPickerViewDataSource*/ {

    @IBOutlet weak var transactionTitleAnimatedTextField: AnimatedTextInput!
    
    @IBOutlet weak var headerView: UIView!
    
    @IBOutlet weak var transactionName: UITextField!

    @IBOutlet weak var transactionValue: UITextField!

    @IBOutlet weak var accountPicker: UIPickerView!

    @IBOutlet weak var transactionTypeSwitchButton: UISegmentedControl!

    @IBOutlet weak var categoriesPicker: TransactionDetailsCategoryPicker!

    @IBOutlet weak var datePicker: UIDatePicker!

    @IBOutlet weak var currencySymbolLabel: UILabel!

    @IBOutlet weak var transactionCategoryLabel: UILabel!

    @IBOutlet weak var transactionTypePrefixLabel: UILabel!

    @IBOutlet weak var chooseAccountButton: UIButton!

    @IBOutlet weak var chooseCategoryButton: UIButton!

    private let chooseAccountDropDown = DropDown()

    private let chooseCategoryDropDown = DropDown()

    private let persistanceManager = PersistanceManager.sharedInstance

    var accounts: [Account?] = []

    var transaction: Transaction?

    var selectedAccount: Account?
    private var selectedAccountIndex: Index?

    private var selectedCategoryIndex: Index?

    private let expenseTransactionValuePrefix = "-"
    private let incomeTransactionValuePrefix = "+"

    private var selectedTransactionType = TransactionType.expense

    private var transactionEditMode = TransactionEditMode.defaultMode
    
    private var transactionEditProgress : TransactionEditProgressState = TransactionEditProgressState.inProgress

    private let userDefaults = UserDefaults.standard

    override public var prefersStatusBarHidden: Bool {
        return false
    }

    override public var preferredStatusBarStyle: UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }

    func openPINLockViewIfNeeded(){

        let pinEnabled = userDefaults.bool(forKey: "security_pin_lock_enabled")
        if pinEnabled {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let vc = (
                storyBoard.instantiateViewController(
                    withIdentifier: "CSASLandingLoginView")
            )
            vc.modalTransitionStyle = .crossDissolve
            present(vc, animated: false, completion: nil)
        }
    }

    override func viewWillAppear(_ animated: Bool) {

        if (persistanceManager.getCachedAccountsCount() < 1) {
            accountNotSet()
        } else {

            /*accountPicker.delegate = self
            accountPicker.dataSource = self

            categoriesPicker.delegate = categoriesPicker
            categoriesPicker.dataSource = categoriesPicker*/

            // categoriesPicker.categories = persistanceManager.getAllCachedCustomCategories()!
            accounts = persistanceManager.getAllCachedAccounts()!

            if let attachedTransaction = self.transaction {

                transactionEditMode = TransactionEditMode.existingTransaction

                //currency symbol

                let selectedAccountIndex = accounts.index {
                    $0 === attachedTransaction.account
                }

                selectedAccount = accounts[selectedAccountIndex!]
                // accountPicker.selectRow(selectedAccountIndex!, inComponent: 0, animated: true)

                switchCurrencySymbolLabel(currencyLabel: attachedTransaction.account?.currency?.symbol_native)

                transactionName.text = attachedTransaction.title
                transactionValue.text = self.getFormatedTransactionValue(transaction: attachedTransaction)

                switchTransactionTypeSwitchButton(transactionType: getTransactionType(transaction: attachedTransaction))

                switchTransactionPrefixLabel(transactionType: getTransactionType(transaction: attachedTransaction))

                if let transactionDate = transaction?.date {
                    datePicker.setDate(transactionDate as Date, animated: false)
                } else {
                    datePicker.setDate(Date.init(), animated: false)
                }

                /*if let categoryTitle = transaction?.category?.title {
                    categoriesPicker.selectCategoryByTitle(title: categoryTitle)
                }*/

                if getTransactionType(transaction: transaction) == TransactionType.income {
                    // categoriesPicker.isHidden = true
                    chooseCategoryButton.isHidden = true
                } else {
                    chooseCategoryButton.isHidden = false
                    // categoriesPicker.isHidden = false
                }

                selectedTransactionType = getTransactionType(transaction: transaction)

            } else {

                transactionEditMode = TransactionEditMode.newTransaction
                selectedAccount = accounts[0]
                switchCurrencySymbolLabel(currencyLabel: selectedAccount?.currency?.symbol_native)
            }

            setupNavigationHeaderColorScheme(backgroundColor: UIColor(hex: ColorConstants.sharedInstance.transactionsTabHeaderColor), textColor: UIColor.white)
            
            fillHeaderViewWithGradient(fromColor: UIColor(hex: ColorConstants.sharedInstance.transactionsTabHeaderColor), toColor: UIColor(hex: ColorConstants.sharedInstance.transactionsTabHeaderColor))

            setAppIconNotificationBadge(shouldNotify: true)
        }
    }
    
    
    private func fillHeaderViewWithGradient(fromColor: UIColor, toColor: UIColor) {
        
        let layer = CAGradientLayer()
        layer.frame = CGRect(x: 0, y: 0, width: headerView.frame.width, height: 80)
        layer.colors = [fromColor.cgColor, toColor.cgColor]
        
        headerView.layer.insertSublayer(layer, at: 0)
    }


    private func setupNavigationHeaderColorScheme(backgroundColor: UIColor, textColor: UIColor) {

        self.navigationController?.navigationBar.barTintColor = backgroundColor
        self.navigationController?.navigationBar.tintColor = textColor
        self.navigationController!.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: textColor]
    }

    override func viewDidLoad() {

        super.viewDidLoad()

        transactionTypeSwitchButton.tintColor = UIColor(hex: ColorConstants.sharedInstance.transactionsTabHeaderColor)

        switchTransactionTitleTextFieldState(transaction: self.transaction)

        setupAccountDropDown(anchor: chooseAccountButton, items: persistanceManager.getAllCachedAccounts()!.map {
            $0.title!
        }, transaction: self.transaction)

        persistanceManager.reloadAllCategoriesCache()

        setupCategoryDropDown(anchor: chooseCategoryButton, items: persistanceManager.getAllCachedCategories()!.map {
            $0.title!
        }, transaction: self.transaction)

        NotificationCenter.default.addObserver(self, selector:"openPINLockViewIfNeeded", name:
            NSNotification.Name.UIApplicationWillEnterForeground, object: nil)

    }

    private func setupCategoryDropDown(anchor: UIView, items: [String], transaction: Transaction?) {

        chooseCategoryDropDown.anchorView = anchor
        chooseCategoryDropDown.bottomOffset = CGPoint(x: 0, y: anchor.bounds.height + 10)
        chooseCategoryDropDown.direction = .bottom
        chooseCategoryDropDown.dismissMode = .automatic

        chooseCategoryButton.setTitleColor(UIColor(hex: ColorConstants.sharedInstance.transactionsTabHeaderColor), for: .normal)

        chooseCategoryDropDown.dataSource = items

        let appearance = DropDown.appearance()
        appearance.cellHeight = 40
        appearance.backgroundColor = UIColor(white: 1, alpha: 1)
        appearance.selectionBackgroundColor = UIColor(hex: ColorConstants.sharedInstance.transactionsTabHeaderColor).alpha(0.25)
        appearance.cornerRadius = 10
        appearance.shadowColor = UIColor(white: 0.6, alpha: 1)
        appearance.shadowOpacity = 0.9
        appearance.shadowRadius = 20
        appearance.animationduration = 0.25
        appearance.textColor = .darkGray

        if let transaction = transaction {
            if let category = transaction.category {
                if let cachedCategoryIndex = persistanceManager.getCachedCategoryIndex(title: category.title) {
                    chooseCategoryDropDown.selectRow(at: cachedCategoryIndex)
                    chooseCategoryButton.setTitle(category.title, for: .normal)
                    selectedCategoryIndex = cachedCategoryIndex
                } else {
                    selectedCategoryIndex = 0
                }
            }
        } else {
            chooseCategoryDropDown.selectRow(at: 0)
            chooseCategoryButton.setTitle(items[0], for: .normal)
            selectedCategoryIndex = 0
        }

        chooseCategoryDropDown.selectionAction = { [unowned self] (index, item) in
            self.chooseCategoryButton.setTitle(item, for: .normal)
            self.selectedCategoryIndex = self.chooseCategoryDropDown.indexForSelectedRow
        }


        chooseCategoryDropDown.cancelAction = { [unowned self] in
            if self.selectedCategoryIndex == nil && self.transactionEditMode == TransactionEditMode.newTransaction {
                self.chooseCategoryDropDown.selectRow(at: 0)
                self.chooseCategoryButton.setTitle(items[0], for: .normal)
                self.selectedCategoryIndex = 0
            }
        }
    }

    private func setupAccountDropDown(anchor: UIView, items: [String], transaction: Transaction?) {

        chooseAccountDropDown.anchorView = anchor
        chooseAccountDropDown.bottomOffset = CGPoint(x: 0, y: anchor.bounds.height + 10)
        chooseAccountDropDown.direction = .bottom
        chooseAccountDropDown.dismissMode = .automatic

        chooseAccountButton.setTitleColor(UIColor(hex: ColorConstants.sharedInstance.transactionsTabHeaderColor), for: .normal)

        chooseAccountDropDown.dataSource = items

        let appearance = DropDown.appearance()
        appearance.cellHeight = 40
        appearance.backgroundColor = UIColor(white: 1, alpha: 1)
        appearance.selectionBackgroundColor = UIColor(hex: ColorConstants.sharedInstance.transactionsTabHeaderColor).alpha(0.25)
        appearance.cornerRadius = 10
        appearance.shadowColor = UIColor(white: 0.6, alpha: 1)
        appearance.shadowOpacity = 0.9
        appearance.shadowRadius = 20
        appearance.animationduration = 0.25
        appearance.textColor = .darkGray

        if let transaction = transaction {
            if let account = transaction.account {
                if let cachedAccountIndex = persistanceManager.getCachedAccountIndex(title: account.title) {
                    chooseAccountDropDown.selectRow(at: cachedAccountIndex)
                    chooseAccountButton.setTitle(account.title, for: .normal)
                    selectedAccountIndex = cachedAccountIndex
                } else {
                    selectedAccountIndex = 0
                }
            }
        } else {
            chooseAccountDropDown.selectRow(at: 0)
            chooseAccountButton.setTitle(items[0], for: .normal)
            selectedAccountIndex = 0
        }

        chooseAccountDropDown.selectionAction = { [unowned self] (index, item) in
            self.chooseAccountButton.setTitle(item, for: .normal)
            self.selectedAccountIndex = self.chooseAccountDropDown.indexForSelectedRow
            self.currencySymbolLabel.text = self.persistanceManager.getCachedAccount(index: self.selectedAccountIndex)?.currency?.symbol_native
        }


        chooseAccountDropDown.cancelAction = { [unowned self] in
            if self.selectedAccountIndex == nil && self.transactionEditMode == TransactionEditMode.newTransaction {
                self.chooseAccountDropDown.selectRow(at: 0)
                self.chooseAccountButton.setTitle(items[0], for: .normal)
                self.selectedAccountIndex = 0
            }
        }
    }


    @IBAction func chooseAccountButtonTapped(_ sender: UIButton) {

        chooseAccountDropDown.show()
    }


    @IBAction func chooseCategoryButtonTapped(_ sender: UIButton) {

        chooseCategoryDropDown.show()
    }
    

    private func switchTransactionTitleTextFieldState(transaction: Transaction?) {
        
        transactionTitleAnimatedTextField.style = TransactionDetailsTextInputStyle()
        transactionTitleAnimatedTextField.placeHolderText = "Transaction title"
        
        if let transaction = transaction {
            transactionTitleAnimatedTextField.text = transaction.title
        }
    }
    

    private func switchTransactionPrefixLabel(transactionType: TransactionType) {

        switch transactionType {
        case .expense:
            transactionTypePrefixLabel.text = expenseTransactionValuePrefix
        case .income:
            transactionTypePrefixLabel.text = incomeTransactionValuePrefix
        default:
            transactionTypePrefixLabel.text = ""
        }
    }

    private func switchCurrencySymbolLabel(currencyLabel: String?) {

        if let symbol = currencyLabel {
            currencySymbolLabel.text = symbol
        } else {
            // TODO: move defaults to NSPreferences
            currencySymbolLabel.text = "$"
        }
    }

    private func switchTransactionTypeSwitchButton(transactionType: TransactionType) {

        switch transactionType {
        case .expense:
            transactionTypeSwitchButton.selectedSegmentIndex = 0
        case .income:
            transactionTypeSwitchButton.selectedSegmentIndex = 1
        default:
            transactionTypeSwitchButton.selectedSegmentIndex = 0
        }
    }

    private func getAccountCurrencySymbol(account: Account?) -> String? {

        var currencySymbol: String?

        if let account = account {
            if let currency = account.currency {
                currencySymbol = currency.symbol_native
            }
        }

        return currencySymbol
    }

    func getFormatedTransactionValue(transaction: Transaction?) -> String? {

        var amountPrefix: String?
        var formattedTransactionString: String?

        let type = getTransactionType(transaction: transaction)
        switch type {
        case .expense:
            amountPrefix = "-"
        case .income:
            amountPrefix = "+"
        case .undefined:
            amountPrefix = "?"
        }

        if let formattedDecimalValue = formatDecimalTransactionValue(decimalValue: transaction?.value) {
            if let currency_symbol = transaction?.account?.currency?.symbol_native {
                // formattedTransactionString = "\(amountPrefix!) \(formattedDecimalValue) \(currency_symbol)"
                formattedTransactionString = "\(formattedDecimalValue)"
            }
        }

        return formattedTransactionString
    }

    private func formatDecimalTransactionValue(decimalValue: NSDecimalNumber?) -> String? {

        var formattedDecimalValue: String?

        if let decimalValue = decimalValue {
            formattedDecimalValue = String(describing: decimalValue)
        }

        return formattedDecimalValue
    }

    private func getTransactionType(transaction: Transaction?) -> TransactionType {

        if let transaction = transaction {
            if let type = transaction.type {
                if type.lowercased() == String(describing: TransactionType.expense).lowercased() {
                    return TransactionType.expense
                } else if type.lowercased() == String(describing: TransactionType.income).lowercased() {
                    return TransactionType.income
                } else {
                    return TransactionType.undefined
                }
            } else {
                return TransactionType.undefined
            }
        } else {
            return TransactionType.undefined
        }
    }

    func accountNotSet() {

        let alert = UIAlertController(title: "No Account found", message: "Before creating a new Transaction, you need to set an Account. Do you want to set it now?", preferredStyle: .alert)

        let add = UIAlertAction(title: "Create", style: .default) {
            (action) in
            let accountStoryboard = UIStoryboard(name: "Accounts", bundle: Bundle.main)
            let next: AccountDetailsViewController = accountStoryboard.instantiateViewController(withIdentifier: "AccountEditView") as! AccountDetailsViewController
            self.navigationController?.pushViewController(next, animated: true)
        }

        let cancel = UIAlertAction(title: "Cancel", style: .cancel) {
            (action) in
            self.closeTransactionDetailsView()
        }

        alert.addAction(add)
        alert.addAction(cancel)

        present(alert, animated: true, completion: nil)
    }

    /*func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return persistanceManager.getCachedAccountsCount()
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return accounts[row]?.title
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if let account = accounts[row] {
            if (selectedAccount?.currency != account.currency) {
                self.changeTransactionCurrency(previousCurrency: (selectedAccount?.currency)!, currency: account.currency!)
            }

            selectedAccount = account
            switchCurrencySymbolLabel(currencyLabel: account.currency?.symbol_native)
        }
    }*/

    func changeTransactionCurrency(previousCurrency: Currency, currency: Currency) {

        /*transactionValue.placeholder = "0 " + currency.symbol!
        let value = transactionValue.text
        if (value != nil) && (value?.characters.count)! > 0 {
            let symbolLength = previousCurrency.symbol!.characters.count
            let to = value?.index((value?.endIndex)!, offsetBy: -symbolLength)
            transactionValue.text = (value?.substring(to: to!))! + currency.symbol!
        }*/
    }

    //Expense Income
    @IBAction func incomeExpenseButton(_ sender: UISegmentedControl) {

        switch (sender.selectedSegmentIndex) {
        case 0:
            transactionName.placeholder = "Expense Title"
            selectedTransactionType = TransactionType.expense // "Expense"
            switchTransactionPrefixLabel(transactionType: TransactionType.expense)
            // categoriesPicker.isHidden = false
            chooseCategoryButton.isHidden = false
            transactionCategoryLabel.isHidden = false
            chooseCategoryDropDown.selectRow(at: selectedCategoryIndex)
            chooseCategoryButton.setTitle(persistanceManager.getCachedCustomCategory(index: 0)?.title, for: .normal)
            break
        case 1:
            transactionName.placeholder = "Income Title"
            switchTransactionPrefixLabel(transactionType: TransactionType.income)
            selectedTransactionType = TransactionType.income // "Income"
            // categoriesPicker.isHidden = true
            chooseCategoryButton.isHidden = true
            transactionCategoryLabel.isHidden = true
            break
        default:
            selectedTransactionType = TransactionType.undefined
            break
        }
    }

    @IBAction func transactionValueTouchDown(_ sender: UITextField) {

        /*let value = transactionValue.text
        if (value != nil) && (value?.characters.count)! > 0 {
            transactionValue.text = self.getNumberFromValue(givenValue: value!)
        }*/
    }

    @IBAction func transactionValueEditingDidEnd(_ sender: UITextField) {

        /*let value = transactionValue.text
        if (value != nil) && (value?.characters.count)! > 0 {
            var minusPrefix = ""
            if selectedTransactionType == "Expense" {
                minusPrefix = "-"
            }
            transactionValue.text = minusPrefix + value! + " " + (selectedAccount?.currency?.symbol)!
        }*/
    }

    func getNumberFromValue(givenValue: String) -> String {

        /*let symbolLength = (selectedAccount?.currency?.symbol!.characters.count)! + 1
        let to = givenValue.index((givenValue.endIndex), offsetBy: -symbolLength)
        var value = (givenValue.substring(to: to))

        if let positionOfMinus = value.range(of: "-") {
            value = value.substring(from: positionOfMinus.upperBound)
        }*/

        return givenValue
    }

    //Save transaction
    @IBAction func saveButtonTapped(_ sender: UIBarButtonItem) {

        transactionEditProgress = TransactionEditProgressState.inProgress

        switch transactionEditMode {
        case .existingTransaction:
            validateTransactionTitleField()
            if self.transaction != nil {
                let previousAmountTransaction = self.transaction?.value
                let previousTypeTransaction = self.transaction?.type
                let previousAccountTransaction = self.transaction?.account

                if (transactionTitleAnimatedTextField.text! != "") {

                    self.transaction?.title = transactionTitleAnimatedTextField.text!//transactionName.text!
                    let _selectedAccount = persistanceManager.getCachedAccount(index: selectedAccountIndex)
                    self.transaction?.account = _selectedAccount!

                    if let amount = Utils.convertStringToDecimal(stringNumber: transactionValue.text) {
                        self.transaction?.value = amount
                    }

                    self.transaction?.type = String(describing: selectedTransactionType)
                    self.transaction?.date = datePicker.date as NSDate

                    if selectedTransactionType == TransactionType.income {
                        self.transaction?.category = nil
                    } else {
                        if let selectedCategory = persistanceManager.getCachedCustomCategory(index: selectedCategoryIndex)/*categoriesPicker.selectedCategory*/ {
                            self.transaction?.category = selectedCategory
                        }
                    }

                    persistanceManager.updateTransaction(
                        previousTransactionAmount: previousAmountTransaction!,
                        previousTransactionType: previousTypeTransaction!,
                        previousTransactionAccount: previousAccountTransaction!,
                        updatedTransaction: self.transaction)

                        self.dismiss(animated: true)
                }
            }
            // self.dismiss(animated: true)
            setAppIconNotificationBadge(shouldNotify: false)
        case .newTransaction:
            
            let saveResult: EntitySaveResult?
            
            validateTransactionTitleField()
            
            if transactionEditProgress != TransactionEditProgressState.error {
                
                transactionEditProgress = TransactionEditProgressState.completed
            
                if let amount = Utils.convertStringToDecimal(stringNumber: getNumberFromValue(givenValue:   transactionValue.text!)) {

                    let _selectedAccount = persistanceManager.getCachedAccount(index: selectedAccountIndex)
                
                    if let selectedCategory = /*categoriesPicker.selectedCategory*/ persistanceManager.getCachedCustomCategory(index: selectedCategoryIndex) {
                        saveResult = persistanceManager.createTransaction(title: transactionTitleAnimatedTextField.text!,
                            account: _selectedAccount!,
                            value: amount,
                            type: String(describing: selectedTransactionType),
                            date: datePicker.date,
                            category: selectedCategory)
                        
                        closeView(saveResult: saveResult)
                    }
                }
                //closeView(saveResult: saveResult)
                //self.dismiss(animated: true)
            }
        default:
            DDLogWarn("Undefined transaction edit mode : \(transactionEditMode)")
            self.dismiss(animated: true)
        }
    }

    @IBAction func cancelButtonTapped(_ sender: UIBarButtonItem) {
        self.closeTransactionDetailsView()
    }

    func closeTransactionDetailsView() {
        self.dismiss(animated: true)
        setAppIconNotificationBadge(shouldNotify: false)
    }
    
    private func validateTransactionTitleField() {
        
        if transactionTitleAnimatedTextField.text!.isEmpty {
            switchEnvelope(textField: transactionTitleAnimatedTextField,
                           state: TextFieldValidityState.error,
                           errorText: "Title can't be empty",
                           placeholderText: "Please type valid title value")
            
            transactionEditProgress = TransactionEditProgressState.error
        }
    }
    
    private func switchEnvelope(textField: AnimatedTextInput?, state: TextFieldValidityState, errorText: String, placeholderText: String) {
        
        if let textField = textField {
            switch state {
            case .valid:
                break
            case .error:
                textField.show(error: errorText, placeholderText: placeholderText)
                textField.shake()
                break
            case .warning:
                textField.shake()
                break
            }
        }
    }
    
    private func closeView(saveResult: EntitySaveResult?) {
        
        if let saveResult = saveResult {
            if saveResult.succeed {
                transactionEditProgress = TransactionEditProgressState.save
                setAppIconNotificationBadge(shouldNotify: false)
                self.dismiss(animated: true)
            } else {
                transactionEditProgress = TransactionEditProgressState.inProgress
                showErrorPopup(title: "Save account error", description: (saveResult.nserror?.localizedDescription)!)
            }
        }
    }
    
    private func showErrorPopup(title: String, description: String) {
        
        let alert = UIAlertController(title: title, message: description, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

}
