//
//  TransactionDetailsCategoryPicker.swift
//  bank
//
//  Created by Iurii Kuznetcov on 29/04/17.
//  Copyright © 2017 awesomefm. All rights reserved.
//

import Foundation
import UIKit

import CocoaLumberjack

class TransactionDetailsCategoryPicker: UIPickerView, UIPickerViewDataSource, UIPickerViewDelegate {

    var categories: [Category] = [] {
        didSet {
            if categories.count > 0 {
                selectedCategory = categories[0]
            }
        }
    }

    var selectedCategory: Category?

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {

        return categories.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {

        var categoryTitle: String?
        categoryTitle = categories[row].title
        return categoryTitle
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {

            if row > -1 && row < categories.count {
                selectedCategory = categories[row]
            }
    }

    func selectCategoryByIndex(index: Int) {

        if index > -1 && index < categories.count {
            self.selectRow(index, inComponent: 0, animated: false)
        }
    }

    func selectCategoryByTitle(title: String) -> Bool {

        var selected = false

        if let foundIndex = categories.index(where: { $0.title == title }) {
            selectCategoryByIndex(index: foundIndex)
            selected = true
        }
        return selected
    }

}
