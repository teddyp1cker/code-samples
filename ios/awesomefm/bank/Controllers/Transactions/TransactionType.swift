//
//  TransactionType.swift
//  bank
//
//  Created by Iurii Kuznetcov on 28/04/17.
//  Copyright © 2017 awesomefm. All rights reserved.
//

import Foundation

enum TransactionType {
    case expense
    case income
    case undefined
}
