//
//  TransactionEditMode.swift
//  bank
//
//  Created by Iurii Kuznetcov on 30/04/17.
//  Copyright © 2017 awesomefm. All rights reserved.
//

import Foundation

enum TransactionEditMode {
    
    case newTransaction
    case existingTransaction
    case defaultMode
}
