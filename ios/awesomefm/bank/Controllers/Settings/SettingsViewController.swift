import Foundation
import UIKit

import ReachabilitySwift


import CocoaLumberjack

class SettingsViewController: UIViewController {
    
    @IBOutlet weak var webView: UIWebView!
    
    @IBOutlet weak var offlineAboutView: UIView!

    @IBOutlet weak var onlineAboutView: UIView!

    private let reachability = Reachability()!

    private let aboutInfoURL = "https://www.fi.muni.cz/~xhubeny2/pv239/"

    private let userDefaults = UserDefaults.standard

    override func viewDidLoad() {

        super.viewDidLoad()

        reachability.whenReachable = { reachability in
            DispatchQueue.main.async {
                DDLogInfo("Network reachability notifier: network is reachable")
                self.hideView(view: self.offlineAboutView)
                self.showView(view: self.onlineAboutView)
                self.reloadAboutWebView(aboutUrl: self.aboutInfoURL)
            }
        }
        reachability.whenUnreachable = { reachability in

            DispatchQueue.main.async {
                DDLogInfo("Network reachability notifier: network is unreachable")
                self.hideView(view: self.onlineAboutView)
                self.showView(view: self.offlineAboutView)
            }
        }
        
        do {
            try reachability.startNotifier()
        } catch {
            DDLogWarn("Unable to start network reachability notifier. Switching to offline mode")
            self.hideView(view: self.onlineAboutView)
            self.showView(view: self.offlineAboutView)
        }
        
        setupNavigationHeaderColorScheme(backgroundColor: UIColor(hex: ColorConstants.sharedInstance.aboutTabHeaderColor), textColor: UIColor.white)

        NotificationCenter.default.addObserver(self, selector:"openPINLockViewIfNeeded", name:
            NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
    }

    func openPINLockViewIfNeeded(){

        let pinEnabled = userDefaults.bool(forKey: "security_pin_lock_enabled")
        if pinEnabled {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let vc = (
                storyBoard.instantiateViewController(
                    withIdentifier: "CSASLandingLoginView")
            )
            vc.modalTransitionStyle = .crossDissolve
            present(vc, animated: false, completion: nil)
        }
    }

    private func setupNavigationHeaderColorScheme(backgroundColor: UIColor, textColor: UIColor) {
        
        self.navigationController?.navigationBar.barTintColor = backgroundColor
        self.navigationController?.navigationBar.tintColor = textColor
        self.navigationController!.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: textColor]
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    private func reloadAboutWebView(aboutUrl: String) {

        let aboutURL = URL(string: aboutUrl)
        let aboutURLRequest = URLRequest(url: aboutURL!)
        webView.loadRequest(aboutURLRequest)
        webView.reload()
    }

    private func hideView(view: UIView) {

        if !view.isHidden {
            view.fadeOut(completion: {
                (finished: Bool) -> Void in
                view.isHidden = true
            })
        }
    }

    private func showView(view: UIView) {

        if view.isHidden {
            view.fadeIn(completion: {
                (finished: Bool) -> Void in
                view.isHidden = false
            })
        }
    }
}
