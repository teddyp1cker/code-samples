//
//  BundledCategoriesTableViewCell.swift
//  bank
//
//  Created by Iurii Kuznetcov on 04/05/17.
//  Copyright © 2017 awesomefm. All rights reserved.
//

import Foundation
import UIKit

class BundledCategoriesTableViewCell : UITableViewCell {

    @IBOutlet weak var categoryTitleLabel: UILabel!
}
