//
//  CategoriesListTableViewCell.swift
//  bank
//
//  Created by Iurii Kuznetcov on 27/04/17.
//  Copyright © 2017 awesomefm. All rights reserved.
//

import Foundation
import UIKit

class CategoriesListTableViewCell: UITableViewCell {

    @IBOutlet weak var categoryTitleLabel: UILabel!
    @IBOutlet weak var categoryDefaultMarkButton: UIButton!
    @IBOutlet weak var categoryColorTagLabel: UILabel!

    var category: Category?


    @IBAction func categoryDefaultMarkButtonTapped(_ sender: UIButton) {

        /*if let categoryIsMarkedAsDefault = category?.isDefault {
            if categoryIsMarkedAsDefault {
                categoryDefaultMarkButton?.setImage(UIImage(named: "star_mark_unselected"), for: UIControlState.normal)
            } else {
                categoryDefaultMarkButton?.setImage(UIImage(named: "star_mark_selected"), for: UIControlState.normal)
            }
        }*/
    }
    
}
