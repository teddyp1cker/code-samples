//
//  CategoryEditMode.swift
//  bank
//
//  Created by Michael Hubeny on 4/28/17.
//  Copyright © 2017 awesomefm. All rights reserved.
//

import Foundation

enum CategoryEditMode {
    case newCategory
    case existingCategory
    case defaultMode
}
