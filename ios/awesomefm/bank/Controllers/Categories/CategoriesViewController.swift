import Foundation
import UIKit
import CoreData

import CocoaLumberjack

class CategoriesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, NSFetchedResultsControllerDelegate {

    @IBOutlet weak var ListTableView: UITableView!

    @IBOutlet weak var categoriesListHeaderLabel: UILabel!

    @IBOutlet weak var emptyCategoriesListImage: UIImageView!

    @IBOutlet weak var emptyCategoriesLabel: UILabel!

    @IBOutlet weak var bundledCategoriesTableView: UITableView!

    private let persistanceManager = PersistanceManager.sharedInstance

    private let userDefaults = UserDefaults.standard

    override func viewDidLoad() {

        super.viewDidLoad()
        ListTableView.dataSource = self
        ListTableView.delegate = self

        bundledCategoriesTableView.delegate = self
        bundledCategoriesTableView.dataSource = self

        NotificationCenter.default.addObserver(self, selector:"openPINLockViewIfNeeded", name:
            NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func openPINLockViewIfNeeded(){

        let pinEnabled = userDefaults.bool(forKey: "security_pin_lock_enabled")
        if pinEnabled {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let vc = (
                storyBoard.instantiateViewController(
                    withIdentifier: "CSASLandingLoginView")
            )
            vc.modalTransitionStyle = .crossDissolve
            present(vc, animated: false, completion: nil)
        }
    }

    override func viewWillAppear(_ animated: Bool) {

        persistanceManager.fillCache()
        if persistanceManager.getCachedCustomCategoriesCount() == 0 {
            ListTableView.isHidden = true
            categoriesListHeaderLabel.isHidden = true
            emptyCategoriesListImage.isHidden = false
            emptyCategoriesLabel.isHidden = false
        } else {
            ListTableView.reloadData()
            emptyCategoriesListImage.isHidden = true
            emptyCategoriesLabel.isHidden = true
            ListTableView.isHidden = false
            categoriesListHeaderLabel.isHidden = false
        }

        setupNavigationHeaderColorScheme(backgroundColor: UIColor(hex: ColorConstants.sharedInstance.categoriesTabHeaderColor), textColor: UIColor.white)
    }

    private func setupNavigationHeaderColorScheme(backgroundColor: UIColor, textColor: UIColor) {

        self.navigationController?.navigationBar.barTintColor = backgroundColor
        self.navigationController?.navigationBar.tintColor = textColor
        self.navigationController!.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: textColor]
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        var cell: UITableViewCell?

        if tableView == self.ListTableView {

            let cell = tableView.dequeueReusableCell(
                    withIdentifier: "CategoriesListTableViewCell", for: indexPath) as! CategoriesListTableViewCell

            if let category = persistanceManager.getCachedCustomCategory(index: indexPath.row) {
                cell.category = category

                if let categoryTitle = category.title {
                    cell.categoryTitleLabel?.text = categoryTitle
                }

                if category.isDefault {
                    cell.categoryDefaultMarkButton?.setImage(UIImage(named: "star_mark_selected"), for: UIControlState.normal)
                } else {
                    cell.categoryDefaultMarkButton?.setImage(UIImage(named: "star_mark_unselected"), for: UIControlState.normal)
                }

                if let colorTagValue = category.color {
                    cell.categoryColorTagLabel.layer.cornerRadius = cell.categoryColorTagLabel.frame.height / 2
                    cell.categoryColorTagLabel.clipsToBounds = true
                    cell.categoryColorTagLabel.backgroundColor = UIColor(hex: colorTagValue)
                    cell.categoryColorTagLabel.isHidden = false
                } else {
                    DDLogInfo("\(category) doesn't have any color tag")
                    cell.categoryColorTagLabel.isHidden = true
                }
            }

            return cell
        } else if tableView == self.bundledCategoriesTableView {

            let cell = tableView.dequeueReusableCell(
                    withIdentifier: "BundledCategoriesTableViewCell", for: indexPath) as! BundledCategoriesTableViewCell

            if let category = persistanceManager.getBundledCategoryByIndex(index: indexPath.row) {

                if let categoryTitle = category.title {
                    cell.categoryTitleLabel?.text = categoryTitle
                }
            }

            return cell
        } else {
            return cell!
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        var count: Int?

        if tableView == self.ListTableView {
            count = persistanceManager.getCachedCustomCategoriesCount()
        }

        if tableView == self.bundledCategoriesTableView {
            count = persistanceManager.getAllBundledCategoriesCount()
        }

        return count!
    }


    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        if tableView == self.ListTableView {

            let category = persistanceManager.getCachedCustomCategory(index: indexPath.row)
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let categoryDetailsViewController = storyBoard.instantiateViewController(withIdentifier: "EditCategoryView") as! CategoryDetailsViewController
            categoryDetailsViewController.category = category
            let categoryDetailsNavController = UINavigationController(rootViewController: categoryDetailsViewController)
            self.navigationController?.present(categoryDetailsNavController, animated: true, completion: nil)
        }
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {

        if editingStyle == .delete {
            let category = persistanceManager.getCachedCustomCategory(index: indexPath.row)

            persistanceManager.deleleCustomCategory(category: category)
            persistanceManager.reloadCustomCategoriesCache()

            if persistanceManager.getCachedCustomCategoriesCount() == 0 {
                ListTableView.isHidden = true
                categoriesListHeaderLabel.isHidden = true
                emptyCategoriesListImage.isHidden = false
                emptyCategoriesLabel.isHidden = false
            } else {
                ListTableView.reloadData()
                emptyCategoriesListImage.isHidden = true
                emptyCategoriesLabel.isHidden = true
                ListTableView.isHidden = false
                categoriesListHeaderLabel.isHidden = false
            }
        }
        tableView.reloadData()
    }

}
