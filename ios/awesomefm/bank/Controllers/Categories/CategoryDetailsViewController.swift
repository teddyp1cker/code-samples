import Foundation
import UIKit

import RappleColorPicker

import CocoaLumberjack

import AnimatedTextInput

enum CategoryEditProgressState {
    case inProgress
    case error
    case completed
    case save
}

struct CategoryDetailsTextInputStyle: AnimatedTextInputStyle {
    
    let activeColor = UIColor.gray
    let inactiveColor = UIColor.gray.withAlphaComponent(0.5)
    let lineInactiveColor = UIColor.gray.withAlphaComponent(0.5)
    let errorColor = UIColor.red
    let textInputFont = UIFont.systemFont(ofSize: 21)
    let textInputFontColor = UIColor.black
    let placeholderMinFontSize: CGFloat = 14
    let counterLabelFont: UIFont? = UIFont.systemFont(ofSize: 9)
    let leftMargin: CGFloat = 0
    let topMargin: CGFloat = 30
    let rightMargin: CGFloat = 0
    let bottomMargin: CGFloat = 10
    let yHintPositionOffset: CGFloat = 7
    let yPlaceholderPositionOffset: CGFloat = 0
    public let textAttributes: [String: Any]? = nil
}

class CategoryDetailsViewController: UIViewController, RappleColorPickerDelegate {


    @IBOutlet weak var CategoryTitleAnimatedTextField: AnimatedTextInput!
    
    @IBOutlet weak var categoryEditCancelButton: UIBarButtonItem!

    @IBOutlet weak var categoryEditSaveButton: UIBarButtonItem!

    @IBOutlet weak var NewCategoryTextField: UITextField!

    @IBOutlet weak var ColorTextField: UITextField!

    @IBOutlet weak var DefaultCategorySwitch: UISwitch!

    @IBOutlet weak var colorTagButton: UIButton!

    @IBOutlet weak var headerView: UIView!
    
    private let persistanceManager = PersistanceManager.sharedInstance

    var categoryEditMode: CategoryEditMode = CategoryEditMode.defaultMode
    
    private var categoryEditProgress : CategoryEditProgressState = CategoryEditProgressState.inProgress

    var category: Category?

    private var isAllowedToSave: Bool = true

    private var selectedColorString: String?

    private let userDefaults = UserDefaults.standard
    
    override func viewWillAppear(_ animated: Bool) {
        
        setAppIconNotificationBadge(shouldNotify: true)
        
        setupNavigationHeaderColorScheme(backgroundColor: UIColor(hex: ColorConstants.sharedInstance.categoriesTabHeaderColor), textColor: UIColor.white)
        
        fillHeaderViewWithGradient(fromColor: UIColor(hex: ColorConstants.sharedInstance.categoriesTabHeaderColor), toColor: UIColor(hex: ColorConstants.sharedInstance.categoriesTabHeaderColor))
        
        super.viewWillAppear(animated)
    }

    func openPINLockViewIfNeeded(){

        let pinEnabled = userDefaults.bool(forKey: "security_pin_lock_enabled")
        if pinEnabled {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let vc = (
                storyBoard.instantiateViewController(
                    withIdentifier: "CSASLandingLoginView")
            )
            vc.modalTransitionStyle = .crossDissolve
            present(vc, animated: false, completion: nil)
        }
    }

    private func fillHeaderViewWithGradient(fromColor: UIColor, toColor: UIColor) {
        
        let layer = CAGradientLayer()
        layer.frame = CGRect(x: 0, y: 0, width: headerView.frame.width, height: 80)
        layer.colors = [fromColor.cgColor, toColor.cgColor]
        
        headerView.layer.insertSublayer(layer, at: 0)
    }

    @IBAction func colorTagButtonTapped(_ sender: UIButton) {

        let attributes: [RappleCPAttributeKey: AnyObject] = [
                .BGColor: UIColor.white.withAlphaComponent(1.0),
                .TintColor: UIColor.white.withAlphaComponent(1.0),
                .Style: RappleCPStyleCircle as AnyObject,
                .BorderColor: UIColor.white.withAlphaComponent(1.0)
        ]

        RappleColorPicker.openColorPallet(onViewController: self, origin: CGPoint(x: sender.frame.midX - 115, y: sender.frame.minY - 50), delegate: self, attributes: attributes)
    }

    func colorSelected(_ color: UIColor) {

        selectedColorString = color.hex()
        DDLogInfo("Selected color : \(selectedColorString!)")

        colorTagButton.layer.borderWidth = 1
        colorTagButton.layer.borderColor = UIColor(hex: selectedColorString!).cgColor
        colorTagButton.backgroundColor = UIColor(hex: selectedColorString!)
        colorTagButton.setTitle("", for: .normal)

        RappleColorPicker.close()
    }
    
    private func closeView(saveResult: EntitySaveResult?) {
        
        if let saveResult = saveResult {
            if saveResult.succeed {
                categoryEditProgress = CategoryEditProgressState.save
                self.dismiss(animated: true)
                setAppIconNotificationBadge(shouldNotify: false)
            } else {
                categoryEditProgress = CategoryEditProgressState.inProgress
                showErrorPopup(title: "Save category error", description: (saveResult.nserror?.localizedDescription)!)
                setAppIconNotificationBadge(shouldNotify: true)
            }
        }
        
    }
    
    private func showErrorPopup(title: String, description: String) {
        
        let alert = UIAlertController(title: title, message: description, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

    @IBAction func cancelButtonTapped(_ sender: UIBarButtonItem) {

        DDLogInfo("Cancel category details tapped")
        self.dismiss(animated: true, completion: {
            setAppIconNotificationBadge(shouldNotify: false)
            DDLogInfo("Category details view closed")
        })
    }

    @IBAction func saveButtonTapped(_ sender: UIBarButtonItem) {

        categoryEditProgress = CategoryEditProgressState.inProgress
        
        switch categoryEditMode {
        case .existingCategory:
            
            let saveResult: EntitySaveResult?
            
            validateCategoryTitleField()
            
            if categoryEditProgress != CategoryEditProgressState.error {
                
                categoryEditProgress = CategoryEditProgressState.completed
                
                category?.title = CategoryTitleAnimatedTextField.text
                category?.isDefault = DefaultCategorySwitch.isOn

                if let selectedColorString = selectedColorString {
                    category?.color = selectedColorString
                }

                saveResult = persistanceManager.updateCustomCategory(category: category)
                closeView(saveResult: saveResult)
            }

        case .newCategory:
            
            let saveResult: EntitySaveResult?
            
            validateCategoryTitleField()
            
            if categoryEditProgress != CategoryEditProgressState.error {
                
                categoryEditProgress = CategoryEditProgressState.completed
                
                saveResult = persistanceManager.createCustomCategory(
                    title: CategoryTitleAnimatedTextField.text,
                    color: selectedColorString,
                    isDefault: DefaultCategorySwitch.isOn)
                
                closeView(saveResult: saveResult)
            }
        default:
            DDLogInfo("Undefined behavior for category details saving")
        }

            /*if let categoryTitle = NewCategoryTextField.text {
                if categoryTitle.isEmpty {
                    switchEnvelope(textField: NewCategoryTextField, state: TextFieldValidityState.error)
                    isAllowedToSave = false
                } else {
                    switchEnvelope(textField: NewCategoryTextField, state: TextFieldValidityState.valid)
                    isAllowedToSave = true
                }
            }

            if isAllowedToSave {
                persistanceManager.createCustomCategory(
                        title: NewCategoryTextField.text,
                        color: /*ColorTextField.text*/ selectedColorString,
                        isDefault: DefaultCategorySwitch.isOn)

                self.dismiss(animated: true)
            }
        default:
            DDLogInfo("Save category button tapped")
        }*/
    }

    override func viewDidLoad() {

        super.viewDidLoad()
        
        
        switchCategoryTitleTextFieldState(category: self.category)
        switchColorPickerState(category: self.category)
        
        setupNavigationHeaderColorScheme(backgroundColor: UIColor(hex: ColorConstants.sharedInstance.categoriesTabHeaderColor), textColor: UIColor.white)

        NotificationCenter.default.addObserver(self, selector:"openPINLockViewIfNeeded", name:
            NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
    }

    private func setupNavigationHeaderColorScheme(backgroundColor: UIColor, textColor: UIColor) {

        self.navigationController?.navigationBar.barTintColor = backgroundColor
        self.navigationController?.navigationBar.tintColor = textColor
        self.navigationController!.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: textColor]
    }

    private func switchEnvelope(textField: AnimatedTextInput?, state: TextFieldValidityState, errorText: String, placeholderText: String) {
        
        if let textField = textField {
            switch state {
            case .valid:
                break
            case .error:
                textField.show(error: errorText, placeholderText: placeholderText)
                textField.shake()
                break
            case .warning:
                textField.shake()
                break
            }
        }
    }

    /*@IBAction func categoryEditTitleFieldTextChanged(_ sender: UITextField) {

        if let categoryTitle = sender.text {
            if categoryTitle.isEmpty {
                switchEnvelope(textField: sender, state: TextFieldValidityState.error)
                isAllowedToSave = false
            } else {
                switchEnvelope(textField: sender, state: TextFieldValidityState.valid)
                isAllowedToSave = true
            }
        }
    }*/

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    private func switchCategoryTitleTextFieldState(category: Category?) {
        
        CategoryTitleAnimatedTextField.style = CategoryDetailsTextInputStyle()
        CategoryTitleAnimatedTextField.placeHolderText = "Category title"
        
        if let category = category {
            CategoryTitleAnimatedTextField.text = category.title
        }
    }
    
    private func switchColorPickerState(category: Category?) {
        if let attachedCategory = self.category {
            categoryEditMode = CategoryEditMode.existingCategory
            NewCategoryTextField.text = attachedCategory.title
            ColorTextField.text = attachedCategory.color
            DefaultCategorySwitch.isOn = attachedCategory.isDefault
            if let categoryColor = attachedCategory.color {
                if !categoryColor.isEmpty {
                    colorTagButton.layer.cornerRadius = 18
                    colorTagButton.clipsToBounds = true
                    colorTagButton.layer.borderWidth = 1
                    colorTagButton.layer.borderColor = UIColor(hex: attachedCategory.color!).cgColor
                    colorTagButton.backgroundColor = UIColor(hex: attachedCategory.color!)
                    colorTagButton.setTitle("", for: .normal)
                } else {
                    colorTagButton.layer.cornerRadius = 18
                    colorTagButton.clipsToBounds = true
                    colorTagButton.layer.borderWidth = 1
                    colorTagButton.layer.borderColor = UIColor.darkGray.cgColor
                    colorTagButton.setTitle("?", for: .normal)
                }
            } else {
                colorTagButton.layer.cornerRadius = 18
                colorTagButton.clipsToBounds = true
                colorTagButton.layer.borderWidth = 1
                colorTagButton.layer.borderColor = UIColor.darkGray.cgColor
                colorTagButton.setTitle("?", for: .normal)
            }
        } else {
            categoryEditMode = CategoryEditMode.newCategory
            colorTagButton.layer.cornerRadius = 18
            colorTagButton.clipsToBounds = true
            colorTagButton.layer.borderWidth = 1
            colorTagButton.layer.borderColor = UIColor.darkGray.cgColor
            colorTagButton.setTitle("?", for: .normal)
        }
    }
    
    private func validateCategoryTitleField() {
        
        if CategoryTitleAnimatedTextField.text!.isEmpty {
            switchEnvelope(textField: CategoryTitleAnimatedTextField,
                           state: TextFieldValidityState.error,
                           errorText: "Title can't be empty",
                           placeholderText: "Please type valid title value")
            
            categoryEditProgress = CategoryEditProgressState.error
        }
    }
    
}
