import Foundation
import UIKit

import PinCodeTextField

class CSASLandingViewController: UIViewController, PinCodeTextFieldDelegate {

    @IBOutlet weak var verifyButton: UIButton!

    @IBOutlet weak var pinCodeTextField: PinCodeTextField!

    @IBOutlet weak var headerIconView: UIImageView!

    private let userDefaults = UserDefaults.standard

    private let pinLength = 4

    private var pinString : String?

    @IBAction func verifyPINCodeButtonTapped(_ sender: UIButton) {
        closeView()
    }

    override func viewDidLoad() {

        super.viewDidLoad()
        pinCodeTextField.delegate = self
        pinCodeTextField.keyboardType = .numberPad
        let pinEnabled = userDefaults.bool(forKey: "security_pin_lock_enabled")
        if !pinEnabled {
            if (userDefaults.string(forKey: "security_pin_code_string")?.characters.count)! < pinLength {
                closeView()
            }
        }
    }

    override public var prefersStatusBarHidden: Bool {
        return false
    }

    override public var preferredStatusBarStyle: UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    private func  closeView() {
        self.dismiss(animated: true, completion: nil)
    }

    func textFieldShouldBeginEditing(_ textField: PinCodeTextField) -> Bool {
        return true
    }

    func textFieldDidBeginEditing(_ textField: PinCodeTextField) {
        pinString = userDefaults.string(forKey: "security_pin_code_string")
    }

    func textFieldValueChanged(_ textField: PinCodeTextField) {

        if textField.text?.characters.count == pinLength {
            if textField.text == pinString {
                closeView()
            } else {
                pinCodeTextField.shake()
                headerIconView.shake()
            }
        }
    }

    func textFieldShouldEndEditing(_ textField: PinCodeTextField) -> Bool {
        return true
    }

    func textFieldShouldReturn(_ textField: PinCodeTextField) -> Bool {
        return true
    }
}
