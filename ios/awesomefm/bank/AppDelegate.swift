import UIKit
import CoreData

import CocoaLumberjack

import DropDown

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    private let fileLogsDirectoryName = "bankAppLogs"

    var window: UIWindow?

    private var currenciesManager: CurrenciesManager?

    private var predefinedCategoriesManager: PredefinedCategoriesManager?

    private let userDefaults = UserDefaults.standard

    private var lastOpenedViewController : UIViewController?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

        setupLogging()

        self.currenciesManager = CurrenciesManager()
        self.predefinedCategoriesManager = PredefinedCategoriesManager()

        if self.currenciesManager?.parseBundledCurrenciesJson(jsonFile: "currencies") == true {
            if self.currenciesManager?.fillCurrencies(jsonFile: "currencies") == true {
                DDLogInfo("Currencies were updated.")
            }
        }

        if self.predefinedCategoriesManager?.parseBundledCategoriesJson(jsonFile: "predefined_categories") == true {
            if self.predefinedCategoriesManager?.fillPredefinedCategories(jsonFile: "predefined_categories") == true {
                DDLogInfo("Predefined categories were updated.")
            }
        }

        DropDown.startListeningToKeyboard()

        setupBadgePermissions()

        setAppIconNotificationBadge(shouldNotify: false)

        return true
    }

    private func setupLogging() {

        let ttyLogger = DDTTYLogger.sharedInstance!
        ttyLogger.logFormatter = CustomLoggingFormatter()

        let ASLogger = DDASLLogger.sharedInstance!
        ASLogger.logFormatter = CustomLoggingFormatter()

        DDLog.add(ttyLogger)
        DDLog.add(ASLogger)

        let logsDirectoryURL =
            getDocumentsDirectoryURL()?.appendingPathComponent(fileLogsDirectoryName)

        let logFileManager = DDLogFileManagerDefault(logsDirectory: logsDirectoryURL!.path)
        let fileLogger: DDFileLogger = DDFileLogger(logFileManager: logFileManager)
        fileLogger.rollingFrequency = TimeInterval(60 * 60 * 24 * 3)
        fileLogger.logFileManager.maximumNumberOfLogFiles = 10
        fileLogger.logFormatter = CustomLoggingFormatter()

        DDLog.add(fileLogger)

        self.currenciesManager = CurrenciesManager()
        self.predefinedCategoriesManager = PredefinedCategoriesManager()
    }

    func setupBadgePermissions() {
        let notificationSettings: UIUserNotificationSettings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
        UIApplication.shared.registerUserNotificationSettings(notificationSettings)
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        DDLogInfo("App did enter in background")
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        DDLogInfo("App did enter in foreground")
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        DDLogInfo("App did enter in active state")
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        DDLogInfo("App did will terminate")
    }

    func getTopViewController()->UIViewController{
        return topViewControllerWithRootViewController(rootViewController: UIApplication.shared.keyWindow!.rootViewController!)
    }

    func topViewControllerWithRootViewController(rootViewController:UIViewController)->UIViewController{
        if rootViewController is UITabBarController{
            let tabBarController = rootViewController as! UITabBarController
            return topViewControllerWithRootViewController(rootViewController: tabBarController.selectedViewController!)
        }
        if rootViewController is UINavigationController{
            let navBarController = rootViewController as! UINavigationController
            return topViewControllerWithRootViewController(rootViewController: navBarController.visibleViewController!)
        }
        if let presentedViewController = rootViewController.presentedViewController {
            return topViewControllerWithRootViewController(rootViewController: presentedViewController)
        }
        return rootViewController
    }

}
