//
//  StarViewController.swift
//  s3ImageGrid
//
//  Created by Iurii Kuznetcov on 20/07/2017.
//  Copyright © 2017 YuriKuznetcov. All rights reserved.
//

import Foundation
import UIKit

import CocoaLumberjack

class StarViewController: UIViewController, Observer {

    @IBOutlet weak var detailedImageViewContainer: UIView!

    @IBOutlet weak var detailedImageView: UIImageView!

    @IBOutlet weak var starDescriptionLabel: UILabel!

    @IBOutlet weak var detailedImageLoadingActivityIndicator: UIActivityIndicatorView!

    private let starsList = StarsList.sharedManager

    var selectedStarIndex: Int?

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        refresh()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        detailedImageView.addObserver(observer: self)

        self.navigationController?.navigationBar.barTintColor = UIColor.black
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController!.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]

        let swipeDown = UISwipeGestureRecognizer(target: self, action: "respondToSwipeGesture:")
        swipeDown.direction = UISwipeGestureRecognizerDirection.down
        self.view.addGestureRecognizer(swipeDown)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    func respondToSwipeGesture(_ gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.down:
                dismiss(animated: true, completion: {})
            default:
                break
            }
        }
    }

    func refresh() {
        if let selectedStarIndex = selectedStarIndex {
            title = starsList.stars[selectedStarIndex].title
            starDescriptionLabel.text = starsList.stars[selectedStarIndex].description
            refreshStarDetailedImageView(starsList.stars[selectedStarIndex].originalUrl)
        }
    }

    func refreshStarDetailedImageView(_ detailedImageUrl: String?) {
        if let detailedImageUrl = detailedImageUrl {
            self.detailedImageLoadingActivityIndicator.startAnimating()
            self.detailedImageViewContainer.isHidden = true
            detailedImageView.displayFromURL(urlString: detailedImageUrl)
        }
    }

    func handleDownloadCompleteEvent() {
        DispatchQueue.main.async {
            self.detailedImageLoadingActivityIndicator.stopAnimating()
            self.detailedImageViewContainer.isHidden = false
            self.detailedImageView.isHidden = false
        }
    }
}
