//
//  StarsItemCell.swift
//  s3ImageGrid
//
//  Created by Iurii Kuznetcov on 17/07/2017.
//  Copyright © 2017 YuriKuznetcov. All rights reserved.
//

import Foundation
import UIKit

import CocoaLumberjack

class StarsItemCell: UICollectionViewCell, Observer {

    @IBOutlet weak var starThumbnailImageView: UIImageView!

    @IBOutlet weak var starItemTitleLabel: UILabel!

    @IBOutlet weak var starThumbnailImageLoadingIndicator: UIActivityIndicatorView!

    var title: String?

    var thumbnailUrl: String?

    func setImageDownloadCompleteHandler() {
        starThumbnailImageView.addObserver(observer: self)
    }

    func repaint() {
        starItemTitleLabel.text = title
        if let thumbnailUrl = self.thumbnailUrl {
            DispatchQueue.main.async(execute: { () -> Void in
                self.starThumbnailImageLoadingIndicator.isHidden = false
                self.starThumbnailImageLoadingIndicator.startAnimating()
            })
            self.starThumbnailImageView.displayFromURL(urlString: thumbnailUrl)
        }
    }

    func handleDownloadCompleteEvent() {
        DispatchQueue.main.async(execute: { () -> Void in
            self.starThumbnailImageLoadingIndicator.stopAnimating()
        })
    }
}
