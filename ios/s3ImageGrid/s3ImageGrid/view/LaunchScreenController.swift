//
//  LaunchScreenController.swift
//  s3ImageGrid
//
//  Created by Iurii Kuznetcov on 24/06/2017.
//  Copyright © 2017 YuriKuznetcov. All rights reserved.
//

import Foundation
import UIKit

import CocoaLumberjack

protocol Observer {
    func handleDownloadCompleteEvent()
}

class ColumnFlowLayout: UICollectionViewFlowLayout {

    let cellsPerRow: Int

    override var itemSize: CGSize {
        get {
            guard let collectionView = collectionView else {
                return super.itemSize
            }
            let marginsAndInsets = sectionInset.left + sectionInset.right + minimumInteritemSpacing * CGFloat(cellsPerRow - 1)
            let itemWidth = ((collectionView.bounds.size.width - marginsAndInsets) / CGFloat(cellsPerRow)).rounded(.down)
            return CGSize(width: itemWidth, height: itemWidth)
        }
        set {
            super.itemSize = newValue
        }
    }

    init(cellsPerRow: Int, minimumInteritemSpacing: CGFloat = 0, minimumLineSpacing: CGFloat = 0, sectionInset: UIEdgeInsets = .zero) {
        self.cellsPerRow = cellsPerRow
        super.init()

        self.minimumInteritemSpacing = minimumInteritemSpacing
        self.minimumLineSpacing = minimumLineSpacing
        self.sectionInset = sectionInset
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func invalidationContext(forBoundsChange newBounds: CGRect) -> UICollectionViewLayoutInvalidationContext {
        let context = super.invalidationContext(forBoundsChange: newBounds) as! UICollectionViewFlowLayoutInvalidationContext
        context.invalidateFlowLayoutDelegateMetrics = newBounds != collectionView?.bounds
        return context
    }
}

class LaunchScreenController: UIViewController, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, Observer {

    @IBOutlet weak var listLoadingIndicator: UIActivityIndicatorView!

    @IBOutlet weak var countLabel: UILabel!

    @IBOutlet weak var animatingView: UIView!

    @IBOutlet weak var starsCollectionView: UICollectionView!

    private let starsList = StarsList.sharedManager

    let columnLayout = ColumnFlowLayout(
            cellsPerRow: 3,
            minimumInteritemSpacing: 2,
            minimumLineSpacing: 2,
            sectionInset: UIEdgeInsets(top: 2, left: 2, bottom: 2, right: 2)
    )

    override func viewDidLoad() {
        super.viewDidLoad()
        starsCollectionView.isHidden = true
        starsCollectionView.collectionViewLayout = columnLayout
        listLoadingIndicator.startAnimating()
        starsList.addObserver(observer: self)

        starsCollectionView.delegate = self
        starsCollectionView.dataSource = self

        DispatchQueue.global(qos: .background).sync {
            if AppSettingsProvider.sharedInstance.isImageCacheEnabled() {
                self.starsList.jsonListObtainStrategy = CachedStarsListObtainStrategy()
            } else {
                self.starsList.jsonListObtainStrategy = NonCachedStarsListObtainStrategy()
                self.starsList.loadStarsList()
            }
        }
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "StarView", bundle: nil)
        let starDetailsViewController = storyBoard.instantiateViewController(withIdentifier: "StarViewController") as! StarViewController
        let starDetailsNavController = UINavigationController(rootViewController: starDetailsViewController)
        self.navigationController?.present(starDetailsNavController, animated: true, completion: nil)
        starDetailsViewController.selectedStarIndex = indexPath.row
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }

    func collectionView(_ collectionView: UICollectionView, numberOfSectionsInCollectionView: Int) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return starsList.stars.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = starsCollectionView.dequeueReusableCell(withReuseIdentifier: "StarsItemCell", for: indexPath) as! StarsItemCell
        cell.setImageDownloadCompleteHandler()
        cell.title = starsList.stars[indexPath.row].title
        cell.thumbnailUrl = starsList.stars[indexPath.row].thumbnailUrl
        cell.repaint()
        return cell
    }

    func handleDownloadCompleteEvent() {
        DispatchQueue.main.async {
            self.listLoadingIndicator.stopAnimating()
            self.starsCollectionView.isHidden = false
        }
    }
}
