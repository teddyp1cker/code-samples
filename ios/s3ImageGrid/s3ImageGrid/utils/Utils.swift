//
//  Utils.swift
//  s3ImageGrid
//
//  Created by Iurii Kuznetcov on 24/06/2017.
//  Copyright © 2017 YuriKuznetcov. All rights reserved.
//

import Foundation
import UIKit

extension UIImageView {

    private struct ObserversStruct {
        static var observers: [Observer] = []
    }

    func addObserver(observer: Observer?) {
        if let observer = observer {
            ObserversStruct.observers.append(observer)
        }
    }

    func notifyObservers() {
        for observer in ObserversStruct.observers {
            observer.handleDownloadCompleteEvent()
        }
    }

    public func displayFromURL(urlString: String) {
        URLSession.shared.dataTask(with: NSURL(string: urlString)! as URL, completionHandler: { (data, response, error) -> Void in
            if error != nil {
                return
            }
            DispatchQueue.main.async(execute: { () -> Void in
                let image = UIImage(data: data!)
                self.image = image
                self.notifyObservers()
            })

        }).resume()
    }
}

extension UIView {

    func fadeIn(_ duration: TimeInterval = 1.0, delay: TimeInterval = 0.0, completion: @escaping ((Bool) -> Void) = { (finished: Bool) -> Void in
        }) {
        UIView.animate(withDuration: duration, delay: delay, options: UIViewAnimationOptions.curveEaseIn, animations: {
            self.alpha = 1.0
        }, completion: completion)
    }

    func fadeOut(_ duration: TimeInterval = 1.0, delay: TimeInterval = 0.0, completion: @escaping (Bool) -> Void = { (finished: Bool) -> Void in
        }) {
        UIView.animate(withDuration: duration, delay: delay, options: UIViewAnimationOptions.curveEaseIn, animations: {
            self.alpha = 0.0
        }, completion: completion)
    }
}

/** Returns user directory URL **/

func getDocumentsDirectoryURL() -> URL? {

    let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
    let documentsDirectory = paths[0]
    return documentsDirectory
}
