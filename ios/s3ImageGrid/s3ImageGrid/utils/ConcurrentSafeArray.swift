//
//  ConcurrentSafeArray.swift
//  s3ImageGrid
//
//  Created by Iurii Kuznetcov on 18/07/2017.
//  Copyright © 2017 YuriKuznetcov. All rights reserved.
//

import Foundation

public struct ConcurrentSafeArray<Element> {

    fileprivate var internalUnsafeArray = Array<Element>()
    fileprivate var dispatchQueue: DispatchQueue = DispatchQueue(label: "ConcurrentSafeArrayQueue")

    public init(withElements elements: [Element]? = nil) {
        guard let elements = elements else {
            return
        }

        dispatchQueue.sync {
            self.internalUnsafeArray.append(contentsOf: elements)
        }
    }
}

public extension ConcurrentSafeArray {

    public var elements: [Element] {
        get {
            var elements: [Element] = []

            dispatchQueue.sync {
                elements.append(contentsOf: internalUnsafeArray)
            }

            return elements
        }
    }

    public mutating func reset(withElements elements: [Element]) {

        dispatchQueue.sync {
            self.internalUnsafeArray = elements
        }
    }

    public mutating func append(_ element: Element) {

        dispatchQueue.sync {
            internalUnsafeArray.append(element)
        }
    }

    public mutating func append(contentsOf elements: [Element]) {

        dispatchQueue.sync {
            self.internalUnsafeArray.append(contentsOf: elements)
        }
    }

    public func size() -> Int {

        var count = 0
        dispatchQueue.sync {
            count = self.internalUnsafeArray.count
        }
        return count
    }

    public func map<T>(_ transform: (Element) throws -> T) rethrows -> ConcurrentSafeArray<T> {

        var safeArray = ConcurrentSafeArray<T>()

        var results: [T] = []

        try dispatchQueue.sync {
            results = try self.internalUnsafeArray.map(transform)
        }

        safeArray.append(contentsOf: results)

        return safeArray
    }

    public func filter(_ isIncluded: (Element) throws -> Bool) rethrows -> ConcurrentSafeArray<Element> {

        var safeArray = ConcurrentSafeArray<Element>()

        var results: [Element] = []

        try dispatchQueue.sync {
            results = try self.internalUnsafeArray.filter(isIncluded)
        }

        safeArray.append(contentsOf: results)

        return safeArray
    }
}
