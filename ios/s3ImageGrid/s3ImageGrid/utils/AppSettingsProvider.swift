//
//  AppSettings.swift
//  s3ImageGrid
//
//  Created by Iurii Kuznetcov on 24/06/2017.
//  Copyright © 2017 YuriKuznetcov. All rights reserved.
//

import Foundation
import CocoaLumberjack

class AppSettingsProvider {

    private let userDefaults: UserDefaults?

    static let sharedInstance = AppSettingsProvider()

    init() {
        userDefaults = UserDefaults.standard
    }

    func getImageSetURL() -> NSURL? {
        var imageSetURL: NSURL?
        if let userDefaults = userDefaults {
            if let imagesListUrl = userDefaults.string(forKey: "images_list_url") {
                if imagesListUrl.isEmpty {
                    imageSetURL = NSURL(string: imagesListUrl)
                } else {
                    DDLogError("Stars image set URL is not set or empty. Please check app settings.")
                }
            } else {
                DDLogError("Stars image set URL is not set or empty. Please check app settings.")
            }
        } else {
            DDLogError("User settings wasn't properly initialized.")
        }
        return imageSetURL
    }

    func setImageSetURL(imageSetURL: NSURL?) {
        if let userDefaults = userDefaults {
            if let imageSetURL = imageSetURL {
                if (imageSetURL.absoluteString?.isEmpty)! {
                    DDLogError("Trying to save empty URL. Skipping save....")
                } else {
                    userDefaults.set(imageSetURL.absoluteString, forKey: "images_list_url")
                }
            } else {
                DDLogError("Trying to save non-existing URL. Skipping save...")
            }
        } else {
            DDLogError("User settings wasn't properly initialized.")
        }
    }

    func isImageCacheEnabled() -> Bool {
        var imageCacheEnabled = false
        if let userDefaults = userDefaults {
            imageCacheEnabled = userDefaults.bool(forKey: "images_cache_enabled")
        } else {
            DDLogError("User settings wasn't properly initialized.")
        }
        return imageCacheEnabled
    }

    func setImageCacheEnabled(imagesCacheEnabled: Bool) {
        if let userDefaults = userDefaults {
            userDefaults.set(imagesCacheEnabled, forKey: "images_cache_enabled")
        } else {
            DDLogError("User settings wasn't properly initialized.")
        }
    }
}
