//
//  StarsItem.swift
//  s3ImageGrid
//
//  Created by Iurii Kuznetcov on 25/06/2017.
//  Copyright © 2017 YuriKuznetcov. All rights reserved.
//

import Foundation

class StarItem {

    var title: String?
    var description: String?
    var thumbnailUrl: String?
    var originalUrl: String?

    init(title: String?, description: String?, thumbnailUrl: String?, originalUrl: String?) {
        self.description = description
        self.title = title
        self.originalUrl = originalUrl
        self.thumbnailUrl = thumbnailUrl
    }
}
