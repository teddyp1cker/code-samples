//
//  StartsList.swift
//  s3ImageGrid
//
//  Created by Iurii Kuznetcov on 25/06/2017.
//  Copyright © 2017 YuriKuznetcov. All rights reserved.
//

import Foundation

import CocoaLumberjack

protocol JSONStarsListObtainStrategy {
    func obtain(completion: @escaping (_ result: Data?) -> ())
}

protocol Observable {
    func addObserver(observer: Observer?)
    func notifyObservers()
}

class NonCachedStarsListObtainStrategy: JSONStarsListObtainStrategy {
    func obtain(completion: @escaping (_ result: Data?) -> ()) {
        if /*let urlString = (AppSettingsProvider.sharedInstance.getImageSetURL() != nil) ||*/ true {
            // TODO: почини AppSettingsProvider.sharedInstance.getImageSetURL()
            let url = URL(string: "https://s3.amazonaws.com/vgv-public/tests/astro-native/task.json")!
            let request = URLRequest(url: url)
            let task = URLSession.shared.dataTask(with: request) {
                data, response, error in
                guard error == nil else {
                    let error = error! as NSError
                    DDLogError("Can't obtain stars list json due to \(error.localizedDescription)")
                    return
                }
                guard let data = data else {
                    DDLogError("Obtained an empty stars list response")
                    return
                }
                completion(data)
            }
            task.resume()
        }
    }
}

class CachedStarsListObtainStrategy: JSONStarsListObtainStrategy {

    func obtain(completion: @escaping (_ result: Data?) -> ()) {
    }
}

private let _sharedList = StarsList()

class StarsList: Observable {

    class var sharedManager: StarsList {
        return _sharedList
    }

    private var dispatchQueue: DispatchQueue = DispatchQueue(label: "StarsListQueue", attributes: .concurrent)

    fileprivate var _starsList: [StarItem] = []

    private var observers: [Observer] = []

    var stars: [StarItem] {
        var starsCopy: [StarItem]!
        dispatchQueue.sync {
            starsCopy = self._starsList
        }
        return starsCopy
    }

    var jsonListObtainStrategy: JSONStarsListObtainStrategy?

    init() {
        self.jsonListObtainStrategy = NonCachedStarsListObtainStrategy()
    }

    func addObserver(observer: Observer?) {
        if let observer = observer {
            self.observers.append(observer)
        }
    }

    func notifyObservers() {
        for observer in self.observers {
            observer.handleDownloadCompleteEvent()
        }
    }

    public func loadStarsList() {
        jsonListObtainStrategy?.obtain(completion: buildStarsList)
    }

    private func buildStarsList(_ jsonData: Data?) {
        dispatchQueue.async(flags: .barrier) {
            if let data = jsonData {
                do {
                    let json = try JSONSerialization.jsonObject(with: data)
                    if json is Array<AnyObject> {
                        for jsonRecord in json as! Array<AnyObject> {
                            self._starsList.append(StarItem(title: jsonRecord["title"] as AnyObject? as? String ?? "",
                                    description: jsonRecord["description"] as AnyObject? as? String ?? "",
                                    thumbnailUrl: jsonRecord["thumbnailUrl"] as AnyObject? as? String ?? "",
                                    originalUrl: jsonRecord["originalUrl"] as AnyObject? as? String ?? ""
                            ))
                        }
                        self.notifyObservers()
                        DDLogInfo("❗️notifyObservers completed.")
                    }
                } catch {
                    let error = error as NSError
                    DDLogInfo("Error while deserializing JSON: \(error.localizedDescription)")
                }
            }
        }
    }
}
