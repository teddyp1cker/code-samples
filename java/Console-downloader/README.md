### Console downloader app.

Simple http only (w/o authorization) file downloader. Reads links from input text file that contains pairs <url> <filname> and downloads them to output folder. Supports partial, chunks downloading with given number of downloading threads. Also supports download speed limit. Doesn't downloads files with the same url twice - just copies them.

Uses Gradle as build system and dependencies manager.