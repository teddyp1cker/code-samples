package com.ecwid.testtask;


import com.ecwid.testtask.utils.Config;
import com.ecwid.testtask.utils.Intervals;
import com.ecwid.testtask.utils.SpeedRatio;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

public class SequentialDownloader {

    private static Logger log = Logger.getLogger(SequentialDownloader.class.getName());

    private String outFolderPath;

    public SequentialDownloader() {

    }

    public void init() throws IOException {

        DownloadList.getInstance().loadFromFile(Config.getInstance().getLinksFilePath());

        this.outFolderPath = Config.getInstance().getOutputDirectoryPath();

        updateDownloadItemSizes();

    }

    private void updateDownloadItemSizes() {

        ExecutorService executorService = Executors.newFixedThreadPool(DownloadList.getInstance().getDownloadTasksNumber());

        for (int i = 0; i < DownloadList.getInstance().getDownloadTasksNumber(); i++) {

            final int finalI = i;

            executorService.submit(new Runnable() {

                public void run() {

                    URL itemUrl = null;

                    try {

                        itemUrl = new URL(DownloadList.getInstance().getItemsList().get(finalI).getUrl());

                    } catch (MalformedURLException e) {

                        log.log(Level.INFO, e.getMessage());

                    }

                    HttpURLConnection conn = null;

                    try {

                        conn = (HttpURLConnection) itemUrl.openConnection();
                    } catch (IOException e) {

                        log.log(Level.INFO, e.getMessage());
                    }

                    conn.setConnectTimeout(Config.getInstance().getDefaultConnectTimeout());

                    try {

                        conn.setRequestMethod("GET");

                    } catch (ProtocolException e) {

                        log.log(Level.INFO, e.getMessage());
                    }
                    conn.setRequestProperty("User-Agent", Config.getInstance().getDefaultUserAgent());

                    try {

                        conn.connect();

                    } catch (IOException e) { log.log(Level.INFO, e.getMessage()); }


                    if (conn.getHeaderField("Content-Length") != null || !conn.getHeaderField("Content-Length").isEmpty()) {

                        DownloadList.getInstance().getItemsList().get(finalI).setSize(Long.parseLong(conn.getHeaderField("Content-Length")));
                    }
                }
            });
        }

        executorService.shutdown();

        try {

            executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);

        } catch (InterruptedException e) {

            log.log(Level.INFO, e.getMessage());
        }

    }

    public long downloadAll() throws Exception {

        long totalBytesReceived = 0;

        int downloadListSize = DownloadList.getInstance().getDownloadTasksNumber();

        for (int i = 0; i < downloadListSize; i++) {

            try {

                totalBytesReceived += downloadItemByIndex(i);

            } catch (Exception e) {

                System.out.println("Error while downloading file : " + DownloadList.getInstance().getItemByIndex(i)
                        .getUrl());
            }

        }

        return totalBytesReceived;
    }

    public long downloadItemByIndex(int index) throws Exception {

        long totalBytesReceived = 0;
        boolean isErrorOccurred = false;

        if (index >= 0 && index < DownloadList.getInstance().getDownloadTasksNumber()) {

            String filePathSeparator = File.separator;

            String fullOutFilePath = Config.getInstance().getOutputDirectoryPath()
                    + filePathSeparator + DownloadList.getInstance().getItemByIndex(index)
                    .getFilename();

            if (DownloadList.getInstance().getItemByIndex(index).getSize() != 0) {

                ExecutorService executorService = Executors.newFixedThreadPool(
                        Config.getInstance().getDownloadingThreadsNumber());

                Intervals fileChunksIntervals = new Intervals();

                ArrayList<String> bytesRanges = fileChunksIntervals.upTo(DownloadList.getInstance()
                        .getItemByIndex(index)
                        .getSize()).make(Config.getInstance()
                        .getDownloadingThreadsNumber());

                ArrayList<FutureTask<DownloadItemState>> downloadTasksFutures = new ArrayList<FutureTask</*Long*/ DownloadItemState>>();

                for (String bytesRange : bytesRanges) {

                    HttpFilePartDownloadTask httpFilePartDownloadTask = new HttpFilePartDownloadBuilder()
                            .url(DownloadList.getInstance().getItemByIndex(index).getUrl())
                            .filename(fullOutFilePath + ".part")
                            .downloadSpeed(new SpeedRatio(Config.getInstance()
                                    .getDownloadSpeedLimit())
                                    .toBytesPerSecond())
                            .fileBytesRange(bytesRange)
                            .build();

                    FutureTask<DownloadItemState> task = new FutureTask</*Long*/DownloadItemState>(httpFilePartDownloadTask);
                    executorService.submit(task);
                    downloadTasksFutures.add(task);

                }

                for (FutureTask<DownloadItemState> downloadTaskFuture : downloadTasksFutures) {

                    DownloadItemState taskResult = downloadTaskFuture.get();

                    long currentTaskBytesReceived = taskResult.getTotalBytesProcessed();

                    if (taskResult.getStatus() != DownloadItemStatus.SUCCEED) {

                        isErrorOccurred = true;

                    } else {

                        totalBytesReceived += currentTaskBytesReceived;
                    }

                }

                executorService.shutdown();

                if (isErrorOccurred) {

                    System.out.println("Error while downloading part of file : " + fullOutFilePath);

                } else {

                    Files.move(Paths.get(fullOutFilePath + ".part"), Paths.get(fullOutFilePath), REPLACE_EXISTING);

                    System.out.println("File downloaded successfully : " + DownloadList.getInstance().getItemByIndex(index).getUrl());
                }

            } else {

                ExecutorService executorService = Executors.newFixedThreadPool(1);

                HttpFileDownloadTask httpFileDownloadTask = new HttpFileDownloadTaskBuilder()
                        .url(DownloadList.getInstance().getItemByIndex(index).getUrl())
                        .filename(fullOutFilePath + ".part")
                        .downloadSpeed(new SpeedRatio(Config.getInstance().getDownloadSpeedLimit()).toBytesPerSecond())
                        .build();

                FutureTask<DownloadItemState> future = new FutureTask</*Long*/DownloadItemState>(httpFileDownloadTask);
                executorService.submit(future);
                executorService.shutdown();

                DownloadItemState taskResult = future.get();

                long currentTaskBytesReceived = taskResult.getTotalBytesProcessed();

                if (taskResult.getStatus() != DownloadItemStatus.SUCCEED) {

                    System.out.println("Error while downloading file : " + taskResult.getFilename());

                } else {

                    totalBytesReceived += currentTaskBytesReceived;

                    Files.move(Paths.get(fullOutFilePath + ".part"), Paths.get(fullOutFilePath), REPLACE_EXISTING);

                    System.out.println("File downloaded successfully : " + taskResult.getUrl());

                }

            }

            // TODO: не забудь про сжатые ответы - там может быть неверный content-length ->
            // качаем в один поток, чанки не сделать корректно

        } else {

            log.log(Level.SEVERE, "Download list items's index is out ot range");

        }

        return totalBytesReceived;

    }

}
