package com.ecwid.testtask.utils;


import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

public class Intervals {

    private static final long START_VALUE = 0;
    ArrayList subIntervalsBorders;
    ArrayList<String> subIntervals;
    private long endValue;
    private long subIntervalsNumber;
    private float roundedOffset;

    public Intervals() {

        this.subIntervalsBorders = new ArrayList<Long>();
        this.subIntervals = new ArrayList<String>();
    }

    public Intervals upTo(long endValue) {

        this.endValue = endValue;

        return this;
    }

    public ArrayList<String> make(long intervalsNumber) {

        this.subIntervalsNumber = intervalsNumber;

        float offset = (float) this.endValue / this.subIntervalsNumber;
        this.roundedOffset = new BigDecimal(offset).setScale(0, RoundingMode.HALF_UP).longValue();

        int j = 0;

        for (long i = 0; i < this.endValue; i += roundedOffset) {

            if (j != this.subIntervalsNumber) {

                this.subIntervalsBorders.add(new Long(i));
                j++;
            }
        }

        this.subIntervalsBorders.add(this.endValue - 1);

        for (int i = 1; i < this.subIntervalsBorders.size(); i++) {

            Long rightBorder;
            Long leftBorder;

            leftBorder = (Long) this.subIntervalsBorders.get(i - 1);

            if (i != subIntervalsBorders.size() - 1) {

                rightBorder = ((Long) this.subIntervalsBorders.get(i)) - 1;

            } else {

                rightBorder = ((Long) this.subIntervalsBorders.get(i));
            }

            this.subIntervals.add(leftBorder + "-" + rightBorder);
        }


        return this.subIntervals;
    }

    @Override
    public String toString() {

        String strRepresentation = "";

        for (String subInterval : subIntervals) {

            strRepresentation += subInterval + " ";
        }

        return strRepresentation;
    }
}
