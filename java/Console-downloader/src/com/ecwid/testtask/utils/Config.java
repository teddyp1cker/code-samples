package com.ecwid.testtask.utils;


import com.ecwid.testtask.DownloadMode;

public class Config {

    private static final int CONNECT_TIMEOUT = 10000;
    private static final String USER_AGENT = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.85 Safari/537.36";
    private static final String LINK_AND_FILENAME_SEPARATOR = " ";

    private static volatile Config instance;

    private int downloadingThreadsNumber;
    private String downloadSpeedLimit;
    private String linksFilePath;
    private String outputDirectoryPath;
    private DownloadMode downloadMode;

    private Config() {

    }

    public static Config getInstance() {

        Config r = instance;

        if (r == null) {

            synchronized (Config.class) {

                r = instance;
                if (r == null) {
                    r = new Config();
                    instance = r;
                }
            }
        }
        return r;
    }

    public static String getLinkAndFilenameSeparator() {

        return LINK_AND_FILENAME_SEPARATOR;
    }

    public String getDefaultUserAgent() {
        return USER_AGENT;
    }

    public int getDefaultConnectTimeout() {
        return CONNECT_TIMEOUT;
    }

    public DownloadMode getDownloadMode() {
        return downloadMode;
    }

    public void setDownloadMode(DownloadMode downloadMode) {
        this.downloadMode = downloadMode;
    }

    public int getDownloadingThreadsNumber() {
        return downloadingThreadsNumber;
    }

    public void setDownloadingThreadsNumber(int downloadingThreadsNumber) {
        this.downloadingThreadsNumber = downloadingThreadsNumber;
    }

    public String getDownloadSpeedLimit() {
        return downloadSpeedLimit;
    }

    public void setDownloadSpeedLimit(String downloadSpeedLimit) {
        this.downloadSpeedLimit = downloadSpeedLimit;
    }

    public String getLinksFilePath() {
        return linksFilePath;
    }

    public void setLinksFilePath(String linksFilePath) {
        this.linksFilePath = linksFilePath;
    }

    public String getOutputDirectoryPath() {
        return outputDirectoryPath;
    }

    public void setOutputDirectoryPath(String outputDirectoryPath) {
        this.outputDirectoryPath = outputDirectoryPath;
    }

    public void parse(String args) {
        throw new UnsupportedOperationException();
    }

    @Override
    public String toString() {
        return "Config@" + hashCode() + "{" +
                "downloadingThreadsNumber=" + downloadingThreadsNumber +
                ", downloadSpeedLimit='" + downloadSpeedLimit + '\'' +
                ", linksFilePath='" + linksFilePath + '\'' +
                ", outputDirectoryPath='" + outputDirectoryPath + '\'' +
                ", downloadMode=" + downloadMode +
                '}';
    }

}
