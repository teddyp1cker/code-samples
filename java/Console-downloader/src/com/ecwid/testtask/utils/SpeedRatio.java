package com.ecwid.testtask.utils;

// допустимый формат <число> | <число>k | <число>m

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SpeedRatio {

    private final String RATIO_FORMAT_PATTERN = "\\b\\d+\\s?[kKmM]?\\b";

    private Pattern pattern;
    private Matcher matcher;

    private String ratio;
    private long bytesPerSec;

    public SpeedRatio(String ratio) {

        this.ratio = ratio.replaceAll("\\s+", "");

        if (this.ratio != null) {

            pattern = Pattern.compile(RATIO_FORMAT_PATTERN);
            matcher = pattern.matcher(ratio);

        }
    }

    public boolean isCorrect() {

        if (this.ratio != null) {

            return matcher.matches();

        } else {

            return false;
        }

    }


    public long toBytesPerSecond() {

        if (this.ratio != null && isCorrect()) {

            if (this.ratio.toLowerCase().endsWith("k") /* || this.ratio.endsWith("K")*/) {

                this.bytesPerSec = 1024 * (Long.parseLong(this.ratio.replaceAll("[kK]", "")));

            } else if (this.ratio.toLowerCase().endsWith("m") /* || this.ratio.endsWith("M")*/) {

                this.bytesPerSec = 1048576 * (Long.parseLong(this.ratio.replaceAll("[mM]", "")));

            } else {

                this.bytesPerSec = Long.parseLong(this.ratio);
            }

        } else {

            this.bytesPerSec = 0;
        }

        return this.bytesPerSec;
    }


}
