package com.ecwid.testtask;


public enum DownloadMode {

    SEQUENTIAL,
    PARALLEL
}
