package com.ecwid.testtask;

public enum DownloadItemStatus {

    SUCCEED,
    FAILED_BY_URL_CONNECTION,
    FAILED_BY_WRITER,
    FAILED_BY_URL_STREAM,
    INTERRUPTED
}
