package com.ecwid.testtask;

import org.apache.commons.cli.*;

public class CliOptionsHandler {

    public static final String N = "n";
    public static final String L = "l";
    public static final String M = "m";
    public static final String F = "f";
    private final String O = "o";

    private Options options;
    private CommandLine line;

    public CliOptionsHandler() {

        options = new Options();

        options.addOption(N, "number", true, "Download threads number");
        options.addOption(L, "limit", true, "Download speed limit");
        options.addOption(M, "mode", true, "Download mode : S | P");
        options.addOption(F, "file", true, "Links file");
        options.addOption(O, "output", true, "Output folder");

        options.getOption(N).setArgs(1);
        options.getOption(N).setOptionalArg(false);
        options.getOption(N).setArgName("Download threads number");

        options.getOption(L).setArgs(1);
        options.getOption(L).setOptionalArg(false);
        options.getOption(L).setArgName("Download speed limit");

        options.getOption(M).setArgs(1);
        options.getOption(M).setOptionalArg(true);
        options.getOption(M).setArgName("Download mode : S | P");

        options.getOption(F).setArgs(1);
        options.getOption(F).setOptionalArg(false);
        options.getOption(F).setArgName("Links file");

        options.getOption(O).setArgs(1);
        options.getOption(O).setOptionalArg(false);
        options.getOption(O).setArgName("Output folder");

        options.getOption(N).setRequired(true);
        options.getOption(L).setRequired(true);
        options.getOption(M).setRequired(false);
        options.getOption(F).setRequired(true);
        options.getOption(O).setRequired(true);

    }

    public void parse(String[] args) throws ParseException {

        CommandLineParser parser = new DefaultParser();

        line = parser.parse(options, args);

    }

    public void printCliHelp() {

        HelpFormatter formatter = new HelpFormatter();

        formatter.printHelp("java -jar utility.jar -n 5 -l 2000k -o output_folder -f links.txt", "Read following instructions for starting download utility",
                options, "");

    }

    public CommandLine getLine() {
        return line;
    }

    public Options getOptions() {
        return options;
    }

    public boolean areAllArgumentsValid() {

        if (line != null) {

            return (line.hasOption(N) && line.hasOption(L) && line.hasOption(F) && line.hasOption(O));
        } else {

            return false;
        }

    }
}
