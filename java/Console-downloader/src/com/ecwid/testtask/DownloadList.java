package com.ecwid.testtask;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DownloadList {

    private final static String LINK_AND_FILENAME_SEPARATOR = " ";
    private static volatile DownloadList instance;
    private String path;
    private List<DownloadItem> list;

    private DownloadList() {

        this.list = Collections.synchronizedList(new ArrayList<DownloadItem>());
    }

    public static DownloadList getInstance() {

        DownloadList r = instance;

        if (r == null) {

            synchronized (DownloadList.class) {

                r = instance;
                if (r == null) {
                    r = new DownloadList();
                    instance = r;
                }
            }
        }
        return r;
    }

    public ArrayList<DownloadItem> getItemByUrl(String url) {

        ArrayList<DownloadItem> itemsByUrl = new ArrayList<DownloadItem>();

        for (DownloadItem item : this.list) {

            if (item.getUrl().equals(url)) {

                itemsByUrl.add(item);
            }
        }

        return itemsByUrl;
    }

    public ArrayList<DownloadItem> getItemByFilename(String filename) {

        ArrayList<DownloadItem> itemsByFilename = new ArrayList<DownloadItem>();

        for (DownloadItem item : this.list) {

            if (item.getFilename().equals(filename)) {

                itemsByFilename.add(item);
            }
        }

        return itemsByFilename;
    }

    public DownloadItem getItemByIndex(int index) {

        return this.list.get(index);

    }

    public void loadFromFile(String filePath) throws IOException {

        this.path = filePath;

        String record;
        BufferedReader reader = null;

        reader = new BufferedReader(new FileReader(this.path));

        while ((record = reader.readLine()) != null) {

            if (record.length() > 0) {

                String[] parts = record.split(LINK_AND_FILENAME_SEPARATOR);

                DownloadItem item = new DownloadItem();

                item.setFilename(parts[1]);
                item.setUrl(parts[0]);

                if (!this.list.contains(item)) {

                    this.list.add(item);
                }

            }
        }
    }

    public String getPath() {

        return this.path;
    }

    public List<DownloadItem> getHashMap() {

        return this.list;
    }

    public List<DownloadItem> getItemsList() {

        return this.list;
    }

    @Override
    public String toString() {

        StringBuffer buffer = new StringBuffer();

        for (DownloadItem downloadItem : list) {

            buffer.append(downloadItem.toString());

        }

        return buffer.toString();
    }

    public int getDownloadTasksNumber() {

        return this.list.size();
    }

    public ArrayList<String> getUrls() {

        ArrayList<String> urls = new ArrayList<>();

        for (DownloadItem downloadItem : list) {

            if (!urls.contains(downloadItem.getUrl())) {

                urls.add(downloadItem.getUrl());
            }
        }

        return urls;
    }

    public ArrayList<String> getDestinationFileNames() {

        ArrayList<String> filenames = new ArrayList<>();

        for (DownloadItem downloadItem : list) {

            if (!filenames.contains(downloadItem.getFilename())) {

                filenames.add(downloadItem.getFilename());
            }
        }

        return filenames;
    }

    public ArrayList<DownloadItem> getItemsWithZeroFileLength() {

        ArrayList<DownloadItem> zeroBytesItems = new ArrayList<>();

        for (DownloadItem item : list) {

            if (item.getSize() == 0) {

                zeroBytesItems.add(item);
            }
        }

        return zeroBytesItems;
    }

    public ArrayList<DownloadItem> getItemsByUrlAndFilename(String url, String filename) {

        ArrayList<DownloadItem> items = new ArrayList<>();

        for (DownloadItem item : list) {

            if (item.getUrl() == url && item.getFilename() == filename) {

                items.add(item);
            }
        }

        return items;
    }

}
