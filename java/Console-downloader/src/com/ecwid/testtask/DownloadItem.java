package com.ecwid.testtask;

public class DownloadItem {

    private String url;
    private String filename;
    private long size;
    private boolean isValid;

    public boolean isValid() {
        return isValid;
    }

    public void setIsValid(boolean isValid) {
        this.isValid = isValid;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    @Override
    public String toString() {
        return "DownloadItem@" + this.hashCode() + "{" +
                "url='" + url + '\'' +
                ", filename='" + filename + '\'' +
                ", size=" + size +
                '}';
    }
}
