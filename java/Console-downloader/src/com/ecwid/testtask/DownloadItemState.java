package com.ecwid.testtask;

public class DownloadItemState {

    private String url;
    private String filename;

    private DownloadItemStatus status;
    private String errDescription;
    private long totalBytesProcessed;
    private long totalTimeSpent;

    @Override
    public String toString() {
        return "DownloadItemState@" + hashCode() + "{" +
                "url=" + url +
                ", filename=" + filename +
                ", status=" + status +
                ", errDescription='" + errDescription + '\'' +
                ", totalBytesProcessed=" + totalBytesProcessed +
                ", totalTimeSpent=" + totalTimeSpent +
                '}';
    }

    public DownloadItemStatus getStatus() {
        return status;
    }

    public void setStatus(DownloadItemStatus status) {
        this.status = status;
    }

    public String getErrDescription() {
        return errDescription;
    }

    public void setErrDescription(String errDescription) {
        this.errDescription = errDescription;
    }

    public long getTotalBytesProcessed() {
        return totalBytesProcessed;
    }

    public void setTotalBytesProcessed(long totalBytesProcessed) {
        this.totalBytesProcessed = totalBytesProcessed;
    }

    public long getTotalTimeSpent() {
        return totalTimeSpent;
    }

    public void setTotalTimeSpent(long totalTimeSpent) {
        this.totalTimeSpent = totalTimeSpent;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }
}
