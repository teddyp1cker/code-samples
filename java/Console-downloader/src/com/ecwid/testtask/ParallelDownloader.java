package com.ecwid.testtask;


import com.ecwid.testtask.utils.Config;
import com.ecwid.testtask.utils.SpeedRatio;

import java.io.File;
import java.io.IOException;
import java.net.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;


public class ParallelDownloader {

    private static Logger log = Logger.getLogger(ParallelDownloader.class.getName());

    private HashMap<String, List<String>> urlsMap;
    private String outFolderPath;
    private String fullOutFilePathPrefix;

    public ParallelDownloader() {

        this.urlsMap = new HashMap<>();
        fullOutFilePathPrefix = Config.getInstance().getOutputDirectoryPath()
                + File.separator;
    }

    private void updateDownloadItemSizes() throws Exception {

        ExecutorService executorService = Executors.newFixedThreadPool(DownloadList.getInstance().getDownloadTasksNumber());

        for (int i = 0; i < DownloadList.getInstance().getDownloadTasksNumber(); i++) {

            final int final_i = i;

            executorService.submit(new Runnable() {

                public void run() {

                    URL itemUrl = null;

                    try {

                        itemUrl = new URL(DownloadList.getInstance().getItemsList().get(final_i).getUrl());

                    } catch (MalformedURLException e) {

                        log.log(Level.INFO, "MalformedURLException exception raised " + e.getMessage());

                    }

                    HttpURLConnection conn = null;

                    try {

                        conn = (HttpURLConnection) itemUrl.openConnection();

                    } catch (IOException e) {

                        log.log(Level.INFO, "I/O exception raised " + e.getMessage());
                    }

                    conn.setConnectTimeout(Config.getInstance().getDefaultConnectTimeout());

                    try {

                        conn.setRequestMethod("GET");

                    } catch (ProtocolException e) {

                        log.log(Level.INFO, "Protocol exception raised " + e.getMessage());

                    }
                    conn.setRequestProperty("User-Agent", Config.getInstance().getDefaultUserAgent());

                    try {

                        conn.connect();

                    } catch (SocketTimeoutException e) {

                        log.log(Level.INFO, "Socket timeout exception raised " + e.getMessage());

                    } catch (IOException e) {

                        log.log(Level.INFO, "I/O exception raised " + e.getMessage());
                    }

                    if (conn.getHeaderField("Content-Length") != null || !conn.getHeaderField("Content-Length").isEmpty()) {

                        DownloadList.getInstance().getItemsList().get(final_i).setSize(Long.parseLong(conn.getHeaderField("Content-Length")));
                    }
                }
            });
        }

        executorService.shutdown();

        try {

            executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);

        } catch (InterruptedException e) {

            log.log(Level.INFO, "InterruptedExecution raised " + e.getMessage());
        }

    }

    public long download() throws Exception {

        long totalBytesReceived = 0;

        ExecutorService executorService = Executors.newFixedThreadPool(urlsMap.size());

        ArrayList<FutureTask<DownloadItemState>> downloadTasksFutures = new ArrayList<FutureTask</*Long*/ DownloadItemState>>();

        for (Map.Entry<String, List<String>> downloadItemByMap : urlsMap.entrySet()) {

            // добавлем только первые в списке файлов для кажого url
            // остальное просто перенесем (Files.copy)

            if (downloadItemByMap.getValue().size() > 0) {

                HttpFileDownloadTask httpFileDownloadTask = new HttpFileDownloadTaskBuilder()
                        .url(downloadItemByMap.getKey())
                        .filename(fullOutFilePathPrefix + downloadItemByMap.getValue().get(0) + ".part")
                        .downloadSpeed(
                                (long) (new SpeedRatio(Config.getInstance()
                                        .getDownloadSpeedLimit()).toBytesPerSecond()) / urlsMap.size())
                        .build();

                FutureTask<DownloadItemState> task = new FutureTask<>(httpFileDownloadTask);
                executorService.submit(task);
                downloadTasksFutures.add(task);

            }
        }

        for (FutureTask<DownloadItemState> downloadTaskFuture : downloadTasksFutures) {

            DownloadItemState taskResult = null;

            taskResult = downloadTaskFuture.get();

            if (taskResult.getStatus() == DownloadItemStatus.SUCCEED) {

                System.out.println("File downloaded successfully : " + taskResult.getUrl());

                totalBytesReceived += taskResult.getTotalBytesProcessed();

                Files.move(Paths.get(taskResult.getFilename()),
                        Paths.get(taskResult.getFilename().substring(0,
                                taskResult.getFilename().lastIndexOf("."))), REPLACE_EXISTING);

            }

        }

        executorService.shutdown();

        copyFileBySameUrl();

        return totalBytesReceived;
    }

    private void copyFileBySameUrl() throws IOException {

        for (Map.Entry<String, List<String>> urlsMapEntry : urlsMap.entrySet()) {

            final List<String> filenames = urlsMapEntry.getValue();

            if (filenames.size() > 1) {

                for (int i = 1; i < filenames.size(); i++) {

                    final int finalI = i;

                    new Thread(new Runnable() {
                        @Override
                        public void run() {

                            try {
                                Files.copy(Paths.get(fullOutFilePathPrefix + filenames.get(0)),
                                        Paths.get(fullOutFilePathPrefix + filenames.get(finalI)),
                                        REPLACE_EXISTING);

                                System.out.println("File '" + filenames.get(0) +
                                        "' copied to '" + filenames.get(finalI) + "'");


                            } catch (IOException e) {

                                System.out.println("Error while copying file '" + filenames.get(0) +
                                        "' to '" + filenames.get(finalI) + "'");

                            }
                        }
                    }).start();

                }
            }
        }
    }

    public void init() throws Exception {

        DownloadList.getInstance().loadFromFile(Config.getInstance().getLinksFilePath());

        this.outFolderPath = Config.getInstance().getOutputDirectoryPath();

        updateDownloadItemSizes();

        initializeUrlsMap();

    }


    public boolean isDownloadListValid() {

        boolean valid = true;

        for (DownloadItem downloadItem : DownloadList.getInstance().getItemsList()) {

            ArrayList<DownloadItem> itemsByFilename = DownloadList.getInstance().
                    getItemByFilename(downloadItem.getFilename());

            if (itemsByFilename.size() > 1) {

                valid = false;

                break;
            }
        }

        return valid;
    }

    private void initializeUrlsMap() {

        ArrayList<String> urls = DownloadList.getInstance().getUrls();

        for (String url : urls) {

            ArrayList<String> filenamesByUrlStr = new ArrayList<>();
            ArrayList<DownloadItem> filenamesByUrl = DownloadList.getInstance().getItemByUrl(url);

            for (DownloadItem filenameByUrl : filenamesByUrl) {

                if (!containsFilename(filenameByUrl.getFilename())) {

                    filenamesByUrlStr.add(filenameByUrl.getFilename());
                }
            }

            urlsMap.put(url, filenamesByUrlStr);
        }
    }

    private boolean containsFilename(String fileName) {

        boolean contains = false;

        for (Map.Entry<String, List<String>> urlMapEntry : urlsMap.entrySet()) {

             for (String filename : urlMapEntry.getValue()) {

                    if (filename.equals(fileName)) {

                        contains = true;

                        break;
                    }
             }
        }

        return contains;
    }

    public void updateUrlsMap() {

        ArrayList<String> usedFilename = new ArrayList<>();

        for (List<String> filenames : urlsMap.values()) {

            for (String filename : filenames) {

                usedFilename.add(filename);
            }
        }
    }

}
