package com.ecwid.testtask;


import java.io.FileNotFoundException;

public class HttpFilePartDownloadBuilder {

    private String urlString;
    private String destFilename;
    private long bytesPerSec;

    private String bytesRange;

    public HttpFilePartDownloadBuilder fileBytesRange(String bytesRange) {

        this.bytesRange = bytesRange;
        return this;
    }

    public HttpFilePartDownloadBuilder url(String urlString) {

        this.urlString = urlString;
        return this;
    }


    public HttpFilePartDownloadBuilder filename(String destFilename) {

        this.destFilename = destFilename;
        return this;
    }

    public HttpFilePartDownloadBuilder downloadSpeed(long bytesPerSec) {

        this.bytesPerSec = bytesPerSec;
        return this;
    }


    public String getUrl() {

        return urlString;
    }

    public String getFilename() {

        return destFilename;
    }

    public long getDownloadSpeed() {

        return bytesPerSec;
    }

    public String getBytesRange() {

        return bytesRange;
    }

    public HttpFilePartDownloadTask build() throws FileNotFoundException {

        return new HttpFilePartDownloadTask(this);
    }
}
