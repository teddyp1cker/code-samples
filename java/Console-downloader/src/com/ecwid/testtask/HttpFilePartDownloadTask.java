package com.ecwid.testtask;

import com.ecwid.testtask.utils.Config;

import java.io.BufferedInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.Callable;
import java.util.logging.Logger;

public class HttpFilePartDownloadTask implements Callable {

    private static Logger log = Logger.getLogger(HttpFilePartDownloadTask.class.getName());

    public static final int CONNECT_TIMEOUT = Config.getInstance().getDefaultConnectTimeout();
    public static final int BUFFER_SIZE = 4096;
    public static final String USER_AGENT_HEADER_STRING = Config.getInstance().getDefaultUserAgent();
    public static final long TIME_UNIT = 1000;

    private /*final*/ RandomAccessFile raf;
    private String url;
    private String destFilename;
    private String bytesRange;

    private long bytesPerSec;
    private long totalBytesRead;
    private long totalTime;
    private BufferedInputStream connInputStream;
    private long sleepDuration;
    private HttpURLConnection conn;

    private DownloadItemState state;

    HttpFilePartDownloadTask(HttpFilePartDownloadBuilder httpFilePartDownloadBuilder) throws FileNotFoundException {

        this.url = httpFilePartDownloadBuilder.getUrl();
        this.destFilename = httpFilePartDownloadBuilder.getFilename();
        this.bytesRange = httpFilePartDownloadBuilder.getBytesRange();
        this.bytesPerSec = httpFilePartDownloadBuilder.getDownloadSpeed();

        this.totalBytesRead = 0;

        this.state = new DownloadItemState();

        this.state.setFilename(this.getDestFilename());
        this.state.setUrl(this.getUrl());

    }

    public long getDownloadSpeed() {

        return bytesPerSec;
    }

    public void setDownloadSpeed(long downloadSpeed) {

        this.bytesPerSec = downloadSpeed;
    }

    public long getTotalBytesRead() {

        return totalBytesRead;
    }

    public long getTotalTime() {

        return totalTime;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDestFilename() {
        return destFilename;
    }

    public void setDestFilename(String destFilename) {
        this.destFilename = destFilename;
    }

    public String getBytesRange() {
        return bytesRange;
    }

    public void setBytesRange(String bytesRange) {
        this.bytesRange = bytesRange;
    }

    public void download() throws IOException, InterruptedException {

        long curTime = System.currentTimeMillis();
        long startTime = System.currentTimeMillis();

        URL url = null;

        try {

            url = new URL(this.url);

        } catch (MalformedURLException e) {

            totalTime = System.currentTimeMillis() - startTime;
        }

        try {

            url.openConnection();

            URL downloadUrl = new URL(this.url);

            conn = null;

            setupHttpURLConnection(downloadUrl);

            conn.connect();

            String[] parts = bytesRange.split("-");
            String startBytesRange = parts[0];
            String endBytesRange = parts[1];

            // synchronized (raf) {

            raf = new RandomAccessFile(destFilename, "rw");

            raf.seek(Long.parseLong(startBytesRange));

            // }

            connInputStream = new BufferedInputStream(conn.getInputStream());

            byte[] buffer = new byte[BUFFER_SIZE];

            long currentBytesRead = 0;

            int bytesRead = 0;

            while ((bytesRead = connInputStream.read(buffer, 0, BUFFER_SIZE)) != -1) {

                // synchronized (raf) {

                raf.write(buffer, 0, bytesRead);

                // }

                currentBytesRead += bytesRead;
                totalBytesRead += bytesRead;

                if (currentBytesRead >= bytesPerSec) {

                    currentBytesRead = 0;

                    sleepDuration = TIME_UNIT - (System.currentTimeMillis() - curTime);

                    if (sleepDuration > 0) {

                        Thread.sleep(sleepDuration);
                    }

                    curTime = System.currentTimeMillis();
                }
            }

        } catch (IOException e) {

            totalBytesRead = -1;

            this.state.setStatus(DownloadItemStatus.FAILED_BY_URL_STREAM);
            this.state.setErrDescription("Error opening or reading url input stream");
            this.state.setTotalBytesProcessed(totalBytesRead);
            this.state.setTotalTimeSpent(System.currentTimeMillis() - startTime);

        } finally {

            if (raf != null) {

                try {

                    raf.close();

                    this.state.setStatus(DownloadItemStatus.SUCCEED);
                    this.state.setErrDescription("");
                    this.state.setTotalBytesProcessed(totalBytesRead);
                    this.state.setTotalTimeSpent(System.currentTimeMillis() - startTime);

                } catch (IOException e) {

                    totalBytesRead = -1;

                    this.state.setStatus(DownloadItemStatus.FAILED_BY_WRITER);
                    this.state.setErrDescription("Error while closing random access file stream");
                    this.state.setTotalBytesProcessed(totalBytesRead);
                    this.state.setTotalTimeSpent(System.currentTimeMillis() - startTime);

                }
            }

            if (connInputStream != null) {
                try {

                    connInputStream.close();


                } catch (IOException e) {

                    totalBytesRead = -1;

                    this.state.setStatus(DownloadItemStatus.FAILED_BY_URL_STREAM);
                    this.state.setErrDescription("Error while closing url input stream");
                    this.state.setTotalBytesProcessed(totalBytesRead);
                    this.state.setTotalTimeSpent(System.currentTimeMillis() - startTime);
                }
            }
        }
    }

    private void setupHttpURLConnection(URL downloadUrl) throws IOException {

        conn = (HttpURLConnection) downloadUrl.openConnection();
        conn.setConnectTimeout(CONNECT_TIMEOUT);
        conn.setRequestMethod("GET");
        conn.setRequestProperty("Range", "bytes=" + this.bytesRange);
        conn.setRequestProperty("User-Agent", USER_AGENT_HEADER_STRING);
    }

    @Override
    public Object call() throws Exception {

        download();

        return state;
    }

    @Override
    public String toString() {
        return "HttpDownloadTask@" + this.hashCode() + "{" +
                "url='" + url + '\'' +
                ", destFilename='" + destFilename + '\'' +
                ", bytesRange='" + bytesRange + '\'' +
                ", bytesPerSec=" + bytesPerSec +
                ", totalBytesRead=" + totalBytesRead +
                ", totalTime=" + totalTime +
                '}';
    }

//    public void setIncorrectURL() {
//
//        if (url != null) {
//
//            url = "http://fake.org";
//        }
//
//    }
}
