package com.ecwid.testtask;


public class HttpFileDownloadTaskBuilder {

    private String urlString;
    private String destFilename;
    private long bytesPerSec;

    public HttpFileDownloadTaskBuilder url(String urlString) {

        this.urlString = urlString;
        return this;
    }

    public HttpFileDownloadTaskBuilder filename(String destFilename) {

        this.destFilename = destFilename;
        return this;
    }

    public HttpFileDownloadTaskBuilder downloadSpeed(long bytesPerSec) {

        this.bytesPerSec = bytesPerSec;
        return this;
    }


    public String getUrl() {

        return urlString;
    }

    public String getFilename() {

        return destFilename;
    }

    public long getDownloadSpeed() {

        return bytesPerSec;
    }

    public HttpFileDownloadTask build() {

        return new HttpFileDownloadTask(this);
    }
}
