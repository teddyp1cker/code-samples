package com.ecwid.testtask;

import com.ecwid.testtask.utils.Config;
import org.apache.commons.cli.ParseException;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.TimeUnit;

public class Runner {

    Config config;

    public static void main(String[] args) throws Exception {

        CliOptionsHandler cliOptionsHandler = new CliOptionsHandler();

        try {

            cliOptionsHandler.parse(args);

            if (cliOptionsHandler.areAllArgumentsValid()) {

                Config.getInstance().setDownloadingThreadsNumber(Integer.parseInt(cliOptionsHandler.getLine().getOptionValue("n")));
                Config.getInstance().setDownloadSpeedLimit(cliOptionsHandler.getLine().getOptionValue("l"));
                Config.getInstance().setLinksFilePath(cliOptionsHandler.getLine().getOptionValue("f"));
                Config.getInstance().setOutputDirectoryPath(cliOptionsHandler.getLine().getOptionValue("o"));

                boolean outDirExists = Files.exists(Paths.get(Config.getInstance().getOutputDirectoryPath()));
                boolean outDirCreated = false;

                if (!outDirExists) {

                        outDirCreated = new File(Config.getInstance().getOutputDirectoryPath()).mkdir();

                }

                if (outDirExists || (!outDirExists && outDirCreated)) {

                    if (cliOptionsHandler.getLine().hasOption("m")) {

                        if (cliOptionsHandler.getLine().getOptionValue("m").toLowerCase().equals("p")) {

                            Config.getInstance().setDownloadMode(DownloadMode.PARALLEL);

                            ParallelDownloader parallelDownloader = new ParallelDownloader();
                            parallelDownloader.init();

                            parallelDownloader.updateUrlsMap();

                            long startDownloadingTime = System.currentTimeMillis();
                            long totalBytesDownloaded = parallelDownloader.download();
                            long endDownloadingTime = System.currentTimeMillis();

                            System.out.println("Total bytes received : " + totalBytesDownloaded);
                            System.out.println("Total time spent : " + String.format("%d min, %d sec",
                                    TimeUnit.MILLISECONDS.toMinutes(endDownloadingTime - startDownloadingTime),
                                    TimeUnit.MILLISECONDS.toSeconds(endDownloadingTime - startDownloadingTime) -
                                            TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(endDownloadingTime
                                                    - startDownloadingTime))));


                        } else {

                            Config.getInstance().setDownloadMode(DownloadMode.SEQUENTIAL);

                            SequentialDownloader sequentialDownloader = new SequentialDownloader();

                            try {

                                long startDownloadingTime = System.currentTimeMillis();

                                sequentialDownloader.init();

                                long totalBytesDownloaded = sequentialDownloader.downloadAll();

                                long endDownloadingTime = System.currentTimeMillis();

                                System.out.println("Total bytes received : " + totalBytesDownloaded);
                                System.out.println("Total time spent : " + String.format("%d min, %d sec",
                                        TimeUnit.MILLISECONDS.toMinutes(endDownloadingTime - startDownloadingTime),
                                        TimeUnit.MILLISECONDS.toSeconds(endDownloadingTime - startDownloadingTime) -
                                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(endDownloadingTime
                                                        - startDownloadingTime))));
                            } catch (IOException e) {

                                System.out.println("I/O Exception raised : " + e.getMessage());
                            }

                        }

                    } else {

                        Config.getInstance().setDownloadMode(DownloadMode.SEQUENTIAL);

                        SequentialDownloader sequentialDownloader = new SequentialDownloader();

                        try {

                            long startDownloadingTime = System.currentTimeMillis();

                            sequentialDownloader.init();

                            long totalBytesDownloaded = sequentialDownloader.downloadAll();

                            long endDownloadingTime = System.currentTimeMillis();

                            System.out.println("Total bytes received : " + totalBytesDownloaded);
                            System.out.println("Total time spent : " + String.format("%d min, %d sec",
                                    TimeUnit.MILLISECONDS.toMinutes(endDownloadingTime - startDownloadingTime),
                                    TimeUnit.MILLISECONDS.toSeconds(endDownloadingTime - startDownloadingTime) -
                                            TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(endDownloadingTime
                                                    - startDownloadingTime))));


                        } catch (IOException e) {

                            System.out.println("I/O Exception raised : " + e.getMessage());
                        }
                    }
                } else {

                    System.out.println("Out dir is not accessible. Aborting...");
                }

            } else {

                cliOptionsHandler.printCliHelp();
            }

        } catch (ParseException e) {

            cliOptionsHandler.printCliHelp();

        }


    }
}
