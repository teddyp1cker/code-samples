package com.ecwid.testtask;

import com.ecwid.testtask.utils.Config;

import java.io.BufferedInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.Callable;

public class HttpFileDownloadTask implements Callable {

    public static final int CONNECT_TIMEOUT = Config.getInstance().getDefaultConnectTimeout();
    public static final int BUFFER_SIZE = 4096;
    public static final String USER_AGENT_HEADER_STRING = Config.getInstance().getDefaultUserAgent();
    public static final long TIME_UNIT = 1000;

    private FileOutputStream fos;
    private String urlString;
    private String destFilename;

    private long bytesPerSec;
    private long totalBytesRead;
    private long totalTime;

    private BufferedInputStream connInputStream;
    private long sleepDuration;

    private HttpURLConnection conn;
    private DownloadItemState state;

    private boolean isRunning;

    private long contentLength = -1;
    private long currentByteOffset = 0;

    HttpFileDownloadTask(HttpFileDownloadTaskBuilder httpFileDownloadTaskBuilder) {

        this.urlString = httpFileDownloadTaskBuilder.getUrl();
        this.destFilename = httpFileDownloadTaskBuilder.getFilename();
        this.bytesPerSec = httpFileDownloadTaskBuilder.getDownloadSpeed();

        this.totalBytesRead = 0;

        this.state = new DownloadItemState();

        this.state.setUrl(this.urlString);
        this.state.setFilename(this.getDestFilename());

    }

    public long getTotalBytesRead() {

        return totalBytesRead;
    }

    public long getTotalTime() {

        return totalTime;
    }

    public String getUrlString() {

        return urlString;
    }

    public void setUrlString(String urlString) {

        this.urlString = urlString;
    }

    public String getDestFilename() {

        return destFilename;
    }

    public void setDestFilename(String destFilename) {

        this.destFilename = destFilename;
    }

    void download() {

        long curTime = System.currentTimeMillis();
        long startTime = System.currentTimeMillis();

        URL downloadUrl = null;

        try {

            downloadUrl = new URL(this.urlString);

            initializeHttpURLConnection(downloadUrl);

            conn.connect();

            if (conn.getHeaderField("Content-Length") != null) {

                this.contentLength = Long.parseLong(conn.getHeaderField("Content-Length"));

            }

        } catch (IOException e) {

            totalBytesRead = -1;

            changeState(DownloadItemStatus.FAILED_BY_URL_CONNECTION,
                    "Error while opening url " + this.urlString,
                    totalBytesRead, totalTime);

            conn.disconnect();
        }

        byte[] buffer = new byte[BUFFER_SIZE];

        long currentBytesRead = 0;
        int bytesRead = 0;

        try {

            connInputStream = new BufferedInputStream(conn.getInputStream());

        } catch (IOException e) {

            totalBytesRead = -1;

            changeState(DownloadItemStatus.FAILED_BY_URL_STREAM,
                    "Error while opening connection buffer stream " + this.urlString,
                    totalBytesRead, totalTime);

            this.isRunning = false;

        }

        try {

            if (connInputStream != null) {

                fos = new FileOutputStream(destFilename);
            }

        } catch (FileNotFoundException e) {

            totalBytesRead = -1;

            changeState(DownloadItemStatus.FAILED_BY_WRITER,
                    "Error while opening output file " + this.destFilename,
                    totalBytesRead, totalTime);

            this.isRunning = false;
        }

        try {

            if (connInputStream != null && fos != null) {

                while ((this.isRunning == true) &&
                        (bytesRead = connInputStream.read(buffer, 0, BUFFER_SIZE)) != -1) {

                    fos.write(buffer, 0, bytesRead);

                    currentBytesRead += bytesRead;
                    totalBytesRead += bytesRead;

                    if (currentBytesRead >= bytesPerSec) {

                        currentBytesRead = 0;

                        sleepDuration = TIME_UNIT - (System.currentTimeMillis() - curTime);

                        if (sleepDuration > 0) {

                            Thread.sleep(sleepDuration);
                        }

                        curTime = System.currentTimeMillis();
                    }
                }

                if (this.isRunning) {

                    this.isRunning = false;
                }

                long endTime = System.currentTimeMillis();

                totalTime = endTime - startTime;

                connInputStream.close();
                fos.flush();
                fos.close();

                changeState(DownloadItemStatus.SUCCEED, "", totalBytesRead, totalTime);
            }

        } catch (IOException e) {

            this.totalBytesRead = -1;

            totalTime = System.currentTimeMillis() - startTime;

            changeState(DownloadItemStatus.FAILED_BY_URL_STREAM,
                    "Error while reading input stream / writing to file",
                    totalBytesRead, totalTime);


        } catch (InterruptedException e) {

            this.totalBytesRead = -1;

            changeState(DownloadItemStatus.INTERRUPTED,
                    "Interrupted", totalBytesRead, totalTime);

            this.isRunning = false;
        }

    }

    private void changeState(DownloadItemStatus status, String errDesc, long bytesProcessed, long timeSpent) {

        this.state.setStatus(status);
        this.state.setErrDescription(errDesc);
        this.state.setTotalBytesProcessed(bytesProcessed);
        this.state.setTotalTimeSpent(timeSpent);
    }

    private void initializeHttpURLConnection(URL downloadUrl) throws IOException {

        conn = (HttpURLConnection) downloadUrl.openConnection();
        conn.setConnectTimeout(CONNECT_TIMEOUT);
        conn.setRequestMethod("GET");
        conn.setRequestProperty("User-Agent", USER_AGENT_HEADER_STRING);
    }

    public void pause() {

        this.isRunning = false;
        this.currentByteOffset = totalBytesRead;
    }

    public boolean resume() {

        boolean isPossibleToResume = false;

        if (this.contentLength != -1) {

            isPossibleToResume = true;

            this.isRunning = true;
        }

        return isPossibleToResume;
    }

    @Override
    public Object call() {

        this.isRunning = true;

        download();

        return state;
    }

    @Override
    public String toString() {
        return "HttpDownloadTask{" +
                "urlString='" + urlString + '\'' +
                ", destFilename='" + destFilename + '\'' +
                ", bytesPerSec=" + bytesPerSec +
                ", totalTime=" + totalTime +
                ", totalBytesRead=" + totalBytesRead +
                ", sleepDuration=" + sleepDuration +
                ", isRunning=" + isRunning +
                '}';
    }
}
