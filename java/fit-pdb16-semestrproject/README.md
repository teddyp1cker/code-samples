**Helikoptéra taxi - Plán projektu**

xkuzne00@vutbr.cz (Iurii Kuznetcov)

[https://gitlab.com/xkuzne00/fit-pdb16-semestrproject](https://gitlab.com/xkuzne00/fit-pdb16-semestrproject)

**1. Specifikace aplikace**

**1.1. Popis projektu**

Aplikace umožní plánovat cestu na vrtulníku mezi body na některou mapě.

Pracovní aplikace vypíše letovou trasu od výchozího bodu (A, B, C, D) do konečného bodu (A, B, C, D). V případě, že může být několik cest, tehdy se zobrazí seznam všech dostupných tras (z řazením dle ceny trasy, po délce letu).

Zákazník si může rezervovat letenku na let, který bude uložen v historii letu.

**1.2. Vstupná ****data objednávku letu**

* požadovaný datum letu
* počet cestujících
* výchozí bod (bod počátku)
* bod určení (konečný bod)

**1.3. Výsledný data**

* seznam cest. Seznam muže byt prázdný - pak neexistuje ani jedna trasa mezi počátečním a koncovým bodem.

Letu cestou (mezi 2 body) je považována přímce spojující tyto 2 body. Trasa letu je přijatelná, pokud neprochází přímce polygon "mrtvé zóny" letu (uzavřený prostor, přes který helikoptéra neletá)

**2. Specifikace uloženích entit**

**2.1. Mapa**

Mapa představena v databázi jako obdélník se sadou geometrických objektů:

* přistávací plocha pro vrtulníky - bod se souřadnicemi
* uzavřený prostor, přes který helikoptéra neletá - uzavřený polygon

**2.2. Přistávací plocha pro vrtulníky**

* název
* status (zavřeno nebo otevřeno)
* souřadnice

**2.3. Vrtulník**

* foto
* název (výrobce a model)
* cena letu (pro 1 hodinu)
* rychlost
* kapacita (maximální počet osob)

**2.4. Polygon - uzavřený prostor (přes který helikoptéra neletá)**

* název
* uzavřený polygon

**2.5. Let**

* počáteční přistávací plocha pro vrtulníky
* koneční přistávací plocha pro vrtulníky
* datum a čas letu (časový interval)
* vrtulník
* zákazník
* vzdálenost letu
* cena letu

**2.6. Zákazník**

* jméno
* prijmeni  
* email
* telefone číslo

**3. Aplikace má následující typy uživatelé (role)**

* Uživatel-zákazník - může zařizovat a potvrzovat zákaz letu. Néma možnost změnit nějaký entity (zákazník, let, mapa a tak dále).

* Uživatel-administrátor - má možnost změnit geometrické a jiný uloženy entity (zákazník, let, mapa a tak dále).

  
**4. Možnosti (funkce) zakaznik**

Uživatel-zákazník generuje žádost o letu a získá seznam dostupných letů. Uživatel může potvrdit let a zatím ten let bude uložen v tabulce historii letu. Vybraný vrtulník bude obsazený během letu.

**5. Možnosti (funkce) administrátora**

Změna (vložení, upravovaní(změna), smazaní) mapy (interaktivní):

1. Přistávací plocha pro vrtulníky - bod, ve kterém můžete změnit umístění a název.
2. Polygon (uzavřený prostor) - změna souřadnic.
3. Změna vrtulníku - smazání a upravovaní všech atributů (fotky, rychlost, ceny)
4. Změna (editování, smazání) operace. Úpravě dat (souřadnice).
5. Hledání v historii let:
* počet letů v danem časovém intervale
* cena lety
* letový vzdálenost

**6. Uživatelský interface**

Aplikace je klient-server webové aplikace. Serverní část aplikace použije Oracle 12c databáze z rozšíření pro podporu multimediální, geometrické a časových údajů. Uživatelské rozhraní se skládá z následujících obrazovek:

* Obrazovka správy vlastnosti vrtulníky
* Obrazovka správy vlastnosti přistávacích ploch pro vrtulníky
* Obrazovka vytvoření nového letu
* Obrazovka zobrazení předchozí lety
