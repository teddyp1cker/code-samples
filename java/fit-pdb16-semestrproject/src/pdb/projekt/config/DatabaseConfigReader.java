package pdb.projekt.config;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * @author Iurii xkuzne00@stud.fit.vutbr.cz Kuznetcov
 */
public class DatabaseConfigReader {

    private static Logger logger = Logger.getLogger(DatabaseConfigReader.class.getName());

    private final String DEFAULT_DATABASE_CONFIG_FILENAME = "database.properties";

    private final int DEFAULT_PORT_NUMBER = 1521;

    private Properties properties;

    public DatabaseConfigReader() throws DatabaseConfigException {
        init();
    }

    private void init() throws DatabaseConfigException {

        properties = null;
        FileInputStream input = null;

        try {

            properties = new Properties();
            input = new FileInputStream(DEFAULT_DATABASE_CONFIG_FILENAME);

            properties.load(input);
        } catch (IOException e) {
            logger.log(Level.FATAL, "Error while parsing database configuration properties file : file not found or "
                    + "it's unable to parse. Exception is : ", e);
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    logger.log(Level.FATAL, "Error while closing database configuration properties file Exception is : ", e);
                }
            }
        }
    }

    /**
     * Build database connection config object from default file-based property configuration
     *
     * @return DatabaseConfig - database connection configuration
     */
    public DatabaseConfig buildDatabaseConfig() {

        if (properties.size() > 0) {
            DatabaseConfigBuilder databaseConfigBuilder = new DatabaseConfigBuilder();
            databaseConfigBuilder.setHostname(properties.getProperty("hostname"))
                    .setUsername(properties.getProperty("username"))
                    .setPassword(properties.getProperty("password"))
                    .setServiceName(properties.getProperty("serviceName"));

            int portNumber = (properties.getProperty("port").isEmpty()) ? DEFAULT_PORT_NUMBER
                    : Integer.valueOf(properties.getProperty("port"));
            databaseConfigBuilder.setPort(portNumber);

            return databaseConfigBuilder.createDatabaseConfig();
        } else {
            return null;
        }
    }

}
