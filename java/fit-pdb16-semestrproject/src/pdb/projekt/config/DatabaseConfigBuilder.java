package pdb.projekt.config;

/**
 *
 * @author Iurii xkuzne00@stud.fit.vutbr.cz Kuznetcov
 */
public class DatabaseConfigBuilder {

    private String username;
    private String password;
    private String hostname;
    private int port;
    private String serviceName;

    public DatabaseConfigBuilder setUsername(String username) {
        this.username = username;
        return this;
    }

    public DatabaseConfigBuilder setPassword(String password) {
        this.password = password;
        return this;
    }

    public DatabaseConfigBuilder setHostname(String hostname) {
        this.hostname = hostname;
        return this;
    }

    public DatabaseConfigBuilder setPort(int port) {
        this.port = port;
        return this;
    }

    public DatabaseConfigBuilder setServiceName(String serviceName) {
        this.serviceName = serviceName;
        return this;
    }

    public DatabaseConfig createDatabaseConfig() {
        return new DatabaseConfig(username, password, hostname, port, serviceName);
    }
}
