package pdb.projekt.config;

/**
 *
 * @author Iurii xkuzne00@stud.fit.vutbr.cz Kuznetcov
 */
public class DatabaseConfigException extends Exception {

    public DatabaseConfigException() {
    }

    public DatabaseConfigException(String message) {
        super(message);
    }
}
