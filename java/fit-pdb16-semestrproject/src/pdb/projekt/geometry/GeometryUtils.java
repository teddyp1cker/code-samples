package pdb.projekt.geometry;

import java.awt.geom.Line2D;
import java.awt.geom.Path2D;
import java.awt.geom.PathIterator;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;

/**
 * Various utility functions for AWT Shape objects
 *
 * @author Iurii xkuzne00@stud.fit.vutbr.cz Kuznetcov
 */
public class GeometryUtils {

    /**
     * Determines if given line 'contains' a point by given coords - https://docs.oracle.com/javase/8/docs/api/java/awt/geom/Line2D.html#contains-java.awt.geom.Point2D-
     *
     * @param path AWT Path2D path
     * @param x    point x coord
     * @param y    point y coord
     * @return
     */
    public static boolean isPointNearLine(final Path2D path, final int x, final int y) {

        boolean selected = false;
        Rectangle2D.Float boundingRectangle = new Rectangle2D.Float(x, y, 5, 5);
        selected = path.intersects(boundingRectangle);
        return selected;
    }

    /**
     * Determines if given line 'contains' a point by given coords - https://docs.oracle.com/javase/8/docs/api/java/awt/geom/Line2D.html#contains-java.awt.geom.Point2D-
     *
     * @param path AWT Line2D line
     * @param x    point x coord
     * @param y    point y coord
     * @return true if point is 'near' line (there is intersection between Line2D and rectangle 5x5 defined by point)
     */
    public static boolean isPointNearLine(final Line2D path, final int x, final int y) {

        boolean selected = false;
        Rectangle2D.Float boundingRectangle = new Rectangle2D.Float(x, y, 5, 5);
        selected = path.intersects(boundingRectangle);
        return selected;
    }

    /**
     * Iterates AWT Path2D and return Point2D's list
     *
     * @param path AWT Path2D path
     * @return Point2D list
     */
    public static List<Point2D> getPath2DPoints(final Path2D path) {

        List<Point2D> points = new ArrayList<>();
        PathIterator pi = path.getPathIterator(null);
        while (pi.isDone() == false) {
            double[] coordinates = new double[6];
            int type = pi.currentSegment(coordinates);
            points.add(new Point2D.Double(coordinates[0], coordinates[1]));
            pi.next();
        }

        return points;
    }
}
