package pdb.projekt.geometry.operations;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;
import oracle.jdbc.pool.OracleDataSource;
import oracle.spatial.geometry.JGeometry;
import pdb.projekt.ApplicationConfiguration;
import pdb.projekt.database.DatabaseConnection;
import pdb.projekt.exceptions.JGeometry2ShapeException;
import pdb.projekt.geometry.operations.GeometryConverter;
import pdb.projekt.model.ShapeWrapper;

/**
 *
 * @author Iurii xkuzne00@stud.fit.vutbr.cz Kuznetcov
 */
public class NearestObjectsCalculator {

    /**
     * Return a @limit count of nearest to shape another shape objects
     * 
     * @param shapeWrapper shape
     * @param limit maximum nearest objects count
     * @return list of nearest shapes objects
     * @throws SQLException
     * @throws JGeometry2ShapeException
     * @throws Exception 
     */
    public static Map<ShapeWrapper, Float> calculate(ShapeWrapper shapeWrapper, int limit) throws Exception {

        Map<ShapeWrapper, Float> nearestShapes = new LinkedHashMap<>();

        OracleDataSource ods = DatabaseConnection.getDatasource(ApplicationConfiguration.getInstance().getDatabaseConfig());

        try (Connection conn = ods.getConnection();
                PreparedStatement stmt = conn.prepareStatement("select name, geometry, SDO_NN_DISTANCE(1) as distance from taximap where SDO_NN(geometry, "
                        + "(SELECT x.geometry FROM taximap x WHERE x.name = ?), "
                        + "'sdo_batch_size=1', 1) = 'TRUE' AND ROWNUM <= ? AND name <> ? ORDER BY distance")) {

            stmt.setString(1, shapeWrapper.name);
            stmt.setInt(2, limit);
            stmt.setString(3, shapeWrapper.name);

            try (ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    byte[] image = rs.getBytes("geometry");
                    String name = rs.getString("name");
                    float distance = (float) rs.getInt("distance");
                    JGeometry jGeometry = JGeometry.load(image);
                    Object shape = GeometryConverter.jGeometry2Shape(jGeometry);
                    nearestShapes.put(new ShapeWrapper(name, shape), distance);
                }
            }
        }

        return nearestShapes;
    }
}
