package pdb.projekt.geometry.operations;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import pdb.projekt.ApplicationConfiguration;
import pdb.projekt.database.DatabaseConnection;
import pdb.projekt.model.ShapeWrapper;
import oracle.jdbc.pool.OracleDataSource;
import org.apache.log4j.Logger;

/**
 * @author Iurii xkuzne00@stud.fit.vutbr.cz Kuznetcov
 */
public class PerimeterCalculator {

    private static Logger logger = Logger.getLogger(PerimeterCalculator.class.getName());

    /**
     * Calculates perimeter of given shape
     *
     * @param shape
     * @return perimeter of shape
     * @throws SQLException
     */
    public static double calculate(ShapeWrapper shape) throws SQLException {

        double perimeter = 0;

        OracleDataSource ods = DatabaseConnection.getDatasource(ApplicationConfiguration.getInstance().getDatabaseConfig());

        try (Connection conn = ods.getConnection();
             PreparedStatement stmt = conn.prepareStatement("select SDO_GEOM.SDO_LENGTH(geometry, 0.001) as perimeter from taximap where name = ?")) {
            stmt.setString(1, shape.name);

            try (ResultSet rs = stmt.executeQuery()) {
                if (rs.next()) {
                    perimeter = rs.getFloat("perimeter");
                }
            }
            return perimeter;
        }
    }
}
