package pdb.projekt.geometry.operations;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import pdb.projekt.ApplicationConfiguration;
import pdb.projekt.database.DatabaseConnection;
import pdb.projekt.model.ShapeWrapper;
import oracle.jdbc.pool.OracleDataSource;
import org.apache.log4j.Logger;

/**
 * @author Iurii xkuzne00@stud.fit.vutbr.cz Kuznetcov
 */
public class AreaCalculator {

    private static Logger logger = Logger.getLogger(AreaCalculator.class.getName());

    /**
     * Calculates area of given shape
     *
     * @param shape given shape
     * @return area
     * @throws SQLException
     */
    public static double calculate(final ShapeWrapper shape) throws SQLException {

        double area = 0;

        OracleDataSource ods = DatabaseConnection.getDatasource(ApplicationConfiguration.getInstance().getDatabaseConfig());

        try (Connection conn = ods.getConnection();
             PreparedStatement stmt = conn.prepareStatement("select SDO_GEOM.SDO_AREA(geometry, 0.001) as area from taximap where name = ?")) {
            stmt.setString(1, shape.name);

            try (ResultSet rs = stmt.executeQuery()) {
                if (rs.next()) {
                    area = rs.getFloat("area");
                }
            }
        }
        return area;
    }
}
