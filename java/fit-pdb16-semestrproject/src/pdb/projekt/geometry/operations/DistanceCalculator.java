package pdb.projekt.geometry.operations;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import pdb.projekt.ApplicationConfiguration;
import pdb.projekt.database.DatabaseConnection;
import pdb.projekt.model.ShapeWrapper;
import oracle.jdbc.pool.OracleDataSource;
import org.apache.log4j.Logger;

/**
 * @author Iurii xkuzne00@stud.fit.vutbr.cz Kuznetcov
 */
public class DistanceCalculator {

    private static Logger logger = Logger.getLogger(DistanceCalculator.class.getName());

    /**
     * Calculates distance between given 2 shapes
     *
     * @param firstShape    shape
     * @param secondShape   shape
     * @return              distance
     * @throws              SQLException
     */
    public static double calculate(ShapeWrapper firstShape, ShapeWrapper secondShape) throws SQLException {

        double distance = Double.NEGATIVE_INFINITY;

        OracleDataSource ods = DatabaseConnection.getDatasource(ApplicationConfiguration.getInstance().getDatabaseConfig());

        try (Connection conn = ods.getConnection();
             PreparedStatement stmt = conn.prepareStatement("SELECT SDO_GEOM.SDO_DISTANCE(a.geometry, g.geometry, 1) distance "
                     + "FROM taximap a, taximap g WHERE a.name = ? AND g.name = ?")) {
            stmt.setString(1, firstShape.name);
            stmt.setString(2, secondShape.name);

            try (ResultSet rs = stmt.executeQuery()) {
                if (rs.next()) {
                    distance = rs.getFloat("distance");
                }
            }
            return distance;
        }
    }
}
