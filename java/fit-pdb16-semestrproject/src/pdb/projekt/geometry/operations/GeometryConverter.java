package pdb.projekt.geometry.operations;

import java.awt.Polygon;
import java.awt.Shape;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Path2D;
import java.awt.geom.PathIterator;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;
import oracle.spatial.geometry.JGeometry;
import pdb.projekt.exceptions.JGeometry2ShapeException;
import org.apache.log4j.Logger;

/**
 * Geometry objects oracle/awt converter
 *
 * @author Iurii xkuzne00@stud.fit.vutbr.cz Kuznetcov
 */
public class GeometryConverter {

    private static Logger logger = Logger.getLogger(GeometryConverter.class.getName());

    private static final int POLYGON_SDO_ETYPE = 1003;
    private static final int POLYGON_SDO_GTYPE = 2003;

    private static final int PATH_SDO_ETYPE = 2;
    private static final int PATH_SDO_GTYPE = 2002;

    private static final int CIRCULAR_SDO_INTERPRETATION = 4;
    private static final int RECTANGLE_SDO_INTERPRETATION = 3;
    private static final int SIMPLE_POLYGON_SDO_INTERPRETATION = 1;

    private static final int POLYGON_TYPE = 1;
    private static final int CIRCULAR_TYPE = 2;
    private static final int LINE_TYPE = 3;
    private static final int RECTANGLE_TYPE = 4;

    private static final int L_LINE_TYPE = 5;

    private static final int COORDS_NUMBER = 6;

    /**
     * Converts Oracle Spatial geometry object to AWT Shape geometry entities
     *
     * @param jGeometry Oracle Spatial object
     * @return AWT Shape object
     * @throws JGeometry2ShapeException
     */
    public static Object jGeometry2Shape(final JGeometry jGeometry) throws JGeometry2ShapeException {
        Object shape = null;
        switch (jGeometry.getType()) {
            case JGeometry.GTYPE_POLYGON:
                shape = jGeometry.createShape();
                break;
            case JGeometry.GTYPE_POINT:
                shape = jGeometry.getJavaPoint();
                break;
            case JGeometry.GTYPE_CURVE:
            case JGeometry.GTYPE_MULTICURVE:
                shape = jGeometry.createShape();
                break;
            default:
                break;
        }
        return shape;
    }

    /**
     * Converts AWT Shape geometry entity to Oracle Spatial geometry object
     *
     * @param shape AWT Shape object
     * @return Oracle Spatial object
     */
    public static JGeometry shape2JGeometry(final Shape shape) {

        JGeometry jGeometryObject = null;

        int etype = POLYGON_SDO_ETYPE;
        int gtype = POLYGON_SDO_GTYPE;
        int shapeType = 0; // not supported types
        int shapeDBInterpretationType = 0; // not supported types

        if (shape instanceof Ellipse2D) {
            shapeType = CIRCULAR_TYPE;
            shapeDBInterpretationType = CIRCULAR_SDO_INTERPRETATION;
        } else if (shape instanceof Rectangle2D) {
            shapeType = RECTANGLE_TYPE;
            shapeDBInterpretationType = RECTANGLE_SDO_INTERPRETATION;
        } else if (shape instanceof Polygon) {
            shapeType = POLYGON_TYPE;
            shapeDBInterpretationType = SIMPLE_POLYGON_SDO_INTERPRETATION;
        } else if (shape instanceof Path2D) {
            shapeType = LINE_TYPE;
            shapeDBInterpretationType = SIMPLE_POLYGON_SDO_INTERPRETATION;
            etype = PATH_SDO_ETYPE;
            gtype = PATH_SDO_GTYPE;
        } else if (shape instanceof Line2D) {
            shapeType = L_LINE_TYPE;
            shapeDBInterpretationType = SIMPLE_POLYGON_SDO_INTERPRETATION;
            etype = PATH_SDO_ETYPE;
            gtype = PATH_SDO_GTYPE;
        }

        if (shapeType == 0) {
            return null;
        }

        List<Double> points = new ArrayList<>();

        if (shapeType == RECTANGLE_TYPE) {

            Rectangle2D rectangle = shape.getBounds2D();
            points.add(rectangle.getMinX());
            points.add(rectangle.getMinY());
            points.add(rectangle.getMaxX());
            points.add(rectangle.getMaxY());
        } else if (shapeType == L_LINE_TYPE) {
            points.add(((Line2D) shape).getX1());
            points.add(((Line2D) shape).getY1());
            points.add(((Line2D) shape).getX2());
            points.add(((Line2D) shape).getY2());
            //            ((Line2D) shape).getX1();
            //            ((Line2D) shape).getY1();
            //            ((Line2D) shape).getX2();
            //            ((Line2D) shape).getY2();
        } else {

            double[] coords = new double[COORDS_NUMBER];

            // http://docs.oracle.com/javase/8/docs/api/java/awt/geom/PathIterator.html#SEG_CUBICTO
            int SEG_CUBICTO_Counter = 0;

            for (PathIterator pathIterator = shape.getPathIterator(null); !pathIterator.isDone(); pathIterator.next()) {

                for (int i = 0; i < COORDS_NUMBER; i++) {
                    coords[i] = 0.0;
                }

                int type = pathIterator.currentSegment(coords);

                if (shapeType == POLYGON_TYPE || shapeType == LINE_TYPE) {

                    if (type != PathIterator.SEG_LINETO && type != PathIterator.SEG_MOVETO) {
                        continue;
                    }
                    points.add(coords[0]);
                    points.add(coords[1]);

                } else {

                    if (type != PathIterator.SEG_CUBICTO) {
                        continue;
                    }
                    if (SEG_CUBICTO_Counter < 3) {

                        points.add(coords[4]);
                        points.add(coords[5]);
                        SEG_CUBICTO_Counter++;
                    }
                }
            }
        }

        if (shapeType == POLYGON_TYPE) {
            double x, y;
            x = points.get(0);
            y = points.get(1);

            points.add(x);
            points.add(y);
        }

        double[] ordinates = new double[points.size()];
        int i = 0;
        for (Double point : points) {
            ordinates[i++] = point;
        }

        int[] elemInfo = new int[]{1, etype, shapeDBInterpretationType};

        jGeometryObject = new JGeometry(gtype, 0, elemInfo, ordinates);

        return jGeometryObject;
    }
}
