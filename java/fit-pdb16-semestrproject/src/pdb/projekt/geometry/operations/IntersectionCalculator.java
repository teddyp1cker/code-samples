package pdb.projekt.geometry.operations;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

import oracle.jdbc.pool.OracleDataSource;
import oracle.spatial.geometry.JGeometry;
import pdb.projekt.ApplicationConfiguration;
import pdb.projekt.database.DatabaseConnection;
import pdb.projekt.model.ShapeWrapper;

/**
 * @author Iurii xkuzne00@stud.fit.vutbr.cz Kuznetcov
 */
public class IntersectionCalculator {

    /**
     * Determines if 2 shapes have an intersection
     *
     * @param firstShape  shape
     * @param secondShape shape
     * @return 'Temporal' intersection shape object or null if there is no intersection between given shapes
     * @throws SQLException
     * @throws Exception
     */
    public static ShapeWrapper calculate(ShapeWrapper firstShape, ShapeWrapper secondShape) throws Exception {

        ShapeWrapper intersectionShape = null;

        OracleDataSource ods = DatabaseConnection.getDatasource(ApplicationConfiguration.getInstance().getDatabaseConfig());

        try (Connection conn = ods.getConnection();
             PreparedStatement stmt = conn.prepareStatement("SELECT SDO_GEOM.SDO_INTERSECTION(a.GEOMETRY, b.GEOMETRY, 0.001) FROM taximap a, taximap b "
                     + "WHERE a.name = ? AND b.name = ?")) {
            stmt.setString(1, firstShape.name);
            stmt.setString(2, secondShape.name);

            try (ResultSet rs = stmt.executeQuery()) {
                if (rs.next()) {
                    byte[] image = rs.getBytes("geometry");
                    JGeometry jGeometry = JGeometry.load(image);
                    Object shape = GeometryConverter.jGeometry2Shape(jGeometry);
                    intersectionShape = new ShapeWrapper("Intersection_Object" + UUID.randomUUID(), shape);
                }
            }
        }

        return intersectionShape;
    }

    /**
     * Determines if 2 shapes have an intersection
     *
     * @param firstShapeName shape name
     * @param firstShapeName shape name
     * @return 'Temporal' intersection shape object or null if there is no intersection between given shapes
     * @throws SQLException
     * @throws Exception
     */
    public static ShapeWrapper calculate(String firstShapeName, String secondShapeName) throws SQLException, Exception {

        ShapeWrapper intersectionShape = null;

        OracleDataSource ods = DatabaseConnection.getDatasource(ApplicationConfiguration.getInstance().getDatabaseConfig());

        try (Connection conn = ods.getConnection();
             PreparedStatement stmt = conn.prepareStatement("SELECT SDO_GEOM.SDO_INTERSECTION(a.GEOMETRY, b.GEOMETRY, 0.001) geometry FROM taximap a, taximap b "
                     + "WHERE a.name = ? AND b.name = ?")) {
            stmt.setString(1, firstShapeName);
            stmt.setString(2, secondShapeName);

            try (ResultSet rs = stmt.executeQuery()) {
                if (rs.next()) {
                    byte[] image = rs.getBytes("geometry");
                    if (image != null) {
                        JGeometry jGeometry = JGeometry.load(image);
                        Object shape = GeometryConverter.jGeometry2Shape(jGeometry);
                        intersectionShape = new ShapeWrapper("Intersection_Object" + UUID.randomUUID(), shape);
                    }
                }
            }
        }

        return intersectionShape;
    }
}
