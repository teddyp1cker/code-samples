package pdb.projekt.geometry;

/**
 * @author Iurii xkuzne00@stud.fit.vutbr.cz Kuznetcov
 */
public enum MapObjectType {
    MILITARY_ZONE,
    TOWER,
    HELIPORT,
    ROUTE,
    PARK_ZONE,
    UNDFINED_TYPE
}
