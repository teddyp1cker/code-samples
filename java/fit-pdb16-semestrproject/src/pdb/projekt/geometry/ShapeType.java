package pdb.projekt.geometry;

/**
 * @author Iurii xkuzne00@stud.fit.vutbr.cz Kuznetcov
 */
public enum ShapeType {
    POINT,
    LINE,
    CIRCLE,
    RECTANGLE,
    POLYGON
}
