package pdb.projekt.autorization;

/**
 * @author Iurii xkuzne00@stud.fit.vutbr.cz Kuznetcov
 */
public interface CredentialsAuthenticator {

    boolean authenticate(final String login, final String password);
}
