package pdb.projekt.autorization;

import pdb.projekt.autorization.CredentialsAuthenticator;

/**
 *
 * @author Iurii xkuzne00@stud.fit.vutbr.cz Kuznetcov
 */
public class DefaultCredentialsAuthenticator implements CredentialsAuthenticator {

    /**
     * Dummy authenticator with hardcoded login/password values
     * 
     * @param login
     * @param password
     * @return true if authentication process was succesefull
     */
    @Override
    public boolean authenticate(String login, String password) {
        return "login".equals(login) && "password".equals(password);
    }

}
