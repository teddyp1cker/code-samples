package pdb.projekt.ui;

import pdb.projekt.geometry.operations.DistanceCalculator;
import pdb.projekt.geometry.operations.AreaCalculator;
import pdb.projekt.geometry.operations.PerimeterCalculator;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.Shape;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Ellipse2D;
import java.awt.geom.GeneralPath;
import java.awt.geom.Line2D;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.apache.log4j.Level;
import pdb.projekt.geometry.GeometryUtils;
import pdb.projekt.geometry.MapObjectType;
import pdb.projekt.geometry.ShapeType;
import pdb.projekt.geometry.operations.NearestObjectsCalculator;
import pdb.projekt.model.ShapeWrapper;
import pdb.projekt.model.ShapesWrapperList;

/**
 * Interactive map viewer panel
 *
 * @author Iurii xkuzne00@stud.fit.vutbr.cz Kuznetcov
 */
public class MapInteractiveViewer extends JPanel implements MouseListener, MouseMotionListener, KeyListener {

    private static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(MapInteractiveViewer.class.getName());

    private static final short maxX = 500;
    private static final short maxY = 500;
    private static final short windowZoom = 1;
    private final int verticalOffset = 0;
    private final int horizontalOffset = 0;

    private static final int MAX_POLYGON_POINT_COUNT = 5;
    private int currentPolygonPointCount = 0;
    private List<Point2D> polygonPoints = new ArrayList<>();

    private GeometryOperation currentGeometryOperationMode;
    private GeometryEditMode currentGeometyEditMode;

    private final ReentrantLock shapesListLock = new ReentrantLock();
    private ShapesWrapperList shapesList = new ShapesWrapperList();

    public String newObjectName;
    public ShapeType selectedShapeType;

    private ShapeWrapper newShapeWrapper;

    private ShapeWrapper selectedShape;

    private ShapeWrapper firstSelectedShape;
    private ShapeWrapper secondSelectedShape;

    // TODO: move to ShapeWrapper object
    private Point2D startShapePoint;

    private int nearestObjectsCount = 1;

    public void setNearestObjectsCount(int nearestObjectsCount) {
        this.nearestObjectsCount = nearestObjectsCount;
    }

    public MapInteractiveViewer() {
        currentGeometryOperationMode = GeometryOperation.NO_OPERATION_MODE;
        currentGeometyEditMode = GeometryEditMode.NO_OPERATION_MODE;
        this.addMouseListener(this);
        try {
            shapesList.loadShapesFromDb();
        } catch (Exception exception) {
            logger.log(Level.FATAL, "Exception :", exception);
            ExceptionMessage.show(exception);
        }
    }

    @Override
    public void paint(Graphics g) {

        shapesList.clear();
        Graphics2D g2D = (Graphics2D) g;
        try {
            shapesList.loadShapesFromDb();
        } catch (Exception exception) {
            logger.log(Level.FATAL, "Exception while getting shapes list from database :", exception);
            ExceptionMessage.show(exception);
        }

        for (ShapeWrapper shape : shapesList.shapes) {
            if (shape.shape instanceof Shape) {
                switch (shape.objectType) {
                    case TOWER:
                        g2D.setPaint(Color.PINK);
                        g2D.fill((Shape) shape.shape);
                        g2D.setPaint(Color.BLACK);
                        break;
                    case MILITARY_ZONE:
                        g2D.setPaint(Color.RED);
                        g2D.fill((Shape) shape.shape);
                        g2D.setPaint(Color.RED);
                        break;
                    case ROUTE:
                        g2D.setPaint(Color.BLUE);
                        g2D.fill((Shape) shape.shape);
                        g2D.setPaint(Color.BLUE);
                        break;
                    case PARK_ZONE:
                        g2D.setPaint(Color.GREEN);
                        g2D.fill((Shape) shape.shape);
                        g2D.setPaint(Color.GREEN);
                        break;
                    default:
                        break;
                }

                g2D.draw((Shape) shape.shape);
                g2D.setColor(Color.BLACK);
                g2D.setFont(new Font("Verdana", Font.BOLD, 10));

                String shapeName = "?";

                if (shape.name != null) {
                    shapeName = shape.name;
                }

                g2D.drawString(shapeName,
                        (int) ((Shape) shape.shape).getBounds2D().getMinX() - 5,
                        (int) ((Shape) shape.shape).getBounds2D().getMinY() - 5);
            } else if (shape.shape instanceof Point2D) {
                switch (shape.objectType) {
                    case HELIPORT:
                        g2D.setPaint(Color.BLACK);
                        float r = (float) 2;
                        Ellipse2D point = new Ellipse2D.Float((float) ((Point2D) shape.shape).getX() - r, (float) ((Point2D) shape.shape).getY() - r, 2 * r, 2 * r);
                        g2D.fill(point);
                        g2D.draw(point);
                        g2D.setColor(Color.GRAY);
                        g2D.setFont(new Font("Verdana", Font.BOLD, 10));
                        g2D.drawString(shape.name,
                                (int) point.getBounds2D().getMinX() - 10,
                                (int) point.getBounds2D().getMaxY() + 10);
                        break;
                    default:
                        break;
                }
            }
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {

        int clickX = e.getX();
        int clickY = e.getY();

        System.out.println("x: " + clickX);
        System.out.println("y: " + clickY);

        shapesListLock.lock();

        try {
            if (currentGeometyEditMode == GeometryEditMode.ADD_OBJECT) {
                switch (selectedShapeType) {
                    case POINT:
                        newShapeWrapper = new ShapeWrapper();
                        newObjectName = (String) JOptionPane.showInputDialog(
                                this,
                                "Enter point name:\n",
                                "Adding new point",
                                JOptionPane.PLAIN_MESSAGE,
                                null,
                                null,
                                "");

                        if (newObjectName != null) {

                            newShapeWrapper.name = "heliport_" + newObjectName;
                            newShapeWrapper.objectType = MapObjectType.HELIPORT;
                            Point2D.Float newPoint = new Point2D.Float(clickX, clickY);

                            try {
                                newShapeWrapper.shape = newPoint;
                                shapesList.addShape(newShapeWrapper);
                                repaint();
                                newShapeWrapper = null;
                            } catch (Exception exception) {
                                logger.log(Level.FATAL, "", exception);
                                ExceptionMessage.show(exception);
                            }
                        }
                        break;
                    case LINE:
                        if (newShapeWrapper == null) {
                            newObjectName = (String) JOptionPane.showInputDialog(
                                    this,
                                    "Enter line name:\n",
                                    "Adding new line",
                                    JOptionPane.PLAIN_MESSAGE,
                                    null,
                                    null,
                                    "");

                            newShapeWrapper = new ShapeWrapper();
                            newShapeWrapper.name = "route_" + newObjectName;
                            newShapeWrapper.objectType = MapObjectType.ROUTE;
                            //                            newShapeWrapper.shape = new Path2D.Float();
                            //                            ((Path2D) newShapeWrapper.shape).moveTo(clickX, clickY);
                            startShapePoint = new Point2D.Float((float) e.getX() - horizontalOffset, (float) e.getY() - verticalOffset);
                        } else {
                            Point2D endShapePoint = new Point2D.Float((float) e.getX() - horizontalOffset, (float) e.getY() - verticalOffset);
                            newShapeWrapper.shape = new Line2D.Float(startShapePoint, endShapePoint);
                            // ((Path2D) newShapeWrapper.shape).lineTo(clickX, clickY);
                            try {
                                shapesList.addShape(newShapeWrapper);
                                repaint();
                                newShapeWrapper = null;
                            } catch (Exception exception) {
                                logger.log(Level.FATAL, "", exception);
                                ExceptionMessage.show(exception);
                            }
                        }
                        break;
                    case CIRCLE:
                        if (newShapeWrapper == null) {

                            newShapeWrapper = new ShapeWrapper();
                            JTextField nameField = new JTextField(50);
                            JTextField raduisField = new JTextField(5);

                            JPanel circleCreatePanel = new JPanel();
                            circleCreatePanel.setLayout(new BoxLayout(circleCreatePanel, BoxLayout.Y_AXIS));
                            circleCreatePanel.add(new JLabel("Circle name:"));
                            circleCreatePanel.add(nameField);
                            circleCreatePanel.add(Box.createVerticalStrut(5));
                            circleCreatePanel.add(new JLabel("Circle radius:"));
                            circleCreatePanel.add(raduisField);

                            int result = JOptionPane.showConfirmDialog(null, circleCreatePanel,
                                    "Enter circle name and radius", JOptionPane.OK_CANCEL_OPTION);
                            if (result == JOptionPane.OK_OPTION) {

                                newShapeWrapper.name = "military_area_" + nameField.getText();
                                newShapeWrapper.objectType = MapObjectType.MILITARY_ZONE;
                                String radiusString = raduisField.getText();

                                newShapeWrapper.shape = new Ellipse2D.Float(e.getX() - horizontalOffset,
                                        e.getY() - verticalOffset, Float.valueOf(radiusString),
                                        Float.valueOf(radiusString));

                                shapesList.addShape(newShapeWrapper);
                                repaint();
                                newShapeWrapper = null;
                            }
                        }
                        break;
                    case RECTANGLE:
                        if (newShapeWrapper == null) {

                            JTextField nameField = new JTextField(50);
                            JTextField xField = new JTextField(5);
                            JTextField yField = new JTextField(5);
                            JTextField widthField = new JTextField(5);
                            JTextField heightField = new JTextField(5);

                            JPanel rectangleCreatePanel = new JPanel();
                            rectangleCreatePanel.setLayout(new BoxLayout(rectangleCreatePanel, BoxLayout.Y_AXIS));
                            rectangleCreatePanel.add(new JLabel("Rectangle name:"));
                            rectangleCreatePanel.add(nameField);
                            rectangleCreatePanel.add(Box.createHorizontalStrut(5));
                            rectangleCreatePanel.add(new JLabel("Left corner X coord:"));
                            rectangleCreatePanel.add(xField);
                            rectangleCreatePanel.add(Box.createHorizontalStrut(5));
                            rectangleCreatePanel.add(new JLabel("Left corner Y coord:"));
                            rectangleCreatePanel.add(yField);
                            rectangleCreatePanel.add(Box.createHorizontalStrut(5));
                            rectangleCreatePanel.add(new JLabel("Width:"));
                            rectangleCreatePanel.add(widthField);
                            rectangleCreatePanel.add(Box.createHorizontalStrut(5));
                            rectangleCreatePanel.add(new JLabel("Height:"));
                            rectangleCreatePanel.add(heightField);

                            int result = JOptionPane.showConfirmDialog(null, rectangleCreatePanel,
                                    "Set name and other rectangle params", JOptionPane.OK_CANCEL_OPTION);
                            if (result == JOptionPane.OK_OPTION) {

                                newShapeWrapper = new ShapeWrapper();
                                newObjectName = nameField.getText();

                                Rectangle2D newRectangle = new Rectangle2D.Float(
                                        Float.parseFloat(xField.getText()),
                                        Float.parseFloat(yField.getText()),
                                        Float.parseFloat(widthField.getText()),
                                        Float.parseFloat(heightField.getText()));
                                newShapeWrapper.shape = newRectangle;
                                newShapeWrapper.name = "tower_" + newObjectName;
                                newShapeWrapper.objectType = MapObjectType.TOWER;

                                try {
                                    shapesList.addShape(newShapeWrapper);
                                    repaint();
                                    newShapeWrapper = null;
                                } catch (Exception exception) {
                                    logger.log(Level.FATAL, "", exception);
                                    ExceptionMessage.show(exception);
                                }
                            }
                        }
                        break;
                    case POLYGON:
                        if (currentPolygonPointCount < MAX_POLYGON_POINT_COUNT) {
                            if (currentPolygonPointCount == 0) {
                                newObjectName = (String) JOptionPane.showInputDialog(
                                        this,
                                        "Enter polygon name:\n",
                                        "Adding new polygon",
                                        JOptionPane.PLAIN_MESSAGE,
                                        null,
                                        null,
                                        "");
                            }

                            polygonPoints.add(new Point2D.Float(clickX, clickY));
                            currentPolygonPointCount++;
                        } else {
                            int npoints = polygonPoints.size();
                            int[] xpoints = new int[npoints];
                            int[] ypoints = new int[npoints];

                            for (int i = 0; i < polygonPoints.size(); i++) {
                                xpoints[i] = (int) polygonPoints.get(i).getX();
                                ypoints[i] = (int) polygonPoints.get(i).getY();
                            }

                            newShapeWrapper = new ShapeWrapper();
                            Polygon polygon = new Polygon(xpoints, ypoints, npoints);
                            newShapeWrapper.shape = polygon;
                            newShapeWrapper.name = "park_" + newObjectName;
                            newShapeWrapper.objectType = MapObjectType.PARK_ZONE;

                            try {
                                shapesList.addShape(newShapeWrapper);
                                repaint();
                                newShapeWrapper = null;
                                polygonPoints.clear();
                                currentPolygonPointCount = 0;
                            } catch (Exception exception) {
                                logger.log(Level.FATAL, "", exception);
                                ExceptionMessage.show(exception);
                            }
                        }

                        break;
                    default:
                        break;
                }
            } else if (currentGeometyEditMode == GeometryEditMode.REMOVE_OBJECT) {

                ShapeWrapper clickedShape = getSelectedShapeByCoords(clickX, clickY);
                removeShape(clickedShape);

            } else if (currentGeometryOperationMode == GeometryOperation.CALCULATE_AREA) {

                ShapeWrapper clickedShape = getSelectedShapeByCoords(clickX, clickY);
                double calculatedArea = AreaCalculator.calculate(clickedShape);
                System.out.println("Area : " + calculatedArea);
                JOptionPane.showMessageDialog(getParent(),
                        "Area of " + selectedShape.name + " = " + String.valueOf(calculatedArea));

            } else if (currentGeometyEditMode == GeometryEditMode.EDIT_OBJECT) {

                ShapeWrapper clickedShape = getSelectedShapeByCoords(clickX, clickY);

                if (clickedShape != null) {
                    switch (clickedShape.objectType) {
                        case HELIPORT:
                            JTextField pointNameField = new JTextField(50);
                            JTextField pointXCoordField = new JTextField(5);
                            JTextField pointYCoordField = new JTextField(5);

                            JPanel pointEditPanel = new JPanel();
                            pointEditPanel.setLayout(new BoxLayout(pointEditPanel, BoxLayout.Y_AXIS));
                            pointEditPanel.add(new JLabel("Point name:"));
                            pointEditPanel.add(pointNameField);
                            pointEditPanel.add(Box.createHorizontalStrut(5));
                            pointEditPanel.add(new JLabel("X Coord:"));
                            pointEditPanel.add(pointXCoordField);
                            pointEditPanel.add(Box.createHorizontalStrut(5));
                            pointEditPanel.add(new JLabel("Y Coord:"));
                            pointEditPanel.add(pointYCoordField);

                            pointNameField.setText(selectedShape.name);
                            pointXCoordField.setText(String.valueOf(clickX));
                            pointYCoordField.setText(String.valueOf(clickY));

                            int result = JOptionPane.showConfirmDialog(null, pointEditPanel,
                                    "Edit point params", JOptionPane.OK_CANCEL_OPTION);
                            if (result == JOptionPane.OK_OPTION) {

                                ShapeWrapper newShapeWrapper = new ShapeWrapper();
                                newShapeWrapper.name = pointNameField.getText();
                                newShapeWrapper.objectType = MapObjectType.HELIPORT;
                                Point2D.Float newPoint = new Point2D.Float(Float.parseFloat(pointXCoordField.getText()), Float.parseFloat(pointYCoordField.getText()));
                                newShapeWrapper.shape = newPoint;
                                try {
                                    shapesList.deleteShape(clickedShape);
                                    shapesList.addShape(newShapeWrapper);
                                    repaint();
                                    newShapeWrapper = null;
                                } catch (Exception exception) {
                                    logger.log(Level.FATAL, "", exception);
                                    ExceptionMessage.show(exception);
                                }
                            }
                            break;
                        case MILITARY_ZONE:
                            JTextField circleNameField = new JTextField(50);
                            JTextField circleXCoordField = new JTextField(5);
                            JTextField circleYCoordField = new JTextField(5);
                            JTextField circleRadiusField = new JTextField(5);

                            JPanel circleEditPanel = new JPanel();
                            circleEditPanel.setLayout(new BoxLayout(circleEditPanel, BoxLayout.Y_AXIS));
                            circleEditPanel.add(new JLabel("Circle name:"));
                            circleEditPanel.add(circleNameField);
                            circleEditPanel.add(Box.createHorizontalStrut(5));
                            circleEditPanel.add(new JLabel("Upper left X Coord:"));
                            circleEditPanel.add(circleXCoordField);
                            circleEditPanel.add(Box.createHorizontalStrut(5));
                            circleEditPanel.add(new JLabel("Upper left Y Coord:"));
                            circleEditPanel.add(circleYCoordField);
                            circleEditPanel.add(Box.createHorizontalStrut(5));
                            circleEditPanel.add(new JLabel("Radius:"));
                            circleEditPanel.add(circleRadiusField);

                            circleNameField.setText(selectedShape.name);
                            circleXCoordField.setText(String.valueOf(((Ellipse2D.Double) selectedShape.shape).getBounds2D().getMinX()));
                            circleYCoordField.setText(String.valueOf(((Ellipse2D.Double) selectedShape.shape).getBounds2D().getMinY()));
                            circleRadiusField.setText(String.valueOf(((Ellipse2D.Double) selectedShape.shape).getWidth()));

                            result = JOptionPane.showConfirmDialog(null, circleEditPanel,
                                    "Edit circle params", JOptionPane.OK_CANCEL_OPTION);
                            if (result == JOptionPane.OK_OPTION) {

                                ShapeWrapper newShapeWrapper = new ShapeWrapper();
                                newShapeWrapper.name = circleNameField.getText();
                                newShapeWrapper.objectType = MapObjectType.MILITARY_ZONE;

                                newShapeWrapper.shape = new Ellipse2D.Double(
                                        Double.valueOf(circleXCoordField.getText()),
                                        Double.valueOf(circleYCoordField.getText()),
                                        Double.valueOf(circleRadiusField.getText()),
                                        Double.valueOf(circleRadiusField.getText()));
                                try {
                                    shapesList.deleteShape(clickedShape);
                                    shapesList.addShape(newShapeWrapper);
                                    repaint();
                                    newShapeWrapper = null;
                                } catch (Exception exception) {
                                    logger.log(Level.FATAL, "", exception);
                                    ExceptionMessage.show(exception);
                                }
                            }

                            break;
                        case ROUTE:

                            JTextField lineNameField = new JTextField(50);
                            JTextField lineX1CoordField = new JTextField(5);
                            JTextField lineY1CoordField = new JTextField(5);
                            JTextField lineX2CoordField = new JTextField(5);
                            JTextField lineY2CoordField = new JTextField(5);

                            JPanel lineEditPanel = new JPanel();
                            lineEditPanel.setLayout(new BoxLayout(lineEditPanel, BoxLayout.Y_AXIS));
                            lineEditPanel.add(new JLabel("Route name:"));
                            lineEditPanel.add(lineNameField);
                            lineEditPanel.add(Box.createHorizontalStrut(5));
                            lineEditPanel.add(new JLabel("Start X Coord:"));
                            lineEditPanel.add(lineX1CoordField);
                            lineEditPanel.add(Box.createHorizontalStrut(5));
                            lineEditPanel.add(new JLabel("Start Y Coord:"));
                            lineEditPanel.add(lineY1CoordField);
                            lineEditPanel.add(Box.createHorizontalStrut(5));
                            lineEditPanel.add(new JLabel("End X Coord:"));
                            lineEditPanel.add(lineY2CoordField);
                            lineEditPanel.add(Box.createHorizontalStrut(5));
                            lineEditPanel.add(new JLabel("End Y Coord:"));
                            lineEditPanel.add(lineX2CoordField);

                            lineNameField.setText(selectedShape.name);

                            List<Point2D> path2DPoints = GeometryUtils.getPath2DPoints((Path2D) clickedShape.shape);

                            lineX1CoordField.setText(String.valueOf(path2DPoints.get(0).getX()));
                            lineY1CoordField.setText(String.valueOf(path2DPoints.get(0).getY()));
                            lineX2CoordField.setText(String.valueOf(path2DPoints.get(1).getX()));
                            lineY2CoordField.setText(String.valueOf(path2DPoints.get(1).getY()));
                            result = JOptionPane.showConfirmDialog(null, lineEditPanel,
                                    "Edit line params", JOptionPane.OK_CANCEL_OPTION);

                            if (result == JOptionPane.OK_OPTION) {

                                ShapeWrapper newShapeWrapper = new ShapeWrapper();
                                newShapeWrapper.name = lineNameField.getText();
                                newShapeWrapper.objectType = MapObjectType.ROUTE;
                                newShapeWrapper.shape = new Path2D.Float();
                                ((Path2D) newShapeWrapper.shape).moveTo(
                                        Float.valueOf(lineX1CoordField.getText()),
                                        Float.valueOf(lineY1CoordField.getText()));

                                ((Path2D) newShapeWrapper.shape).lineTo(
                                        Float.valueOf(lineX2CoordField.getText()),
                                        Float.valueOf(lineY2CoordField.getText())
                                );

                                try {
                                    shapesList.deleteShape(clickedShape);
                                    shapesList.addShape(newShapeWrapper);
                                    repaint();
                                    newShapeWrapper = null;
                                } catch (Exception exception) {
                                    logger.log(Level.FATAL, "", exception);
                                    ExceptionMessage.show(exception);
                                }
                            }
                            break;
                        case TOWER:

                            JTextField rectangleNameField = new JTextField(50);
                            JTextField rectangleXCoordField = new JTextField(5);
                            JTextField rectangleYCoordField = new JTextField(5);
                            JTextField rectangleHeightField = new JTextField(5);
                            JTextField rectangleWidthField = new JTextField(5);

                            JPanel rectangleEditPanel = new JPanel();
                            rectangleEditPanel.setLayout(new BoxLayout(rectangleEditPanel, BoxLayout.Y_AXIS));
                            rectangleEditPanel.add(new JLabel("Rectangle name:"));
                            rectangleEditPanel.add(rectangleNameField);
                            rectangleEditPanel.add(Box.createHorizontalStrut(5));
                            rectangleEditPanel.add(new JLabel("Upper left X Coord:"));
                            rectangleEditPanel.add(rectangleXCoordField);
                            rectangleEditPanel.add(Box.createHorizontalStrut(5));
                            rectangleEditPanel.add(new JLabel("Upper left Y Coord:"));
                            rectangleEditPanel.add(rectangleYCoordField);
                            rectangleEditPanel.add(Box.createHorizontalStrut(5));
                            rectangleEditPanel.add(new JLabel("Width:"));
                            rectangleEditPanel.add(rectangleWidthField);
                            rectangleEditPanel.add(Box.createHorizontalStrut(5));
                            rectangleEditPanel.add(new JLabel("Height:"));
                            rectangleEditPanel.add(rectangleHeightField);

                            rectangleNameField.setText(clickedShape.name);
                            rectangleXCoordField.setText(String.valueOf(((Shape) clickedShape.shape).getBounds2D().getMinX()));
                            rectangleYCoordField.setText(String.valueOf(((Shape) clickedShape.shape).getBounds2D().getMinY()));
                            rectangleWidthField.setText(String.valueOf(((Shape) clickedShape.shape).getBounds2D().getWidth()));
                            rectangleHeightField.setText(String.valueOf(((Shape) clickedShape.shape).getBounds2D().getHeight()));

                            result = JOptionPane.showConfirmDialog(null, rectangleEditPanel,
                                    "Edit rectangle params", JOptionPane.OK_CANCEL_OPTION);
                            if (result == JOptionPane.OK_OPTION) {

                                ShapeWrapper newShapeWrapper = new ShapeWrapper();
                                newShapeWrapper.name = rectangleNameField.getText();
                                newShapeWrapper.objectType = MapObjectType.TOWER;

                                newShapeWrapper.shape = new Rectangle2D.Double(
                                        Double.valueOf(rectangleXCoordField.getText()),
                                        Double.valueOf(rectangleYCoordField.getText()),
                                        Double.valueOf(rectangleWidthField.getText()),
                                        Double.valueOf(rectangleHeightField.getText()));
                                try {
                                    shapesList.deleteShape(clickedShape);
                                    shapesList.addShape(newShapeWrapper);
                                    repaint();
                                    newShapeWrapper = null;
                                } catch (Exception exception) {
                                    logger.log(Level.FATAL, "", exception);
                                    ExceptionMessage.show(exception);
                                }
                            }
                            break;
                        case PARK_ZONE:

                            List<Point2D> genPathPoints = GeometryUtils.getPath2DPoints((GeneralPath) clickedShape.shape);

                            int xpoints[] = new int[MAX_POLYGON_POINT_COUNT];

                            xpoints[0] = (int) genPathPoints.get(0).getX();
                            xpoints[1] = (int) genPathPoints.get(1).getX();
                            xpoints[2] = (int) genPathPoints.get(2).getX();
                            xpoints[3] = (int) genPathPoints.get(3).getX();
                            xpoints[4] = (int) genPathPoints.get(4).getX();

                            int ypoints[] = new int[MAX_POLYGON_POINT_COUNT];

                            ypoints[0] = (int) genPathPoints.get(0).getY();
                            ypoints[1] = (int) genPathPoints.get(1).getY();
                            ypoints[2] = (int) genPathPoints.get(2).getY();
                            ypoints[3] = (int) genPathPoints.get(3).getY();
                            ypoints[4] = (int) genPathPoints.get(4).getY();

                            PolygonEditPanel polygonEditPanel = new PolygonEditPanel(clickedShape.name,
                                    String.valueOf(xpoints[0]),
                                    String.valueOf(ypoints[0]),
                                    String.valueOf(xpoints[1]),
                                    String.valueOf(ypoints[1]),
                                    String.valueOf(xpoints[2]),
                                    String.valueOf(ypoints[2]),
                                    String.valueOf(xpoints[3]),
                                    String.valueOf(ypoints[3]),
                                    String.valueOf(xpoints[4]),
                                    String.valueOf(ypoints[4])
                            );

                            result = JOptionPane.showConfirmDialog(null, polygonEditPanel,
                                    "Edit polygon params", JOptionPane.OK_CANCEL_OPTION);

                            if (result == JOptionPane.OK_OPTION) {

                                ShapeWrapper newShapeWrapper = new ShapeWrapper();
                                newShapeWrapper.objectType = MapObjectType.PARK_ZONE;

                                xpoints[0] = Integer.valueOf(polygonEditPanel.getX1Field().getText());
                                xpoints[1] = Integer.valueOf(polygonEditPanel.getX2Field().getText());
                                xpoints[2] = Integer.valueOf(polygonEditPanel.getX3Field().getText());
                                xpoints[3] = Integer.valueOf(polygonEditPanel.getX4Field().getText());
                                xpoints[4] = Integer.valueOf(polygonEditPanel.getX5Field().getText());

                                ypoints[0] = Integer.valueOf(polygonEditPanel.getY1Field().getText());
                                ypoints[1] = Integer.valueOf(polygonEditPanel.getY2Field().getText());
                                ypoints[2] = Integer.valueOf(polygonEditPanel.getY3Field().getText());
                                ypoints[3] = Integer.valueOf(polygonEditPanel.getY4Field().getText());
                                ypoints[4] = Integer.valueOf(polygonEditPanel.getY5Field().getText());

                                Polygon polygon = new Polygon(xpoints, ypoints, MAX_POLYGON_POINT_COUNT);
                                newShapeWrapper.shape = polygon;
                                newShapeWrapper.name = polygonEditPanel.getPolygonNameField().getText();

                                try {
                                    shapesList.deleteShape(clickedShape);
                                    shapesList.addShape(newShapeWrapper);
                                    repaint();
                                    newShapeWrapper = null;
                                } catch (Exception exception) {
                                    logger.log(Level.FATAL, "", exception);
                                    ExceptionMessage.show(exception);
                                }
                            }
                            break;
                        case UNDFINED_TYPE:
                            break;
                        default:
                            break;
                    }
                }

            } else if (currentGeometryOperationMode == GeometryOperation.CALCULATE_DISTANCE) {

                if (firstSelectedShape == null) {
                    firstSelectedShape = getSelectedShapeByCoords(clickX, clickY);
                } else {
                    secondSelectedShape = getSelectedShapeByCoords(clickX, clickY);
                    if (secondSelectedShape != null) {
                        double calculatedDistance = DistanceCalculator.calculate(firstSelectedShape, secondSelectedShape);
                        System.out.println("Distance : " + calculatedDistance);
                        JOptionPane.showMessageDialog(getParent(),
                                "Distance between " + firstSelectedShape.name + " and " + secondSelectedShape.name + " = " + String.valueOf(calculatedDistance));
                        firstSelectedShape = null;
                        secondSelectedShape = null;
                    }
                }
            } else if (currentGeometryOperationMode == GeometryOperation.CALCULATE_PERIMETER) {

                ShapeWrapper clickedShape = getSelectedShapeByCoords(clickX, clickY);
                double calculatedPerimeter = PerimeterCalculator.calculate(clickedShape);
                System.out.println("Perimeter : " + calculatedPerimeter);
                JOptionPane.showMessageDialog(getParent(),
                        "Perimeter of " + clickedShape.name + " = " + String.valueOf(calculatedPerimeter
                        ));
            } else if (currentGeometryOperationMode == GeometryOperation.CALCULATE_NEAREST) {

                ShapeWrapper clickedShape = getSelectedShapeByCoords(clickX, clickY);

                Map<ShapeWrapper, Float> nearestObjects = NearestObjectsCalculator.calculate(clickedShape, nearestObjectsCount);

                StringBuilder sb = new StringBuilder();

                System.out.println("Nearest objects and distance: ");
                for (Map.Entry<ShapeWrapper, Float> nearestEntry : nearestObjects.entrySet()) {
                    System.out.println(nearestEntry.getKey().name + ", distance is " + nearestEntry.getValue());
                    sb.append(nearestEntry.getKey().name + ", distance is " + nearestEntry.getValue() + "\n");
                }

                JOptionPane.showConfirmDialog(null, new NearestObjectsResultPanel(sb.toString()),
                        "Nearest objects list", JOptionPane.PLAIN_MESSAGE);
            }
        } catch (Exception exception) {
            logger.log(Level.FATAL, "", exception);
            ExceptionMessage.show(exception);
        } finally {
            shapesListLock.unlock();
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    public void setModel(ShapesWrapperList list) {
        shapesList = list;
    }

    public void removeSelectedShape() throws SQLException {
        if (shapesList != null && shapesList.shapes.size() > 0) {
            if (selectedShape != null) {
                shapesList.deleteShape(selectedShape);
                repaint();
            }
        }
    }

    public void removeShape(ShapeWrapper shape) throws SQLException {
        if (shapesList != null && shapesList.shapes.size() > 0) {
            if (shape != null && shape.name != null) {
                shapesList.deleteShape(shape);
                repaint();
            }
        }
    }

    public void enableObjectAddMode() {
        currentGeometyEditMode = GeometryEditMode.ADD_OBJECT;
        currentGeometryOperationMode = GeometryOperation.NO_OPERATION_MODE;
    }

    public void enableObjectDeleteMode() {
        currentGeometyEditMode = GeometryEditMode.REMOVE_OBJECT;
        currentGeometryOperationMode = GeometryOperation.NO_OPERATION_MODE;
    }

    public void enableCalculateAreaMode() {
        currentGeometyEditMode = GeometryEditMode.NO_OPERATION_MODE;
        currentGeometryOperationMode = GeometryOperation.CALCULATE_AREA;
    }

    public void enableCalculateDistanceMode() {
        currentGeometyEditMode = GeometryEditMode.NO_OPERATION_MODE;
        currentGeometryOperationMode = GeometryOperation.CALCULATE_DISTANCE;
    }

    public void enableCalculatePerimeterMode() {
        currentGeometyEditMode = GeometryEditMode.NO_OPERATION_MODE;
        currentGeometryOperationMode = GeometryOperation.CALCULATE_PERIMETER;
    }

    void enableCalculateNearestObjectsMode() {
        currentGeometryOperationMode = GeometryOperation.CALCULATE_NEAREST;
        currentGeometyEditMode = GeometryEditMode.NO_OPERATION_MODE;
    }

    void enableEditObjectMode() {
        currentGeometyEditMode = GeometryEditMode.EDIT_OBJECT;
        currentGeometryOperationMode = GeometryOperation.NO_OPERATION_MODE;
    }

    /**
     * Returns clicked shape wrapper object
     *
     * @param x click x coord
     * @param y click y coord
     * @return clicked shape wrapper object or null
     */
    public ShapeWrapper getSelectedShapeByCoords(final int x, final int y) {

        ShapeWrapper clickedShape = null;
        for (ShapeWrapper currentShapeWrapper : shapesList.shapes) {
            if (currentShapeWrapper.shape instanceof Shape) {
                Shape shape = (Shape) currentShapeWrapper.shape;
                if (shape instanceof Path2D) {
                    boolean pathContainsClickedPoint = GeometryUtils.isPointNearLine((Path2D) currentShapeWrapper.shape, x, y);
                    if (pathContainsClickedPoint) {
                        selectedShape = currentShapeWrapper;
                        clickedShape = currentShapeWrapper;
                    }
                }
                boolean shapeContainsClickedPoint = shape.contains(x - horizontalOffset, y - verticalOffset);
                if (shapeContainsClickedPoint) {
                    selectedShape = currentShapeWrapper;
                    clickedShape = currentShapeWrapper;
                }
            } else {
                Point2D point = (Point2D) currentShapeWrapper.shape;
                boolean isNearPointClicked = point.distance(x - horizontalOffset, y - verticalOffset) <= 2;
                if (isNearPointClicked) {
                    selectedShape = currentShapeWrapper;
                    clickedShape = currentShapeWrapper;
                }
            }
        }
        return clickedShape;
    }

    public void setSelectedShapeType(ShapeType selectedShapeType) {
        this.selectedShapeType = selectedShapeType;
    }

    @Override
    public void mouseDragged(MouseEvent e) {

    }

    @Override
    public void mouseMoved(MouseEvent e) {
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

}
