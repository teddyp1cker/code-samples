package pdb.projekt.ui;

/**
 *
 * @author Iurii xkuzne00@stud.fit.vutbr.cz Kuznetcov
 */
public enum GeometryOperation {
    CALCULATE_DISTANCE,
    CALCULATE_AREA,
    CALCULATE_PERIMETER,
    NO_OPERATION_MODE, 
    CALCULATE_NEAREST
}
