package pdb.projekt.ui;

import pdb.projekt.autorization.DefaultCredentialsAuthenticator;
import pdb.projekt.autorization.CredentialsAuthenticator;

/**
 * Login window
 *
 * @author Iurii xkuzne00@stud.fit.vutbr.cz Kuznetcov
 */
public class LoginWindowFrame extends javax.swing.JFrame {

    CredentialsAuthenticator authenticator = new DefaultCredentialsAuthenticator();

    public LoginWindowFrame() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always regenerated by the Form
     * Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        LoginTextField = new javax.swing.JTextField();
        PasswordField = new javax.swing.JPasswordField();
        LoginAsGuestButton = new javax.swing.JButton();
        LoginAsAdministatorButton = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel1 = new javax.swing.JLabel();
        LoginStatusLabel = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setName("Login Window"); // NOI18N
        setResizable(false);

        LoginTextField.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        LoginTextField.setText("login");
        LoginTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LoginTextFieldActionPerformed(evt);
            }
        });

        PasswordField.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        PasswordField.setText("password");

        LoginAsGuestButton.setText("Login as Customer");
        LoginAsGuestButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LoginAsGuestButtonActionPerformed(evt);
            }
        });

        LoginAsAdministatorButton.setText("Login as Administrator");
        LoginAsAdministatorButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LoginAsAdministatorButtonActionPerformed(evt);
            }
        });

        jLabel1.setText("Helicopter flight reservation application");

        LoginStatusLabel.setFont(LoginStatusLabel.getFont().deriveFont(LoginStatusLabel.getFont().getStyle() | java.awt.Font.BOLD));
        LoginStatusLabel.setForeground(new java.awt.Color(255, 0, 0));
        LoginStatusLabel.setToolTipText("");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator1, javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(203, 203, 203)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(PasswordField, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(LoginTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(92, 92, 92)
                        .addComponent(LoginAsGuestButton)
                        .addGap(33, 33, 33)
                        .addComponent(LoginAsAdministatorButton))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(133, 133, 133)
                        .addComponent(jLabel1))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(41, 41, 41)
                        .addComponent(LoginStatusLabel)))
                .addContainerGap(78, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addComponent(jLabel1)
                .addGap(42, 42, 42)
                .addComponent(LoginTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(PasswordField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(31, 31, 31)
                .addComponent(LoginStatusLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 60, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(LoginAsAdministatorButton)
                    .addComponent(LoginAsGuestButton))
                .addGap(75, 75, 75))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void LoginTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_LoginTextFieldActionPerformed
    }//GEN-LAST:event_LoginTextFieldActionPerformed

    private void LoginAsAdministatorButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_LoginAsAdministatorButtonActionPerformed
        boolean authenticated = authenticator.authenticate(LoginTextField.getText(), String.valueOf(PasswordField.getPassword()));

        if (authenticated) {
            LoginStatusLabel.setText("");

            AdministratorWindowFrame administratorWindowFrame = new AdministratorWindowFrame(this);
            administratorWindowFrame.setLocationRelativeTo(null);
            administratorWindowFrame.setVisible(true);

            this.setVisible(false);

        } else {
            LoginStatusLabel.setText("Bad credentials given. Use 'login' as login and 'password' as password");
        }
    }//GEN-LAST:event_LoginAsAdministatorButtonActionPerformed

    private void LoginAsGuestButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_LoginAsGuestButtonActionPerformed
        boolean authenticated = authenticator.authenticate(LoginTextField.getText(), String.valueOf(PasswordField.getPassword()));

        if (authenticated) {
            LoginStatusLabel.setText("");

            FlightReservationWindow reservationWindow = new FlightReservationWindow(this);
            reservationWindow.setLocationRelativeTo(null);
            reservationWindow.setVisible(true);

            this.setVisible(false);

        } else {
            LoginStatusLabel.setText("Bad credentials given. Use 'login' as login and 'password' as password");
        }
    }//GEN-LAST:event_LoginAsGuestButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton LoginAsAdministatorButton;
    private javax.swing.JButton LoginAsGuestButton;
    private javax.swing.JLabel LoginStatusLabel;
    private javax.swing.JTextField LoginTextField;
    private javax.swing.JPasswordField PasswordField;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JSeparator jSeparator1;
    // End of variables declaration//GEN-END:variables
}
