package pdb.projekt.ui;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import pdb.projekt.model.Flight;
import pdb.projekt.model.ShapesWrapperList;
import pdb.projekt.ui.validation.EmptyStringVerifier;
import pdb.projekt.utils.Utils;

/**
 * New flight reservation admin creation dialog
 *
 * @author Iurii xkuzne00@stud.fit.vutbr.cz Kuznetcov
 */
public class FlightCreateDialog extends javax.swing.JDialog {

    private static final Logger logger = Logger.getLogger(FlightCreateDialog.class.getName());
    private EmptyStringVerifier emptyFieldVerifier = new EmptyStringVerifier();
    public static final int RET_CANCEL = 0;
    public static final int RET_OK = 1;

    /**
     * Original JTextField border (I'm don't know any other ways to restore original Border that matches current
     * Java Swing's theme (LAF).
     */
    private Border textFieldOriginalBorder;

    public FlightCreateDialog(java.awt.Frame parent, boolean modal) {

        super(parent, modal);
        initComponents();
        saveOriginalInputFieldEnvelope(StartDateField);
        hideErrorLabels();

        // Close the dialog when Esc is pressed
        String cancelName = "cancel";
        InputMap inputMap = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), cancelName);
        ActionMap actionMap = getRootPane().getActionMap();
        actionMap.put(cancelName, new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                doClose(RET_CANCEL);
            }
        });

        enableTextFieldsFocusValidation();
    }

    private void hideErrorLabels() {

        startHeliportErrorLabel.setVisible(false);
        endHeliportErrorLabel.setVisible(false);
        helikopterErrorLabel.setVisible(false);
        clientErrorLabel.setVisible(false);
    }

    private void enableTextFieldsFocusValidation() {

        StartDateField.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                boolean valid = emptyFieldVerifier.verify(StartDateField);
                switchTextFieldEnvelope(StartDateField, valid);
            }
        });

        EndDateField.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                boolean valid = emptyFieldVerifier.verify(EndDateField);
                switchTextFieldEnvelope(EndDateField, valid);
            }
        });

        StartPointField.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                boolean valid = emptyFieldVerifier.verify(StartPointField);
                switchTextFieldEnvelope(StartPointField, valid);
            }
        });

        EndPointField.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                boolean valid = emptyFieldVerifier.verify(EndPointField);
                switchTextFieldEnvelope(EndPointField, valid);
            }
        });

        ClientField.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                boolean valid = emptyFieldVerifier.verify(ClientField);
                switchTextFieldEnvelope(ClientField, valid);
            }
        });

        HelikopterField.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                boolean valid = emptyFieldVerifier.verify(HelikopterField);
                switchTextFieldEnvelope(HelikopterField, valid);
            }
        });

        StartDateField.addFocusListener(new FocusListener() {
            public void focusGained(FocusEvent e) {
            }

            public void focusLost(FocusEvent e) {
                if (!e.isTemporary()) {
                    boolean valid = emptyFieldVerifier.verify(StartDateField);
                    switchTextFieldEnvelope(StartDateField, valid);
                }
            }
        });

        EndDateField.addFocusListener(new FocusListener() {
            public void focusGained(FocusEvent e) {
            }

            public void focusLost(FocusEvent e) {
                if (!e.isTemporary()) {
                    boolean valid = emptyFieldVerifier.verify(EndDateField);
                    switchTextFieldEnvelope(EndDateField, valid);
                }
            }
        });

        StartPointField.addFocusListener(new FocusListener() {
            public void focusGained(FocusEvent e) {
            }

            public void focusLost(FocusEvent e) {
                if (!e.isTemporary()) {
                    boolean valid = emptyFieldVerifier.verify(StartPointField);
                    switchTextFieldEnvelope(StartPointField, valid);
                }
            }
        });

        EndPointField.addFocusListener(new FocusListener() {
            public void focusGained(FocusEvent e) {
            }

            public void focusLost(FocusEvent e) {
                if (!e.isTemporary()) {
                    boolean valid = emptyFieldVerifier.verify(EndPointField);
                    switchTextFieldEnvelope(EndPointField, valid);
                }
            }
        });

        HelikopterField.addFocusListener(new FocusListener() {
            public void focusGained(FocusEvent e) {
            }

            public void focusLost(FocusEvent e) {
                if (!e.isTemporary()) {
                    boolean valid = emptyFieldVerifier.verify(HelikopterField);
                    switchTextFieldEnvelope(HelikopterField, valid);
                }
            }
        });

        ClientField.addFocusListener(new FocusListener() {
            public void focusGained(FocusEvent e) {
            }

            public void focusLost(FocusEvent e) {
                if (!e.isTemporary()) {
                    boolean valid = emptyFieldVerifier.verify(ClientField);
                    switchTextFieldEnvelope(ClientField, valid);
                }
            }
        });
    }

    public int getReturnStatus() {
        return returnStatus;
    }

    /**
     * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always regenerated by the Form
     * Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        okButton = new javax.swing.JButton();
        cancelButton = new javax.swing.JButton();
        StartDateField = new javax.swing.JTextField();
        EndDateField = new javax.swing.JTextField();
        StartPointField = new javax.swing.JTextField();
        EndPointField = new javax.swing.JTextField();
        ClientField = new javax.swing.JTextField();
        HelikopterField = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        startHeliportErrorLabel = new javax.swing.JLabel();
        endHeliportErrorLabel = new javax.swing.JLabel();
        clientErrorLabel = new javax.swing.JLabel();
        helikopterErrorLabel = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();

        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        okButton.setFont(okButton.getFont().deriveFont(okButton.getFont().getStyle() | java.awt.Font.BOLD));
        okButton.setText("Save");
        okButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                okButtonActionPerformed(evt);
            }
        });

        cancelButton.setText("Cancel");
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });

        ClientField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ClientFieldActionPerformed(evt);
            }
        });

        jLabel1.setText("Start date :");

        jLabel2.setText("End date :");

        jLabel3.setText("Start heliport :");

        jLabel4.setText("End heliport :");

        jLabel5.setText("Client email :");

        jLabel6.setText("Helicopter :");

        jLabel7.setFont(jLabel7.getFont().deriveFont(jLabel7.getFont().getStyle() | java.awt.Font.BOLD, jLabel7.getFont().getSize()+1));
        jLabel7.setText("Enter flight data :");

        jLabel8.setFont(jLabel8.getFont().deriveFont(jLabel8.getFont().getSize()-2f));
        jLabel8.setForeground(new java.awt.Color(102, 102, 102));
        jLabel8.setText("'MM/dd/yyyy HH' format");

        jLabel9.setFont(jLabel9.getFont().deriveFont(jLabel9.getFont().getSize()-2f));
        jLabel9.setForeground(new java.awt.Color(102, 102, 102));
        jLabel9.setText("'MM/dd/yyyy HH' format");

        startHeliportErrorLabel.setFont(startHeliportErrorLabel.getFont().deriveFont(startHeliportErrorLabel.getFont().getSize()-2f));
        startHeliportErrorLabel.setForeground(new java.awt.Color(255, 0, 0));
        startHeliportErrorLabel.setText("Heliport id does'nt exists");

        endHeliportErrorLabel.setFont(endHeliportErrorLabel.getFont().deriveFont(endHeliportErrorLabel.getFont().getSize()-2f));
        endHeliportErrorLabel.setForeground(new java.awt.Color(255, 0, 0));
        endHeliportErrorLabel.setText("Heliport id does'nt exists");

        clientErrorLabel.setFont(clientErrorLabel.getFont().deriveFont(clientErrorLabel.getFont().getSize()-2f));
        clientErrorLabel.setForeground(new java.awt.Color(255, 0, 0));
        clientErrorLabel.setText("Client was'nt found");

        helikopterErrorLabel.setFont(helikopterErrorLabel.getFont().deriveFont(helikopterErrorLabel.getFont().getSize()-2f));
        helikopterErrorLabel.setForeground(new java.awt.Color(255, 0, 0));
        helikopterErrorLabel.setText("Heliport id does'nt exists");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(40, 40, 40)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(endHeliportErrorLabel)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGap(35, 35, 35)
                        .addComponent(EndDateField, javax.swing.GroupLayout.PREFERRED_SIZE, 373, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel9)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addGap(35, 35, 35)
                        .addComponent(StartPointField, javax.swing.GroupLayout.PREFERRED_SIZE, 373, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(startHeliportErrorLabel)
                    .addComponent(jLabel8)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(35, 35, 35)
                        .addComponent(StartDateField, javax.swing.GroupLayout.PREFERRED_SIZE, 373, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel5)
                                .addGap(35, 35, 35)
                                .addComponent(ClientField, javax.swing.GroupLayout.PREFERRED_SIZE, 373, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(clientErrorLabel)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel6)
                                .addGap(35, 35, 35)
                                .addComponent(HelikopterField, javax.swing.GroupLayout.PREFERRED_SIZE, 373, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(helikopterErrorLabel))
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(jLabel4)
                            .addGap(35, 35, 35)
                            .addComponent(EndPointField, javax.swing.GroupLayout.PREFERRED_SIZE, 373, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(44, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jLabel7)
                .addGap(218, 218, 218))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator1, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jSeparator2)))
            .addGroup(layout.createSequentialGroup()
                .addGap(215, 215, 215)
                .addComponent(okButton, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(cancelButton)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {cancelButton, okButton});

        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addComponent(jLabel7)
                .addGap(18, 18, 18)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(24, 24, 24)
                .addComponent(jLabel8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(StartDateField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel9)
                .addGap(5, 5, 5)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(EndDateField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addGap(18, 18, 18)
                .addComponent(startHeliportErrorLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(StartPointField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addGap(18, 18, 18)
                .addComponent(endHeliportErrorLabel)
                .addGap(1, 1, 1)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(EndPointField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addGap(18, 18, 18)
                .addComponent(clientErrorLabel)
                .addGap(2, 2, 2)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(ClientField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addGap(18, 18, 18)
                .addComponent(helikopterErrorLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(HelikopterField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6))
                .addGap(27, 27, 27)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(okButton)
                    .addComponent(cancelButton))
                .addContainerGap(25, Short.MAX_VALUE))
        );

        getRootPane().setDefaultButton(okButton);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void okButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_okButtonActionPerformed
        doClose(RET_OK);
    }//GEN-LAST:event_okButtonActionPerformed

    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        doClose(RET_CANCEL);
    }//GEN-LAST:event_cancelButtonActionPerformed


    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
        doClose(RET_CANCEL);
    }//GEN-LAST:event_closeDialog

    private void ClientFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ClientFieldActionPerformed
    }//GEN-LAST:event_ClientFieldActionPerformed

    private void doClose(int retStatus) {

        returnStatus = retStatus;
        if (returnStatus == RET_OK) {

            if (validateNewFlightData()) {

                Flight flight = new Flight();
                flight.clientId = Integer.valueOf(ClientField.getText());
                flight.helicopterId = Integer.valueOf(HelikopterField.getText());
                flight.startPointId = Integer.valueOf(StartPointField.getText());
                flight.endPointId = Integer.valueOf(EndPointField.getText());
                flight.startDateTime = Utils.parseDateFormat(StartDateField.getText(), "MM/dd/yyyy HH");
                flight.endDateTime = Utils.parseDateFormat(EndDateField.getText(), "MM/dd/yyyy HH");

                ShapesWrapperList shapes = new ShapesWrapperList();

                try {

                    boolean startPointExists = shapes.isShapeExistsByName(StartPointField.getText());
                    boolean endPointExists = shapes.isShapeExistsByName(EndPointField.getText());
                    boolean clientExists = shapes.isShapeExistsByName(ClientField.getText());
                    boolean helikopterExists = shapes.isShapeExistsByName(HelikopterField.getText());

                    if (!startPointExists) {
                        switchTextFieldEnvelope(StartPointField, false);
                        startHeliportErrorLabel.setVisible(true);
                    }

                    if (!endPointExists) {
                        switchTextFieldEnvelope(EndPointField, false);
                        endHeliportErrorLabel.setVisible(true);
                    }

                    if (!clientExists) {
                        switchTextFieldEnvelope(ClientField, false);
                        clientErrorLabel.setVisible(true);
                    }

                    if (!helikopterExists) {
                        switchTextFieldEnvelope(HelikopterField, false);
                        helikopterErrorLabel.setVisible(true);
                    }

                    if (clientExists && helikopterExists && startPointExists && endPointExists) {

                        ((FlightEditableListFrame) getParent()).getFlightsList().addFlight(flight);
                        ((FlightEditableListFrame) getParent()).revalidateTableModel();
                        setVisible(false);
                        dispose();
                    }
                } catch (Exception e) {
                    logger.log(Level.FATAL, "", e);
                    ExceptionMessage.show(e);
                }
            }
        } else {
            setVisible(false);
            dispose();
        }
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField ClientField;
    private javax.swing.JTextField EndDateField;
    private javax.swing.JTextField EndPointField;
    private javax.swing.JTextField HelikopterField;
    private javax.swing.JTextField StartDateField;
    private javax.swing.JTextField StartPointField;
    private javax.swing.JButton cancelButton;
    private javax.swing.JLabel clientErrorLabel;
    private javax.swing.JLabel endHeliportErrorLabel;
    private javax.swing.JLabel helikopterErrorLabel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JButton okButton;
    private javax.swing.JLabel startHeliportErrorLabel;
    // End of variables declaration//GEN-END:variables

    private int returnStatus = RET_CANCEL;

    private void saveOriginalInputFieldEnvelope(JTextField textField) {
        textFieldOriginalBorder = textField.getBorder();
    }

    private void switchTextFieldEnvelope(JTextField textField, boolean isTextFieldDataValid) {

        if (isTextFieldDataValid) {
            textField.setBorder(textFieldOriginalBorder);
        } else {
            textField.setBorder(new LineBorder(Color.red));
        }
    }

    private boolean validateNewFlightData() {

        boolean isValid = true;
        boolean isStartDateFieldEmpty = emptyFieldVerifier.verify(StartDateField);
        boolean isEndDateFieldEmpty = emptyFieldVerifier.verify(EndDateField);
        boolean isStartPointFieldEmpty = emptyFieldVerifier.verify(StartPointField);
        boolean isEndPointFieldEmpty = emptyFieldVerifier.verify(EndPointField);
        boolean isCustomerEmailFieldEmpty = emptyFieldVerifier.verify(ClientField);
        boolean isHelikopterFieldEmpty = emptyFieldVerifier.verify(HelikopterField);

        if (!isStartDateFieldEmpty) {
            switchTextFieldEnvelope(StartDateField, false);
            isValid = false;
        }

        if (!isEndDateFieldEmpty) {
            switchTextFieldEnvelope(EndDateField, false);
            isValid = false;
        }

        if (!isStartPointFieldEmpty) {
            switchTextFieldEnvelope(StartPointField, false);
            isValid = false;
        }

        if (!isEndPointFieldEmpty) {
            switchTextFieldEnvelope(EndPointField, false);
            isValid = false;
        }

        if (!isCustomerEmailFieldEmpty) {
            switchTextFieldEnvelope(ClientField, false);
            isValid = false;
        }

        if (!isHelikopterFieldEmpty) {
            switchTextFieldEnvelope(HelikopterField, false);
            isValid = false;
        }
        return isValid;
    }
}
