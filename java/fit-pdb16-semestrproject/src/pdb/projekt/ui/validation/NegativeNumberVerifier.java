package pdb.projekt.ui.validation;

import javax.swing.InputVerifier;
import javax.swing.JComponent;
import javax.swing.JTextField;

/**
 * @author Iurii xkuzne00@stud.fit.vutbr.cz Kuznetcov
 */
public class NegativeNumberVerifier extends InputVerifier {

    @Override
    public boolean verify(JComponent input) {
        String value = ((JTextField) input).getText();
        Double intValue = Double.valueOf(value);
        return intValue >= 0;
    }
}
