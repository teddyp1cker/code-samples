package pdb.projekt.ui;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import pdb.projekt.model.Helikopter;
import pdb.projekt.ui.validation.EmptyStringVerifier;
import pdb.projekt.ui.validation.NegativeNumberVerifier;
import pdb.projekt.utils.Utils;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.sql.SQLException;

/**
 * Helicopter create/update dialog
 *
 * @author Iurii xkuzne00@stud.fit.vutbr.cz Kuznetcov
 */

public class HelikopterEditDialog extends javax.swing.JDialog {

    private static final Logger logger = Logger.getLogger(HelikopterEditDialog.class.getName());

    public static final int RET_CANCEL = 0;
    public static final int RET_OK = 1;

    private EmptyStringVerifier emptyFieldVerifier = new EmptyStringVerifier();
    private NegativeNumberVerifier negativeNumberVerifier = new NegativeNumberVerifier();

    /**
     * Original JTextField border (I'm don't know any other ways to restore original Border that matches current
     * Java Swing's theme (LAF).
     */
    private Border textFieldOriginalBorder;

    private enum EditMode {
        ADD_OBJECT,
        EDIT_OBJECT
    }

    private EditMode editMode;

    /**
     * New/or updated helikopter object
     */
    private Helikopter currentHelikopter;

    /**
     * Original helikopter object (null for New Object Mode)
     */
    private Helikopter originalHelikopter;

    private boolean helikopterPictureRotationSelected = false;

    private String selectedNewImagePath;
    private byte[] selectedNewImageByteContent;

    public HelikopterEditDialog(java.awt.Frame parent, boolean modal, Helikopter helikopter, boolean newObjectMode) {

        super(parent, modal);
        initComponents();

        saveOriginalInputFieldEnvelope(nameTextField);

        currentHelikopter = helikopter;
        PictureLabel.setVisible(true);
        // We can't do oracle image rotation, because we did'nt save non-empty ordImage object yet
        RotatePictureButton.setEnabled(false);

        if (helikopter != null) {
            nameTextField.setText(helikopter.name);
            speedTextField.setText(String.valueOf(helikopter.speed));
            priceTextField.setText(String.valueOf(helikopter.price));
            capacityTextField.setText(String.valueOf(helikopter.capacity));
        }

        if (newObjectMode) {
            editMode = EditMode.ADD_OBJECT;
            PictureLabel.setIcon(null);
            originalHelikopter = null;
        } else {
            editMode = EditMode.EDIT_OBJECT;
            nameTextField.setEnabled(false);
            originalHelikopter = helikopter;
            if (currentHelikopter != null
                    && currentHelikopter.pictureByteArray != null
                    && currentHelikopter.pictureByteArray.length > 0) {
                PictureLabel.setIcon(new ImageIcon(currentHelikopter.pictureByteArray));
                PictureLabel.setVisible(true);
                PictureLabel.setText(null);
                RotatePictureButton.setEnabled(true);
            } else {
                PictureLabel.setIcon(null);
                // We can't do oracle image rotation, because we did'nt save non-empty ordImage object yet
                RotatePictureButton.setEnabled(false);
            }
        }

        String cancelName = "cancel";
        InputMap inputMap = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), cancelName);
        ActionMap actionMap = getRootPane().getActionMap();
        actionMap.put(cancelName, new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                doClose(RET_CANCEL);
            }
        });

        enableTextFieldsFocusValidation();
    }

    private void enableTextFieldsFocusValidation() {
        nameTextField.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                boolean valid = emptyFieldVerifier.verify(nameTextField);
                switchTextFieldEnvelope(nameTextField, valid);
            }
        });

        priceTextField.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                boolean valid = emptyFieldVerifier.verify(priceTextField) &&
                        negativeNumberVerifier.verify(priceTextField);
                switchTextFieldEnvelope(priceTextField, valid);
            }
        });

        capacityTextField.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                boolean valid = emptyFieldVerifier.verify(capacityTextField) &&
                        negativeNumberVerifier.verify(capacityTextField);
                switchTextFieldEnvelope(capacityTextField, valid);
            }
        });

        speedTextField.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                boolean valid = emptyFieldVerifier.verify(speedTextField) &&
                        negativeNumberVerifier.verify(speedTextField);
                switchTextFieldEnvelope(speedTextField, valid);
            }
        });

        nameTextField.addFocusListener(new FocusListener() {
            public void focusGained(FocusEvent e) {
            }

            public void focusLost(FocusEvent e) {
                if (!e.isTemporary()) {
                    boolean valid = emptyFieldVerifier.verify(nameTextField);
                    switchTextFieldEnvelope(nameTextField, valid);
                }
            }
        });

        priceTextField.addFocusListener(new FocusListener() {
            public void focusGained(FocusEvent e) {
            }

            public void focusLost(FocusEvent e) {
                if (!e.isTemporary()) {
                    boolean valid = emptyFieldVerifier.verify(priceTextField) &&
                            negativeNumberVerifier.verify(priceTextField);
                    switchTextFieldEnvelope(priceTextField, valid);
                }
            }
        });

        capacityTextField.addFocusListener(new FocusListener() {
            public void focusGained(FocusEvent e) {
            }

            public void focusLost(FocusEvent e) {
                if (!e.isTemporary()) {
                    boolean valid = emptyFieldVerifier.verify(capacityTextField) &&
                            negativeNumberVerifier.verify(capacityTextField);
                    switchTextFieldEnvelope(capacityTextField, valid);
                }
            }
        });

        speedTextField.addFocusListener(new FocusListener() {
            public void focusGained(FocusEvent e) {
            }

            public void focusLost(FocusEvent e) {
                if (!e.isTemporary()) {
                    boolean valid = emptyFieldVerifier.verify(speedTextField) &&
                            negativeNumberVerifier.verify(speedTextField);
                    switchTextFieldEnvelope(speedTextField, valid);
                }
            }
        });
    }

    public int getReturnStatus() {
        return returnStatus;
    }

    /**
     * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always regenerated by the Form
     * Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        SaveButton = new javax.swing.JButton();
        CancelButton = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        nameTextField = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        speedTextField = new javax.swing.JTextField();
        priceTextField = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        capacityTextField = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        ChooseImageFileButton = new javax.swing.JButton();
        ImageLabel = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        RotatePictureButton = new javax.swing.JButton();
        HelikopterImageScrolledPane = new javax.swing.JScrollPane();
        PictureLabel = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        RemoveImageButton = new javax.swing.JButton();

        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        SaveButton.setFont(SaveButton.getFont().deriveFont(SaveButton.getFont().getStyle() | java.awt.Font.BOLD, SaveButton.getFont().getSize() - 1));
        SaveButton.setText("Save");
        SaveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SaveButtonActionPerformed(evt);
            }
        });

        CancelButton.setFont(CancelButton.getFont().deriveFont(CancelButton.getFont().getStyle() & ~java.awt.Font.BOLD, CancelButton.getFont().getSize() - 1));
        CancelButton.setText("Cancel");
        CancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CancelButtonActionPerformed(evt);
            }
        });

        jLabel1.setText("Name  :");

        jLabel2.setText("Speed :");

        jLabel3.setText("Price :");

        jLabel4.setText("Capacity :");

        ChooseImageFileButton.setFont(ChooseImageFileButton.getFont().deriveFont(ChooseImageFileButton.getFont().getSize() - 1f));
        ChooseImageFileButton.setText("Choose image file..");
        ChooseImageFileButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ChooseImageFileButtonActionPerformed(evt);
            }
        });

        RotatePictureButton.setFont(RotatePictureButton.getFont().deriveFont(RotatePictureButton.getFont().getSize() - 1f));
        RotatePictureButton.setText("Rotate (90 degrees right)");
        RotatePictureButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RotatePictureButtonActionPerformed(evt);
            }
        });

        PictureLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        PictureLabel.setText("No image selected");
        HelikopterImageScrolledPane.setViewportView(PictureLabel);

        jLabel5.setFont(jLabel5.getFont().deriveFont(jLabel5.getFont().getStyle() | java.awt.Font.BOLD, jLabel5.getFont().getSize() + 1));
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setText("Enter helikopter info");

        RemoveImageButton.setText("Remove image");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                                .addGap(21, 21, 21)
                                                .addComponent(jLabel4))
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                                .addContainerGap()
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING)
                                                        .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING)
                                                        .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING))))
                                .addGap(32, 32, 32)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(capacityTextField)
                                        .addComponent(priceTextField)
                                        .addComponent(speedTextField)
                                        .addComponent(nameTextField))
                                .addGap(52, 52, 52))
                        .addComponent(jSeparator1, javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(HelikopterImageScrolledPane)
                                        .addComponent(jSeparator2, javax.swing.GroupLayout.Alignment.TRAILING))
                                .addContainerGap())
                        .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                                .addContainerGap()
                                                .addComponent(ChooseImageFileButton)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(RotatePictureButton)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 95, Short.MAX_VALUE)
                                                .addComponent(RemoveImageButton))
                                        .addGroup(layout.createSequentialGroup()
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addGroup(layout.createSequentialGroup()
                                                                .addGap(157, 157, 157)
                                                                .addComponent(ImageLabel))
                                                        .addGroup(layout.createSequentialGroup()
                                                                .addGap(194, 194, 194)
                                                                .addComponent(SaveButton, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                .addComponent(CancelButton))
                                                        .addGroup(layout.createSequentialGroup()
                                                                .addGap(193, 193, 193)
                                                                .addComponent(jLabel5)))
                                                .addGap(0, 0, Short.MAX_VALUE)))
                                .addContainerGap())
        );

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[]{CancelButton, SaveButton});

        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGap(14, 14, 14)
                                .addComponent(jLabel5)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(12, 12, 12)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel1)
                                        .addComponent(nameTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(speedTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel2))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(priceTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel3))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(capacityTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel4))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 6, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(ChooseImageFileButton)
                                        .addComponent(RotatePictureButton)
                                        .addComponent(RemoveImageButton))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(HelikopterImageScrolledPane, javax.swing.GroupLayout.DEFAULT_SIZE, 289, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(ImageLabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(CancelButton)
                                        .addComponent(SaveButton))
                                .addContainerGap())
        );

        getRootPane().setDefaultButton(SaveButton);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void SaveButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SaveButtonActionPerformed
        doClose(RET_OK);
    }//GEN-LAST:event_SaveButtonActionPerformed

    private void CancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CancelButtonActionPerformed
        doClose(RET_CANCEL);
    }//GEN-LAST:event_CancelButtonActionPerformed

    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
        doClose(RET_CANCEL);
    }//GEN-LAST:event_closeDialog

    private void ChooseImageFileButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ChooseImageFileButtonActionPerformed

        JFileChooser imageFileChooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter(
                "JPG & PNG Images", "jpg", "png");
        imageFileChooser.setFileFilter(filter);
        int returnVal = imageFileChooser.showOpenDialog(this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            selectedNewImagePath = imageFileChooser.getSelectedFile().getAbsolutePath();
            File picturePath = new File(selectedNewImagePath);
            try {
                byte[] imgContent = Files.readAllBytes(picturePath.toPath());
                selectedNewImageByteContent = imgContent;

                PictureLabel.setIcon(new ImageIcon(selectedNewImageByteContent));
                PictureLabel.setVisible(true);
                PictureLabel.setText(null);
            } catch (IOException e) {
                logger.log(Level.FATAL, e);
                ExceptionMessage.show(e);
            }
        }
    }//GEN-LAST:event_ChooseImageFileButtonActionPerformed

    private void RotatePictureButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RotatePictureButtonActionPerformed

        helikopterPictureRotationSelected = true;
        ImageIcon picture = (ImageIcon) PictureLabel.getIcon();
        ImageIcon rotatedPicture = Utils.rotateImageIcon(picture, 90);
        PictureLabel.setIcon(rotatedPicture);
        PictureLabel.setVisible(true);
        PictureLabel.setText(null);

    }//GEN-LAST:event_RotatePictureButtonActionPerformed

    private void doClose(int retStatus) {

        returnStatus = retStatus;
        if (retStatus == RET_OK) {
            try {
                if (editMode == EditMode.ADD_OBJECT) {
                    currentHelikopter = new Helikopter();
                    currentHelikopter.name = nameTextField.getText();
                }
                switch (editMode) {
                    case ADD_OBJECT:
                        if (validateNewHelikopterData()) {
                            currentHelikopter.speed = Double.valueOf(speedTextField.getText());
                            currentHelikopter.price = Double.valueOf(priceTextField.getText());
                            currentHelikopter.capacity = Integer.valueOf(capacityTextField.getText());

                            if (selectedNewImageByteContent != null) {
                                currentHelikopter.pictureByteArray = selectedNewImageByteContent;
                            }

                            ((HelikopterEditableListFrame) getParent()).getHelikoptersList().addHelikopter(currentHelikopter);
                            ((HelikopterEditableListFrame) getParent()).revalidateTableModel();
                            setVisible(false);
                            dispose();
                        }
                        break;
                    case EDIT_OBJECT:
                        currentHelikopter.speed = Double.valueOf(speedTextField.getText());
                        currentHelikopter.price = Double.valueOf(priceTextField.getText());
                        currentHelikopter.capacity = Integer.valueOf(capacityTextField.getText());

                        if (selectedNewImageByteContent != null) {
                            currentHelikopter.pictureByteArray = selectedNewImageByteContent;
                        }

                        ((HelikopterEditableListFrame) getParent()).getHelikoptersList().updateHelikopter(currentHelikopter);
                        if (originalHelikopter.pictureByteArray != null && originalHelikopter.pictureByteArray.length > 0) {
                            if (helikopterPictureRotationSelected) {
                                ((HelikopterEditableListFrame) getParent()).getHelikoptersList().rotatePictureByName(currentHelikopter.name);
                            }
                        }
                        setVisible(false);
                        dispose();
                        break;
                    default:
                        break;
                }
            } catch (SQLException | IOException ex) {
                logger.log(Level.FATAL, ex);
                ExceptionMessage.show(ex);
            }
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton CancelButton;
    private javax.swing.JButton ChooseImageFileButton;
    private javax.swing.JScrollPane HelikopterImageScrolledPane;
    private javax.swing.JLabel ImageLabel;
    private javax.swing.JLabel PictureLabel;
    private javax.swing.JButton RemoveImageButton;
    private javax.swing.JButton RotatePictureButton;
    private javax.swing.JButton SaveButton;
    private javax.swing.JTextField capacityTextField;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JTextField nameTextField;
    private javax.swing.JTextField priceTextField;
    private javax.swing.JTextField speedTextField;
    // End of variables declaration//GEN-END:variables

    private int returnStatus = RET_CANCEL;

    private void saveOriginalInputFieldEnvelope(JTextField textField) {
        textFieldOriginalBorder = textField.getBorder();
    }

    private void switchTextFieldEnvelope(JTextField textField, boolean isTextFieldDataValid) {

        if (isTextFieldDataValid) {
            textField.setBorder(textFieldOriginalBorder);
        } else {
            textField.setBorder(new LineBorder(Color.red));
        }
    }

    private boolean validateNewHelikopterData() {

        boolean isValid = true;
        boolean isNameFieldEmpty = emptyFieldVerifier.verify(nameTextField);
        boolean isCapacityFieldEmpty = emptyFieldVerifier.verify(capacityTextField);
        boolean isSpeedFieldEmpty = emptyFieldVerifier.verify(speedTextField);
        boolean isPriceFieldEmpty = emptyFieldVerifier.verify(priceTextField);

        if (!isNameFieldEmpty) {
            switchTextFieldEnvelope(nameTextField, false);
            isValid = false;
        }

        if (!isCapacityFieldEmpty) {
            switchTextFieldEnvelope(capacityTextField, false);
            isValid = false;
        }

        if (!isSpeedFieldEmpty) {
            switchTextFieldEnvelope(speedTextField, false);
            isValid = false;
        }

        if (!isPriceFieldEmpty) {
            switchTextFieldEnvelope(priceTextField, false);
            isValid = false;
        }

        boolean isPositiveCapacityValue = negativeNumberVerifier.verify(capacityTextField);
        boolean isPositivePriceValue = negativeNumberVerifier.verify(priceTextField);
        boolean isPositiveSpeedValue = negativeNumberVerifier.verify(speedTextField);

        if (!isPositiveCapacityValue) {
            switchTextFieldEnvelope(capacityTextField, false);
            isValid = false;
        }

        if (!isPositiveSpeedValue) {
            switchTextFieldEnvelope(speedTextField, false);
            isValid = false;
        }

        if (!isPositivePriceValue) {
            switchTextFieldEnvelope(priceTextField, false);
            isValid = false;
        }
        return isValid;
    }
}
