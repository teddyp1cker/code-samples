package pdb.projekt.ui;

import javax.swing.JFrame;

/**
 * Administration tools launch window
 *
 * @author Iurii xkuzne00@stud.fit.vutbr.cz Kuznetcov
 */
public class AdministratorWindowFrame extends javax.swing.JFrame {

    private JFrame previousFrame;

    public AdministratorWindowFrame() {
        initComponents();
    }

    public AdministratorWindowFrame(JFrame previousFrame) {
        initComponents();
        this.previousFrame = previousFrame;
    }

    /**
     * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always regenerated by the Form
     * Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        MapEditorWindowButton = new javax.swing.JButton();
        CustomersEditorWindowButton = new javax.swing.JButton();
        HelikoptersEditorButton = new javax.swing.JButton();
        FlightsEditorButton = new javax.swing.JButton();
        LogoutButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Administration");
        setResizable(false);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Choose administation window"));

        MapEditorWindowButton.setText("Map editor");
        MapEditorWindowButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MapEditorWindowButtonActionPerformed(evt);
            }
        });

        CustomersEditorWindowButton.setText("Customers editor");
        CustomersEditorWindowButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CustomersEditorWindowButtonActionPerformed(evt);
            }
        });

        HelikoptersEditorButton.setText("Helicopters editor");
        HelikoptersEditorButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                HelikoptersEditorButtonActionPerformed(evt);
            }
        });

        FlightsEditorButton.setText("Flights editor");
        FlightsEditorButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                FlightsEditorButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
                jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(20, 20, 20)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(MapEditorWindowButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(CustomersEditorWindowButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(HelikoptersEditorButton, javax.swing.GroupLayout.DEFAULT_SIZE, 201, Short.MAX_VALUE)
                                        .addComponent(FlightsEditorButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addContainerGap(18, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
                jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addGap(32, 32, 32)
                                .addComponent(MapEditorWindowButton)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(CustomersEditorWindowButton)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(HelikoptersEditorButton)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(FlightsEditorButton)
                                .addContainerGap(50, Short.MAX_VALUE))
        );

        LogoutButton.setText("Logout");
        LogoutButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LogoutButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addGroup(layout.createSequentialGroup()
                                                .addGap(92, 92, 92)
                                                .addComponent(LogoutButton)
                                                .addGap(0, 0, Short.MAX_VALUE)))
                                .addGap(14, 14, 14))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(LogoutButton)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void LogoutButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_LogoutButtonActionPerformed

        if (previousFrame != null) {
            previousFrame.setVisible(true);
        }
        this.setVisible(false);
        this.dispose();
    }//GEN-LAST:event_LogoutButtonActionPerformed

    private void MapEditorWindowButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MapEditorWindowButtonActionPerformed

        MapEditorFrame mapEditorFrame = new MapEditorFrame();
        mapEditorFrame.setPreviousFrame(this);
        mapEditorFrame.setLocationRelativeTo(null);
        mapEditorFrame.setVisible(true);

        setVisible(false);

    }//GEN-LAST:event_MapEditorWindowButtonActionPerformed

    private void CustomersEditorWindowButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CustomersEditorWindowButtonActionPerformed

        CustomersListEditableFrame customerEditableFrame = new CustomersListEditableFrame();
        customerEditableFrame.setPreviousFrame(this);
        customerEditableFrame.setLocationRelativeTo(null);
        customerEditableFrame.setVisible(true);

        setVisible(false);

    }//GEN-LAST:event_CustomersEditorWindowButtonActionPerformed

    private void HelikoptersEditorButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_HelikoptersEditorButtonActionPerformed

        HelikopterEditableListFrame helikopterEditableListFrame = new HelikopterEditableListFrame();
        helikopterEditableListFrame.setLocationRelativeTo(null);
        helikopterEditableListFrame.setPreviousFrame(this);
        helikopterEditableListFrame.setVisible(true);

        setVisible(false);

    }//GEN-LAST:event_HelikoptersEditorButtonActionPerformed

    private void FlightsEditorButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_FlightsEditorButtonActionPerformed

        FlightEditableListFrame flightEditableListFrame = new FlightEditableListFrame();
        flightEditableListFrame.setLocationRelativeTo(null);
        flightEditableListFrame.setPreviousFrame(this);
        flightEditableListFrame.setVisible(true);

        setVisible(false);
    }//GEN-LAST:event_FlightsEditorButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton CustomersEditorWindowButton;
    private javax.swing.JButton FlightsEditorButton;
    private javax.swing.JButton HelikoptersEditorButton;
    private javax.swing.JButton LogoutButton;
    private javax.swing.JButton MapEditorWindowButton;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables
}
