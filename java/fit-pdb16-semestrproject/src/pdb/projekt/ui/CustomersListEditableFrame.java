package pdb.projekt.ui;

import java.sql.SQLException;
import javax.swing.JFrame;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import pdb.projekt.model.CustomersList;

/**
 * Customers's list edit window
 *
 * @author Iurii xkuzne00@stud.fit.vutbr.cz Kuznetcov
 */
public class CustomersListEditableFrame extends javax.swing.JFrame {

    private static final Logger logger = Logger.getLogger(CustomersListEditableFrame.class.getName());

    private CustomersList customersList;

    private JFrame previousFrame;
    private CustomerCreateDialog customerCreateDialog;

    public void setPreviousFrame(JFrame previousFrame) {
        this.previousFrame = previousFrame;
    }

    public CustomersListEditableFrame() {

        customersList = new CustomersList();

        try {
            customersList.loadCustomersFromDB();
        } catch (SQLException exception) {
            ExceptionMessage.show(exception);
        }

        initComponents();

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent windowEvent) {
                if (previousFrame != null) {
                    previousFrame.setVisible(true);
                }
                setVisible(false);
                dispose();
            }
        });
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        CustomersListTable = new javax.swing.JTable();
        SaveCustomersChangesButton = new javax.swing.JButton();
        DeleteCustomersChangesButton = new javax.swing.JButton();
        SaveStatusLabel = new javax.swing.JLabel();
        AddNewCustomerButton = new javax.swing.JButton();
        RefreshListButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        CustomersListTable.setModel(customersList);
        CustomersListTable.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jScrollPane1.setViewportView(CustomersListTable);

        SaveCustomersChangesButton.setText("Save changed");
        SaveCustomersChangesButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SaveCustomersChangesButtonActionPerformed(evt);
            }
        });

        DeleteCustomersChangesButton.setText("Remove selected");
        DeleteCustomersChangesButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DeleteCustomersChangesButtonActionPerformed(evt);
            }
        });

        AddNewCustomerButton.setText("Add new");
        AddNewCustomerButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AddNewCustomerButtonActionPerformed(evt);
            }
        });

        RefreshListButton.setText("Refresh");
        RefreshListButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RefreshListButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(AddNewCustomerButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(DeleteCustomersChangesButton)
                        .addGap(133, 133, 133)
                        .addComponent(SaveStatusLabel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(SaveCustomersChangesButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(RefreshListButton))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 759, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 652, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(SaveCustomersChangesButton)
                    .addComponent(DeleteCustomersChangesButton)
                    .addComponent(SaveStatusLabel)
                    .addComponent(AddNewCustomerButton)
                    .addComponent(RefreshListButton))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void SaveCustomersChangesButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SaveCustomersChangesButtonActionPerformed
        customersList.updateCustomers();
    }//GEN-LAST:event_SaveCustomersChangesButtonActionPerformed

    private void DeleteCustomersChangesButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DeleteCustomersChangesButtonActionPerformed

        int selectedRow = CustomersListTable.getSelectedRow();
        if (selectedRow != -1) {
            try {
                customersList.deleteCustomer(customersList.get(selectedRow));
                customersList.reloadCustomersFromDB();
                CustomersListTable.repaint();
            } catch (SQLException exception) {
                logger.log(Level.FATAL, exception);
                ExceptionMessage.show(exception);
            }
        }
    }//GEN-LAST:event_DeleteCustomersChangesButtonActionPerformed

    private void RefreshListButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RefreshListButtonActionPerformed

        try {
            customersList.reloadCustomersFromDB();
            CustomersListTable.repaint();
        } catch (SQLException sqlEx) {
            logger.log(Level.FATAL, sqlEx);
            ExceptionMessage.show(sqlEx);
        }
    }//GEN-LAST:event_RefreshListButtonActionPerformed

    private void AddNewCustomerButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AddNewCustomerButtonActionPerformed

        customerCreateDialog = new CustomerCreateDialog(this, true);
        customerCreateDialog.setLocationRelativeTo(this);
        customerCreateDialog.setVisible(true);
    }//GEN-LAST:event_AddNewCustomerButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton AddNewCustomerButton;
    private javax.swing.JTable CustomersListTable;
    private javax.swing.JButton DeleteCustomersChangesButton;
    private javax.swing.JButton RefreshListButton;
    private javax.swing.JButton SaveCustomersChangesButton;
    private javax.swing.JLabel SaveStatusLabel;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables

    public CustomersList getCustomersList() {
        return customersList;
    }

    public void revalidateTableModel() {
        CustomersListTable.revalidate();
    }
}
