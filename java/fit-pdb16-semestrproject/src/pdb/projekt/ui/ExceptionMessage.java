package pdb.projekt.ui;

import javax.swing.*;
import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * @author Iurii xkuzne00@stud.fit.vutbr.cz Kuznetcov
 */
public class ExceptionMessage {

    public static void show(Exception exception) {
        StringWriter sw = new StringWriter();
        exception.printStackTrace(new PrintWriter(sw));
        String exceptionAsString = sw.toString();

        JOptionPane.showConfirmDialog(null, new ErrorMessagePanel(exceptionAsString),
                "Exception occured", JOptionPane.PLAIN_MESSAGE);
    }
}
