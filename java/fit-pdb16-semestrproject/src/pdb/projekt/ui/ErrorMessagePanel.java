package pdb.projekt.ui;

/**
 * Exception message window
 *
 * @author Iurii xkuzne00@stud.fit.vutbr.cz Kuznetcov
 */
public class ErrorMessagePanel extends javax.swing.JPanel {

    /**
     *
     */
    public ErrorMessagePanel() {
        initComponents();
    }

    /**
     *
     * @param message
     */
    public ErrorMessagePanel(String message) {
        initComponents();
        ExceptionMessageTextArea.setText(message);
    }

    /**
     * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always regenerated by the Form
     * Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        ExceptionMessageTextArea = new javax.swing.JTextArea();
        jLabel1 = new javax.swing.JLabel();

        ExceptionMessageTextArea.setEditable(false);
        ExceptionMessageTextArea.setColumns(20);
        ExceptionMessageTextArea.setFont(ExceptionMessageTextArea.getFont().deriveFont(ExceptionMessageTextArea.getFont().getStyle() & ~java.awt.Font.BOLD, ExceptionMessageTextArea.getFont().getSize()-2));
        ExceptionMessageTextArea.setRows(5);
        jScrollPane1.setViewportView(ExceptionMessageTextArea);

        jLabel1.setFont(jLabel1.getFont().deriveFont(jLabel1.getFont().getStyle() | java.awt.Font.BOLD, jLabel1.getFont().getSize()-2));
        jLabel1.setForeground(new java.awt.Color(255, 0, 51));
        jLabel1.setText("Error message  :");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 558, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(9, 9, 9)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 377, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextArea ExceptionMessageTextArea;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}
