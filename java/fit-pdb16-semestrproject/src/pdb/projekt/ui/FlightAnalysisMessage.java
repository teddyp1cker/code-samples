package pdb.projekt.ui;

import javax.swing.JOptionPane;

/**
 *
 * @author Iurii xkuzne00@stud.fit.vutbr.cz Kuznetcov
 */
public class FlightAnalysisMessage {

    public static void show(
            final String analysisType,
            final String analysisValue,
            final String analysisUnits) {

        JOptionPane.showConfirmDialog(null, new FlightsAnalysisPanel(analysisType, analysisValue, analysisUnits),
                "Flights analysis result", JOptionPane.PLAIN_MESSAGE);
    }
}
