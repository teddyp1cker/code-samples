package pdb.projekt.ui;

/**
 *
 * @author Iurii xkuzne00@stud.fit.vutbr.cz Kuznetcov
 */
public enum GeometryEditMode {
    ADD_OBJECT,
    REMOVE_OBJECT,
    EDIT_OBJECT,
    NO_OPERATION_MODE
}
