package pdb.projekt;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import pdb.projekt.config.DatabaseConfig;
import pdb.projekt.config.DatabaseConfigException;
import pdb.projekt.config.DatabaseConfigReader;
import pdb.projekt.database.DatabaseConnection;
import pdb.projekt.database.DatabaseConnectionException;
import pdb.projekt.ui.ExceptionMessage;
import pdb.projekt.ui.LoginWindowFrame;

public class Application {

    private static Logger logger = Logger.getLogger(Application.class.getName());

    public static void main(String[] args) {

        BasicConfigurator.configure();

        try {
            DatabaseConfigReader configReader = new DatabaseConfigReader();
            DatabaseConfig buildDatabaseConfig = configReader.buildDatabaseConfig();
            if (buildDatabaseConfig != null) {
                try {
                    if (DatabaseConnection.checkDatabaseConnectionByConfig(buildDatabaseConfig)) {

                        ApplicationConfiguration.getInstance().setDatabaseConfig(buildDatabaseConfig);

                        LoginWindowFrame mainWindow = new LoginWindowFrame();
                        mainWindow.setLocationRelativeTo(null);
                        mainWindow.setVisible(true);
                    }
                } catch (DatabaseConnectionException e) {
                    logger.log(Level.FATAL, "Error while starting application - can't connect to database using given config : ", e);
                    ExceptionMessage.show(e);
                }
            } else {
                logger.log(Level.FATAL, "Error while starting application - can't connect to database using given config");
            }
        } catch (DatabaseConfigException e) {
            ExceptionMessage.show(e);
        }
    }
}
