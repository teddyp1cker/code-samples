package pdb.projekt.utils;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import javax.swing.*;

/**
 * DateTimes utils object
 *
 * @author Iurii xkuzne00@stud.fit.vutbr.cz Kuznetcov
 */
public class Utils {

    private static final Logger logger = Logger.getLogger(Utils.class.getName());

    private static final String DEFAULT_DATETIME_PATTERN = "MM/dd/yyyy HH:mm";
    private static final String ORACLE_DEFAULT_DATETIME_PATTERN = "mm/dd/yyyy HH:mm";
    private static final String ORACLE_DEFAULT_INSERT_DATETIME_PATTERN = "yyyy/MM/dd HH";

    public static String get_ORACLE_DEFAULT_DATETIME_PATTERN() {
        return ORACLE_DEFAULT_DATETIME_PATTERN;
    }

    public static String get_DEFAULT_DATETIME_PATTERN() {
        return DEFAULT_DATETIME_PATTERN;
    }

    public static Date parseDefaultDateFormat(final String dateStr) {

        Date date = null;
        SimpleDateFormat sdf = new SimpleDateFormat(DEFAULT_DATETIME_PATTERN);
        try {
            date = sdf.parse(dateStr);
        } catch (ParseException ex) {
            logger.log(Level.FATAL, ex);
        }
        return date;
    }

    public static Date parseDateFormat(final String dateStr, final String Pattern) {

        Date date = null;
        SimpleDateFormat sdf = new SimpleDateFormat(Pattern);
        try {
            date = sdf.parse(dateStr);
        } catch (ParseException ex) {
            logger.log(Level.FATAL, ex);
        }
        return date;
    }

    public static String getDateStringInDefaultFormat(final Date date) {
        return new SimpleDateFormat(ORACLE_DEFAULT_INSERT_DATETIME_PATTERN).format(date);
    }

    public static ImageIcon rotateImageIcon(ImageIcon picture, double angle) {

        int w = picture.getIconWidth();
        int h = picture.getIconHeight();
        int type = BufferedImage.TYPE_INT_RGB;  // other options, see api
        BufferedImage image = new BufferedImage(h, w, type);
        Graphics2D g2 = image.createGraphics();
        double x = (h - w) / 2.0;
        double y = (w - h) / 2.0;
        AffineTransform at = AffineTransform.getTranslateInstance(x, y);
        at.rotate(Math.toRadians(angle), w / 2.0, h / 2.0);
        g2.drawImage(picture.getImage(), at, null);
        g2.dispose();
        picture = new ImageIcon(image);

        return picture;
    }
}
