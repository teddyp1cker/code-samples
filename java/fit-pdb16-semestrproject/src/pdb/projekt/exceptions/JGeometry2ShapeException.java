package pdb.projekt.exceptions;

/**
 * Oracle JGeometry to AWT Shape conversion exception
 *
 * @author Iurii xkuzne00@stud.fit.vutbr.cz Kuznetcov
 */
public class JGeometry2ShapeException extends Exception {

    public JGeometry2ShapeException() {
    }

    public JGeometry2ShapeException(String message) {
        super(message);
    }
}
