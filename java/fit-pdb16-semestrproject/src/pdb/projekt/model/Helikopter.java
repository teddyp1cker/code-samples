package pdb.projekt.model;

import oracle.ord.im.OrdImage;

/**
 * Helicopter model
 *
 * @author Iurii xkuzne00@stud.fit.vutbr.cz Kuznetcov
 */
public class Helikopter {

    public int id;
    public String name;
    public double speed;
    public double price;
    public int capacity;
    public OrdImage picture;
    public byte[] pictureByteArray;

    public Helikopter(String name, double speed, double price, int capacity) {
        this.name = name;
        this.speed = speed;
        this.price = price;
        this.capacity = capacity;
    }

    public Helikopter(String name, double speed, double price, int capacity, OrdImage picture) {
        this.name = name;
        this.speed = speed;
        this.price = price;
        this.capacity = capacity;
        this.picture = picture;
    }

    public Helikopter(int id, String name, double speed, double price, int capacity, OrdImage picture, byte[] pictureByteArray) {
        this.id = id;
        this.name = name;
        this.speed = speed;
        this.price = price;
        this.capacity = capacity;
        this.picture = picture;
        this.pictureByteArray = pictureByteArray;
    }

    public Helikopter(String name, double speed, double price, int capacity, byte[] pictureByteArray) {
        this.name = name;
        this.speed = speed;
        this.price = price;
        this.capacity = capacity;
        this.pictureByteArray = pictureByteArray;
    }

    public Helikopter(int id, String name, double speed, double price, int capacity, byte[] pictureByteArray) {
        this.id = id;
        this.name = name;
        this.speed = speed;
        this.price = price;
        this.capacity = capacity;
        this.pictureByteArray = pictureByteArray;
    }

    public Helikopter() {
    }

    @Override
    public String toString() {
        return "Helikopter{" + "name=" + name + ", speed=" + speed + ", price=" + price + ", capacity=" + capacity + ", picture=" + picture + ", pictureByteArray=" + pictureByteArray + '}';
    }

}
