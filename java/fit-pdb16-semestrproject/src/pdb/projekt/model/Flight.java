package pdb.projekt.model;

import java.util.Date;

/**
 * Helicopter's flight model
 *
 * @author Iurii xkuzne00@stud.fit.vutbr.cz Kuznetcov
 */
public class Flight {

    public int id;
    public Date startDateTime;
    public Date endDateTime;
    public int startPointId;
    public int endPointId;
    public int clientId;
    public int helicopterId;

    public Flight(Date startDateTime, Date endDateTime, int startPointId, int endPointId, int clientId, int helicopterId) {

        this.startDateTime = startDateTime;
        this.endDateTime = endDateTime;
        this.startPointId = startPointId;
        this.endPointId = endPointId;
        this.clientId = clientId;
        this.helicopterId = helicopterId;
    }

    public Flight(int id, Date startDateTime, Date endDateTime, int startPointId, int endPointId, int clientId, int helicopterId) {

        this.id = id;
        this.startDateTime = startDateTime;
        this.endDateTime = endDateTime;
        this.startPointId = startPointId;
        this.endPointId = endPointId;
        this.clientId = clientId;
        this.helicopterId = helicopterId;
    }

    public Flight() {
    }

    @Override
    public String toString() {
        return "Flight{" + "id=" + id + ", startDateTime=" + startDateTime + ", endDateTime=" + endDateTime + ", startPointId=" + startPointId + ", endPointId=" + endPointId + ", clientId=" + clientId + ", helicopterId=" + helicopterId + '}';
    }

}
