package pdb.projekt.model;

import oracle.jdbc.OraclePreparedStatement;
import oracle.jdbc.OracleResultSet;
import oracle.jdbc.pool.OracleDataSource;
import oracle.ord.im.OrdImage;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import pdb.projekt.ApplicationConfiguration;
import pdb.projekt.database.DatabaseConnection;
import pdb.projekt.ui.ExceptionMessage;
import pdb.projekt.utils.Utils;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Flights DAO object. Performs CRUD operations and some additional operations with flights.
 *
 * @author Iurii xkuzne00@stud.fit.vutbr.cz Kuznetcov
 */
public class FlightsList implements TableModel {

    private static final Logger logger = Logger.getLogger(FlightsList.class.getName());
    private List<Flight> flightsList;

    public FlightsList() {
        flightsList = new ArrayList<>();
    }

    public void loadFlightsFromDB() throws SQLException {

        if (flightsList != null) {
            flightsList = new ArrayList<>();
        }

        OracleDataSource ods = DatabaseConnection.getDatasource(ApplicationConfiguration.getInstance().getDatabaseConfig());
        try (Connection conn = ods.getConnection();
             Statement stmt = conn.createStatement()) {
            try (OracleResultSet resultSet
                         = (OracleResultSet) stmt.executeQuery("SELECT id, start_point_id, end_point_id, "
                    + "to_char(start_datetime, '" + Utils.get_ORACLE_DEFAULT_DATETIME_PATTERN() + "') AS start_datetime,"
                    + "to_char(end_datetime, '" + Utils.get_ORACLE_DEFAULT_DATETIME_PATTERN() + "') AS end_datetime,"
                    + "client_id, helikopter_id FROM flight")) {
                try {
                    while (resultSet.next()) {
                        int id = resultSet.getInt("id");
                        int start_point_d = resultSet.getInt("start_point_id");
                        int end_point_d = resultSet.getInt("end_point_id");
                        String start_datetime = resultSet.getString("start_datetime");
                        String end_datetime = resultSet.getString("end_datetime");
                        int client_id = resultSet.getInt("client_id");
                        int helikopter_id = resultSet.getInt("helikopter_id");

                        Date start_date = Utils.parseDefaultDateFormat(start_datetime);
                        Date end_date = Utils.parseDefaultDateFormat(end_datetime);

                        flightsList.add(new Flight(
                                id,
                                start_date,
                                end_date,
                                start_point_d,
                                end_point_d,
                                client_id,
                                helikopter_id));
                    }
                } finally {
                    resultSet.close();
                }
            }
        }
    }

    public void reloadHelikoptersFromDB() throws SQLException {
        clear();
        loadFlightsFromDB();
    }

    public void reloadFlightsFromDB() throws SQLException {
        flightsList.clear();
        loadFlightsFromDB();
    }

    public void addFlight(final Flight flight) {

        try {
            addToDB(flight);
            flightsList.add(flight);
        } catch (SQLException exception) {
            ExceptionMessage.show(exception);
            logger.log(Level.FATAL, "", exception);
        }
    }

    private void addToDB(final Flight flight) throws SQLException {

        OracleDataSource ods = DatabaseConnection.getDatasource(ApplicationConfiguration.getInstance().getDatabaseConfig());
        try (Connection conn = ods.getConnection()) {
            OraclePreparedStatement stmt = (OraclePreparedStatement) conn.prepareStatement("INSERT INTO flight ("
                    + "START_DATETIME,"
                    + "END_DATETIME,"
                    + "START_POINT_ID, END_POINT_ID, CLIENT_ID, HELIKOPTER_ID) "
                    + "VALUES ("
                    + "TO_DATE('" + Utils.getDateStringInDefaultFormat(flight.startDateTime) + "','" + "yyyy/mm/dd hh24" + "'), "
                    + "TO_DATE('" + Utils.getDateStringInDefaultFormat(flight.endDateTime) + "','" + "yyyy/mm/dd hh24" + "'), "
                    + "?, ?, ?, ?)");

            /*stmt.setString(1, flight.startDateTime);
            stmt.setString(2, flight.endDateTime);*/
            stmt.setInt(1, flight.startPointId);
            stmt.setInt(2, flight.endPointId);
            stmt.setInt(3, flight.clientId);
            stmt.setInt(4, flight.helicopterId);

            stmt.execute();
        }
    }

    public void deleteFlight(final Flight flight) {

        try {
            deleteFromDB(flight);
            flightsList.remove(flight);
        } catch (SQLException exception) {
            ExceptionMessage.show(exception);
            logger.log(Level.FATAL, exception);
        }
    }

    private void deleteFromDB(final Flight flight) throws SQLException {

        OracleDataSource ods = DatabaseConnection.getDatasource(ApplicationConfiguration.getInstance().getDatabaseConfig());
        try (Connection conn = ods.getConnection()) {
            OraclePreparedStatement stmt = (OraclePreparedStatement) conn.prepareStatement("DELETE FROM flight WHERE START_DATETIME = ? AND END_DATETIME = ? AND START_POINT_ID = ? AND END_POINT_ID = ? AND CLIENT_ID = ? AND HELIKOPTER_ID = ?");
            /*stmt.setString(1, flight.startDateTime);
            stmt.setString(2, flight.endDateTime);*/
            stmt.setInt(3, flight.startPointId);
            stmt.setInt(4, flight.endPointId);
            stmt.setInt(5, flight.clientId);
            stmt.setInt(6, flight.helicopterId);

            stmt.execute();
        }
    }

    public void deleteFlightById(final int id) {

        try {
            deleteFromDBById(id);
            flightsList.remove(getCachedFlightById(id));
        } catch (SQLException ex) {
            logger.log(Level.FATAL, ex);
            ExceptionMessage.show(ex);
        }
    }

    private void deleteFromDBById(final int id) throws SQLException {

        OracleDataSource ods = DatabaseConnection.getDatasource(ApplicationConfiguration.getInstance().getDatabaseConfig());
        try (Connection conn = ods.getConnection()) {
            OraclePreparedStatement stmt = (OraclePreparedStatement) conn.prepareStatement("DELETE FROM flight WHERE id = ?");
            stmt.setInt(1, id);

            stmt.execute();
        }
    }

    public Flight getCachedFlightById(final int givenId) {

        Flight flight = null;
        for (int i = 0; i < flightsList.size(); i++) {
            if (flightsList.get(i).id == givenId) {
                flight = flightsList.get(i);
            }
        }
        return flight;
    }

    /**
     * Delete all flight reservations in given period. It also shifts start or end dates of some flights if they have at least one date inside given period
     *
     * @param start_date flight start date
     * @param end_date   flight end date
     * @throws SQLException
     */
    public void deleteFlightReservationsInPeriod(final Date start_date, final Date end_date) throws SQLException {

        OracleDataSource ods = DatabaseConnection.getDatasource(ApplicationConfiguration.getInstance().getDatabaseConfig());
        try (Connection conn = ods.getConnection()) {
            PreparedStatement stmt = conn.prepareStatement("CALL DELETEALLFLIGHTSINPERIOD ("
                    + "TO_DATE('" + Utils.getDateStringInDefaultFormat(start_date) + "'," + "'yyyy/mm/dd hh24'),"
                    + "TO_DATE('" + Utils.getDateStringInDefaultFormat(end_date) + "'," + "'yyyy/mm/dd hh24')"
                    + ")");

            stmt.execute();
        }
    }

    /**
     * Returns all flights which have at least one date inside given period.
     *
     * @param from_datetime flight start date
     * @param to_datetime   flight end date
     * @return list of fligts which have at least one date inside given period.
     * @throws SQLException
     */
    public List<Flight> getAllFlightsInPeriod(final String from_datetime, final String to_datetime) throws SQLException {

        List<Flight> filteredFlights = new ArrayList<>();

        OracleDataSource ods = DatabaseConnection.getDatasource(ApplicationConfiguration.getInstance().getDatabaseConfig());
        try (Connection conn = ods.getConnection();
             Statement stmt = conn.createStatement()) {

            String query = "SELECT ID, START_POINT_ID, END_POINT_ID,"
                    + " TO_CHAR(start_datetime, '" + Utils.get_ORACLE_DEFAULT_DATETIME_PATTERN() + "') as START_DATETIME,"
                    + " TO_CHAR(end_datetime, '" + Utils.get_ORACLE_DEFAULT_DATETIME_PATTERN() + "') as END_DATETIME,"
                    + " CLIENT_ID, HELIKOPTER_ID"
                    + " FROM flight WHERE "
                    + " START_DATETIME BETWEEN to_date('" + from_datetime + "', 'mm/dd/yyyy hh24') AND to_date('" + to_datetime + "', 'mm/dd/yyyy hh24') "
                    + " OR (to_date('" + from_datetime + "', 'mm/dd/yyyy hh24') BETWEEN START_DATETIME AND END_DATETIME) "
                    + " OR (to_date('" + to_datetime + "', 'mm/dd/yyyy hh24') BETWEEN START_DATETIME AND END_DATETIME)";

            try (OracleResultSet resultSet = (OracleResultSet) stmt.executeQuery(query)) {
                try {
                    while (resultSet.next()) {
                        int id = resultSet.getInt("id");
                        int start_point_d = resultSet.getInt("start_point_id");
                        int end_point_d = resultSet.getInt("end_point_id");
                        int client_id = resultSet.getInt("client_id");
                        int helikopter_id = resultSet.getInt("helikopter_id");

                        String start_datetime = resultSet.getString("START_DATETIME");
                        String end_datetime = resultSet.getString("END_DATETIME");

                        Date start_date = Utils.parseDefaultDateFormat(start_datetime);
                        Date end_date = Utils.parseDefaultDateFormat(end_datetime);

                        filteredFlights.add(new Flight(
                                id,
                                start_date,
                                end_date,
                                start_point_d,
                                end_point_d,
                                client_id,
                                helikopter_id));
                    }
                } finally {
                    resultSet.close();
                }
            }
        }
        return filteredFlights;
    }

    public void reloadFlightsFromDBInPeriod(final String startDate, final String endDate) throws SQLException {
        this.flightsList = getAllFlightsInPeriod(startDate, endDate);
    }

    /**
     * Returns count of all distinct customers in given period
     *
     * @param from_datetime flight start date
     * @param to_datetime   flight end date
     * @return count of all distinct customers in given period
     * @throws SQLException
     */
    public int getAllPassengersCountInPeriod(final String from_datetime, final String to_datetime) throws SQLException {

        int passengersCount = 0;
        OracleDataSource ods = DatabaseConnection.getDatasource(ApplicationConfiguration.getInstance().getDatabaseConfig());
        try (Connection conn = ods.getConnection();
             Statement stmt = conn.createStatement()) {

            String query = "SELECT COUNT(DISTINCT(CLIENT_ID)) AS PASSENGERS_COUNT FROM flight WHERE "
                    + " START_DATETIME BETWEEN to_date('" + from_datetime + "', 'mm/dd/yyyy hh24') AND to_date('" + to_datetime + "', 'mm/dd/yyyy hh24') "
                    + " OR (to_date('" + from_datetime + "', 'mm/dd/yyyy hh24') BETWEEN START_DATETIME AND END_DATETIME) "
                    + " OR (to_date('" + to_datetime + "', 'mm/dd/yyyy hh24') BETWEEN START_DATETIME AND END_DATETIME)";

            try (OracleResultSet resultSet = (OracleResultSet) stmt.executeQuery(query)) {
                try {
                    while (resultSet.next()) {
                        passengersCount = resultSet.getInt("PASSENGERS_COUNT");
                    }
                } finally {
                    resultSet.close();
                }
            }
        }
        return passengersCount;
    }

    /**
     * Returns passengers count on all flights which have at both dates inside given period.
     *
     * @param from_datetime flight start date
     * @param to_datetime   flight end date
     * @return passengers count on all flights which have at both dates inside given period.
     * @throws SQLException
     */
    public int getAllPassengersCountOnlyInPeriod(final String from_datetime, final String to_datetime, final int helicopterId) throws SQLException {

        int passengersCount = 0;

        OracleDataSource ods = DatabaseConnection.getDatasource(ApplicationConfiguration.getInstance().getDatabaseConfig());
        try (Connection conn = ods.getConnection();
             Statement stmt = conn.createStatement()) {
            String query = "SELECT COUNT(DISTINCT(CLIENT_ID)) AS PASSENGERS_COUNT FROM flight WHERE "
                    + " START_DATETIME >= to_date('" + from_datetime + "', 'mm/dd/yyyy hh24') AND START_DATETIME <= to_date('"
                    + to_datetime + "', 'mm/dd/yyyy hh24') AND HELIKOPTER_ID = " + helicopterId + "";
//                    + " OR (to_date('" + from_datetime + "', 'mm/dd/yyyy hh24') BETWEEN START_DATETIME AND END_DATETIME) "
//                    + " OR (to_date('" + to_datetime + "', 'mm/dd/yyyy hh24') BETWEEN START_DATETIME AND END_DATETIME)";

            try (OracleResultSet resultSet = (OracleResultSet) stmt.executeQuery(query)) {
                try {
                    while (resultSet.next()) {
                        passengersCount = resultSet.getInt("PASSENGERS_COUNT");
                    }
                } finally {
                    resultSet.close();
                }
            }
        }
        return passengersCount;
    }

    /**
     * Returns list of all available helicopters in given period
     *
     * @param from_datetime flight start date
     * @param to_datetime   flight end date
     * @return Returns list of all available helicopters in given period
     * @throws SQLException
     * @throws IOException
     */
    public List<Helikopter> getAvailableHelicoptersForPeriod(final String from_datetime, final String to_datetime) throws SQLException, IOException {

        List<Helikopter> helikoptersList = new ArrayList<>();

        OracleDataSource ods = DatabaseConnection.getDatasource(ApplicationConfiguration.getInstance().getDatabaseConfig());
        try (Connection conn = ods.getConnection();
             Statement stmt = conn.createStatement()) {

            String query = "SELECT helikopter.id as id, helikopter.speed as speed, helikopter.capacity as capacity, helikopter.price as price, helikopter.name as name, helikopter.picture as picture FROM flight"
                    + " INNER JOIN helikopter ON flight.HELIKOPTER_ID = helikopter.id "
                    + " WHERE NOT(START_DATETIME BETWEEN to_date('" + from_datetime + "', 'mm/dd/yyyy hh24') AND to_date('" + to_datetime + "', 'mm/dd/yyyy hh24')"
                    + " OR (to_date('" + from_datetime + "', 'mm/dd/yyyy hh24') BETWEEN START_DATETIME AND END_DATETIME) "
                    + " OR (to_date('" + to_datetime + "', 'mm/dd/yyyy hh24') BETWEEN START_DATETIME AND END_DATETIME))";

            try (OracleResultSet resultSet = (OracleResultSet) stmt.executeQuery(query)) {
                try {
                    while (resultSet.next()) {
                        byte[] pictureByteArray = null;
                        String heli_name = resultSet.getString("name");
                        double heli_speed = resultSet.getDouble("speed");
                        int heli_capacity = resultSet.getInt("capacity");
                        double heli_price = resultSet.getDouble("price");
                        int heli_id = resultSet.getInt("id");

                        OrdImage imageProxy = (OrdImage) resultSet.getORAData("picture", OrdImage.getORADataFactory());

                        if (imageProxy != null) {
                            pictureByteArray = imageProxy.getDataInByteArray();
                        }

                        helikoptersList.add(new Helikopter(heli_id, heli_name, heli_speed, heli_price, heli_capacity, null, pictureByteArray));
                    }
                } finally {
                    resultSet.close();
                }
            }
        }
        return helikoptersList;
    }

    /**
     * Determines if a helicopter has at least one free seat in given period
     *
     * @param helikopterId helkopter id
     * @param startDate    flight start date
     * @param endDate      flight end date
     * @return true if helikopter has at least one free seat
     */
    public boolean checkHelicopterCapacityForPeriod(int helikopterId, String startDate, String endDate) {

        boolean available = false;
        try {
            Helikopter helikopterById = HelikoptersList.getHelikopterById(helikopterId);
            int passengersCount = getAllPassengersCountOnlyInPeriod(startDate, endDate, helikopterId);
            available = helikopterById.capacity > passengersCount;
        } catch (SQLException | IOException ex) {
            logger.log(Level.FATAL, "", ex);
            ExceptionMessage.show(ex);
        }
        return available;
    }

    /**
     * @param from_datetime
     * @param to_datetime
     * @return
     * @throws SQLException
     */
    public double getOverallFlightsTimeInPerPeriod(final String from_datetime, final String to_datetime) throws SQLException {

        double overallDistance = Double.NaN;

        String query = "SELECT SUM((((END_DATETIME) - (START_DATETIME)) * 24)) AS time FROM flight" +
                "  WHERE" +
                "  START_DATETIME BETWEEN to_date('" + from_datetime + "', 'mm/dd/yyyy hh24') AND to_date('" + to_datetime + "', 'mm/dd/yyyy hh24')" +
                "  OR (to_date('" + from_datetime + "', 'mm/dd/yyyy hh24') BETWEEN START_DATETIME AND END_DATETIME)" +
                "  OR (to_date('" + to_datetime + "', 'mm/dd/yyyy hh24') BETWEEN START_DATETIME AND END_DATETIME)";

        OracleDataSource ods = DatabaseConnection.getDatasource(ApplicationConfiguration.getInstance().getDatabaseConfig());
        try (Connection conn = ods.getConnection();
             Statement stmt = conn.createStatement()) {
            try (OracleResultSet resultSet = (OracleResultSet) stmt.executeQuery(query)) {
                try {
                    while (resultSet.next()) {
                        overallDistance = resultSet.getDouble("time");
                    }
                } finally {
                    resultSet.close();
                }
            }
        }
        return overallDistance;
    }

    /**
     * @param from_datetime
     * @param to_datetime
     * @return
     * @throws SQLException
     */
    public double getOverallFlightsCostInPerPeriod(final String from_datetime, final String to_datetime) throws SQLException {

        double overallCost = Double.NaN;

        String query = "SELECT SUM((((END_DATETIME) - (START_DATETIME)) * 24) * HELIKOPTER.PRICE) AS cost FROM FLIGHT INNER JOIN HELIKOPTER ON FLIGHT.HELIKOPTER_ID = HELIKOPTER.ID" +
                "  WHERE" +
                "  START_DATETIME BETWEEN to_date('" + from_datetime + "', 'mm/dd/yyyy hh24') AND to_date('" + to_datetime + "', 'mm/dd/yyyy hh24')" +
                "  OR (to_date('" + from_datetime + "', 'mm/dd/yyyy hh24') BETWEEN START_DATETIME AND END_DATETIME)" +
                "  OR (to_date('" + to_datetime + "', 'mm/dd/yyyy hh24') BETWEEN START_DATETIME AND END_DATETIME)";

        OracleDataSource ods = DatabaseConnection.getDatasource(ApplicationConfiguration.getInstance().getDatabaseConfig());
        try (Connection conn = ods.getConnection();
             Statement stmt = conn.createStatement()) {
            try (OracleResultSet resultSet = (OracleResultSet) stmt.executeQuery(query)) {
                try {
                    while (resultSet.next()) {
                        overallCost = resultSet.getDouble("cost");
                    }
                } finally {
                    resultSet.close();
                }
            }
        }
        return overallCost;
    }

    /**
     * @param oldCustomerId
     * @param newCustomerId
     * @param helikopterId
     * @param to_datetime
     * @param from_datetime
     * @throws SQLException
     */
    public void swapPassengerForSelectedFlight(final int oldCustomerId, final int newCustomerId,
                                                 /*final int flightId,*/ final int helikopterId,
                                               final String to_datetime, final String from_datetime) throws SQLException {

        String query = "UPDATE FLIGHT SET CLIENT_ID = ? WHERE HELIKOPTER_ID = ? AND CLIENT_ID = ?" +
                "      AND START_DATETIME = to_date('" + from_datetime + "', 'mm/dd/yyyy hh24')" +
                "      AND END_DATETIME = to_date('" + to_datetime + "', 'mm/dd/yyyy hh24')";

        OracleDataSource ods = DatabaseConnection.getDatasource(ApplicationConfiguration.getInstance().getDatabaseConfig());
        try (Connection conn = ods.getConnection()) {
            OraclePreparedStatement stmt = (OraclePreparedStatement) conn.prepareStatement(query);

            stmt.setInt(1, newCustomerId);
            stmt.setInt(2, helikopterId);
            stmt.setInt(3, oldCustomerId);

            stmt.execute();
        }
    }

    /**
     * @param from_datetime
     * @param to_datetime
     * @param limit
     * @return
     * @throws SQLException
     */
    public List<PassengerFrequency> getMostFrequentPassengers(final String from_datetime, final String to_datetime,
                                                              final int limit) throws SQLException {

        ArrayList<PassengerFrequency> passengerFrequencies = new ArrayList<>();
        OracleDataSource ods = DatabaseConnection.getDatasource(ApplicationConfiguration.getInstance().getDatabaseConfig());
        try (Connection conn = ods.getConnection()) {

            OraclePreparedStatement stmt = (OraclePreparedStatement) conn.prepareStatement("SELECT name, count FROM (SELECT CUSTOMER.EMAIL as name, count(*) AS count "
                    + " FROM FLIGHT INNER JOIN CUSTOMER ON FLIGHT.CLIENT_ID = CUSTOMER.ID WHERE "
                    + " START_DATETIME BETWEEN to_date('" + from_datetime + "', 'mm/dd/yyyy hh24')" +
                    "  AND to_date('" + to_datetime + "', 'mm/dd/yyyy hh24') " +
                    "  OR (to_date('" + from_datetime + "', 'mm/dd/yyyy hh24') BETWEEN START_DATETIME AND END_DATETIME) " +
                    "  OR (to_date('" + to_datetime + "', 'mm/dd/yyyy hh24') BETWEEN START_DATETIME AND END_DATETIME)" +
                    "  GROUP BY CUSTOMER.EMAIL ORDER BY count DESC) WHERE ROWNUM <= ?");

            stmt.setInt(1, limit);

            try (OracleResultSet resultSet
                         = (OracleResultSet) stmt.executeQuery()) {
                while (resultSet.next()) {
                    String name = resultSet.getString("name");
                    int count = resultSet.getInt("count");
                    PassengerFrequency passengerFrequency = new PassengerFrequency();
                    passengerFrequency.name = name;
                    passengerFrequency.count = count;
                    passengerFrequencies.add(passengerFrequency);
                }
            }
        }
        return passengerFrequencies;
    }

    /**
     * @param from_datetime
     * @param to_datetime
     * @param limit
     * @return
     * @throws SQLException
     */
    public List<HelikopterFrequency> getMostFrequentHelikopters(final String from_datetime, final String to_datetime,
                                                                final int limit) throws SQLException {

        ArrayList<HelikopterFrequency> helikopterFrequencies = new ArrayList<>();
        OracleDataSource ods = DatabaseConnection.getDatasource(ApplicationConfiguration.getInstance().getDatabaseConfig());
        try (Connection conn = ods.getConnection()) {

            OraclePreparedStatement stmt = (OraclePreparedStatement) conn.prepareStatement("SELECT name, count FROM (SELECT HELIKOPTER.NAME as name, count(*) AS count "
                    + " FROM FLIGHT INNER JOIN HELIKOPTER ON FLIGHT.HELIKOPTER_ID = HELIKOPTER.ID WHERE "
                    + " START_DATETIME BETWEEN to_date('" + from_datetime + "', 'mm/dd/yyyy hh24')" +
                    "  AND to_date('" + to_datetime + "', 'mm/dd/yyyy hh24') " +
                    "  OR (to_date('" + from_datetime + "', 'mm/dd/yyyy hh24') BETWEEN START_DATETIME AND END_DATETIME) " +
                    "  OR (to_date('" + to_datetime + "', 'mm/dd/yyyy hh24') BETWEEN START_DATETIME AND END_DATETIME)" +
                    "  GROUP BY HELIKOPTER.NAME ORDER BY count DESC) WHERE ROWNUM <= ?");

            stmt.setInt(1, limit);

            try (OracleResultSet resultSet
                         = (OracleResultSet) stmt.executeQuery()) {
                while (resultSet.next()) {
                    String name = resultSet.getString("name");
                    int count = resultSet.getInt("count");
                    HelikopterFrequency helikopterFrequency = new HelikopterFrequency();
                    helikopterFrequency.name = name;
                    helikopterFrequency.count = count;
                    helikopterFrequencies.add(helikopterFrequency);
                }
            }
        }
        return helikopterFrequencies;
    }

    @Override
    public int getRowCount() {
        return flightsList.size();
    }

    @Override
    public int getColumnCount() {
        return 7;
    }

    @Override
    public String getColumnName(int columnIndex) {

        String columnName = null;

        switch (columnIndex) {
            case 0:
                columnName = "ID";
                break;
            case 1:
                columnName = "Start datetime";
                break;
            case 2:
                columnName = "End datetime";
                break;
            case 3:
                columnName = "Start heliport ID";
                break;
            case 4:
                columnName = "End heliport ID";
                break;
            case 5:
                columnName = "Client ID";
                break;
            case 6:
                columnName = "Helicopter ID";
                break;
            default:
                break;
        }
        return columnName;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {

        Class<?> columnClass = null;

        switch (columnIndex) {
            case 0:
                columnClass = Integer.class;
                break;
            case 1:
                columnClass = String.class;
                break;
            case 2:
                columnClass = String.class;
                break;
            case 3:
                columnClass = Integer.class;
                break;
            case 4:
                columnClass = Integer.class;
                break;
            case 5:
                columnClass = Integer.class;
                break;
            case 6:
                columnClass = Integer.class;
                break;
            default:
                break;
        }
        return columnClass;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {

        Object retObject = null;
        switch (columnIndex) {
            case 0:
                retObject = flightsList.get(rowIndex).id;
                break;
            case 1:
                retObject = Utils.getDateStringInDefaultFormat(flightsList.get(rowIndex).startDateTime);// Utils.parseDateFormat(flightsList.get(rowIndex).startDateTime, "MM/dd/yyyy HH");
                break;
            case 2:
                retObject = Utils.getDateStringInDefaultFormat(flightsList.get(rowIndex).endDateTime); //flightsList.get(rowIndex).endDateTime;
                break;
            case 3:
                retObject = flightsList.get(rowIndex).startPointId;
                break;
            case 4:
                retObject = flightsList.get(rowIndex).endPointId;
                break;
            case 5:
                retObject = flightsList.get(rowIndex).clientId;
                break;
            case 6:
                retObject = flightsList.get(rowIndex).helicopterId;
                break;
            default:
                break;
        }
        return retObject;
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {

        switch (columnIndex) {
            case 0:
                flightsList.get(rowIndex).id = (Integer) aValue;
                break;
            case 1:
                flightsList.get(rowIndex).startDateTime = (Date) aValue;
                break;
            case 2:
                flightsList.get(rowIndex).endDateTime = (Date) aValue;
                break;
            case 3:
                flightsList.get(rowIndex).startPointId = (Integer) aValue;
                break;
            case 4:
                flightsList.get(rowIndex).endPointId = (Integer) aValue;
                break;
            case 5:
                flightsList.get(rowIndex).clientId = (Integer) aValue;
                break;
            case 6:
                flightsList.get(rowIndex).helicopterId = (Integer) aValue;
                break;
            default:
                break;
        }
    }

    @Override
    public void addTableModelListener(TableModelListener l) {
    }

    @Override
    public void removeTableModelListener(TableModelListener l) {
    }

    private void clear() {
        if (flightsList != null) {
            flightsList.clear();
        }
    }

    public Flight get(int i) {
        return flightsList.get(i);
    }

    public class PassengerFrequency {
        public String name;
        public int count;
    }

    public class HelikopterFrequency {
        public String name;
        public int count;
    }

}
