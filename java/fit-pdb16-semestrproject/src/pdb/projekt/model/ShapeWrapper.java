package pdb.projekt.model;

import pdb.projekt.geometry.MapObjectType;

/**
 * Map (geometry) object wrapper. Contains name, shape and shape's type fields.
 *
 * @author Iurii xkuzne00@stud.fit.vutbr.cz Kuznetcov
 */
public class ShapeWrapper {

    public String name;
    public Object shape;
    public MapObjectType objectType;

    public ShapeWrapper(String name, Object shape) {
        this.shape = shape;
        this.name = name;
        objectType = MapObjectType.UNDFINED_TYPE;
    }

    public ShapeWrapper(String name, Object shape, MapObjectType objectType) {
        this.shape = shape;
        this.name = name;
        this.objectType = objectType;
    }

    public ShapeWrapper() {
    }

    @Override
    public String toString() {
        return "ShapeWrapper{" + "name=" + name + ", shape=" + shape + ", objectType=" + objectType + '}';
    }
}
