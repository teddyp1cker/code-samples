package pdb.projekt.model;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import pdb.projekt.database.DatabaseConnection;
import oracle.jdbc.OracleResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;
import oracle.jdbc.OraclePreparedStatement;
import oracle.jdbc.pool.OracleDataSource;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import pdb.projekt.ApplicationConfiguration;

/**
 * Customers DAO object. Performs CRUD operations and some additional operations with customers.
 * 
 * @author Iurii xkuzne00@stud.fit.vutbr.cz Kuznetcov
 */
public class CustomersList extends AbstractTableModel implements TableModel {

    private static final Logger logger = Logger.getLogger(CustomersList.class.getName());

    private List<Customer> customersList;

    /**
     * Updated customers list
     */
    private List<Customer> updatedCustomersList;

    public CustomersList() {
        customersList = new ArrayList<>();
        updatedCustomersList = new ArrayList<>();
    }

    public void loadCustomersFromDB() throws SQLException {

        if (customersList != null) {
            customersList = new ArrayList<>();
        }

        OracleDataSource ods = DatabaseConnection.getDatasource(ApplicationConfiguration.getInstance().getDatabaseConfig());

        try (Connection conn = ods.getConnection();
                Statement stmt = conn.createStatement()) {
            try (OracleResultSet resultSet
                    = (OracleResultSet) stmt.executeQuery("SELECT id, first_name, last_name, email, phone, address FROM customer")) {
                while (resultSet.next()) {

                    int id = resultSet.getInt("id");
                    String first_name = resultSet.getString("first_name");
                    String last_name = resultSet.getString("last_name");
                    String email = resultSet.getString("email");
                    String phone = resultSet.getString("phone");
                    String address = resultSet.getString("address");

                    customersList.add(new Customer(id, first_name, last_name, email, phone, address));
                }
            }
        }
    }

    public void addCustomer(final Customer customer) {

        try {
            addToDB(customer);
            customersList.add(customer);
        } catch (SQLException | IOException ex) {
            logger.log(Level.FATAL, "", ex);
        }
    }

    public static Customer getCustomerByEmail(final String givenCustomerEmail) throws SQLException {

        Customer customer = null;
        OracleDataSource ods = DatabaseConnection.getDatasource(ApplicationConfiguration.getInstance().getDatabaseConfig());
        try (Connection conn = ods.getConnection()) {

            OraclePreparedStatement stmt = (OraclePreparedStatement) conn.prepareStatement("SELECT id, first_name, last_name, email, phone, address FROM customer WHERE email = ?");
            stmt.setString(1, givenCustomerEmail);
            try (OracleResultSet resultSet
                    = (OracleResultSet) stmt.executeQuery()) {
                while (resultSet.next()) {
                    int id = resultSet.getInt("id");
                    String first_name = resultSet.getString("first_name");
                    String last_name = resultSet.getString("last_name");
                    String email = resultSet.getString("email");
                    String phone = resultSet.getString("phone");
                    String address = resultSet.getString("address");
                    customer = new Customer(id, first_name, last_name, email, phone, address);
                }
            }
        }

        return customer;
    }

    public static boolean isCustomerExists(final String givenEmail) throws SQLException {

        return getCustomerByEmail(givenEmail) != null;
    }

    private void addToDB(final Customer customer) throws SQLException, IOException {

        OracleDataSource ods = DatabaseConnection.getDatasource(ApplicationConfiguration.getInstance().getDatabaseConfig());
        try (Connection conn = ods.getConnection()) {
            OraclePreparedStatement stmt = (OraclePreparedStatement) conn.prepareStatement("INSERT INTO customer (first_name, last_name, email, phone, address) VALUES (?, ?, ?, ?, ?)");
            stmt.setString(1, customer.firstName);
            stmt.setString(2, customer.lastName);
            stmt.setString(3, customer.email);
            stmt.setString(4, customer.phone);
            stmt.setString(5, customer.address);

            stmt.execute();
        }
    }

    public void deleteCustomer(final Customer customer) {

        try {
            deleteFromDB(customer);
            customersList.remove(customer);
        } catch (SQLException | IOException ex) {
            logger.log(Level.FATAL, "", ex);
        }
    }

    private void deleteFromDB(final Customer customer) throws SQLException, IOException {

        OracleDataSource ods = DatabaseConnection.getDatasource(ApplicationConfiguration.getInstance().getDatabaseConfig());
        try (Connection conn = ods.getConnection()) {
            OraclePreparedStatement stmt = (OraclePreparedStatement) conn.prepareStatement("DELETE FROM customer WHERE email = ?");
            stmt.setString(1, customer.email);
            stmt.execute();
        }
    }

    // ?
    public void updateCustomer(final Customer originalCustomer, final Customer newCustomer) {

        try {
            updateInDB(originalCustomer, newCustomer);
            customersList.remove(getCachedCustomerByEmail(originalCustomer.email));
            customersList.add(newCustomer);
        } catch (SQLException | IOException ex) {
            logger.log(Level.FATAL, "", ex);
        }
    }

    public void updateCustomer(final String customerEmail, final Customer updateCustomer) {

        try {
            updateInDB(customerEmail, updateCustomer);
            customersList.remove(getCachedCustomerByEmail(customerEmail));
            customersList.add(updateCustomer);
        } catch (SQLException | IOException ex) {
            logger.log(Level.FATAL, "", ex);
        }
    }

    private void updateInDB(final Customer originalCustomer, final Customer newCustomer) throws SQLException, IOException {

        OracleDataSource ods = DatabaseConnection.getDatasource(ApplicationConfiguration.getInstance().getDatabaseConfig());
        try (Connection conn = ods.getConnection()) {
            OraclePreparedStatement stmt = (OraclePreparedStatement) conn.prepareStatement("UPDATE customer SET first_name = ?, last_name = ?, phone = ?, address = ? WHERE email = ?");
            stmt.setString(1, newCustomer.firstName);
            stmt.setString(2, newCustomer.lastName);
            stmt.setString(3, newCustomer.phone);
            stmt.setString(4, newCustomer.address);
            stmt.setString(5, originalCustomer.email);
            stmt.execute();
        }
    }

    private void updateInDB(final String customerEmail, final Customer newCustomer) throws SQLException, IOException {

        OracleDataSource ods = DatabaseConnection.getDatasource(ApplicationConfiguration.getInstance().getDatabaseConfig());
        try (Connection conn = ods.getConnection()) {
            OraclePreparedStatement stmt = (OraclePreparedStatement) conn.prepareStatement("UPDATE customer SET first_name = ?, last_name = ?, phone = ?, address = ? WHERE email = ?");
            stmt.setString(1, newCustomer.firstName);
            stmt.setString(2, newCustomer.lastName);
            stmt.setString(3, newCustomer.phone);
            stmt.setString(4, newCustomer.address);
            stmt.setString(5, customerEmail);
            stmt.execute();
        }
    }

    private Customer getCachedCustomerByEmail(final String email) {

        if (customersList != null && customersList.size() > 0) {

            for (int i = 0; i < customersList.size(); i++) {
                if (customersList.get(i).email.equals(email)) {
                    return customersList.get(i);
                }
            }
            return null;
        } else {
            return null;
        }
    }

    public Customer getCustomerById(final int customerId) throws SQLException {

        Customer customer = null;

        OracleDataSource ods = DatabaseConnection.getDatasource(ApplicationConfiguration.getInstance().getDatabaseConfig());

        try (Connection conn = ods.getConnection()) {

            OraclePreparedStatement stmt = (OraclePreparedStatement) conn.prepareStatement("SELECT id, first_name, last_name, email, phone, address FROM customer WHERE id = ?");
            stmt.setInt(1, customerId);

            try (OracleResultSet resultSet
                    = (OracleResultSet) stmt.executeQuery()) {
                while (resultSet.next()) {

                    int id = resultSet.getInt("id");
                    String first_name = resultSet.getString("first_name");
                    String last_name = resultSet.getString("last_name");
                    String email = resultSet.getString("email");
                    String phone = resultSet.getString("phone");
                    String address = resultSet.getString("address");

                    customer = new Customer(id, first_name, last_name, email, phone, address);
                }
            }
        }

        return customer;
    }

    public Customer get(int i) {

        if (customersList != null) {
            return customersList.get(i);
        } else {
            return null;
        }
    }

    @Override
    public int getRowCount() {
        return customersList.size();
    }

    @Override
    public int getColumnCount() {
        return 5;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Object retObject = null;
        switch (columnIndex) {

            case 0:
                retObject = customersList.get(rowIndex).firstName;
                break;
            case 1:
                retObject = customersList.get(rowIndex).lastName;
                break;
            case 2:
                retObject = customersList.get(rowIndex).email;
                break;
            case 3:
                retObject = customersList.get(rowIndex).phone;
                break;
            case 4:
                retObject = customersList.get(rowIndex).address;
                break;
            default:
                break;
        }
        return retObject;
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {

        switch (columnIndex) {
            case 0:
                customersList.get(rowIndex).firstName = (String) aValue;
                break;
            case 1:
                customersList.get(rowIndex).lastName = (String) aValue;
                break;
            case 2:
                customersList.get(rowIndex).email = (String) aValue;
                break;
            case 3:
                customersList.get(rowIndex).phone = (String) aValue;
                break;
            case 4:
                customersList.get(rowIndex).address = (String) aValue;
                break;
            default:
                break;
        }

        addUpdatedCustomer(customersList.get(rowIndex));
    }

    @Override
    public String getColumnName(int columnIndex) {

        String columnName = null;

        switch (columnIndex) {
            case 0:
                columnName = "First name";
                break;
            case 1:
                columnName = "Last name";
                break;
            case 2:
                columnName = "Email";
                break;
            case 3:
                columnName = "Phone";
                break;
            case 4:
                columnName = "Address";
                break;
            default:
                break;
        }

        return columnName;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return String.class;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        if (columnIndex == 2) {
            return false;
        } else {
            return true;
        }
    }

    private void addUpdatedCustomer(final Customer updatedCustomer) {

        boolean customerAlreadyExists = false;
        if (updatedCustomersList.size() == 0) {
            updatedCustomersList.add(updatedCustomer);
        } else {
            for (int i = 0; i < updatedCustomersList.size(); i++) {
                Customer currentCustomer = updatedCustomersList.get(i);
                if (currentCustomer.email.equals(updatedCustomer.email)) {

                    customerAlreadyExists = true;

                    currentCustomer.firstName = updatedCustomer.firstName;
                    currentCustomer.lastName = updatedCustomer.lastName;
                    currentCustomer.email = updatedCustomer.email;
                    currentCustomer.phone = updatedCustomer.phone;
                    currentCustomer.address = updatedCustomer.address;
                }
                if (!customerAlreadyExists) {
                    updatedCustomersList.add(updatedCustomer);
                }
            }
        }
    }

    public void updateCustomers() {

        for (int i = 0; i < updatedCustomersList.size(); i++) {
            updateCustomer(updatedCustomersList.get(i).email, updatedCustomersList.get(i));
        }
        updatedCustomersList.clear();
    }

    public void reloadCustomersFromDB() throws SQLException {
        updatedCustomersList.clear();
        customersList.clear();
        loadCustomersFromDB();
    }
}
