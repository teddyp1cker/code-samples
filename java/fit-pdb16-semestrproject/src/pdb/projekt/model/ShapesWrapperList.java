package pdb.projekt.model;

import java.awt.Shape;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import oracle.jdbc.OraclePreparedStatement;
import oracle.jdbc.OracleResultSet;
import pdb.projekt.ApplicationConfiguration;
import pdb.projekt.database.DatabaseConnection;
import oracle.jdbc.pool.OracleDataSource;
import oracle.spatial.geometry.JGeometry;
import oracle.sql.STRUCT;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import pdb.projekt.geometry.operations.GeometryConverter;
import pdb.projekt.geometry.MapObjectType;
import pdb.projekt.geometry.ShapeType;
import pdb.projekt.geometry.operations.IntersectionCalculator;
import pdb.projekt.ui.ExceptionMessage;

/**
 * @author Iurii xkuzne00@stud.fit.vutbr.cz Kuznetcov
 */
public class ShapesWrapperList {

    private static final Logger logger = Logger.getLogger(ShapesWrapperList.class.getName());

    public List<ShapeWrapper> shapes = new ArrayList<>();

    public void clear() {
        if (shapes != null) {
            shapes.clear();
        }
    }

    public void loadShapesFromDb() throws SQLException, Exception {

        if (shapes == null) {
            shapes = new ArrayList<>();
        }

        OracleDataSource ods = DatabaseConnection.getDatasource(ApplicationConfiguration.getInstance().getDatabaseConfig());

        try (Connection conn = ods.getConnection();
             Statement stmt = conn.createStatement()) {
            try (ResultSet resultSet
                         = stmt.executeQuery("SELECT name, geometry FROM taximap")) {
                while (resultSet.next()) {
                    byte[] image = resultSet.getBytes("geometry");
                    JGeometry jGeometry = JGeometry.load(image);
                    Object shape = GeometryConverter.jGeometry2Shape(jGeometry);
                    if (shape != null) {
                        ShapeWrapper shapeWrapper = new ShapeWrapper();
                        shapeWrapper.name = resultSet.getString("name");
                        shapeWrapper.shape = shape;
                        shapeWrapper.objectType = getMapObjectType(shapeWrapper.name);
                        shapes.add(shapeWrapper);
                    }
                }
            }
        }
    }

    public void addShape(final ShapeWrapper shapeWrapper) throws SQLException, Exception {

        OracleDataSource ods = DatabaseConnection.getDatasource(ApplicationConfiguration.getInstance().getDatabaseConfig());

        try (Connection conn = ods.getConnection();
             PreparedStatement stmt = conn.prepareStatement("INSERT INTO taximap (name, geometry) VALUES (?, ?)")) {
            stmt.setString(1, shapeWrapper.name);
            if (shapeWrapper.shape instanceof Point2D) {
                Point2D point = (Point2D) shapeWrapper.shape;
                JGeometry jgeomPoint = JGeometry.createPoint(new double[]{point.getX(), point.getY()}, 2, 0);
                STRUCT obj = JGeometry.store(conn, jgeomPoint);
                stmt.setObject(2, obj);
                stmt.execute();
                shapes.add(shapeWrapper);
            } else if (shapeWrapper.shape instanceof Shape) {
                STRUCT obj = JGeometry.store(conn, GeometryConverter.shape2JGeometry((Shape) shapeWrapper.shape));
                stmt.setObject(2, obj);
                stmt.execute();
                shapes.add(shapeWrapper);
            } else {
                logger.log(Level.WARN, "Invalid shape type");
            }
        }
    }

    private MapObjectType getMapObjectType(final String name) {

        MapObjectType objectMapType = MapObjectType.UNDFINED_TYPE;
        if (name != null) {
            if (name.startsWith("tower_")) {
                objectMapType = MapObjectType.TOWER;
            } else if (name.startsWith("military_")) {
                objectMapType = MapObjectType.MILITARY_ZONE;
            } else if (name.startsWith("route_")) {
                objectMapType = MapObjectType.ROUTE;
            } else if (name.startsWith("heliport_")) {
                objectMapType = MapObjectType.HELIPORT;
            } else if (name.startsWith("park_")) {
                objectMapType = MapObjectType.PARK_ZONE;
            }
        }
        return objectMapType;
    }

    private String getMapObjectNamePrefix(ShapeType shapeType) {

        String shapeNamePrefix = null;

        switch (shapeType) {

            case CIRCLE:
                shapeNamePrefix = "military_";
                break;
            case POINT:
                shapeNamePrefix = "heliport_";
                break;
            case LINE:
                shapeNamePrefix = "route_";
                break;
            case RECTANGLE:
                shapeNamePrefix = "tower_";
                break;
            case POLYGON:
                shapeNamePrefix = "park_";
                break;
            default:
                break;
        }
        return shapeNamePrefix;
    }

    public void deleteShape(final ShapeWrapper shapeWrapper) throws SQLException {

        if (shapeWrapper != null && shapeWrapper.shape != null && shapeWrapper.name != null) {
            OracleDataSource ods = DatabaseConnection.getDatasource(ApplicationConfiguration.getInstance().getDatabaseConfig());
            try (
                    Connection conn = ods.getConnection();
                    PreparedStatement stmt = conn.prepareStatement("DELETE FROM taximap WHERE name = ?")) {
                stmt.setString(1, shapeWrapper.name);
                stmt.execute();

                shapes.remove(shapeWrapper);
            }
        }
    }

    public ShapeWrapper getShapeById(final int shapeId) throws SQLException, Exception {

        ShapeWrapper resultShape = null;

        OracleDataSource ods = DatabaseConnection.getDatasource(ApplicationConfiguration.getInstance().getDatabaseConfig());

        try (Connection conn = ods.getConnection()) {

            OraclePreparedStatement stmt = (OraclePreparedStatement) conn.prepareStatement("SELECT name, geometry FROM taximap WHERE id = ?");
            stmt.setInt(1, shapeId);

            OracleResultSet resultSet = (OracleResultSet) stmt.executeQuery();

            while (resultSet.next()) {
                byte[] image = resultSet.getBytes("geometry");
                JGeometry jGeometry = JGeometry.load(image);
                Object shape = GeometryConverter.jGeometry2Shape(jGeometry);
                if (shape != null) {
                    ShapeWrapper shapeWrapper = new ShapeWrapper();
                    shapeWrapper.name = resultSet.getString("name");
                    shapeWrapper.shape = shape;
                    shapeWrapper.objectType = getMapObjectType(shapeWrapper.name);
                    resultShape = shapeWrapper;
                }
            }
        }

        return resultShape;
    }

    public List<ShapeWrapper> getAllShapesByType(ShapeType shapeType) throws SQLException, Exception {

        List<ShapeWrapper> shapes = new ArrayList<>();

        OracleDataSource ods = DatabaseConnection.getDatasource(ApplicationConfiguration.getInstance().getDatabaseConfig());

        try (Connection conn = ods.getConnection()) {

            String shapeNamePrefix = getMapObjectNamePrefix(shapeType);

            if (shapeNamePrefix != null) {

                OraclePreparedStatement stmt = (OraclePreparedStatement) conn.prepareStatement("SELECT name, geometry FROM taximap WHERE name like '" + shapeNamePrefix + "%'");
                // stmt.setString(1, shapeNamePrefix);

                OracleResultSet resultSet = (OracleResultSet) stmt.executeQuery();

                while (resultSet.next()) {
                    byte[] image = resultSet.getBytes("geometry");
                    JGeometry jGeometry = JGeometry.load(image);
                    Object shape = GeometryConverter.jGeometry2Shape(jGeometry);
                    if (shape != null) {
                        ShapeWrapper shapeWrapper = new ShapeWrapper();
                        shapeWrapper.name = resultSet.getString("name");
                        shapeWrapper.shape = shape;
                        shapeWrapper.objectType = getMapObjectType(shapeWrapper.name);
                        shapes.add(shapeWrapper);
                    }
                }
            }
        }

        return shapes;
    }

    public ShapeWrapper getShapeByName(final String shapeName) throws Exception {

        ShapeWrapper _shapeWrapper = null;
        OracleDataSource ods = DatabaseConnection.getDatasource(ApplicationConfiguration.getInstance().getDatabaseConfig());
        try (Connection conn = ods.getConnection()) {
            OraclePreparedStatement stmt = (OraclePreparedStatement) conn.prepareStatement("SELECT name, geometry FROM taximap WHERE name = ?");
            stmt.setString(1, shapeName);
            OracleResultSet resultSet = (OracleResultSet) stmt.executeQuery();
            try {
                while (resultSet.next()) {
                    byte[] image = resultSet.getBytes("geometry");
                    JGeometry jGeometry = JGeometry.load(image);
                    Object shape = GeometryConverter.jGeometry2Shape(jGeometry);
                    if (shape != null) {
                        ShapeWrapper shapeWrapper = new ShapeWrapper();
                        shapeWrapper.name = resultSet.getString("name");
                        shapeWrapper.shape = shape;
                        shapeWrapper.objectType = getMapObjectType(shapeWrapper.name);
                        _shapeWrapper = shapeWrapper;
                    }
                }
            } finally {
                resultSet.close();
            }
        }
        return _shapeWrapper;
    }

    public boolean isShapeExistsByName(final String shapeName) throws Exception {
        return getShapeByName(shapeName) != null;
    }

    public int getShapeIdByName(final String shapeName) throws Exception {

        int shape_id = -1;
        OracleDataSource ods = DatabaseConnection.getDatasource(ApplicationConfiguration.getInstance().getDatabaseConfig());
        try (Connection conn = ods.getConnection()) {
            OraclePreparedStatement stmt = (OraclePreparedStatement) conn.prepareStatement("SELECT id FROM taximap WHERE name = ?");
            stmt.setString(1, shapeName);
            OracleResultSet resultSet = (OracleResultSet) stmt.executeQuery();
            try {
                while (resultSet.next()) {
                    shape_id = resultSet.getInt("id");
                }
            } finally {
                resultSet.close();
            }
        }
        return shape_id;
    }

    public boolean checkForbiddenZonesIntersection(String startPointName, String endPointName) throws Exception {

        boolean hasIntersection = false;

        ShapeWrapper newShapeWrapper = new ShapeWrapper();
        newShapeWrapper.name = "temp_route_" + UUID.randomUUID();
        newShapeWrapper.objectType = MapObjectType.ROUTE;

        ShapeWrapper startPoint = getShapeByName(startPointName);
        ShapeWrapper endPoint = getShapeByName(endPointName);

//        double x1 = ((Point2D) startPoint.shape).getX();
//        double y1 = ((Point2D) startPoint.shape).getY();
//
//        double x2 = ((Point2D) endPoint.shape).getX();
//        double y2 = ((Point2D) endPoint.shape).getY();
        newShapeWrapper.shape = new Line2D.Float(((Point2D) startPoint.shape), ((Point2D) endPoint.shape));
        // new Line2D.Double(x1, y1, x2, y2);

        addShape(newShapeWrapper);

        try {
            List<ShapeWrapper> militaryZonesList = getAllShapesByType(ShapeType.CIRCLE);
            List<ShapeWrapper> towersList = getAllShapesByType(ShapeType.RECTANGLE);

            for (int i = 0; i < militaryZonesList.size(); i++) {
                if (IntersectionCalculator.calculate(militaryZonesList.get(i).name, newShapeWrapper.name) != null) {
                    hasIntersection = true;
                }
            }

            for (int i = 0; i < towersList.size(); i++) {
                if (IntersectionCalculator.calculate(towersList.get(i).name, newShapeWrapper.name) != null) {
                    hasIntersection = true;
                }
            }

            deleteShape(newShapeWrapper);

        } catch (Exception exception) {
            logger.log(Level.ERROR, "", exception);
            ExceptionMessage.show(exception);
        }

        return hasIntersection;
    }
}
