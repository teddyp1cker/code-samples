package pdb.projekt.model;

/**
 * Customer's model
 *
 * @author Iurii xkuzne00@stud.fit.vutbr.cz Kuznetcov
 */
public class Customer {

    public int id;
    public String firstName;
    public String lastName;
    public String email;
    public String phone;
    public String address;

    public Customer(String name, String surname, String email, String phone, String address) {
        this.firstName = name;
        this.lastName = surname;
        this.email = email;
        this.phone = phone;
        this.address = address;
    }

    public Customer(int id, String firstName, String lastName, String email, String phone, String address) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phone = phone;
        this.address = address;
    }

    public Customer() {
    }

    @Override
    public String toString() {
        return "Customer{" + "name=" + firstName + ", surname=" + lastName + ", email=" + email + ", phone=" + phone + ", address=" + address + '}';
    }

}
