package pdb.projekt.model;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

import oracle.jdbc.OraclePreparedStatement;
import oracle.jdbc.OracleResultSet;
import oracle.jdbc.pool.OracleDataSource;
import oracle.ord.im.OrdImage;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import pdb.projekt.ApplicationConfiguration;
import pdb.projekt.database.DatabaseConnection;

/**
 * Helicopters DAO object. Performs CRUD operations and some additional operations with helicopters.
 *
 * @author Iurii xkuzne00@stud.fit.vutbr.cz Kuznetcov
 */
public class HelikoptersList implements TableModel {

    private static final Logger logger = Logger.getLogger(ShapesWrapperList.class.getName());

    private List<Helikopter> helikoptersList;

    public HelikoptersList() {
        helikoptersList = new ArrayList<>();
    }

    public void loadHelikoptersFromDB() throws SQLException, IOException {

        if (helikoptersList != null) {
            helikoptersList = new ArrayList<>();
        }

        OracleDataSource ods = DatabaseConnection.getDatasource(ApplicationConfiguration.getInstance().getDatabaseConfig());

        try (Connection conn = ods.getConnection();
             Statement stmt = conn.createStatement()) {
            try (OracleResultSet resultSet
                         = (OracleResultSet) stmt.executeQuery("SELECT name, speed, capacity, price, picture FROM helikopter")) {
                while (resultSet.next()) {

                    byte[] pictureByteArray = null;

                    String heli_name = resultSet.getString("name");
                    double heli_speed = resultSet.getDouble("speed");
                    int heli_capacity = resultSet.getInt("capacity");
                    double heli_price = resultSet.getDouble("price");

                    OrdImage imageProxy = (OrdImage) resultSet.getORAData("picture", OrdImage.getORADataFactory());

                    if (imageProxy != null) {
                        pictureByteArray = imageProxy.getDataInByteArray();
                    }

                    helikoptersList.add(new Helikopter(heli_name, heli_speed, heli_price, heli_capacity, pictureByteArray));
                }
            }
        }
    }

    public void addHelikopter(final Helikopter helikopter) {
        if (helikoptersList != null && helikopter.name != null) {
            try {
                addToDB(helikopter);
                helikoptersList.add(helikopter);
            } catch (SQLException | IOException SQLex) {
                logger.log(Level.FATAL, SQLex);
            }
        }
    }

    public void deleteHelikopter(final Helikopter helikopter) {
        if (helikoptersList != null && helikopter.name != null) {

            try {
                deleteFromDB(helikopter);
                helikoptersList.remove(helikopter.name);
            } catch (SQLException ex) {
                logger.log(Level.FATAL, ex);
            }
        }
    }

    private void addToDB(final Helikopter helikopter) throws SQLException, IOException {

        OracleDataSource ods = DatabaseConnection.getDatasource(ApplicationConfiguration.getInstance().getDatabaseConfig());
        OraclePreparedStatement stmt;

        try (Connection conn = ods.getConnection()) {
            conn.setAutoCommit(false);
            if (helikopter.pictureByteArray != null && helikopter.pictureByteArray.length > 0) {

                OrdImage imgProxy = null;

                stmt = (OraclePreparedStatement) conn.prepareStatement("INSERT INTO helikopter (name, speed, capacity, price, picture) VALUES (?, ?, ?, ?, ordsys.ordimage.init())");
                stmt.setString(1, helikopter.name);
                stmt.setDouble(2, helikopter.speed);
                stmt.setInt(3, helikopter.capacity);
                stmt.setDouble(4, helikopter.price);

                stmt.execute();
                conn.commit();

                stmt = (OraclePreparedStatement) conn.prepareStatement("SELECT picture FROM helikopter WHERE name = ? FOR UPDATE");
                stmt.setString(1, helikopter.name);

                OracleResultSet rset = (OracleResultSet) stmt.executeQuery();
                try {
                    if (rset.next()) {
                        imgProxy = (OrdImage) rset.getORAData("picture", OrdImage.getORADataFactory());
                    }
                } finally {
                    rset.close();
                }

                if (imgProxy != null) {

                    imgProxy.loadDataFromByteArray(helikopter.pictureByteArray);
                    imgProxy.setProperties();

                    OraclePreparedStatement pictureUpdate = (OraclePreparedStatement) conn.prepareStatement(
                            "UPDATE helikopter SET picture = ? WHERE name = ?");
                    try {
                        pictureUpdate.setORAData(1, imgProxy);
                        pictureUpdate.setString(2, helikopter.name);
                        pictureUpdate.executeUpdate();

                    } finally {
                        pictureUpdate.close();
                    }
                }
                conn.commit();
            } else {
                stmt = (OraclePreparedStatement) conn.prepareStatement("INSERT INTO helikopter (name, speed, capacity, price, picture) VALUES (?, ?, ?, ?, ordsys.ordimage.init())");
                stmt.setString(1, helikopter.name);
                stmt.setDouble(2, helikopter.speed);
                stmt.setInt(3, helikopter.capacity);
                stmt.setDouble(4, helikopter.price);

                stmt.execute();
            }
        }
    }

    private void deleteFromDB(final Helikopter helikopter) throws SQLException {

        OracleDataSource ods = DatabaseConnection.getDatasource(ApplicationConfiguration.getInstance().getDatabaseConfig());

        try (
                Connection conn = ods.getConnection();
                PreparedStatement stmt = conn.prepareStatement("DELETE FROM helikopter WHERE name = ?")) {
            stmt.setString(1, helikopter.name);
            stmt.execute();
        }
    }

    private void updatePhotoInDB(final String updatableHelikopterName, OrdImage newPicture) throws SQLException {

        OracleDataSource ods = DatabaseConnection.getDatasource(ApplicationConfiguration.getInstance().getDatabaseConfig());

        try (Connection conn = ods.getConnection()) {
            OraclePreparedStatement stmt = (OraclePreparedStatement) conn.prepareStatement("UPDATE helikopter SET picture = ? WHERE name = ?");
            stmt.setString(1, updatableHelikopterName);
            stmt.setORAData(2, newPicture);

            stmt.execute();
        }
    }

    private void updatePhoto(final String updatableHelikopterName, OrdImage newPicture) {

        try {
            updatePhotoInDB(updatableHelikopterName, newPicture);
            Helikopter updatableHelikopter = getCachedHelikopterByName(updatableHelikopterName);
            updatableHelikopter.picture = newPicture;
        } catch (SQLException ex) {
            logger.log(Level.FATAL, ex);
        }
    }

    // ?
    public Helikopter getCachedHelikopterByName(final String name) {

        Helikopter currentHelikopter = null;
        for (int i = 0; i < helikoptersList.size(); i++) {
            currentHelikopter = helikoptersList.get(i);
            if (currentHelikopter.name.equals(name)) {
                break;
            }
        }
        return currentHelikopter;
    }

    public static Helikopter getHelikopterByName(final String helikopterName) throws SQLException, IOException {

        Helikopter helikopter = null;

        OracleDataSource ods = DatabaseConnection.getDatasource(ApplicationConfiguration.getInstance().getDatabaseConfig());
        OraclePreparedStatement stmt;

        try (Connection conn = ods.getConnection()) {

            stmt = (OraclePreparedStatement) conn.prepareStatement("SELECT id, name, speed, capacity, price, picture FROM helikopter WHERE name = ?");
            stmt.setString(1, helikopterName);

            OracleResultSet resultSet = (OracleResultSet) stmt.executeQuery();
            try {
                if (resultSet.next()) {

                    byte[] pictureByteArray = null;

                    int heli_id = resultSet.getInt("id");
                    String heli_name = resultSet.getString("name");
                    double heli_speed = resultSet.getDouble("speed");
                    int heli_capacity = resultSet.getInt("capacity");
                    double heli_price = resultSet.getDouble("price");

                    OrdImage imageProxy = (OrdImage) resultSet.getORAData("picture", OrdImage.getORADataFactory());

                    if (imageProxy != null) {
                        pictureByteArray = imageProxy.getDataInByteArray();

                    }

                    helikopter = new Helikopter(heli_id, heli_name, heli_speed, heli_price, heli_capacity, pictureByteArray);

                }
            } finally {
                resultSet.close();
            }
        }

        return helikopter;
    }

    public byte[] getPicturebyName(final String heikopterName) throws SQLException, IOException {

        byte[] pictureByteArray = null;

        OracleDataSource ods = DatabaseConnection.getDatasource(ApplicationConfiguration.getInstance().getDatabaseConfig());
        OraclePreparedStatement stmt;

        OrdImage imgProxy = null;

        try (Connection conn = ods.getConnection()) {

            stmt = (OraclePreparedStatement) conn.prepareStatement("SELECT picture FROM helikopter WHERE name = ?");
            stmt.setString(1, heikopterName);

            OracleResultSet rset = (OracleResultSet) stmt.executeQuery();
            try {
                if (rset.next()) {
                    imgProxy = (OrdImage) rset.getORAData("picture", OrdImage.getORADataFactory());
                    if (imgProxy != null) {
                        pictureByteArray = imgProxy.getDataInByteArray();
                    }
                }
            } finally {
                rset.close();
            }
        }
        return pictureByteArray;
    }

    public static boolean isHelikopterExistsByName(final String heikopterName) throws SQLException {

        boolean helikopterExists = false;
        OracleDataSource ods = DatabaseConnection.getDatasource(ApplicationConfiguration.getInstance().getDatabaseConfig());
        OraclePreparedStatement stmt;

        try (Connection conn = ods.getConnection()) {
            stmt = (OraclePreparedStatement) conn.prepareStatement("SELECT count(*) AS count FROM helikopter WHERE name = ?");
            stmt.setString(1, heikopterName);

            OracleResultSet rset = (OracleResultSet) stmt.executeQuery();
            try {
                if (rset.next()) {
                    helikopterExists = rset.getInt("count") > 0;
                }
            } finally {
                rset.close();
            }
        }
        return helikopterExists;
    }

    public void clear() {
        if (helikoptersList != null) {
            helikoptersList.clear();
        }
    }

    public int size() {
        int size = 0;

        if (helikoptersList != null) {
            size = helikoptersList.size();
        }

        return size;
    }

    public void reloadHelikoptersFromDB() throws SQLException, IOException {
        clear();
        loadHelikoptersFromDB();
    }

    @Override
    public int getRowCount() {
        return helikoptersList.size();
    }

    @Override
    public int getColumnCount() {
        return 4;
    }

    @Override
    public String getColumnName(int columnIndex) {

        String columnName = null;

        switch (columnIndex) {
            case 0:
                columnName = "Name";
                break;
            case 1:
                columnName = "Speed";
                break;
            case 2:
                columnName = "Capacity";
                break;
            case 3:
                columnName = "Price";
                break;
            default:
                break;
        }

        return columnName;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return String.class;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {

        Object retObject = null;
        switch (columnIndex) {
            case 0:
                retObject = helikoptersList.get(rowIndex).name;
                break;
            case 1:
                retObject = helikoptersList.get(rowIndex).speed;
                break;
            case 2:
                retObject = helikoptersList.get(rowIndex).capacity;
                break;
            case 3:
                retObject = helikoptersList.get(rowIndex).price;
                break;
            default:
                break;
        }
        return retObject;
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case 0:
                helikoptersList.get(rowIndex).name = (String) aValue;
                break;
            case 1:
                helikoptersList.get(rowIndex).speed = (Double) aValue;
                break;
            case 2:
                helikoptersList.get(rowIndex).capacity = (Integer) aValue;
                break;
            case 3:
                helikoptersList.get(rowIndex).price = (Double) aValue;
                break;
            default:
                break;
        }
    }

    @Override
    public void addTableModelListener(TableModelListener l) {
    }

    @Override
    public void removeTableModelListener(TableModelListener l) {
    }

    public Helikopter get(int i) {

        if (helikoptersList != null) {
            return helikoptersList.get(i);
        } else {
            return null;
        }
    }

    public void updateHelikopter(Helikopter helikopter) throws SQLException, IOException {

        OracleDataSource ods = DatabaseConnection.getDatasource(ApplicationConfiguration.getInstance().getDatabaseConfig());
        OraclePreparedStatement stmt;
        try (Connection conn = ods.getConnection()) {

            conn.setAutoCommit(false);
            if (helikopter.pictureByteArray != null && helikopter.pictureByteArray.length > 0) {

                OrdImage imgProxy = null;
                stmt = (OraclePreparedStatement) conn.prepareStatement("SELECT picture FROM helikopter WHERE name = ? FOR UPDATE");
                stmt.setString(1, helikopter.name);

                OracleResultSet rset = (OracleResultSet) stmt.executeQuery();
                try {
                    if (rset.next()) {
                        imgProxy = (OrdImage) rset.getORAData("picture", OrdImage.getORADataFactory());
                    }
                } finally {
                    rset.close();
                }

                if (imgProxy != null) {

                    imgProxy.loadDataFromByteArray(helikopter.pictureByteArray);
                    imgProxy.setProperties();

                    OraclePreparedStatement pictureUpdate = (OraclePreparedStatement) conn.prepareStatement(
                            "UPDATE helikopter SET picture = ? WHERE name = ?");
                    try {
                        pictureUpdate.setORAData(1, imgProxy);
                        pictureUpdate.setString(2, helikopter.name);
                        pictureUpdate.executeUpdate();
                    } finally {
                        pictureUpdate.close();
                    }
                }
                conn.commit();
            } else {
                stmt = (OraclePreparedStatement) conn.prepareStatement("INSERT INTO helikopter (name, speed, capacity, price) VALUES (?, ?, ?, ?)");
                stmt.setString(1, helikopter.name);
                stmt.setDouble(2, helikopter.speed);
                stmt.setInt(3, helikopter.capacity);
                stmt.setDouble(4, helikopter.price);

                stmt.execute();
            }
        }

        for (int i = 0; i < helikoptersList.size(); i++) {
            if (helikoptersList.get(i).name.equals(helikopter.name)) {
                helikoptersList.get(i).capacity = helikopter.capacity;
                helikoptersList.get(i).price = helikopter.price;
                helikoptersList.get(i).speed = helikopter.speed;
                helikoptersList.get(i).picture = helikopter.picture;
                helikoptersList.get(i).pictureByteArray = helikopter.pictureByteArray;
            }
        }
    }

    /**
     * Rotates helikopter's OrdSys (OrdImage) picture on 90 degres clockwise using Oracle
     *
     * @param name helicoper's name
     * @throws SQLException
     */
    public void rotatePictureByName(final String name) throws SQLException {

        OracleDataSource ods = DatabaseConnection.getDatasource(ApplicationConfiguration.getInstance().getDatabaseConfig());
        try (Connection conn = ods.getConnection();
             PreparedStatement stmt = conn.prepareStatement("CALL ROTATEHELIKOPTERIMAGEBYNAME(?)")) {
            stmt.setString(1, name);
            stmt.execute();
        }
    }

    public static Helikopter getHelikopterById(final int helikopterId) throws SQLException, IOException {

        Helikopter helikopter = null;

        OracleDataSource ods = DatabaseConnection.getDatasource(ApplicationConfiguration.getInstance().getDatabaseConfig());
        OraclePreparedStatement stmt;

        try (Connection conn = ods.getConnection()) {

            stmt = (OraclePreparedStatement) conn.prepareStatement("SELECT id, name, speed, capacity, price, picture FROM helikopter WHERE id = ?");
            stmt.setInt(1, helikopterId);

            OracleResultSet resultSet = (OracleResultSet) stmt.executeQuery();
            try {
                if (resultSet.next()) {

                    byte[] pictureByteArray = null;

                    String heli_name = resultSet.getString("name");
                    double heli_speed = resultSet.getDouble("speed");
                    int heli_capacity = resultSet.getInt("capacity");
                    double heli_price = resultSet.getDouble("price");

                    OrdImage imageProxy = (OrdImage) resultSet.getORAData("picture", OrdImage.getORADataFactory());

                    if (imageProxy != null) {
                        pictureByteArray = imageProxy.getDataInByteArray();

                    }

                    helikopter = new Helikopter(heli_name, heli_speed, heli_price, heli_capacity, pictureByteArray);
                }
            } finally {
                resultSet.close();
            }
        }
        return helikopter;
    }
}
