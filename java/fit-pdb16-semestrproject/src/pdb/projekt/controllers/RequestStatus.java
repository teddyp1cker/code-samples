package pdb.projekt.controllers;

/**
 *
 * @author Iurii xkuzne00@stud.fit.vutbr.cz Kuznetcov
 */
public enum RequestStatus {
    POSSIBLE,
    IMPOSSIBLE_ROUTE,
    CAPACITY_OVERFLOW
}
