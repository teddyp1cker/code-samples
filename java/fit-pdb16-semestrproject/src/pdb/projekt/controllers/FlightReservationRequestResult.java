package pdb.projekt.controllers;

/**
 * A new flight reservation request status
 * 
 * @author Iurii xkuzne00@stud.fit.vutbr.cz Kuznetcov
 */
public class FlightReservationRequestResult {

    public boolean isPossible;
    public RequestStatus requestStatus;
    public String message;

    public FlightReservationRequestResult(boolean isPossible, RequestStatus requestStatus, String message) {
        this.isPossible = isPossible;
        this.requestStatus = requestStatus;
        this.message = message;
    }

    /**
     * Checks new flight reservation possibility status 
     * 
     * @return true if reservation is possible 
     */
    public boolean isSuccessful() {
        return this.requestStatus == RequestStatus.POSSIBLE;
    }

    @Override
    public String toString() {
        return "FlightReservationRequestResult{" + "isPossible=" + isPossible + ", requestStatus=" + requestStatus + ", message=" + message + '}';
    }

}
