package pdb.projekt.controllers;

import java.util.Date;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import pdb.projekt.model.Customer;
import pdb.projekt.model.CustomersList;
import pdb.projekt.model.Flight;
import pdb.projekt.model.FlightsList;
import pdb.projekt.model.ShapesWrapperList;
import pdb.projekt.ui.ExceptionMessage;
import pdb.projekt.utils.Utils;

/**
 * Flight reservation process controller. Performs some basic flight reservation possibility checks and delegates reservation process for valid requests.
 *
 * @author Iurii xkuzne00@stud.fit.vutbr.cz Kuznetcov
 */
public class FlightReservationController {

    private static final Logger logger = Logger.getLogger(FlightReservationController.class.getName());

    /**
     * Performs flight reservation possibility checks
     *
     * @param startDate flight start date
     * @param endDate flight end date
     * @param helikopterId flight helicopter id
     * @param startPoint flight start heliport name
     * @param endPoint flight end heliport name
     * @return FlightReservationRequestResult - flight reservation possibility
     * @throws Exception
     */
    public static FlightReservationRequestResult checkReservation(
            String startDate,
            String endDate,
            int helikopterId,
            String startPoint,
            String endPoint) throws Exception {

        FlightReservationRequestResult result = new FlightReservationRequestResult(true, RequestStatus.POSSIBLE, "");

        if (checkForbiddenZonesIntersection(startPoint, endPoint)) {
            result.isPossible = false;
            result.requestStatus = RequestStatus.IMPOSSIBLE_ROUTE;
        } else if (!checkHelicopterCapacityForPeriod(helikopterId, startDate, endDate)) {
            result.isPossible = false;
            result.requestStatus = RequestStatus.CAPACITY_OVERFLOW;
        }

        return result;
    }

    /**
     * Checks if route between 2 heliports intersects with some another map object
     *
     * @param startPointName flight start heliport name
     * @param endPointName flight end heliport name
     * @return true if route between 2 heliports intersects with some another map object
     * @throws Exception
     */
    public static boolean checkForbiddenZonesIntersection(String startPointName, String endPointName) throws Exception {

        ShapesWrapperList shapesWrapperList = new ShapesWrapperList();
        return shapesWrapperList.checkForbiddenZonesIntersection(startPointName, endPointName);
    }

    /**
     * Check for free seats provided by given helicopter for given period
     *
     * @param helikopterId flight helicopter id
     * @param startDate flight start date
     * @param endDate flight end date
     * @return true if there are at least one free seat provided by given helicopter for given period
     */
    public static boolean checkHelicopterCapacityForPeriod(int helikopterId, String startDate, String endDate) {
        FlightsList flightsList = new FlightsList();
        return flightsList.checkHelicopterCapacityForPeriod(helikopterId, startDate, endDate);
    }

    /**
     * Delegates new flight reservation for new or existing customer
     *
     * @param startDate flight start date
     * @param endDate flight end date
     * @param helikopterId flight helicopter id
     * @param startPointName flight start heliport name
     * @param endPointName flight end heliport name
     * @param orderCustomer 'synthetic' customer model object. If already there is a customer with given email, it will uses that's customer id. If not - it will save a new
     * customer
     * @return true - if reservation was successful
     */
    public static boolean makeReservation(String startDate,
            String endDate,
            int helikopterId,
            String startPointName,
            String endPointName,
            Customer orderCustomer) {

        boolean isSuccessful = false;

        FlightsList flightsList = new FlightsList();
        CustomersList customersList = new CustomersList();
        ShapesWrapperList shapesWrapperList = new ShapesWrapperList();

        try {

            // TODO: Remove double checks
            FlightReservationRequestResult result = checkReservation(startDate, endDate, helikopterId, startPointName, endPointName);

            if (result.isSuccessful()) {

                // int customerId = -1;
                Customer customerByEmail;

                if (customersList.isCustomerExists(orderCustomer.email)) {
                    customerByEmail = customersList.getCustomerByEmail(orderCustomer.email);
                } else {
                    customersList.addCustomer(orderCustomer);
                    customerByEmail = customersList.getCustomerByEmail(orderCustomer.email);
                }

                Flight flight = new Flight();

                flight.clientId = customerByEmail.id;
                flight.helicopterId = helikopterId;
                flight.startPointId = shapesWrapperList.getShapeIdByName(startPointName);
                flight.endPointId = shapesWrapperList.getShapeIdByName(endPointName);

                Date start_date = Utils.parseDateFormat(startDate, "MM/dd/yyyy HH");
                Date end_date = Utils.parseDateFormat(endDate, "MM/dd/yyyy HH");

                flight.startDateTime = start_date;
                flight.endDateTime = end_date;

                flightsList.addFlight(flight);

                isSuccessful = true;
            } else {
                isSuccessful = false;
            }
        } catch (Exception exception) {
            logger.log(Level.FATAL, "Exception :", exception);
            ExceptionMessage.show(exception);
        }

        return isSuccessful;
    }

}
