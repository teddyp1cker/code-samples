package pdb.projekt;

import pdb.projekt.config.DatabaseConfig;

/**
 * @author Iurii xkuzne00@stud.fit.vutbr.cz Kuznetcov
 *         <p>
 *         Application configuration singleton storage
 */
public class ApplicationConfiguration {

    private DatabaseConfig databaseConfig;

    private static ApplicationConfiguration instance = null;

    protected ApplicationConfiguration() {
    }

    // Lazy Initialization (If required then only)
    public static ApplicationConfiguration getInstance() {
        if (instance == null) {
            // Thread Safe. Might be costly operation in some case
            synchronized (ApplicationConfiguration.class) {
                if (instance == null) {
                    instance = new ApplicationConfiguration();
                }
            }
        }
        return instance;
    }

    public void setDatabaseConfig(DatabaseConfig databaseConfig) {
        this.databaseConfig = databaseConfig;
    }

    public DatabaseConfig getDatabaseConfig() {
        return databaseConfig;
    }
}
