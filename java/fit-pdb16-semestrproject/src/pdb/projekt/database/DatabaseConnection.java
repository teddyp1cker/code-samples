package pdb.projekt.database;

import oracle.jdbc.pool.OracleDataSource;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import pdb.projekt.config.DatabaseConfig;
import pdb.projekt.config.DatabaseConfigException;

/**
 * @author Iurii xkuzne00@stud.fit.vutbr.cz Kuznetcov
 */
public class DatabaseConnection {

    private static Logger logger = Logger.getLogger(DatabaseConnection.class.getName());

    /**
     * Builds JDBC connection string from DatabaseConfig configuration object
     *
     * @param databaseConfig
     * @return
     */
    private static String buildDatabaseConnectionString(final DatabaseConfig databaseConfig) {
        if (databaseConfig != null) {
            return String.format("jdbc:oracle:thin:@//%s:%s/%s", databaseConfig.getHostname(),
                    String.valueOf(databaseConfig.getPort()), databaseConfig.getServiceName());
        } else {
            return null;
        }
    }

    public static OracleDataSource getDatasource(final DatabaseConfig databaseConfig) throws SQLException {

        OracleDataSource ods = new OracleDataSource();
        ods.setURL(buildDatabaseConnectionString(databaseConfig));
        ods.setUser(databaseConfig.getUsername());
        ods.setPassword(databaseConfig.getPassword());
        return ods;
    }

    /**
     * Checks database service connectivity with params provided by given DatabaseConfig configuration object
     *
     * @param databaseConfig database connection configuration object
     * @return true - if it possible to execute sample 'dual'-scheme query
     * @throws DatabaseConfigException
     * @throws DatabaseConnectionException
     */
    public static boolean checkDatabaseConnectionByConfig(final DatabaseConfig databaseConfig) throws DatabaseConfigException, DatabaseConnectionException {

        boolean isConnectionAllowed = false;
        if (databaseConfig != null) {
            OracleDataSource ods = null;
            try {
                ods = new OracleDataSource();
                ods.setURL(buildDatabaseConnectionString(databaseConfig));
                ods.setUser(databaseConfig.getUsername());
                ods.setPassword(databaseConfig.getPassword());
                Connection connection = ods.getConnection();
                try {
                    Statement stmt = connection.createStatement();
                    try {
                        ResultSet rset = stmt.executeQuery(
                                "select 1+2 as col1, 3-4 as col2 from dual");
                        try {
                            while (rset.next()) {
                                isConnectionAllowed = true;
                            }
                        } finally {
                            rset.close();
                            return isConnectionAllowed;
                        }
                    } finally {
                        stmt.close();
                        return isConnectionAllowed;
                    }
                } finally {
                    connection.close();
                    return isConnectionAllowed;
                }
            } catch (SQLException e) {
                logger.log(Level.FATAL, "Connection wasn't established, exception is :å", e);
                throw new DatabaseConnectionException("Connection wasn't established");
            }
        } else {
            logger.log(Level.FATAL, "Database config is null or empty");
            throw new DatabaseConfigException("Database config is null or empty");
        }
    }
}
