import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.logging.Level;


public class PluginManager {

    private URLClassLoader classLoader;
    private ModuleInterface plugin;

    public boolean isPluginLoaded() {
        return plugin != null;
    }

    public ModuleInterface getPlugin() {
        return this.plugin;
    }

    public Error unloadPlugin() {
        if ((this.plugin != null) && (this.classLoader != null)) {
            plugin.free();
            try {

                plugin = null;
                classLoader.close();
                classLoader = null;

            } catch (IOException e) {
                Configuration.getInstance().getLOGGER().log(Level.SEVERE, "Error while trying to unload plugin", e);
                System.out.println("Invalid plugin class");
                return Error.INVALID_MODULE_CLASS;
            }
        }
        return Error.NO_ERROR;
    }

    public Error loadPlugin(String pluginFilePath) throws IllegalAccessException, InstantiationException {

        URL jarURL = null;

        try {
            jarURL = new File(pluginFilePath).toURI().toURL();
        } catch (MalformedURLException e) {
            System.out.println("Invalid module path");
            Configuration.getInstance().getLOGGER().log(Level.SEVERE, "Invalid module path", e);
            return Error.PLUGIN_PATH;
        }

        this.classLoader = new URLClassLoader(new URL[]{jarURL});

        try {

            PluginValidator pluginValidator = new PluginValidator("ModuleInterface");

            Object pluginClazz = classLoader.loadClass(Configuration.getInstance()
                    .getDefaultPluginClassName())
                    .newInstance();

            if (pluginValidator.isValid(pluginClazz.getClass())) {

                this.plugin = (ModuleInterface) pluginClazz;

            } else {
                Configuration.getInstance().getLOGGER().log(Level.SEVERE, "Invalid plugin class");
                System.out.println("Invalid plugin class");
                return Error.INVALID_MODULE_CLASS;
            }


        } catch (ClassNotFoundException e) {
            System.out.println("Invalid module loaded: " + pluginFilePath +
                    ". Please make sure that you are trying to load" +
                    " a proper jar plugin.");

            Configuration.getInstance().getLOGGER().log(Level.SEVERE, "Invalid module loaded: " + pluginFilePath +
                    ". Please make sure that you are trying to load" +
                    " a proper jar plugin", e);

            return Error.INVALID_MODULE;

        }
        return Error.NO_ERROR;
    }

}
