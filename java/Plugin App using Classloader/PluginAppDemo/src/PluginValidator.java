public class PluginValidator {

    private String pluginClassName;

    public PluginValidator(String pluginClassName) {
        this.pluginClassName = pluginClassName;
    }

    boolean isValid(Class pluginClass) {
        boolean isValid = false;
        Class[] interfaces = pluginClass.getInterfaces();

        for (Class i : interfaces) {

            if (i.toString().contains(this.pluginClassName)) {
                isValid = true;
                break;
            }
        }
        return isValid;
    }
}
