public enum Error {

    NO_ERROR(0, "No errors"),
    PLUGIN_PATH(0, "Invalid module path"),
    INVALID_MODULE_CLASS(1, "Invalid plugin class"),
    INVALID_MODULE(2, "Invalid module loaded");


    private final int code;
    private final String description;

    private Error(int code, String description) {
        this.code = code;
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public int getCode() {
        return code;
    }

    @Override
    public String toString() {
        return "Error code is " + code + " : " + description;
    }

}
