import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.FileHandler;
import java.util.logging.Logger;

public class Configuration {

    private final static Logger LOGGER = Logger.getLogger(PluginManager.class.getName());
    private final String logFileName = "app.log";

    private final String defaultPluginClassName = "Module";

    private String currentModulePath;
    private String inputFilePath;
    private String outputFilePath;

    private FileHandler fh;

    private Properties prop;

    private Configuration() {

    }

    public static Logger getLOGGER() {
        return LOGGER;
    }

    public static Configuration getInstance() {
        return SingletonHolder.INSTANCE;
    }

    public boolean enableLogging() {

        try {
            fh = new FileHandler(logFileName);
            LOGGER.addHandler(fh);

            return true;

        } catch (IOException e) {

            e.printStackTrace();
            return false;
        }

    }

    public void load(String configFilePath) throws IOException {

        prop = new Properties();

        FileInputStream inputStream = new FileInputStream(configFilePath);

        if (inputStream != null) {
            prop.load(inputStream);
        } else {
            throw new FileNotFoundException("property file '" + configFilePath + "' not found");
        }

        parseConfig();

    }

    private void parseConfig() {
        if (prop != null) {

            currentModulePath = prop.getProperty("pluginFile");
            inputFilePath = prop.getProperty("inputFile");
            outputFilePath = prop.getProperty("outputFile");
        }
    }

    public String getCurrentModulePath() {
        return currentModulePath;
    }

    public void setCurrentModulePath(String currentModulePath) {
        this.currentModulePath = currentModulePath;
    }

    public String getInputFilePath() {
        return inputFilePath;
    }

    public void setInputFilePath(String inputFilePath) {
        this.inputFilePath = inputFilePath;
    }

    public String getDefaultPluginClassName() {
        return defaultPluginClassName;
    }

    @Override
    public String toString() {
        return new StringBuilder(inputFilePath).append(",").append(currentModulePath).toString();

    }

    public String getOutputFilePath() {
        return outputFilePath;
    }

    private static class SingletonHolder {
        private static final Configuration INSTANCE = new Configuration();
    }

}
