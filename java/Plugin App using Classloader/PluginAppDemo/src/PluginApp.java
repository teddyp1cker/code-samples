import java.io.File;
import java.io.IOException;

public class PluginApp {

    public static void println(String message) {
        System.out.println(message);
    }

    public static void main(String[] args) throws InstantiationException, IllegalAccessException, IOException {

        if (args.length < 1) {

            println("Usage: java -jar <app name> <absolute/path/to/config/file>");

        } else {

            Configuration configuration = Configuration.getInstance();

            configuration.load(args[0]);
            configuration.enableLogging();
            configuration.getLOGGER().setUseParentHandlers(false);

            if (new File(configuration.getCurrentModulePath()).isFile()) {

                println("Using module : " +
                        configuration.getCurrentModulePath());
                println("Using input file : " +
                        configuration.getInputFilePath());

                PluginManager pluginManager = new PluginManager();

                pluginManager.loadPlugin(configuration
                        .getCurrentModulePath());

                println("Plugin " + configuration.getCurrentModulePath() + " loaded");

                ModuleInterface plugin = pluginManager.getPlugin();

                plugin.setInputFileName(configuration.getInputFilePath());
                plugin.setOutputFileName(configuration.getOutputFilePath());
                plugin.invoke();
                pluginManager.unloadPlugin();

                println("Plugin " + configuration.getCurrentModulePath() + " unloaded");

                pluginManager.loadPlugin("Module2.jar");

                println("Plugin Module2.jar loaded");

                plugin.setInputFileName(configuration.getInputFilePath());
                plugin.setOutputFileName("out.txt");
                plugin.invoke();
                pluginManager.unloadPlugin();

                println("Plugin Module2.jar unloaded");

                pluginManager.loadPlugin("InvalidModule.jar");

            }
        }
    }
}
