import java.io.IOException;

public interface ModuleInterface {

    public void setInputFileName(String setFileName);

    public String getInputFileName();

    public void setOutputFileName(String fileName);

    public String getOutputFileName();

    public void invoke() throws IOException;

    public void free();

}
