import java.io.*;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Module implements ModuleInterface {

    private String inputFileName;
    private String outputFileName;

    @Override
    public void setInputFileName(String fileName) {
        this.inputFileName = fileName;
    }

    @Override
    public String getInputFileName() {
        return this.inputFileName;
    }

    @Override
    public void setOutputFileName(String fileName) {
        this.outputFileName = fileName;

    }

    @Override
    public String getOutputFileName() {
        return null;
    }

    private void test() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void invoke() {

        if (inputFileName != null) {
            StringBuilder sb = new StringBuilder();
            try {
                InputStream is = new FileInputStream(this.inputFileName);
                byte[] bytes = new byte[1024];
                int numRead = 0;
                try {
                    while ((numRead = is.read(bytes)) != -1)
                        sb.append(new String(bytes, 0, numRead));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {

                String data = sb.toString();

                MessageDigest m = MessageDigest.getInstance("MD5");
                m.reset();
                m.update(data.getBytes());
                BigInteger bigInt = new BigInteger(1, m.digest());
                String hashtext = bigInt.toString(16);
                while (hashtext.length() < 32) {
                    hashtext = "0" + hashtext;
                }

                FileOutputStream fop = null;
                File file;

                try {

                    file = new File(this.outputFileName);
                    fop = new FileOutputStream(file);

                    if (!file.exists()) {
                        file.createNewFile();
                    }

                    byte[] contentInBytes = hashtext.getBytes();

                    fop.write(contentInBytes);
                    fop.flush();
                    fop.close();
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    try {
                        if (fop != null) {
                            fop.close();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }


            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();

            }


        }
    }

    @Override
    public void free() {
        this.inputFileName = null;
        this.outputFileName = null;
    }
}
