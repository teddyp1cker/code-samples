public class Module {

    private String inputFileName;
    private String outputFileName;

    public String getInputFileName() {
        return this.inputFileName;
    }

    public void setInputFileName(String fileName) {
        this.inputFileName = fileName;
    }

    public String getOutputFileName() {
        return null;
    }

    public void setOutputFileName(String fileName) {
        this.outputFileName = fileName;

    }

    public void free() {

    }
}
