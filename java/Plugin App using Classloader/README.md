### Plugin app demo

Some test project, which shows how to use Classloader to implement simple plugin system. Can be opened in IDEA (yep, bad way to distrubute sources - but i did'nt realized that some years ago :)). 
PluginApp - main app, which you run with config file, where you have path to plugin/module in form of usual jar artefact. It process input text file using api, implemented in plugged jar file of module.

Module1 - 1. example of module, that zips given file.
Module2 - 2. example of module, that does md5 of text in given file.
InvalidModule - example of invalid module, which violates plugin API.