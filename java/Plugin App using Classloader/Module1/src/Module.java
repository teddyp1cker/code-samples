import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class Module implements ModuleInterface {

    private String inputFileName;
    private String outputFileName;

    @Override
    public String getInputFileName() {
        return this.inputFileName;
    }

    @Override
    public void setInputFileName(String fileName) {
        this.inputFileName = fileName;
    }

    @Override
    public String getOutputFileName() {
        return null;
    }

    @Override
    public void setOutputFileName(String fileName) {
        this.outputFileName = fileName;

    }

    @Override
    public void invoke() throws IOException {

        byte[] buffer = new byte[1024];

        FileInputStream in = null;
        ZipOutputStream zos = null;

        try {

            if (inputFileName != null && outputFileName != null) {

                FileOutputStream fos = new FileOutputStream(this.outputFileName);
                zos = new ZipOutputStream(fos);
                ZipEntry ze = new ZipEntry((new File(this.inputFileName).getName()));
                zos.putNextEntry(ze);

                in = new FileInputStream(this.inputFileName);

                int len;

                while ((len = in.read(buffer)) > 0) {
                    zos.write(buffer, 0, len);
                }

                if (zos != null && in != null) {

                    in.close();
                    zos.closeEntry();
                    zos.close();

                }

            }

        } finally {

            if (zos != null && in != null) {

                in.close();
                zos.closeEntry();
                zos.close();

            }
        }

    }

    @Override
    public void free() {
        this.inputFileName = null;
        this.outputFileName = null;
    }
}
